
classdef Database < handle
  properties ( SetAccess=private, GetAccess=private )
    rootDir = '';
    subdirs = {};  % list of subdirectories in this Database
    entries = {};  % list of fields in the Database
  end
  
  methods( Access=public )
    
    function self = Database( varargin )
      % Create a Database.
      % Example:
      %   db = Database();  % interactively select the root directory
      %   db2 = Database(dir); % create a Database from the selected root directory
      %
      %   field = db.get_field_data( fieldName, time );  % get the requested field at the given time
      %   field = db.get_all_field_data( fieldName );  % get all entries for the requested field
      %
      % See also get_field_names, get_entry_names and DatabaseEntry
      switch nargin
        case 0
          dbname = uigetdir('','Select Main Data Directory');
        case 1
          dbname = varargin{1};
        otherwise
          error('Improper use of ODTDataSet constructor');
      end
      self.rootDir = dbname;
      % parse the databases.txt file to see what subdirs we have.
      fnam = strcat(dbname,'/databases.txt');
      fid = fopen( fnam );
      if( fid==-1 )
        error('Invalid directory "%s" provided for a Database\nCould not find "%s"\n\n',dbname,fnam);
      end
      while ~feof(fid)
        self.subdirs{end+1} = fgetl(fid);
      end
      fclose(fid);
    end
    
    function dbe = get_database_entry(self, dir)
      % Obtain the requested database entry (sub-directory)
      i = find( strcmp(self.subdirs,dir) );
      if( isempty(i) )
        error( 'Requested database entry "%s" was not found.\nCall get_entry_names() to see available directories',dir );
      end
      dbe = DatabaseEntry( strcat(self.rootDir,'/',dir) );
    end
    
    function field = get_field_data(self, dir, var )
      % Obtain the field 'var' from the subdirectory 'dir'
      field = self.get_database_entry( dir ).get_field_values( var );
    end
    
    function field = get_all_field_data(self, var)
      % Obtain the requested field over all database entries (e.g., for time-varying field values)
      field = cell( length(self.subdirs) );
      for i=1:length(self.entries)
        field{i} = self.get_database(i).get_field_values( var );
      end
    end
    
    function dirs = get_entry_names(self)
      % Obtain the list of subdirectory names for this database
      dirs = self.subdirs;
    end
    
    function nams = get_field_names(self)
      % Obtain a list of fields in the Database. Note that this assumes
      % that all of the fields are present in each of the DatabaseEntry
      % objects.
      nams = self.get_database_entry(self.subdirs{1}).get_field_names();
    end
  end
  
end