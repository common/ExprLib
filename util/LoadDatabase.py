# --------------------------------------------------------------------
import numpy


class FieldInfo:
    '''Provides information about the field including its name, size, and ghosts'''

    def __init__(self):
        self.name = ''
        self.npts = numpy.array( [0, 0, 0] )
        self.nghost = 0

    def __init__(self, name, npts, ng):
        self.name = name
        self.npts = numpy.array( npts )
        self.nghost = ng

    def __str__(self):
        return "%s  size:%s  nghost:%s" % (self.name, self.npts, self.nghost)


# --------------------------------------------------------------------

class Field:
    '''Holds a field including its data and FieldInfo'''

    def __init__(self, info, data):
        assert isinstance( info, FieldInfo )
        assert isinstance( data, numpy.ndarray )
        self.__data = data
        self.__info = info

    def name(self): return self.__info.name

    def npoints(self): return self.__info.npts

    def nghost(self): return self.__info.nghost

    def data(self): return self.__data


# --------------------------------------------------------------------

class DatabaseEntry:
    '''Holds metadata for one entry in the database.
    Each DatabaseEntry subdirectory should have files 'fields.dat' and 'metadata.txt'.
    When constructed, the metadata is loaded.  Field values are loaded on demand.'''

    def __init__(self, dir):
        '''Build a DatabaseEntry using information in the directory 'dir'
         'dir' should contain 'metadata.txt' and 'fields.dat' files'''
        self.__hasBeenLoaded = False
        self.name = dir.rsplit( '/' ).pop( )
        self.__file = dir + '/' + 'fields.dat'
        self.__mdentries = {}
        self.__load_metadata( dir )

    def get_field_names(self):
        '''Obtain a list of names of fields that are present'''
        names = []
        for fi in self.__fieldInfo:
            names.append( fi.name )
        return names

    def get_field(self, varname):
        '''Obtain the requested field as a Field object'''
        if not self.__hasBeenLoaded:
            self.__load_data( )
        field = self.__fields.get( varname )
        if field is None:
            raise RuntimeError( 'Variable "' + varname + '" was not found in DatabaseEntry ' + self.name )
        return field

    def get_metadata_entries(self):
        '''Obtain a list of metadata key entries available for query.  See also 'get_metadata' '''
        entries = []
        for entry in iter(self.__mdentries):
            entries.append(entry)
        return entries

    def get_metdata(self, varname):
        '''Return any other metadata stored in this file'''
        return self.__mdentries[varname]

    def __load_metadata(self, dir):
        f = open( dir + '/metadata.txt', 'r' )
        self.__fieldInfo = []
        self.__ndoubles = 0
        self.__nints = 0
        self.__nstrings = 0
        endOfFields = False
        nIntRead = 0
        nDblRead = 0
        nStrRead = 0
        for line in f:
            # at the end of the list of fields, we store extra metadata.  First determine how many entries
            if line.find( '# number of doubles' ) != -1:
                self.__ndoubles = int( line[0:line.find( '#' )] )
                endOfFields = True
            elif line.find( '# number of ints' ) != -1:
                self.__nints = int( line[0:line.find( '#' )] )
                endOfFields = True
            elif line.find( '# number of strings' ) != -1:
                self.__nstrings = int( line[0:line.find( '#' )] )
                endOfFields = True
            elif endOfFields:  # we are now ready to load the extra metadata entries as key-value pairs, properly typed.
                sp = line.split( '=' )
                key = sp[0].strip( );
                val = 0
                if nDblRead < self.__ndoubles:
                    nDblRead = nDblRead + 1
                    val = float( sp[1].strip( ) )
                elif nIntRead < self.__nints:
                    nIntRead = nIntRead + 1
                    val = int( sp[1].strip( ) )
                elif nStrRead < self.__nstrings:
                    val = sp[1].strip( )

                self.__mdentries[key] = val
            else:  # field entry
                a = line.split( ':' )
                fieldName = a[0].strip( )
                npts = a[1].strip( ).strip( '[' ).strip( ']' ).split( ',' )
                ng = int( a[2].strip( ) )
                self.__fieldInfo.append( FieldInfo( fieldName, [int( npts[0] ), int( npts[1] ), int( npts[2] )], ng ) )

    def __load_data(self):
        if self.__hasBeenLoaded:
            return
        else:
            print('loading data for ' + self.name)
        f = open( self.__file )
        self.__fields = {}
        i = int( 0 )
        for line in f:
            data = [float( j ) for j in line.strip( ).split( )]
            fi = self.__fieldInfo[i]
            data = numpy.array( data ).reshape( fi.npts )
            self.__fields[fi.name] = Field( fi, data )
            i = i + 1
        self.__hasBeenLoaded = True


# --------------------------------------------------------------------

class FieldDatabase:
    '''Holds a database of fields at various points in time'''

    def __init__(self, dir):
        '''Build a FieldDatabase from the information in the supplied directory.
         The root directory should provide a file called 'databases.txt' that enumerates each of the subdirectories.
         Each subdirectory should contain 'metadata.txt' and 'fields.dat' files'''
        assert isinstance( dir, str )
        f = open( dir + '/databases.txt', 'r' )
        self.__entries = []
        self.__subdirs = []
        for line in f:
            subdir = line.strip( )
            self.__entries.append( DatabaseEntry( dir + '/' + subdir ) )
            self.__subdirs.append( subdir )

    def get_database_entry(self, dir):
        '''Obtain the requested database entry (sub-directory)'''
        return self.__entries[self.__subdirs.index( dir )]

    def get_field(self, dir, var):
        '''Obtain the field 'var' from the subdirectory 'dir' '''
        return self.get_database_entry( dir ).get_field( var )

    def get_all_field_data(self, var):
        '''Obtain the requested field over all database entries (e.g., for time-varying field values)'''
        field = []
        for db in self.__entries:
            field.append( db.get_field( var ).data() )
        return field

    def get_entry_names(self):
        '''Obtain the list of subdirectory names for this database'''
        return self.__subdirs

# --------------------------------------------------------------------

# fi = DatabaseEntry('/Users/james/tmp/odt/test/CCK_single_particle.db/0')
# print fi.get_field_names()
# x = fi.get_data('T')

# fd = FieldDatabase( '/Users/james/tmp/odt/test/CCK_single_particle.db' )
# de = fd.get_database_entry( '0' )
# T = de.get_field( 'T' )
# print T.data( )
# a = de.get_field('failure')
# print de.get_metdata("doubleEntry")
# help( DatabaseEntry )
# help( FieldDatabase )
# print de.get_field_names()

# fd = FieldDatabase('/Users/james/tmp/zdc/database_tstep0')
# de = fd.get_database_entry('iter1')
# print de.get_metadata('Zodiac-Date')