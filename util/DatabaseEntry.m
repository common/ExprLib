classdef DatabaseEntry < handle
  % DatabaseEntry
  %
  % The DatabaseEntry class holds information about a single database entry
  % (subdirectory with fields) including its metadata.  It provides methods
  % to obtain this information easily.
  %
  % AUTHOR: James C. Sutherland
  % DATE: 2016
  %
  
  properties ( SetAccess=private, GetAccess=private )
    dir         = '';
    fieldInfo   = {};
    ndoubles    = 0;
    nints       = 0;
    nstrings    = 0;
    metaData    = containers.Map;
    fieldValues = {};
  end
  
  methods ( Access=public )
    
    function self = DatabaseEntry( dir )
      % Create a DatabaseEntry by loading information from the given
      % directory.  This is typically called from the Database class to
      % load one particular entry.
      %
      % Each DatabaseEntry holds all of the fields and metadata associated
      % with a given timestep output.
      %
      % See also get_field_values and get_field_names
      self.dir = dir;
      self.load_metadata();
    end
    
    function fi = get_field_info( self, name )
      % Obtain the FieldInfo for the requested field.
      % See also FieldInfo
      fi = fieldInfo{ self.find_field_index(name) };
    end
    
    function fields = get_field_names( self )
      % Obtain a cell array containing the list of field names in this
      % DatabaseEntry.
      % See also get_field_values
      fields = cell(length(self.fieldInfo),1);
      for i=1:length(self.fieldInfo)
        fields{i} = self.fieldInfo{i}.name;
      end
    end
    
    function mdKeys = get_metadata_keys( self )
      % obtain a cell array containing the keys for the metadata
      % informaiton, which is stored in key/value pairs. 
      % See also get_metadata
      mdKeys = keys(self.metaData);
    end
    
    function val = get_metadata( self, key )
      % Get the requested metadata entry.
      % Example:
      %   dbEntry.get_metadata( 'key' );
      % See also get_metadata_keys
      if ~isKey( key, self.metaData )
        error( 'Key "%s" was not found in the metadata\n',key );
      end
      val = self.metaData(key);
    end
    
    function field = get_field_values( self, name )
      % Obtain the values for the requested field as an array.
      % See also get_field_names
      self.load_field_values()
      field = self.fieldValues{ self.find_field_index(name) };
    end
  end
  
  methods( Access=private )
    
    function index = find_field_index(self,name)
      index = [];
      for i=1:length(self.fieldInfo)
        if strcmp( self.fieldInfo{i}.name, name )
          index = i;
          break;
        end
      end
      if isempty(index)
        error('No field %s was found\nTry calling get_field_names()',name);
      end
    end
      
    
    function load_field_values( self )
      if ~isempty( self.fieldValues )
        return;
      end
      self.fieldValues = {};
      fid = fopen( strcat(self.dir,'/fields.dat') );
      for i=1:length(self.fieldInfo)
        assert( ~feof(fid) )
        [vals,nvals] = fscanf( fid, '%f', prod(self.fieldInfo{i}.npts) );
        assert( nvals == prod(self.fieldInfo{i}.npts) );
        self.fieldValues{end+1} = reshape(vals,self.fieldInfo{i}.npts)';
      end
      fclose(fid);
    end
    
    function load_metadata( self )
      fnam = strcat(self.dir,'/metadata.txt');
      fid = fopen( fnam );
      endOfFields = false;
      nIntRead = 0;
      nDblRead = 0;
      nStrRead = 0;
      
      while ~feof(fid)
        
        tmp = fgetl( fid );
        
        if( strfind( tmp, '# number of doubles' ) )
          self.ndoubles = sscanf( tmp, '%i' );
          endOfFields = true;
        elseif( strfind( tmp, '# number of ints'    ) )
          self.ndoubles = sscanf( tmp, '%i' );
          endOfFields = true;
        elseif( strfind( tmp, '# number of strings'    ) )
          self.ndoubles = sscanf( tmp, '%i' );
          endOfFields = true;
        elseif( endOfFields )
          % parse key/value pairs for metadata
          sp  = strsplit( tmp, '=' );
          key = strip( sp{0} );
          val = strip( sp{1} );
          if nDblRead < self.ndoubles
            nDblRead = nDblRead + 1;
            val = float( val );
          elseif nIntRead < self.nints
            nIntRead = nIntRead + 1;
            val = round( val );
          elseif nStrRead < self.nstrings
            nStrRead = nStrRead + 1;
          end
          
          self.metaData(key) = val;
          
        else  % field entry
          
          nam  = sscanf( tmp, '%s', 1 );  % extract the field name
          i    = strfind( tmp, '[' );
          npts = sscanf( tmp(i+1:end), '%i,', 3 );
          i    = strfind( tmp, '] :' );
          ng   = sscanf( tmp(i+3:end), '%i', 1 );
          
          self.fieldInfo{end+1} = FieldInfo( nam, npts, ng );
        end
      end
      fclose(fid);
    end
  end
end
