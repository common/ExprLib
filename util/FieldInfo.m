classdef FieldInfo
  % FieldInfo
  % The FieldInfo class holds the name and dimensions of a field.
  
  properties( SetAccess=private, GetAccess=public )
    name   = '';      % name of the field
    npts   = [1,1,1]; % number of points
    nghost = 0;       % number of ghosts
  end
  
  methods( Access=public )
    
    function self = FieldInfo( fieldName, numPts, numGhost )
      % Build a FieldInfo object
      assert( length(numPts) == 3 );
      if iscolumn(numPts) 
        numPts = numPts'; 
      end
      self.name   = fieldName;
      self.npts   = numPts;
      self.nghost = numGhost;
    end
    
  end
  
end