#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/foreach.hpp>

#include "../test/TestHelper.h"

#include <spatialops/structured/IntVec.h>

#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <map>
#include <limits>

using namespace std;

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

bool verboseOutput;

//--------------------------------------------------------------------

void parse_header( const string& header,
                   string& varname,
                   SpatialOps::IntVec& npts,
                   int& nghost )
{
  vector<string> values;
  typedef boost::tokenizer< boost::char_separator<char> > Token;
  boost::char_separator<char> sep("[]:,");
  const Token token( header, sep );
  for( Token::const_iterator i=token.begin(); i!=token.end(); ++i ){
    string tok = *i;
    boost::algorithm::trim(tok);
    if( !tok.empty() ) values.push_back( tok );
  }

  assert( values.size() == 5 );
  varname = values[0];
  npts[0] = boost::lexical_cast<int>( values[1] );
  npts[1] = boost::lexical_cast<int>( values[2] );
  npts[2] = boost::lexical_cast<int>( values[3] );
  nghost  = boost::lexical_cast<int>( values[4] );
}

//--------------------------------------------------------------------

vector<double> parse_values( const string& valstr )
{
  vector<double> values;
  typedef boost::tokenizer< boost::char_separator<char> > Token;
  boost::char_separator<char> sep(" ");
  const Token token( valstr, sep );
  for(Token::const_iterator i=token.begin(); i!=token.end();++i) {
    try{
      values.push_back( boost::lexical_cast<double>(*i) );
    }
    catch( boost::bad_lexical_cast& ){  // handle overflow/underflow
      values.push_back( std::numeric_limits<double>::min() ); // an identifiable value that isn't precisely zero
    }
  }
  return values;
}

//--------------------------------------------------------------------

struct FieldInfo{
  string varname;
  SpatialOps::IntVec npts;
  int nghost;
};

typedef vector<FieldInfo> MetaData;

MetaData load_metadata( const bfs::path& p )
{
  MetaData md;
  bfs::ifstream file( p );
  if( !file ){
    std::ostringstream msg;
    msg << "Could not load file " << p << endl;
    throw std::runtime_error( msg.str() );
  }
  while( !file.eof() ){
    string s;
    getline(file,s);
    if( s.find("# number") != s.npos ) continue;
    if( s.empty() ) continue;
    if( s.find("-Date") != s.npos ) continue;
    if( s.find("-Version") != s.npos ) continue;
    FieldInfo fi;
    parse_header(s,fi.varname,fi.npts,fi.nghost);
    md.push_back(fi);
  }
  return md;
}

bool check_files( const bfs::path& p1, const bfs::path& p2,
                  const double atol, const double rtol,
                  const bool includeGhost )
{
  TestHelper status(false);

  const MetaData md1 = load_metadata( p1/"metadata.txt" );
  const MetaData md2 = load_metadata( p2/"metadata.txt" );

  bfs::ifstream f1( p1/"fields.dat" );
  bfs::ifstream f2( p2/"fields.dat" );

  MetaData::const_iterator ifi1 = md1.begin(), ifi2=md2.begin();
  for( ; ifi1!=md1.end(); ++ifi1, ++ifi2 ){

    if( ifi2 == md2.end() ) return false;

    const FieldInfo& fi1 = *ifi1;
    const FieldInfo& fi2 = *ifi2;

    if( verboseOutput ) cout << fi1.varname << " <-> " << fi2.varname << endl;

    status( fi1.varname == fi2.varname, "varname" );
    status( fi1.npts    == fi2.npts   , "npts"    );
    status( fi1.nghost  == fi2.nghost , "nghost"  );

    string s1, s2;
    getline(f1,s1); getline(f2,s2); // read data

    vector<double> v1, v2;
    v1 = parse_values( s1 );
    v2 = parse_values( s2 );

    vector<double>::const_iterator iv1=v1.begin(), iv1e=v1.end();
    vector<double>::const_iterator iv2=v2.begin(), iv2e=v2.end();

    if( !includeGhost ){
      iv1 += fi1.nghost;  iv1e -= fi1.nghost;
      iv2 += fi2.nghost;  iv2e -= fi2.nghost;
    }

    TestHelper fstatus(false);
    for( ; iv1!=iv1e && iv2!=iv2e; ++iv1, ++iv2 ){
      const double aerr = fabs( *iv1 - *iv2 );
      const double rerr = aerr / (fabs(*iv1)+atol);
      bool isok = rerr <= rtol;
      if( !isok ) isok = aerr <= atol;
      fstatus( isok, "value" );
    }
    if( fstatus.isfailed() ) cout << "  FAILED comparison on: " << fi1.varname << endl;
    status( fstatus.ok(), fi1.varname );
  }

  if( status.isfailed() ) cout << "  FAIL: " << p1.native() << "  <->  " << p2.native() << endl;
  if( verboseOutput ) cout << "  PASS: " << p1.native() << "  <->  " << p2.native() << endl;

  return status.ok();
}

//--------------------------------------------------------------------

bool check_directories( const bfs::path& p1, const bfs::path& p2,
                        const double atol, const double rtol,
                        const bool includeGhosts )
{
  TestHelper status(false);

  if( !bfs::is_directory(p1) ){
    cout << p1 << " is not a directory" << endl;
    status(false);
  }           
  if( !is_directory(p2) ){
    cout << p1 << " is not a directory" << endl;
    status(false);
  }

  bfs::ifstream dbtext1( p1 / "databases.txt" );
  bfs::ifstream dbtext2( p2 / "databases.txt" );

  while( !dbtext1.eof() && !dbtext2.eof() ){
    string s1, s2;
    getline( dbtext1, s1 );
    getline( dbtext2, s2 );
    if( s1 != s2 ){
      cout << "databases.txt files have different entries:\n"
          << "\t" << s1 << endl
          << "\t" << s2 << endl;
      status(false);
    }
    if( s1.empty() ) break;
    if( s1 == "metadata.txt" ){
      std::cout << "**** METADATA *****\n";
    }
    try{
      if( verboseOutput ) cout << "Comparing: '" << s1 << "'  <->  '" << s2 << "'\n";
      status( check_files( bfs::path(p1/s1), bfs::path(p2/s2), atol, rtol, includeGhosts ), "file check" );
    }
    catch( std::exception& err ){
      ostringstream msg;
      msg << err.what()
          << "\nError comparing files: \n"
          << bfs::path(p1/s1) << "\n\t"
          << bfs::path(p2/s2) << "\n";
      throw std::runtime_error( msg.str() );
    }
  }

  if( status.ok() ) cout << "PASS:  ";
  else              cout << "FAIL:  ";
  cout << setw(30) << left << p1.native() << "   " << p2.native() << endl;

  return status.ok();
}

//--------------------------------------------------------------------

int main( int iarg, char* carg[] )
{
  string fn1, fn2;
  double atol, rtol;
  bool includeGhosts = false;

  // parse the command line options input describing the problem
  {
    po::options_description desc("\nUsage: compare_database [db1] [db2]\n\nSupported Options:");
    desc.add_options()
      ( "help", "print this help message and exit" )
      ( "atol", po::value<double>(&atol)->default_value(1e-6), "absolute tolerance" )
      ( "rtol", po::value<double>(&rtol)->default_value(1e-5), "relative tolerance" )
      ( "include_ghosts", "Include ghost values in output" )
      ( "verbose", "activate verbose output" );

    po::options_description hidden("Hidden options");
    hidden.add_options()
      ("db1", po::value<string>(&fn1), "file 1")
      ("db2", po::value<string>(&fn2), "file 2");

    po::positional_options_description p;
    p.add("db1", 1);
    p.add("db2", 1);

    po::options_description cmdline_options;
    cmdline_options.add(desc).add(hidden);

    po::variables_map args;
    po::store( po::command_line_parser(iarg,carg).
               options(cmdline_options).positional(p).run(), args );
    po::notify(args);

    if( args.count("help") ){
      cout << desc << endl;
      return 1;
    }

    verboseOutput = args.count("verbose");
    includeGhosts = args.count("include_ghosts");

    if( fn1.empty() || fn2.empty() ){
      cout << desc << endl;
      return -1;
    }
    cout << "Comparing:\n\t" << fn1 << "\n\t" << fn2 << endl << endl;
  }

  bfs::path f1( fn1 );
  bfs::path f2( fn2 );

  if( check_directories( f1, f2, atol, rtol, includeGhosts ) ){
    cout << endl
         << " -------------------------------" << endl
         << "   The databases are identical  " << endl
         << " -------------------------------" << endl;
    return 0;
  }
  cout << endl
       << " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl
       << "  The databases are different " << endl
       << " !!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
  return -1;
}

//--------------------------------------------------------------------
