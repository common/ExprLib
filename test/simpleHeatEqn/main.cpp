#include <iostream>
using std::cout;
using std::endl;

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/util/TimeLogger.h>

namespace so = SpatialOps;
typedef so::SVolField   CellField;
typedef so::SSurfXField XSideField;
typedef so::SSurfYField YSideField;
typedef so::SSurfZField ZSideField;

typedef so::BasicOpTypes<CellField>::GradX      GradX;
typedef so::BasicOpTypes<CellField>::InterpC2FX InterpX;
typedef so::BasicOpTypes<CellField>::DivX       DivX;

typedef so::BasicOpTypes<CellField>::GradY      GradY;
typedef so::BasicOpTypes<CellField>::InterpC2FY InterpY;
typedef so::BasicOpTypes<CellField>::DivY       DivY;

typedef so::BasicOpTypes<CellField>::GradZ      GradZ;
typedef so::BasicOpTypes<CellField>::InterpC2FZ InterpZ;
typedef so::BasicOpTypes<CellField>::DivZ       DivZ;


//-- boost includes ---//
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <expression/ExprLib.h>
#include <expression/TimeStepper.h>
#include <expression/BoundaryConditionExpression.h>

//--- local includes ---//
#include "RHSExpr.h"
#include "Flux.h"
#include "MonolithicRHS.h"

namespace po = boost::program_options;
using SpatialOps::IntVec;

void jcs_pause()
{
  std::cout << "Press <ENTER> to continue...";
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}

//==============================================================================

void
initialize_mask_points( const so::Grid& grid,
                        std::vector<IntVec>& xminus,
                        std::vector<IntVec>& xplus,
                        std::vector<IntVec>& yminus,
                        std::vector<IntVec>& yplus,
                        std::vector<IntVec>& zminus,
                        std::vector<IntVec>& zplus )
{
  for( int k=0; k<grid.extent(2); ++k ){
    for( int  j=0; j<grid.extent(1); ++j ){
      xminus.push_back( IntVec(0, j, k) );
      xplus .push_back( IntVec(grid.extent(0), j, k) );
    }
  }
  for( int k=0; k<grid.extent(2); ++k ){
    for( int i=0; i<grid.extent(0); ++i ){
      yminus.push_back( IntVec(i,0,k) );
      yplus .push_back( IntVec(i,grid.extent(1), k) );
    }
  }
  for( int j=0; j<grid.extent(1); ++j ){
    for( int i=0; i<grid.extent(0); ++i ){
      zminus.push_back( IntVec(i,j,0) );
      zplus .push_back( IntVec(i,j,grid.extent(2)) );
    }
  }
}

//==============================================================================

void setup_bcs( Expr::ExpressionFactory& segFactory,
                Expr::ExpressionFactory& monoFactory,
                const so::Grid& grid )
{
  std::vector<IntVec> xminusPts, xplusPts, yminusPts, yplusPts, zminusPts, zplusPts;
  initialize_mask_points( grid, xminusPts, xplusPts, yminusPts, yplusPts, zminusPts, zplusPts );

  const so::GhostData ghost(1);
  const so::IntVec hasBC(true,true,true);
  const so::BoundaryCellInfo xInfo = so::BoundaryCellInfo::build<XSideField>(hasBC,hasBC);
  const so::BoundaryCellInfo yInfo = so::BoundaryCellInfo::build<YSideField>(hasBC,hasBC);
  const so::BoundaryCellInfo zInfo = so::BoundaryCellInfo::build<ZSideField>(hasBC,hasBC);
  const so::MemoryWindow xwindow( so::get_window_with_ghost( grid.extent(), ghost, xInfo ) );
  const so::MemoryWindow ywindow( so::get_window_with_ghost( grid.extent(), ghost, yInfo ) );
  const so::MemoryWindow zwindow( so::get_window_with_ghost( grid.extent(), ghost, zInfo ) );

  typedef Expr::ConstantBCOpExpression<CellField,so::XDIR>::Builder XBC;
  typedef Expr::ConstantBCOpExpression<CellField,so::YDIR>::Builder YBC;
  typedef Expr::ConstantBCOpExpression<CellField,so::ZDIR>::Builder ZBC;

  XBC::MaskPtr xminus( new XBC::MaskType( xwindow, xInfo, ghost, xminusPts ) );
  XBC::MaskPtr xplus ( new XBC::MaskType( xwindow, xInfo, ghost, xplusPts  ) );
  YBC::MaskPtr yminus( new YBC::MaskType( ywindow, yInfo, ghost, yminusPts ) );
  YBC::MaskPtr yplus ( new YBC::MaskType( ywindow, yInfo, ghost, yplusPts  ) );
  ZBC::MaskPtr zminus( new ZBC::MaskType( zwindow, zInfo, ghost, zminusPts ) );
  ZBC::MaskPtr zplus ( new ZBC::MaskType( zwindow, zInfo, ghost, zplusPts  ) );

# ifdef ENABLE_CUDA
  // Masks are created on CPU so we need to explicitly transfer them to GPU
  xminus->add_consumer( GPU_INDEX );
  xplus ->add_consumer( GPU_INDEX );
  yminus->add_consumer( GPU_INDEX );
  yplus ->add_consumer( GPU_INDEX );
  zminus->add_consumer( GPU_INDEX );
  zplus ->add_consumer( GPU_INDEX );
# endif

  segFactory.register_expression( new XBC( Expr::Tag("xmbc",Expr::STATE_NONE), xminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  segFactory.register_expression( new XBC( Expr::Tag("xpbc",Expr::STATE_NONE), xplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );
  segFactory.register_expression( new YBC( Expr::Tag("ymbc",Expr::STATE_NONE), yminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  segFactory.register_expression( new YBC( Expr::Tag("ypbc",Expr::STATE_NONE), yplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );
  segFactory.register_expression( new ZBC( Expr::Tag("zmbc",Expr::STATE_NONE), zminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  segFactory.register_expression( new ZBC( Expr::Tag("zpbc",Expr::STATE_NONE), zplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );

  monoFactory.register_expression( new XBC( Expr::Tag("xmbc",Expr::STATE_NONE), xminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  monoFactory.register_expression( new XBC( Expr::Tag("xpbc",Expr::STATE_NONE), xplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );
  monoFactory.register_expression( new YBC( Expr::Tag("ymbc",Expr::STATE_NONE), yminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  monoFactory.register_expression( new YBC( Expr::Tag("ypbc",Expr::STATE_NONE), yplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );
  monoFactory.register_expression( new ZBC( Expr::Tag("zmbc",Expr::STATE_NONE), zminus, so::DIRICHLET, so::MINUS_SIDE,  1.0 ) );
  monoFactory.register_expression( new ZBC( Expr::Tag("zpbc",Expr::STATE_NONE), zplus , so::DIRICHLET, so::PLUS_SIDE,  -1.0 ) );

  const Expr::Tag tempt( "Temperature", Expr::STATE_NONE );

  segFactory.attach_modifier_expression( Expr::Tag("xmbc",Expr::STATE_NONE), tempt );
  segFactory.attach_modifier_expression( Expr::Tag("xpbc",Expr::STATE_NONE), tempt );
  segFactory.attach_modifier_expression( Expr::Tag("ymbc",Expr::STATE_NONE), tempt );
  segFactory.attach_modifier_expression( Expr::Tag("ypbc",Expr::STATE_NONE), tempt );
  segFactory.attach_modifier_expression( Expr::Tag("zmbc",Expr::STATE_NONE), tempt );
  segFactory.attach_modifier_expression( Expr::Tag("zpbc",Expr::STATE_NONE), tempt );

  monoFactory.attach_modifier_expression( Expr::Tag("xmbc",Expr::STATE_NONE), tempt );
  monoFactory.attach_modifier_expression( Expr::Tag("xpbc",Expr::STATE_NONE), tempt );
  monoFactory.attach_modifier_expression( Expr::Tag("ymbc",Expr::STATE_NONE), tempt );
  monoFactory.attach_modifier_expression( Expr::Tag("ypbc",Expr::STATE_NONE), tempt );
  monoFactory.attach_modifier_expression( Expr::Tag("zmbc",Expr::STATE_NONE), tempt );
  monoFactory.attach_modifier_expression( Expr::Tag("zpbc",Expr::STATE_NONE), tempt );
}

//==============================================================================

int main( int iarg, char* carg[] )
{
  double dt, tend;
  IntVec npts;
  so::DoubleVec length;

# ifdef ENABLE_THREADS
  int soThreadCount, exprThreadCount;
# endif

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "dt",   po::value<double>(&dt  )->default_value(2e-4), "timestep" )
      ( "tend", po::value<double>(&tend)->default_value(0.04 ), "end time" )
#     ifdef ENABLE_THREADS
      ( "tc", po::value<int>(&soThreadCount)->default_value(NTHREADS), "Number of threads for Nebo")
      ( "etc", po::value<int>(&exprThreadCount)->default_value(1), "Number of threads for ExprLib")
#     endif
      ( "nx", po::value<int>(&npts[0])->default_value(32), "nx" )
      ( "ny", po::value<int>(&npts[1])->default_value(32), "ny" )
      ( "nz", po::value<int>(&npts[2])->default_value(32), "nz" )
      ( "Lx", po::value<double>(&length[0])->default_value(1.0),"Length in x")
      ( "Ly", po::value<double>(&length[1])->default_value(1.0),"Length in y")
      ( "Lz", po::value<double>(&length[2])->default_value(1.0),"Length in z");

    po::variables_map args;
    po::store( po::parse_command_line(iarg,carg,desc), args );
    po::notify(args);

    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }

# ifdef ENABLE_THREADS
  cout << "ENABLE_THREADS is ON" << endl;
  SpatialOps::set_hard_thread_count( NTHREADS );
  SpatialOps::set_soft_thread_count( soThreadCount );
  Expr::set_hard_thread_count( NTHREADS );
  Expr::set_soft_thread_count( exprThreadCount );
  sleep(1);
# endif

  cout << " [nx,ny,nz] = " << npts << endl
       << " dt = " << dt << ", tend = " << tend << endl
#      ifdef ENABLE_THREADS
       << " SpatialOps NTHREADS (can set at runtime) = " << SpatialOps::get_soft_thread_count()
       << " out of " << SpatialOps::get_hard_thread_count() << endl
       << " ExprLib    NTHREADS (can set at runtime) = "
       << SpatialOps::ThreadPool::get_pool_size() << " out of "
       << SpatialOps::ThreadPool::get_pool_capacity() << endl
#      endif
       << endl;

  // set mesh spacing (uniform, structured mesh)
  const so::DoubleVec spacing = length/npts;
  const so::Grid grid( npts, length );

  // build the spatial operators
  SpatialOps::OperatorDatabase sodb;
  so::build_stencils( grid, sodb );

  Expr::ExpressionFactory segFactory, monoFactory;

  const Expr::Tag tempt( "Temperature", Expr::STATE_NONE );
  const Expr::Tag dct  ( "ThermalCond", Expr::STATE_NONE );

  const Expr::Tag  segrhsTag("segrhs", Expr::STATE_NONE);
  const Expr::Tag monorhsTag("monorhs",Expr::STATE_NONE);

  { //-- segregated approach:
    const Expr::Tag xfluxt( "HeatFluxX", Expr::STATE_NONE );
    const Expr::Tag yfluxt( "HeatFluxY", Expr::STATE_NONE );
    const Expr::Tag zfluxt( "HeatFluxZ", Expr::STATE_NONE );

    segFactory.register_expression( new Flux<GradX,InterpX>::Builder( xfluxt, tempt, dct ) );
    segFactory.register_expression( new Flux<GradY,InterpY>::Builder( yfluxt, tempt, dct ) );
    segFactory.register_expression( new Flux<GradZ,InterpZ>::Builder( zfluxt, tempt, dct ) );

    segFactory.register_expression(
        new RHSExpr<CellField>::Builder( segrhsTag,
                                         npts[0]>1 ? xfluxt : Expr::Tag(),
                                         npts[1]>1 ? yfluxt : Expr::Tag(),
                                         npts[2]>1 ? zfluxt : Expr::Tag() ) );
  }
  { //-- monolithic approach
    monoFactory.register_expression( new MonolithicRHS<CellField>::Builder( monorhsTag, dct, tempt ) );
  }

  //-- build a few coordinate fields
  const so::IntVec hasBC(true,true,true);
  const so::BoundaryCellInfo cellBCInfo = so::BoundaryCellInfo::build<CellField>(hasBC,hasBC);
  const so::GhostData cellGhosts(1);
  const so::MemoryWindow vwindow( so::get_window_with_ghost(npts,cellGhosts,cellBCInfo) );
  CellField xcoord( vwindow, cellBCInfo, cellGhosts, NULL );
  CellField ycoord( vwindow, cellBCInfo, cellGhosts, NULL );
  CellField zcoord( vwindow, cellBCInfo, cellGhosts, NULL );

  grid.set_coord<SpatialOps::XDIR>( xcoord );
  grid.set_coord<SpatialOps::YDIR>( ycoord );
  grid.set_coord<SpatialOps::ZDIR>( zcoord );

# ifdef ENABLE_CUDA
  xcoord.add_device( GPU_INDEX );
  ycoord.add_device( GPU_INDEX );
  zcoord.add_device( GPU_INDEX );
# endif

  segFactory .register_expression( new Expr::ConstantExpr<CellField>::Builder(dct,0.1) );
  monoFactory.register_expression( new Expr::ConstantExpr<CellField>::Builder(dct,0.1) );
  segFactory .register_expression( new Expr::PlaceHolder<CellField>::Builder(tempt) );
  monoFactory.register_expression( new Expr::PlaceHolder<CellField>::Builder(tempt) );

  setup_bcs( segFactory, monoFactory, grid );

  Expr::TimeStepper  segTimeStepper(  segFactory, Expr::FORWARD_EULER, "segregated" );
  Expr::TimeStepper monoTimeStepper( monoFactory, Expr::FORWARD_EULER, "monolithic" );
  Expr::FieldManagerList fml;

  segTimeStepper.request_timings();
  monoTimeStepper.request_timings();

  try{
    const Expr::FieldAllocInfo ainfo( npts, 0, 0, false, false, false );

    segTimeStepper.add_equation<CellField>( "T", segrhsTag, SpatialOps::GhostData(1) );
    segTimeStepper.finalize( fml, sodb, ainfo );

    monoTimeStepper.add_equation<CellField>( "T", monorhsTag, SpatialOps::GhostData(1) );
    monoTimeStepper.finalize( fml, sodb, ainfo );

    std::ofstream  seg("seggraph.dot" );   segTimeStepper.get_tree()->write_tree(seg, true, true );
    std::ofstream mono("monograph.dot");  monoTimeStepper.get_tree()->write_tree(mono,true, true );
  }
  catch( std::exception& err ){
   std::cout << "ERROR allocating/binding fields" << std::endl
       << err.what() << std::endl;
   return -1;
  }

  //-- initialize the fields - temperature and thermal cond.
  {
    using namespace SpatialOps;
    CellField& temp = fml.field_manager<CellField>().field_ref(tempt);
    CellField& dc   = fml.field_manager<CellField>().field_ref(dct);
    temp <<= sin( xcoord ) + cos( ycoord ) + sin( zcoord );
  }

  try{
    SpatialOps::TimeLogger timer;

    timer.start("segTree");
    double t = 0;
    while( t < tend ){
      segTimeStepper.step( dt );
      t += dt;
    }
    timer.stop("segTree");
    cout << "sequential - time taken: " << timer.timer("segTree").elapsed_time() << endl;

    timer.start("monoTree");
    t = 0;
    while( t < tend ){
      monoTimeStepper.step( dt );
      t += dt;
    }
    timer.stop("monoTree");
    cout << "monolithic - time taken: " << timer.timer("monoTree").elapsed_time() << endl << endl;

    return 0;
  }
  catch( std::exception& err ){
    cout << "ERROR!" << endl
         << err.what() << endl;
  }
  return -1;

}
