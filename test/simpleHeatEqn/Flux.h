#ifndef Flux_Expr_h
#define Flux_Expr_h

#include <expression/Expression.h>

template< typename GradT,
          typename InterpT >
class Flux
  : public Expr::Expression< typename GradT::DestFieldType >
{
  typedef typename GradT::DestFieldType FluxT;
  typedef typename GradT::SrcFieldType ScalarT;

  DECLARE_FIELDS( ScalarT, temp_, dCoef_ )

  const GradT* grad_;
  const InterpT* interp_;

  Flux( const Expr::Tag& tempTag,
        const Expr::Tag& diffCoefTag );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag tt_, dct_;
  public:
    Builder( const Expr::Tag& fluxTag,
             const Expr::Tag& tempTag,
             const Expr::Tag& diffCoefTag )
      : ExpressionBuilder(fluxTag),
        tt_ ( tempTag     ),
        dct_( diffCoefTag )
    {}
    ~Builder(){}
    Expr::ExpressionBase*
    build() const
    {
      return new Flux<GradT,InterpT>( tt_, dct_ );
    }
  };

  ~Flux(){}

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename GradT, typename InterpT >
Flux<GradT,InterpT>::
Flux( const Expr::Tag& tempTag,
      const Expr::Tag& diffCoefTag )
  : Expr::Expression<FluxT>()
{
  this->set_gpu_runnable( true );

  temp_  = this->template create_field_request<ScalarT>( tempTag     );
  dCoef_ = this->template create_field_request<ScalarT>( diffCoefTag );
}

//--------------------------------------------------------------------

template< typename GradT, typename InterpT >
void
Flux<GradT,InterpT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  grad_   = opDB.retrieve_operator<GradT  >();
  interp_ = opDB.retrieve_operator<InterpT>();
}

//--------------------------------------------------------------------

template< typename GradT, typename InterpT >
void
Flux<GradT,InterpT>::
evaluate()
{
  using namespace SpatialOps;
  const ScalarT& temp  =  temp_->field_ref();
  const ScalarT& dCoef = dCoef_->field_ref();
  this->value() <<= - (*interp_)(dCoef) * (*grad_)(temp);
}

//--------------------------------------------------------------------

#endif // Flux_Expr_h
