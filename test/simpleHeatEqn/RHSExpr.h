#ifndef RHS_Expr_h
#define RHS_Expr_h

#include <expression/Expression.h>

template< typename FieldT >
class RHSExpr
  : public Expr::Expression< FieldT >
{
  typedef typename SpatialOps::FaceTypes<FieldT>::XFace XFluxT;
  typedef typename SpatialOps::FaceTypes<FieldT>::YFace YFluxT;
  typedef typename SpatialOps::FaceTypes<FieldT>::ZFace ZFluxT;

  typedef typename SpatialOps::BasicOpTypes<FieldT>::DivX DivX;
  typedef typename SpatialOps::BasicOpTypes<FieldT>::DivY DivY;
  typedef typename SpatialOps::BasicOpTypes<FieldT>::DivZ DivZ;

  const bool varyX_, varyY_, varyZ_;

  DECLARE_FIELD( XFluxT, xflux_ )
  DECLARE_FIELD( YFluxT, yflux_ )
  DECLARE_FIELD( ZFluxT, zflux_ )

  const DivX  *xdiv_;
  const DivY  *ydiv_;
  const DivZ  *zdiv_;

  RHSExpr( const Expr::Tag& xFlux,
           const Expr::Tag& yFlux,
           const Expr::Tag& zFlux );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag xFluxt_, yFluxt_, zFluxt_;
  public:
    Builder( const Expr::Tag& rhsTag,
             const Expr::Tag& xFlux,
             const Expr::Tag& yFlux,
             const Expr::Tag& zFlux );
    ~Builder(){}
    Expr::ExpressionBase* build() const;
  };

  ~RHSExpr();

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT >
RHSExpr<FieldT>::
RHSExpr( const Expr::Tag& xFlux,
         const Expr::Tag& yFlux,
         const Expr::Tag& zFlux )
  : Expr::Expression<FieldT>(),
    varyX_( xFlux != Expr::Tag() ),
    varyY_( yFlux != Expr::Tag() ),
    varyZ_( zFlux != Expr::Tag() )
{
  this->set_gpu_runnable( true );

  if( varyX_ ) xflux_ = this->template create_field_request<XFluxT>( xFlux );
  if( varyY_ ) yflux_ = this->template create_field_request<YFluxT>( yFlux );
  if( varyZ_ ) zflux_ = this->template create_field_request<ZFluxT>( zFlux );
}

//--------------------------------------------------------------------

template< typename FieldT >
RHSExpr<FieldT>::
~RHSExpr()
{}

//--------------------------------------------------------------------

template< typename FieldT >
void
RHSExpr<FieldT>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  // bind operators as follows:
  if( varyX_ ) xdiv_ = opDB.retrieve_operator<DivX>();
  if( varyY_ ) ydiv_ = opDB.retrieve_operator<DivY>();
  if( varyZ_ ) zdiv_ = opDB.retrieve_operator<DivZ>();
}

//--------------------------------------------------------------------

template< typename FieldT >
void
RHSExpr<FieldT>::
evaluate()
{
  FieldT& result = this->value();

  using namespace SpatialOps;

  if( varyX_ ){
    const XFluxT& xflux = xflux_->field_ref();
    if( varyY_ ){
      const YFluxT& yflux = yflux_->field_ref();
      if( varyZ_ ){
        const ZFluxT& zflux = zflux_->field_ref();
        result <<= -(*xdiv_)(xflux) - (*ydiv_)(yflux) - (*zdiv_)(zflux);
      }
      else{
        result <<= -(*xdiv_)(xflux) - (*ydiv_)(yflux);
      }
    }
    else{
      if( varyZ_ ){
        const ZFluxT& zflux = zflux_->field_ref();
        result <<= -(*xdiv_)(xflux) - (*zdiv_)(zflux);
      }
      else{
        result <<= -(*xdiv_)(xflux);
      }
    }
  }
  else if( varyY_ ){
    const YFluxT& yflux = yflux_->field_ref();
    if( varyZ_ ){
      const ZFluxT& zflux = zflux_->field_ref();
      result <<= -(*ydiv_)(yflux) - (*zdiv_)(zflux);
    }
    else{
      result <<= -(*ydiv_)(yflux);
    }
  }
  else if( varyZ_ ){
    const ZFluxT& zflux = zflux_->field_ref();
    result <<= -(*zdiv_)(zflux);
  }
}

//--------------------------------------------------------------------

template< typename FieldT >
RHSExpr<FieldT>::
Builder::Builder( const Expr::Tag& rhsTag,
                  const Expr::Tag& xFlux,
                  const Expr::Tag& yFlux,
                  const Expr::Tag& zFlux )
  : ExpressionBuilder(rhsTag),
    xFluxt_( xFlux ),
    yFluxt_( yFlux ),
    zFluxt_( zFlux )
{}

//--------------------------------------------------------------------

template< typename FieldT >
Expr::ExpressionBase*
RHSExpr<FieldT>::
Builder::build() const
{
  return new RHSExpr<FieldT>( xFluxt_, yFluxt_, zFluxt_ );
}

#endif // RHS_Expr_h
