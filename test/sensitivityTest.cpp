/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   sensitivityTest.cpp
 *  \date   Sep 1, 2015
 *  \author James C. Sutherland
 */

#include <test/TestHelper.h>
#include <expression/ExprLib.h>
#include <expression/Functions.h>
#include <expression/ExprPatch.h>
#include <expression/Expression.h>

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>

#include <iostream>
#include <stdexcept>

typedef SpatialOps::SVolField  FieldT;

using namespace Expr;
using namespace std;
using namespace SpatialOps;

typedef Expr::ExprPatch Patch;

//==============================================================================

class MultiExpr : public Expression<FieldT> // computes a vector of constant values
{
  MultiExpr() : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable(true);
  }
public:
  struct Builder : public Expr::ExpressionBuilder{
    Builder( const TagList& tags ) : ExpressionBuilder( tags, 0 ){}
    Expr::ExpressionBase* build() const{ return new MultiExpr(); }
  };  /* end of Builder class */

  void evaluate()
  {
    typename Expr::Expression<FieldT>::ValVec& vars = this->get_value_vec();
    float x=1.1;
    for( auto& var: vars ){
      *var <<= x;
      x += 1.1;
    }
  }

  void sensitivity( const Tag& sensVar )
  {
    for( const Tag& tag: this->get_tags() ){
      FieldT& sens = this->sensitivity_result(tag,sensVar);
      sens <<= -1.0;  // should always be overridden, so we should never see this value.
    }
  }
};

//==============================================================================

class CustomSens : public Expression<FieldT>
{
  DECLARE_FIELDS( FieldT, f1_, f2_, f3_ )
  CustomSens( const Tag& f1Tag, const Tag& f2Tag, const Tag& f3Tag )
    : Expr::Expression<FieldT>()
  {
    this->set_gpu_runnable(true);

    f1_ = this->create_field_request<FieldT>( f1Tag );
    f2_ = this->create_field_request<FieldT>( f2Tag );
    f3_ = this->create_field_request<FieldT>( f3Tag );
  }
public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag f1Tag_, f2Tag_, f3Tag_;
  public:
    Builder( const Tag& resultTag,
             const Tag& f1Tag,
             const Tag& f2Tag,
             const Tag& f3Tag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
  : ExpressionBuilder( resultTag, nghost ),
    f1Tag_( f1Tag ), f2Tag_( f2Tag ), f3Tag_( f3Tag )
  {}
    Expr::ExpressionBase* build() const{ return new CustomSens( f1Tag_,f2Tag_,f3Tag_ ); }
  };  /* end of Builder class */

  void evaluate(){
    this->value() <<= 2.0 * f1_->field_ref() + 3.0 * f2_->field_ref() + 4.0 * f3_->field_ref();
  }

  bool override_sensitivity() const{ return true; }

  void sensitivity( const Tag& var )
  {
    FieldT& sens = this->sensitivity_result(var);
    if     ( var == f1_->tag() ) sens <<= 2.0;
    else if( var == f2_->tag() ) sens <<= 3.0;
    else if( var == f3_->tag() ) sens <<= 4.0;
    else                         sens <<= 0.1234;
  }
};

//==============================================================================

class LinearExpr
    : public Expr::Expression<FieldT>
{
  DECLARE_FIELD( FieldT, a_ )
      DECLARE_FIELD( FieldT, b_ )
      DECLARE_FIELD( FieldT, x_ )

      /* declare any operators associated with this expression here */

      LinearExpr( const Expr::Tag& aTag,
                  const Expr::Tag& bTag,
                  const Expr::Tag& xTag )
        : Expr::Expression<FieldT>()
      {
        this->set_gpu_runnable(true);
        a_ = this->create_field_request<FieldT>( aTag );
        b_ = this->create_field_request<FieldT>( bTag );
        x_ = this->create_field_request<FieldT>( xTag );
      }

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag aTag_, bTag_, xTag_;
  public:
    /**
     *  @brief Build a LinearExpr expression
     *  @param resultTag the tag for the value that this expression computes
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& aTag,
             const Expr::Tag& bTag,
             const Expr::Tag& xTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
  : ExpressionBuilder( resultTag, nghost ),
    aTag_( aTag ),
    bTag_( bTag ),
    xTag_( xTag )
  {}

    Expr::ExpressionBase* build() const{
      return new LinearExpr( aTag_,bTag_,xTag_ );
    }

  };  /* end of Builder class */

  ~LinearExpr(){}

  void evaluate()
  {
    FieldT& result = this->value();
    const FieldT& a = a_->field_ref();
    const FieldT& b = b_->field_ref();
    const FieldT& x = x_->field_ref();
    result <<= a*x+b;
  }

  void sensitivity( const Tag& var )
  {
    const FieldT& a = a_->field_ref();
    const FieldT& b = b_->field_ref();
    const FieldT& x = x_->field_ref();

    FieldT& dydphi = this->sensitivity_result(var);

    if( var == this->get_tag() ){
      dydphi <<= 1.0;
    }
    else{
      const FieldT& dadphi = this->a_->sens_field_ref(var);
      const FieldT& dbdphi = this->b_->sens_field_ref(var);
      const FieldT& dxdphi = this->x_->sens_field_ref(var);

      dydphi <<= dadphi*x + a*dxdphi + b*dbdphi;
    }
  }

};


//==============================================================================

bool compare_results( const Tag& y1t,
                      const Tag& y2t,
                      const Tag& y3t,
                      const Tag& dy1dphitag,
                      const Tag& dy2dphitag,
                      const Tag& dy1dx2tag,
                      const Tag& dy2dx2tag,
                      const Tag& dy3dx2tag,
                      const Tag& dy3dphitag,
                      FieldManagerList& fml )
{
  const bool report = false;
  TestHelper status( report );

  FieldT& dy1dphi = fml.field_ref<FieldT>( dy1dphitag );
  FieldT& dy2dphi = fml.field_ref<FieldT>( dy2dphitag );
  FieldT& dy1dx2  = fml.field_ref<FieldT>( dy1dx2tag  );
  FieldT& dy2dx2  = fml.field_ref<FieldT>( dy2dx2tag  );
  FieldT& dy3dx2  = fml.field_ref<FieldT>( dy3dx2tag  );
  FieldT& dy3dphi = fml.field_ref<FieldT>( dy3dphitag );
  FieldT& y1      = fml.field_ref<FieldT>( y1t );
  FieldT& y2      = fml.field_ref<FieldT>( y2t );
  FieldT& y3      = fml.field_ref<FieldT>( y3t );
# ifdef ENABLE_CUDA
  y1.add_device( CPU_INDEX );
  y2.add_device( CPU_INDEX );
  dy1dphi.add_device( CPU_INDEX );
  dy2dphi.add_device( CPU_INDEX );
# endif

  status( field_equal( 28.2789,      y1, 0.000001, 0.0001 ), "y1" );
  status( field_equal(    41.0,      y2, 0.000001, 0.00001), "y2" );
  status( field_equal(   1.222,      y3, 0.000001, 0.001  ), "y3" );
  status( field_equal(-12.4844, dy1dphi, 0.000001, 0.0001 ), "dy1_dphi" );
  status( field_equal(    20.0, dy2dphi, 0.000001, 0.1e-12), "dy2_dphi" );
  status( field_equal(     0.0,  dy1dx2, 0.000001, 0.1e-12), "dy1_dx2"  );
  status( field_equal(     0.0,  dy2dx2, 0.000001, 0.1e-12), "dy2_dx2"  );
  status( field_equal(     1.0,  dy3dx2, 0.000001, 0.1e-12), "dy3_dx2"  );
  status( field_equal(     0.0, dy3dphi, 0.000001, 0.1e-12), "dy3_dphi" );

  if( report && !status.ok() ){
    std::cout << " Y1       expected  28.2789   Found " << y1[0]      << std::endl
        << " Y2       expected  41        Found " << y2[0]      << std::endl
        << " dY1/dphi expected  12.4844   Found " << dy1dphi[0] << std::endl
        << " dY2/dphi expected  20        Found " << dy2dphi[0] << std::endl
        << " dY1/dx2  expected  0         Found " << dy1dx2[0]  << std::endl
        << " dY2/dx2  expected  20        Found " << dy2dx2[0]  << std::endl
        << " dY3/dphi expected  1         Found " << dy3dphi[0] << std::endl
        << " dY3/dx2  expected  0         Found " << dy3dx2[0]  << std::endl;
  }

  // reset values so that the next run (if applicable) doesn't have the correct values in it to start with.
  y1      <<= 0.0;   y2      <<= 0.0;
  dy1dphi <<= 0.0;   dy2dphi <<= 0.0;

  return status.ok();
}

//==============================================================================

bool test_general_linear_sens()
{
  TestHelper status(false);

  //--------------------------------------------------------------------------
  try{
    const Tag xt("x",STATE_NONE);
    const Tag yt("y",STATE_NONE);
    const Tag at("a",STATE_NONE);
    const Tag bt("b",STATE_NONE);
    const Tag ct("c",STATE_NONE);
    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionFactory factory(false);

    factory.register_expression( new ConstantExpr<FieldT>::Builder(at,2.0) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(ct,1.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(bt,ct,5.0,0.0));
    factory.register_expression( new ConstantExpr<FieldT>::Builder(xt,3.0) );

    const ExpressionID yid = factory.register_expression( new LinearExpr::Builder(yt,at,bt,xt) );

    ExpressionTree tree(yid,factory,0,"general_linear");
    {
      std::ofstream fout("general_linear_sens.dot");
      tree.write_tree(fout,false,true);
    }
    tree.compute_sensitivities( tag_list(yt), tag_list(xt,at,bt) );
    tree.register_fields( fml );
    fml.field_manager<FieldT>().lock_field( yt );
    fml.allocate_fields( patch.field_info() );

    tree.bind_fields(fml);
    tree.lock_fields(fml);

    status( tree.computes_field( sens_tag(xt,xt)), "computes dx/dx" );
    status( tree.computes_field( sens_tag(yt,xt)), "computes dy/dx" );
    status( tree.computes_field( sens_tag(at,xt)), "computes da/dx" );
    status( tree.computes_field( sens_tag(bt,xt)), "computes db/dx" );

    status( tree.computes_field( sens_tag(xt,at)), "computes dx/da" );
    status( tree.computes_field( sens_tag(yt,at)), "computes dy/da" );
    status( tree.computes_field( sens_tag(at,at)), "computes da/da" );
    status( tree.computes_field( sens_tag(bt,at)), "computes db/da" );

    status( tree.computes_field( sens_tag(yt,bt)), "computes dy/db" );
    status( tree.computes_field( sens_tag(xt,bt)), "computes dx/db" );
    status( tree.computes_field( sens_tag(at,bt)), "computes da/db" );
    status( tree.computes_field( sens_tag(bt,bt)), "computes db/db" );

    { // the following shouldn't be computed - they should be short-circuited through the graph
      status( !tree.computes_field( sens_tag(bt,ct)), "does not compute db/dc");
      status( !tree.computes_field( sens_tag(ct,xt)), "does not compute dc/dx");
      status( !tree.computes_field( sens_tag(ct,at)), "does not compute dc/da");
      status( !tree.computes_field( sens_tag(ct,bt)), "does not compute dc/db");
      status( !tree.computes_field( sens_tag(ct,yt)), "does not compute dc/dy" );
      status( !tree.computes_field( sens_tag(yt,yt)), "does not compute dy/dy" );
    }

    tree.execute_tree();

    const FieldT& ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );
    const FieldT& asensx = fml.field_ref<FieldT>( sens_tag(at,xt) );
    const FieldT& bsensx = fml.field_ref<FieldT>( sens_tag(bt,xt) );
    const FieldT& xsensx = fml.field_ref<FieldT>( sens_tag(xt,xt) );

    const FieldT& ysensa = fml.field_ref<FieldT>( sens_tag(yt,at) );
    const FieldT& asensa = fml.field_ref<FieldT>( sens_tag(at,at) );
    const FieldT& bsensa = fml.field_ref<FieldT>( sens_tag(bt,at) );
    const FieldT& xsensa = fml.field_ref<FieldT>( sens_tag(xt,at) );

    const FieldT& ysensb = fml.field_ref<FieldT>( sens_tag(yt,bt) );
    const FieldT& asensb = fml.field_ref<FieldT>( sens_tag(at,bt) );
    const FieldT& bsensb = fml.field_ref<FieldT>( sens_tag(bt,bt) );
    const FieldT& xsensb = fml.field_ref<FieldT>( sens_tag(xt,bt) );

    status( field_equal(2.0,ysensx), "dy/dx" );
    status( field_equal(3.0,ysensa), "dy/da" );
    status( field_equal(5.0,ysensb), "dy/db" );

    status( field_equal(0.0,asensx), "da/dx" );
    status( field_equal(1.0,asensa), "da/da" );
    status( field_equal(0.0,asensb), "da/db" );

    status( field_equal(0.0,bsensx), "db/dx" );
    status( field_equal(0.0,bsensa), "db/da" );
    status( field_equal(1.0,bsensb), "db/db" );

    status( field_equal(1.0,xsensx), "dx/dx" );
    status( field_equal(0.0,xsensa), "dx/da" );
    status( field_equal(0.0,xsensb), "dx/db" );

//    std::cout << "dy/dx =  " << ysensx[0] << " -- expected 2.0\n";
//    std::cout << "dy/da =  " << ysensa[0] << " -- expected 3.0\n";
//    std::cout << "dy/db =  " << ysensb[0] << " -- expected 5.0\n";

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_src_sens()
{
  TestHelper myStatus(false);

  try{
    ExpressionFactory factory(false);
    const Tag at("a",STATE_NONE);
    const Tag xt("x",STATE_NONE);
    const Tag yt("y",STATE_NONE);

    const ExpressionID root = factory.register_expression( new LinearFunction<FieldT>::Builder(at,xt,5.0,2.0) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(xt,3.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(yt,xt,20.0,1.0) );
    factory.attach_dependency_to_expression(yt,at,ADD_SOURCE_EXPRESSION);

    ExpressionTree tree( root, factory, 0, "src" );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    tree.compute_sensitivities( tag_list(at), tag_list(xt) );
    {
      std::ofstream fout("srcSens.dot");
      tree.write_tree(fout,false,true);
    }
    tree.register_fields( fml );
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields(fml);
    tree.lock_fields(fml);
    tree.execute_tree();

    myStatus( tree.computes_field( sens_tag(at, xt)), "computes da/dx" );
    myStatus( tree.computes_field( sens_tag(yt, xt)), "computes dy/dx" );

    const FieldT& asensx = fml.field_ref<FieldT>( sens_tag(at,xt) );
    const FieldT& ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );
    SpatFldPtr<FieldT> tmp = SpatialFieldStore::get<FieldT>(asensx);
    *tmp <<= ysensx + 5.0;

    myStatus( field_equal(20.0,ysensx), "dy/dx" );
//    std::cout << "found: " << ysensx[0] << ", expected: " << 20 << std::endl;
//    std::cout << "found: " << asensx[0] << ", expected: " << 5.0+ysensx[0] << std::endl;

    myStatus( field_equal(asensx,*tmp), "da/dx" );

    return myStatus.ok();
  }
  catch( std::exception& err ){
    std::cout << "ERROR:\n\t" << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_mult_sens()
{
  TestHelper myStatus(false);

  try{
    ExpressionFactory factory(false);
    const Tag at("a",STATE_NONE);
    const Tag bt("b",STATE_NONE);
    const Tag xt("x",STATE_NONE);
    const Tag yt("y",STATE_NONE);

    const ExpressionID root = factory.register_expression( new LinearExpr::Builder(yt,at,bt,xt) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(xt,3.0) );
    factory.register_expression( new MultiExpr::Builder( tag_list(at,bt) ) );

    ExpressionTree tree( root, factory, 0, "multi" );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    tree.compute_sensitivities( tag_list(yt), tag_list(xt) );
    {
      std::ofstream fout("multiSens.dot");
      tree.write_tree(fout,false,true);
    }
    tree.register_fields( fml );
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields(fml);
    tree.lock_fields(fml);

    myStatus( tree.computes_field( sens_tag(at, xt)), "computes da/dx" );
    myStatus( tree.computes_field( sens_tag(bt, xt)), "computes db/dx" );
    myStatus( tree.computes_field( sens_tag(yt, xt)), "computes dy/dx" );

    tree.execute_tree();

    const FieldT& asensx = fml.field_ref<FieldT>( sens_tag(at,xt) );
    const FieldT& xsensx = fml.field_ref<FieldT>( sens_tag(xt,xt) );
    const FieldT& ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );
    const FieldT& a = fml.field_ref<FieldT>( at );

    myStatus( field_equal(1.0,xsensx), "dx/dx" );
    myStatus( field_equal(  a,ysensx), "dy/dx" );
    myStatus( field_equal(0.0,asensx), "da/dx" );
//    std::cout << "found: " << ysensx[0] << ", expected: " << a[0] << std::endl;
//    std::cout << "found: " << asensx[0] << ", expected: 0.0" << std::endl;

    return myStatus.ok();
  }
  catch( std::exception& err ){
    std::cout << "ERROR:\n\t" << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_mult_src()
{
  TestHelper myStatus(false);

  try{
    ExpressionFactory factory(false);
    const Tag at("a",STATE_NONE);
    const Tag bt("b",STATE_NONE);
    const Tag xt("x",STATE_NONE);
    const Tag yt("y",STATE_NONE);

    const ExpressionID root = factory.register_expression( new LinearFunction<FieldT>::Builder(yt,xt,5.0,10.0) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(xt,3.0) );
    factory.register_expression( new MultiExpr::Builder( tag_list(at,bt) ) );
    factory.attach_dependency_to_expression( at, yt, ADD_SOURCE_EXPRESSION );

    ExpressionTree tree( root, factory, 0, "multi src" );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    tree.compute_sensitivities( tag_list(yt), tag_list(xt) );
    {
      std::ofstream fout("multiSrc.dot");
      tree.write_tree(fout,false,true);
    }
    tree.register_fields( fml );
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields(fml);
    tree.lock_fields(fml);

    myStatus( tree.computes_field( sens_tag(at, xt)), "computes da/dx" );
    myStatus( tree.computes_field( sens_tag(bt, xt)), "computes db/dx" );
    myStatus( tree.computes_field( sens_tag(yt, xt)), "computes dy/dx" );
    myStatus( tree.computes_field( sens_tag(xt, xt)), "computes dx/dx" );

    tree.execute_tree();

    const FieldT& asensx = fml.field_ref<FieldT>( sens_tag(at,xt) );
    const FieldT& xsensx = fml.field_ref<FieldT>( sens_tag(xt,xt) );
    const FieldT& ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );
    const FieldT& a = fml.field_ref<FieldT>( at );
    const FieldT& y = fml.field_ref<FieldT>( yt );

    myStatus( field_equal(26.1,y,0.001), "y" );
    myStatus( field_equal( 1.1,a,0.001), "a" );
//    std::cout << "found: " << y[0] << ", expected: " << 26.1 << std::endl;
//    std::cout << "found: " << a[0] << ", expected: 1.1" << std::endl;

    myStatus( field_equal(1.0,xsensx), "dx/dx" );
    myStatus( field_equal(5.0,ysensx), "dy/dx" );
    myStatus( field_equal(0.0,asensx), "da/dx" );
//    std::cout << "found: " << ysensx[0] << ", expected: " << a[0] << std::endl;
//    std::cout << "found: " << asensx[0] << ", expected: 0.0" << std::endl;

    return myStatus.ok();
  }
  catch( std::exception& err ){
    std::cout << "ERROR:\n\t" << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_cleave_sens()
{
  TestHelper cleaveStatus( false );

  try{
    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionFactory factory(false);

    /*
     * y1 = a1 x + b1
     * y2 = a2 phi + b2
     * x  = 3 sin(phi)
     *
     * so
     *
     * dy1/dphi = a1 dx/dphi = 3 a1 cos(phi)
     * dy2/dphi = a2
     */
    const Tag  y1t( "y1",  STATE_NONE );
    const Tag  y2t( "y2",  STATE_NONE );
    const Tag   xt( "x",   STATE_NONE );
    const Tag phit( "phi", STATE_NONE );

    const Tag x2t("x2",STATE_NONE);
    const Tag y3t("y3",STATE_NONE);

    const Tag dy1dphitag( sens_tag(y1t,phit) );
    const Tag dy2dphitag( sens_tag(y2t,phit) );
    const Tag dy1dx2tag ( sens_tag(y1t, x2t) );
    const Tag dy2dx2tag ( sens_tag(y2t, x2t) );
    const Tag dy3dx2tag ( sens_tag(y3t, x2t) );
    const Tag dy3dphitag( sens_tag(y3t,phit) );

    const ExpressionID idx   = factory.register_expression( new SinFunction   <FieldT>::Builder(xt,phit,3.0,1.0,0.0) );
    const ExpressionID idphi = factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
    const ExpressionID idy1  = factory.register_expression( new LinearFunction<FieldT>::Builder(y1t,xt,10.0,1.0)     );
    const ExpressionID idy2  = factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );

    const ExpressionID idx2 = factory.register_expression( new ConstantExpr<FieldT>::Builder(x2t,0.222) );
    const ExpressionID idy3 = factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1));

    factory.cleave_from_children( idy1 );
    factory.cleave_from_children( idy2 );

    //--------------------------------------------------------------------------
    // Ensure proper calculation of sensitivities on the un-cleaved graph

    // create a tree with "id1" and "idy2" as root nodes
    ExpressionTree tree( idy1, factory, 0, "sens" );
    tree.insert_tree( idy2 );
    tree.insert_tree( idy3 );

    {
      std::ofstream fout("sens.dot");
      tree.write_tree(fout,false,true);
    }

    // request sensitivity calculation w.r.t. phi and x2.
    tree.compute_sensitivities( /*   dep vars */ tag_list(y1t,y2t,y3t),
                                /* indep vars */ tag_list(phit,x2t) );

    tree.register_fields( fml );
    tree.lock_fields(fml);
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields( fml );

    // run the graph, which will calculate values and sensitivities
    tree.execute_tree();
    cleaveStatus( compare_results( y1t, y2t, y3t, dy1dphitag, dy2dphitag, dy1dx2tag, dy2dx2tag, dy3dx2tag, dy3dphitag, fml ), "Uncleaved graph sensitivities" );
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // CLEAVE THE TREE AND ENSURE THAT SENSITIVITY CALCULATIONS PROPAGATE
    typedef ExpressionTree::TreeList TreeList;
    const TreeList treeList = tree.split_tree();
    cleaveStatus( treeList[0]->computes_field(phit ), "0 computes phi");
    cleaveStatus( treeList[1]->computes_field(xt   ), "1 computes x"  );
    cleaveStatus( treeList[1]->has_expression(idphi), "1 has phi placeholder" );
    cleaveStatus( treeList[2]->computes_field(y1t  ), "2 computes y1" );
    cleaveStatus( treeList[2]->computes_field(y2t  ), "2 computes y2" );
    cleaveStatus( treeList[2]->has_expression(idphi), "2 has phi placeholder" );
    cleaveStatus( treeList[2]->has_expression(idx  ), "2 has x placeholder" );
    cleaveStatus( treeList[1]->computes_field( sens_tag(xt,phit) ), "1 computes x"  );
    cleaveStatus( treeList[2]->computes_field( dy1dphitag ), "0 computes dy1_dphi");

    for( TreeList::const_iterator i=treeList.begin(); i!=treeList.end(); ++i ){
      Expr::ExpressionTree::TreePtr tt( *i );
      {
        std::ostringstream nam;
        nam << tt->name() << ".dot";
        ofstream fout( nam.str().c_str() );
        tt->write_tree( fout );
      }
      tt->register_fields(fml);
      fml.allocate_fields( patch.field_info() );
      tt->bind_fields( fml );
      tt->execute_tree();
    }
    cleaveStatus( compare_results( y1t, y2t, y3t, dy1dphitag, dy2dphitag, dy1dx2tag, dy2dx2tag, dy3dx2tag, dy3dphitag, fml ), "Cleaved graph sensitivities" );

    return cleaveStatus.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_ivar_deps()
{
  TestHelper status( false );

  try{
    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionFactory factory(false);

    const Tag   xt(   "x", STATE_NONE );
    const Tag   yt(   "y", STATE_NONE );
    const Tag   zt(   "z", STATE_NONE );
    const Tag phit( "phi", STATE_NONE );

    factory.register_expression( new ConstantExpr  <FieldT>::Builder(zt,2.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(xt,zt,5.0,-6.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(yt,xt,10.0,1.0) );
    const ExpressionID idphi  = factory.register_expression( new LinearFunction<FieldT>::Builder(phit,yt,20.0,1.0) );

    ExpressionTree tree( idphi, factory, 0, "ivar-depvar" );

    tree.compute_sensitivities(
          /*   dep vars */ tag_list(phit),
          /* indep vars */ tag_list(xt) );
    {
      std::ofstream fout("ivar-deps.dot");
      tree.write_tree(fout,false,true);
    }

    tree.register_fields( fml );
    tree.lock_fields(fml);
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields( fml );

    status( tree.computes_field(phit), "computes phi");
    status( tree.computes_field(  xt), "computes x");
    status( tree.computes_field(  yt), "computes y");
    status( tree.computes_field(sens_tag(phit,xt) ), "computes dphi/dx");
    status( tree.computes_field(sens_tag(yt,xt) ), "computes dy/dx");
    status( tree.computes_field(sens_tag(xt,xt) ), "computes dx/dx");

    status( !tree.computes_field(sens_tag(phit,yt) ), "doesn't compute dphi/dy");
    status( !tree.computes_field(sens_tag(  yt,yt) ), "doesn't compute dy/dy");
    status( !tree.computes_field(sens_tag(  xt,zt) ), "doesn't compute dx/dz");
    status( !tree.computes_field(sens_tag(  yt,zt) ), "doesn't compute dy/dz");
    status( !tree.computes_field(sens_tag(  zt,xt) ), "doesn't compute dz/dx");
    status( !tree.computes_field(sens_tag(  zt,yt) ), "doesn't compute dz/dy");

    tree.execute_tree();

    const FieldT& phisensx = fml.field_ref<FieldT>( sens_tag(phit,xt) );
    const FieldT&   ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );

    status( field_equal(  10.0,   ysensx ), "dy/dx"   );
    status( field_equal( 200.0, phisensx ), "dphi/dx" );

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

bool test_ivar_deps_2()
{
  TestHelper status( false );

  try{
    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionFactory factory(false);

    const Tag   xt(   "x", STATE_NONE );
    const Tag   yt(   "y", STATE_NONE );
    const Tag   zt(   "z", STATE_NONE );
    const Tag   at(   "a", STATE_NONE );
    const Tag   bt(   "b", STATE_NONE );
    const Tag phit( "phi", STATE_NONE );

    factory.register_expression( new ConstantExpr  <FieldT>::Builder(at,10.0) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(bt,1.0) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(zt,2.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(xt,zt,5.0,-6.0) );
    factory.register_expression( new LinearExpr            ::Builder(yt,at,bt,xt) );
    const ExpressionID idphi  = factory.register_expression( new LinearFunction<FieldT>::Builder(phit,yt,20.0,1.0) );

    ExpressionTree tree( idphi, factory, 0, "ivar-depvar-2" );

    tree.compute_sensitivities(
          /*   dep vars */ tag_list(phit),
          /* indep vars */ tag_list(yt)
                              );
    {
      std::ofstream fout("ivar-deps_2.dot");
      tree.write_tree(fout,false,true);
    }

    tree.register_fields( fml );
    tree.lock_fields(fml);
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields( fml );

    status( tree.computes_field(phit), "computes phi");
    status( tree.computes_field(xt ), "computes x");
    status( tree.computes_field(yt ), "computes y");
    status( tree.computes_field(sens_tag(phit,yt) ), "computes dphi/dy");
    status( tree.computes_field(sens_tag(yt,yt) ), "computes dy/dy");

    status( !tree.computes_field(sens_tag(  at,xt) ), "doesn't compute da/dx");
    status( !tree.computes_field(sens_tag(  bt,xt) ), "doesn't compute db/dx");
    status( !tree.computes_field(sens_tag(phit,xt) ), "doesn't compute dphi/dx");
    status( !tree.computes_field(sens_tag(  yt,xt) ), "doesn't compute dy/dx");
    status( !tree.computes_field(sens_tag(  xt,xt) ), "doesn't compute dx/dx");
    status( !tree.computes_field(sens_tag(  at,yt) ), "doesn't compute da/dy");
    status( !tree.computes_field(sens_tag(  bt,yt) ), "doesn't compute db/dy");
    status( !tree.computes_field(sens_tag(  xt,zt) ), "doesn't compute dx/dz");
    status( !tree.computes_field(sens_tag(  yt,zt) ), "doesn't compute dy/dz");
    status( !tree.computes_field(sens_tag(  zt,xt) ), "doesn't compute dz/dx");
    status( !tree.computes_field(sens_tag(  zt,yt) ), "doesn't compute dz/dy");

    tree.execute_tree();

    status( field_equal(  20.0, fml.field_ref<FieldT>( sens_tag(phit,yt) ) ), "dphi/dy" );

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

bool test_ivar_deps_3()
{
  TestHelper status( false );

  try{
    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionFactory factory(false);

    const Tag x1t( "x1", STATE_NONE );
    const Tag x2t( "x2", STATE_NONE );
    const Tag x3t( "x3", STATE_NONE );
    const Tag x4t( "x4", STATE_NONE );

    const Tag d1t( "d1", STATE_NONE );
    const Tag d2t( "d2", STATE_NONE );

    const Tag  at( "a", STATE_NONE );
    const Tag  bt( "b", STATE_NONE );

    IDSet roots;

    factory.register_expression( new LinearFunction<FieldT>::Builder(at,x4t,10.0,-1.0) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(bt,3.0) );
    factory.register_expression( new PlaceHolder <FieldT>::Builder(x3t) );
    factory.register_expression( new PlaceHolder <FieldT>::Builder(x4t) );
    factory.register_expression( new LinearExpr          ::Builder(x2t,at,x4t,x3t) );
    factory.register_expression( new LinearExpr          ::Builder(x1t,x2t,x3t,at) );

    roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(d1t,x1t,-5.0,6.0) ) );
    roots.insert( factory.register_expression( new LinearExpr            ::Builder(d2t,x4t,x1t,x2t) ) );

    ExpressionTree tree( roots, factory, 0, "ivar-depvar-3" );

    tree.compute_sensitivities(
          /*   dep vars */ tag_list(d1t,d2t),
          /* indep vars */ tag_list(x3t,x4t)
                              );
    {
      std::ofstream fout("ivar-deps_3.dot");
      tree.write_tree(fout,false,true);
    }

    tree.register_fields( fml );
    tree.lock_fields(fml);
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields( fml );

    status( tree.computes_field(d1t), "computes d1");
    status( tree.computes_field(d2t), "computes d2");
    status( !tree.computes_field(x3t), "doesn't compute x3");

    status( tree.computes_field(sens_tag(d1t,x3t) ), "computes dd1/dx3");
    status( tree.computes_field(sens_tag(d1t,x4t) ), "computes dd1/dx4");

    status( tree.computes_field(sens_tag(d2t,x3t) ), "computes dd2/dx3");
    status( tree.computes_field(sens_tag(d2t,x4t) ), "computes dd2/dx4");

    status( tree.computes_field(sens_tag(x1t,x3t) ), "computes dx1/dx3");
    status( tree.computes_field(sens_tag(x2t,x3t) ), "computes dx2/dx3");
    status( tree.computes_field(sens_tag(x3t,x3t) ), "computes dx3/dx3");
    status( tree.computes_field(sens_tag(at,x3t) ), "computes da/dx3");
    status( tree.computes_field(sens_tag(at,x3t) ), "computes da/dx4");

    status( !tree.computes_field(sens_tag(at,x1t) ), "doesn't compute da/dx3");
    status( tree.computes_field(sens_tag(at,x4t) ), "computes da/dx4");

    status( !tree.computes_field(sens_tag(x1t, at) ), "doesn't compute dx1/da");
    status( !tree.computes_field(sens_tag(x3t,x1t) ), "doesn't compute dx3/dx1");
    status( !tree.computes_field(sens_tag(x1t,x2t) ), "doesn't compute dx1/dx2");
    status( !tree.computes_field(sens_tag(x2t,x2t) ), "doesn't compute dx2/dx2");

    tree.execute_tree();

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

bool test_ivar_deps_4()
{
  TestHelper status( false );

  try{
    ExpressionFactory factory(false);

    const Tag x1t( "x1", STATE_NONE );
    const Tag x2t( "x2", STATE_NONE );
    const Tag x3t( "x3", STATE_NONE );

    const Tag d1t( "d1", STATE_NONE );
    const Tag d2t( "d2", STATE_NONE );

    const Tag  at( "a", STATE_NONE );
    const Tag  bt( "b", STATE_NONE );

    IDSet roots;

    factory.register_expression( new ConstantExpr  <FieldT>::Builder(at,10.0) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(bt,2.0) );
    factory.register_expression( new LinearExpr            ::Builder(x2t,at,bt,x3t) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(x3t,4.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(x1t,x2t,5.0,-6.0) );

    roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(d1t,x1t,-5.0,6.0) ) );
    roots.insert( factory.register_expression( new LinearExpr            ::Builder(d2t,bt,x1t,x2t) ) );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionTree tree( roots, factory, 0, "ivar-depvar-4" );

    tree.compute_sensitivities(
          /*   dep vars */ tag_list( d1t, d2t ),
          /* indep vars */ tag_list( x3t )
                              );
    {
      std::ofstream fout( "ivar-deps_4.dot" );
      tree.write_tree( fout, false, true );
    }

    tree.register_fields( fml );
    tree.lock_fields( fml );
    fml.allocate_fields( patch.field_info());
    tree.bind_fields( fml );

    status( tree.computes_field( d1t ), "computes d1" );
    status( tree.computes_field( d2t ), "computes d2" );
    status( tree.computes_field( x3t ), "computes x3" );

    status( tree.computes_field( sens_tag( d1t, x3t )), "computes d1/dx3" );
    status( tree.computes_field( sens_tag( d2t, x3t )), "computes d2/dx3" );
    status( tree.computes_field( sens_tag( x1t, x3t )), "computes dx1/dx3" );
    status( tree.computes_field( sens_tag( x2t, x3t )), "computes dx2/dx3" );
    status( tree.computes_field( sens_tag( x3t, x3t )), "computes dx3/dx3" );
    status( tree.computes_field( sens_tag(  at, x3t )), "computes da/dx3" );
    status( tree.computes_field( sens_tag(  bt, x3t )), "computes db/dx3" );

    status( !tree.computes_field( sens_tag( d1t, x1t )), "doesn't compute d1/dx1" );
    status( !tree.computes_field( sens_tag( x1t, x2t )), "doesn't compute dx1/dx2" );
    status( !tree.computes_field( sens_tag( x1t,  at )), "doesn't compute dx1/da" );
    status( !tree.computes_field( sens_tag( x3t, x1t )), "doesn't compute dx3/dx1" );
    status( !tree.computes_field( sens_tag( x3t, x2t )), "doesn't compute dx3/dx2" );

    tree.execute_tree();

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_ivar_deps_5()
{
  TestHelper status( false );

  try{
    ExpressionFactory factory(false);

    const Tag x1t( "x1", STATE_NONE );
    const Tag x2t( "x2", STATE_NONE );
    const Tag x3t( "x3", STATE_NONE );

    const Tag d1t( "d1", STATE_NONE );
    const Tag d2t( "d2", STATE_NONE );

    const Tag  at( "a", STATE_NONE );
    const Tag  bt( "b", STATE_NONE );

    IDSet roots;

    factory.register_expression( new ConstantExpr  <FieldT>::Builder(at,10.0) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(bt,2.0) );
    factory.register_expression( new LinearExpr            ::Builder(x2t,at,bt,x3t) );
    factory.register_expression( new ConstantExpr  <FieldT>::Builder(x3t,4.0) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(x1t,x2t,5.0,-6.0) );

    roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(d1t,x1t,-5.0,6.0) ) );
    roots.insert( factory.register_expression( new LinearExpr            ::Builder(d2t,bt,x1t,x2t) ) );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    ExpressionTree tree( roots, factory, 0, "ivar-depvar-4" );

    tree.compute_sensitivities(
          /*   dep vars */ tag_list( d1t, d2t ),
          /* indep vars */ tag_list( x2t )
                              );

    tree.register_fields( fml );
    tree.lock_fields( fml );
    fml.allocate_fields( patch.field_info());
    tree.bind_fields( fml );

    status( tree.computes_field( d1t ), "computes d1" );
    status( tree.computes_field( d2t ), "computes d2" );
    status( tree.computes_field( x3t ), "computes x3" );

    status( tree.computes_field( sens_tag( d1t, x2t )), "computes d1/dx2" );
    status( tree.computes_field( sens_tag( d2t, x2t )), "computes d2/dx2" );
    status( tree.computes_field( sens_tag( x1t, x2t )), "computes dx1/dx2" );
    status( tree.computes_field( sens_tag( x2t, x2t )), "computes dx2/dx2" );
    status( tree.computes_field( sens_tag(  bt, x2t )), "computes db/dx2"  );

    status( !tree.computes_field( sens_tag(  at, x3t )), "doesn't compute da/dx2" );
    status( !tree.computes_field( sens_tag(  at, x1t )), "doesn't compute da/dx1" );
    status( !tree.computes_field( sens_tag(  at, x3t )), "doesn't compute da/dx3" );
    status( !tree.computes_field( sens_tag(  bt, x3t )), "doesn't compute db/dx3" );
    status( !tree.computes_field( sens_tag( x1t,  at )), "doesn't compute dx1/da" );
    status( !tree.computes_field( sens_tag( x3t, x1t )), "doesn't compute dx3/dx1" );
    status( !tree.computes_field( sens_tag( x3t, x2t )), "doesn't compute dx3/dx2" );

    tree.execute_tree();

    return status.ok();
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

bool test_custom_sens()
{
  TestHelper status(false);

  const Tag  y1t( "y1",  STATE_NONE );
  const Tag  y2t( "y2",  STATE_NONE );
  const Tag  x1t( "x1",  STATE_NONE );
  const Tag phit( "phi", STATE_NONE );
  const Tag  x2t( "x2",  STATE_NONE );
  const Tag  y3t( "y3",  STATE_NONE );
  const Tag  cst( "customSens",STATE_NONE );

  try{
    {
      ExpressionFactory factory(false);

      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x1t,2.0)  );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222));
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(y1t,3.333));

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,x1t,x2t,y1t) ) );
      ExpressionTree csTree( roots, factory, 0, "custSens1" );
      {
        std::ofstream fout("custSens1.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(cst), tag_list(x1t) );
      csTree.register_fields( fml );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);

      TestHelper csStatus( false );

      csStatus( !csTree.computes_field( sens_tag(y1t,x1t)), "doesn't compute dy1/x1" );
      csStatus( !csTree.computes_field( sens_tag(y1t,x2t)), "doesn't compute dy1/dx2" );
      csStatus( !csTree.computes_field( sens_tag(x2t,x1t)), "doesn't compute dx2/dx1" );
      csStatus( !csTree.computes_field( sens_tag(x2t,y1t)), "doesn't compute dx2/dy1" );
      csStatus( !csTree.computes_field( sens_tag(x1t,x2t)), "doesn't compute dx1/dx2" );
      csStatus( !csTree.computes_field( sens_tag(x1t,y1t)), "doesn't compute dx1/y12" );

      csStatus( csTree.computes_field( sens_tag(cst,x1t)), "computes dcst/x1" );
      csStatus( csTree.computes_field(cst), "computes cst" );
      csStatus( csTree.computes_field(x1t), "computes x1" );
      csStatus( csTree.computes_field(x2t), "computes x2" );
      csStatus( csTree.computes_field(y1t), "computes y1" );

      csTree.execute_tree();

      const FieldT& sensPhi= fml.field_ref<FieldT>( sens_tag(cst,x1t) );

      csStatus( field_equal( 2.0, sensPhi ), "sensitivity value of cst to phi" );

      status( csStatus.ok(), "custom sens test 1" );
    }
    {
      ExpressionFactory factory(false);

      factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222)           );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1)         );

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,phit,y2t,y3t) ) );
      ExpressionTree csTree( roots, factory, 0, "custSens2" );
      {
        std::ofstream fout("custSens2.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(cst), tag_list(phit) );
      csTree.register_fields( fml );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);
      csTree.execute_tree();

      TestHelper csStatus( false );

      csStatus( !csTree.computes_field( sens_tag(y3t,phit)), "doesn't compute dy3/phi" );
      csStatus( !csTree.computes_field( sens_tag(y3t, x2t)), "doesn't compute dy3/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y2t,phit)), "doesn't compute dy2/dphi" );
      csStatus( !csTree.computes_field( sens_tag(y3t,phit)), "doesn't compute dy3/dphi" );
      csStatus( !csTree.computes_field( sens_tag(phit,phit)), "doesn't compute dphi/dphi" );

      csStatus( csTree.computes_field( sens_tag(cst,phit)), "computes dcst/phi" );

      csStatus( csTree.computes_field(cst), "computes cst" );

      const FieldT& sensPhi= fml.field_ref<FieldT>( sens_tag(cst, phit) );

      csStatus( field_equal(2.0,sensPhi), "sensitivity value of cst to phi" );

      status( csStatus.ok(), "custom sens test 2" );
    }

    {
      ExpressionFactory factory(false);

      factory.register_expression( new SinFunction   <FieldT>::Builder(x1t,phit,3.0,1.0,0.0) );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222)           );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1)         );

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(y1t,x1t,10.0,1.0) ) );
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,phit,y2t,y3t) ) );
      ExpressionTree csTree( roots, factory, 0, "custSens3" );
      {
        std::ofstream fout("custSens3.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(y1t,cst), tag_list(phit) );
      csTree.register_fields( fml );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);
      csTree.execute_tree();

      TestHelper csStatus( false );

      csStatus( csTree.computes_field( sens_tag(y1t,phit)), "computes dy1/dphi" );
      csStatus( csTree.computes_field( sens_tag(x1t,phit)), "computes dx1/dphi" );
      csStatus( csTree.computes_field( sens_tag(phit,phit)), "computes dphi/dphi" );
      csStatus( csTree.computes_field( sens_tag(cst,phit)), "computes dcst/phi" );

      csStatus( !csTree.computes_field( sens_tag(y2t,phit)), "doesn't compute dy2/phi" );
      csStatus( !csTree.computes_field( sens_tag(y3t,phit)), "doesn't compute dy3/phi" );
      csStatus( !csTree.computes_field( sens_tag(y1t, x2t)), "doesn't compute dy1/dx2" );

      const FieldT& sensPhi= fml.field_ref<FieldT>( sens_tag(cst, phit) );

      csStatus( field_equal(2.0,sensPhi), "value of cst to phi" );

      status( csStatus.ok(), "custom sens test 3" );
    }

    {
      ExpressionFactory factory(false);

      factory.register_expression( new SinFunction   <FieldT>::Builder(x1t,phit,3.0,1.0,0.0) );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222)           );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1)         );

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(y1t,x1t,10.0,1.0) ) );
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,phit,y2t,y3t) ) );

      ExpressionTree csTree( roots, factory, 0, "custSens4" );
      {
        std::ofstream fout("custSens4.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(y1t), tag_list(phit) );
      csTree.register_fields( fml );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);
      csTree.execute_tree();

      TestHelper csStatus( false );

      csStatus( csTree.computes_field( sens_tag(y1t,phit)), "computes dy1/phi" );
      csStatus( csTree.computes_field( sens_tag(x1t,phit)), "computes dx1/dphi" );
      csStatus( csTree.computes_field( sens_tag(phit,phit)), "computes dphi/dphi" );

      csStatus( !csTree.computes_field( sens_tag(cst,phit)), "doesn't compute dcst/dphi" );
      csStatus( !csTree.computes_field( sens_tag(cst,x2t)), "doesn't compute dcst/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y3t,x2t)), "doesn't compute dy3t/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y2t,x2t)), "doesn't compute dy2t/dx2" );
      csStatus( !csTree.computes_field( sens_tag(x1t,x2t)), "doesn't compute dx1/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y1t,x2t)), "doesn't compute dy1/dx2" );

      status( csStatus.ok(), "Custom sens 4" );
    }
    {
      ExpressionFactory factory(false);

      factory.register_expression( new SinFunction   <FieldT>::Builder(x1t,phit,3.0,1.0,0.0) );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222)           );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1)         );

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(y1t,x1t,10.0,1.0) ) );
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,phit,y2t,y3t) ) );

      ExpressionTree csTree( roots, factory, 0, "custSens5" );
      {
        std::ofstream fout("custSens5.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(y1t), tag_list(phit,x2t) );
      csTree.register_fields( fml );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);
      csTree.execute_tree();

      TestHelper csStatus( false );

      csStatus( csTree.computes_field( sens_tag(y1t,phit)), "computes dy1/phi" );
      csStatus( csTree.computes_field( sens_tag(y1t,x2t)), "computes dy1/dx2" );
      csStatus( csTree.computes_field( sens_tag(x1t,phit)), "computes dx1/dphi" );

      csStatus( !csTree.computes_field( sens_tag(cst,phit)), "doesn't compute dcst/dphi" );
      csStatus( !csTree.computes_field( sens_tag(cst,x2t)), "doesn't compute dcst/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y3t,x2t)), "doesn't compute dy3t/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y2t,x2t)), "doesn't compute dy2t/dx2" );
      csStatus( !csTree.computes_field( sens_tag(x1t,x2t)), "doesn't compute dx1/dx2" );

      status( csStatus.ok(), "custom sens 5" );
    }

    {
      ExpressionFactory factory(false);

      factory.register_expression( new SinFunction   <FieldT>::Builder(x1t,phit,3.0,1.0,0.0) );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(phit,2.0)            );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y2t,phit,20.0,1.0)   );
      factory.register_expression( new ConstantExpr  <FieldT>::Builder(x2t,0.222)           );
      factory.register_expression( new LinearFunction<FieldT>::Builder(y3t,x2t,1,1)         );

      std::set<ExpressionID> roots;
      roots.insert( factory.register_expression( new LinearFunction<FieldT>::Builder(y1t,x1t,10.0,1.0) ) );
      roots.insert( factory.register_expression( new CustomSens::Builder(cst,phit,y2t,y3t) ) );

      ExpressionTree csTree( roots, factory, 0, "custSens" );
      {
        std::ofstream fout("custSens6.dot");
        csTree.write_tree(fout);
      }

      Patch patch( 1 );
      Expr::FieldManagerList& fml = patch.field_manager_list();
      csTree.compute_sensitivities( tag_list(cst), tag_list(x2t,y2t) );
      csTree.register_fields( fml );
      fml.field_manager<FieldT>().lock_field( y2t );
      fml.allocate_fields( patch.field_info() );
      csTree.bind_fields(fml);
      csTree.lock_fields(fml);
      csTree.execute_tree();

      TestHelper csStatus( false );

      csStatus( csTree.computes_field( sens_tag(cst, x2t)), "computes dst/dx2" );
      csStatus( csTree.computes_field( sens_tag(cst, y2t)), "computes dst/dy2" );

      csStatus( !csTree.computes_field( sens_tag(cst,  x1t)), "doesn't compute dcst/dx1" );
      csStatus( !csTree.computes_field( sens_tag(y1t, x2t)), "doesn't compute dy1/dx2" );
      csStatus( !csTree.computes_field( sens_tag(y1t, y2t)), "doesn't compute dy1/dy2" );
      csStatus( !csTree.computes_field( sens_tag(y1t,phit)), "doesn't compute dy1/dphi" );
      csStatus( !csTree.computes_field( sens_tag(cst,phit)), "doesn't compute dcst/dphi" );
      status( csStatus.ok(), "Custom sensitivities 6" );
    }
  }
  catch( std::exception& err ){
    status( false, "Custom" );
    std::cout << err.what() << std::endl;
    return false;
  }
  return status.ok();
}

//==============================================================================

bool test_state_sens()
{
  TestHelper myStatus(false);

  try{
    ExpressionFactory factory(false);
    const Tag at("a",STATE_NONE);
    const Tag xt("x",STATE_NONE);
    const Tag xdt("x",STATE_DYNAMIC);
    const Tag yt("y",STATE_NONE);

    const ExpressionID root = factory.register_expression( new LinearFunction<FieldT>::Builder(at,xdt,5.0,2.0) );
    factory.register_expression( new ConstantExpr<FieldT>::Builder(xt,3.0) );
    factory.register_expression( new PlaceHolder<FieldT>::Builder(xdt) );
    factory.register_expression( new LinearFunction<FieldT>::Builder(yt,xt,20.0,1.0) );
    factory.attach_dependency_to_expression(yt,at,SUBTRACT_SOURCE_EXPRESSION);

    ExpressionTree tree( root, factory, 0, "state" );

    Patch patch( 1 );
    Expr::FieldManagerList& fml = patch.field_manager_list();
    tree.compute_sensitivities( tag_list(at), tag_list(xt) );
    {
      std::ofstream fout("stateSens.dot");
      tree.write_tree(fout,false,true);
    }
    tree.register_fields( fml );
    fml.allocate_fields( patch.field_info() );
    tree.bind_fields(fml);
    tree.lock_fields(fml);
    fml.field_ref<FieldT>(xdt) <<= 13.0;

    tree.execute_tree();

    myStatus( tree.computes_field( sens_tag(at, xt)), "computes da/dx" );
    myStatus( tree.computes_field( sens_tag(yt, xt)), "computes dy/dx" );
    myStatus( tree.computes_field( sens_tag(xdt,xt)), "computes dxd/dx" );

    const FieldT& asensx = fml.field_ref<FieldT>( sens_tag(at,xt) );
    const FieldT& ysensx = fml.field_ref<FieldT>( sens_tag(yt,xt) );

    myStatus( field_equal(20.0,ysensx), "dy/dx" );
    myStatus( field_equal(-20.0,asensx), "da/dx" );
    myStatus( field_equal(0.0,fml.field_ref<FieldT>(sens_tag(xdt,xt))), "dxd/dx" );
//    std::cout << "dy/dx - found: " << ysensx[0] << ", expected: " << 20 << std::endl;
//    std::cout << "da/dx - found: " << asensx[0] << ", expected: " << -20 << std::endl;
//    std::cout << "dxd/dx - found: " << fml.field_ref<FieldT>(sens_tag(xdt,xt))[0] << ", expected: " << 0 << std::endl;

    myStatus( field_equal(61,fml.field_ref<FieldT>(yt)), "y" );
    myStatus( field_equal(6 ,fml.field_ref<FieldT>(at)), "a" );

    return myStatus.ok();
  }
  catch( std::exception& err ){
    std::cout << "ERROR:\n\t" << err.what() << std::endl;
  }
  return false;
}

//==============================================================================

int main()
{
  TestHelper status(true);

  status( test_general_linear_sens(), "general linear sensitivity test" );
  status( test_src_sens(),            "src sensitivities" );
  status( test_cleave_sens(),         "Cleaving on sensitivity graph" );
  status( test_custom_sens(),         "custom sensitivity tests" );
  status( test_ivar_deps(),           "indep-dep var test (1)" );
  status( test_ivar_deps_2(),         "indep-dep var test (2)" );
  status( test_ivar_deps_3(),         "indep-dep var test (3)" );
  status( test_ivar_deps_4(),         "indep-dep var test (4)" );
  status( test_ivar_deps_5(),         "indep-dep var test (5)" );
  status( test_state_sens(),          "STATE_DYNAMIC" );
  status( test_mult_sens(),           "multi-var sens" );
  status( test_mult_src(),            "multi-var src sens" );

  if( status.ok() ){
    std::cout <<"PASS\n";
    return 0;
  }
  std::cout << "FAIL\n";
  return -1;
}
