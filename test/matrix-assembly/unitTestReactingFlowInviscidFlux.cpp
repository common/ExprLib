/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/ExprPatch.h>
#include <expression/matrix-assembly/MatrixExpression.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/Compounds.h>
#include <expression/matrix-assembly/compressible-reactive-flow/InviscidFluxAssembler.h>

#include "Matrix.h"

#include "../TestHelper.h"

enum SpatialDimensions { X, Y, Z, XY, XZ, YZ, XYZ };
enum MatrixCalculation { EMPLACE, ADDIN, SUBIN };
enum Sign {POSITIVE, NEGATIVE};

bool run_inviscid_flux_test( const SpatialDimensions dimensions,
                             const MatrixCalculation operation,
                             const Expr::matrix::OrdinalType nspecies,
                             const Expr::matrix::OrdinalType extraRows,
                             const Sign velocitySign )
{
  using SVolFieldT = SpatialOps::SVolField;
  using ConstantType = Expr::ConstantExpr<SVolFieldT>::Builder;
  using PlaceHolderType = Expr::PlaceHolder<SVolFieldT>::Builder;
  using MatrixExprType = Expr::matrix::MatrixExpression<SVolFieldT>::Builder;
  using InviscidFluxJacT = Expr::matrix::InviscidFluxAssembler<SVolFieldT>;
  using SparseMatrixT = Expr::matrix::SparseMatrix<SVolFieldT>;
  using ScalarMatrixType = densematrix_for_test::Matrix<double>;

  // ------------------------------------------------------------------------------------------------

  const Expr::Tag xCoordTag( "x", Expr::STATE_NONE );
  const Expr::Tag yCoordTag( "y", Expr::STATE_NONE );
  const Expr::Tag zCoordTag( "z", Expr::STATE_NONE );

  const Expr::Tag pressureTag      ( "p"  , Expr::STATE_NONE ); const double pressureValue       = 2.0;
  const Expr::Tag temperatureTag   ( "T"  , Expr::STATE_NONE ); const double temperatureValue    = 3.0;
  const Expr::Tag densityTag       ( "rho", Expr::STATE_NONE ); const double densityValue        = 4.0;
  const Expr::Tag heatCapacityCpTag( "cp" , Expr::STATE_NONE ); const double heatCapacityCpValue = 4.0;
  const Expr::Tag totalEnthalpyTag ( "ht" , Expr::STATE_NONE ); const double totalEnthalpyValue  = 4.0;

  Expr::Tag xVelocityTag; const double xVelocityValue = ( velocitySign == POSITIVE ) ? 5.0 : -15.0;
  Expr::Tag yVelocityTag; const double yVelocityValue = ( velocitySign == POSITIVE ) ? 6.0 : -16.0;
  Expr::Tag zVelocityTag; const double zVelocityValue = ( velocitySign == POSITIVE ) ? 6.0 : -17.0;

  const double universalGasConstant = 10.0;
  std::vector<std::string> speciesNamesRepo = {"A", "B", "C", "D", "E", "F", "G", "H"}; // up to eight species for testing
  std::vector<double> molecularWeightsRepo = {10., 9., 8., 7., 6., 5., 4., 3.}; // up to eight species for testing

  Expr::TagList massFractionTags, enthalpyTags;
  std::vector<double> massFractionValues, enthalpyValues, molecularWeights;
  if( nspecies > 1 ){
    for( std::size_t i=0; i<nspecies; ++i ){
      massFractionTags  .push_back( Expr::Tag( "Y_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
      massFractionValues.push_back( (double) (i + 1) * 1.e-1 );

      enthalpyTags    .push_back( Expr::Tag( "h_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
      enthalpyValues  .push_back( (double) (i + 1) * 1.e3 );
      molecularWeights.push_back( molecularWeightsRepo[i] );
    }
  }

  // ------------------------------------------------------------------------------------------------

  Expr::matrix::OrdinalType ndims;
  bool doX = false;
  bool doY = false;
  bool doZ = false;
  int nx = 1;
  int ny = 1;
  int nz = 1;
  Expr::matrix::OrdinalType xMomIdx;
  Expr::matrix::OrdinalType yMomIdx;
  Expr::matrix::OrdinalType zMomIdx;

  switch( dimensions ){
    case X:   doX = true;                         ndims = 1; nx = 16;                   xMomIdx = 1;                           break;
    case Y:               doY = true;             ndims = 1;          ny = 16;                       yMomIdx = 1;              break;
    case Z:                           doZ = true; ndims = 1;                   nz = 16;                           zMomIdx = 1; break;
    case XY:  doX = true; doY = true;             ndims = 2; nx = 8;  ny = 8;           xMomIdx = 1; yMomIdx = 2;              break;
    case XZ:  doX = true;             doZ = true; ndims = 2; nx = 8;           nz = 8;  xMomIdx = 1;              zMomIdx = 2; break;
    case YZ:              doY = true; doZ = true; ndims = 2;          ny = 8;  nz = 8;               yMomIdx = 1; zMomIdx = 2; break;
    case XYZ: doX = true; doY = true; doZ = true; ndims = 3; nx = 4;  ny = 4;  nz = 4;  xMomIdx = 1; yMomIdx = 2; zMomIdx = 3; break;
    default:
      break;
  }

  if( doX ) xVelocityTag = Expr::Tag( "vx", Expr::STATE_NONE );
  if( doY ) yVelocityTag = Expr::Tag( "vy", Expr::STATE_NONE );
  if( doZ ) zVelocityTag = Expr::Tag( "vz", Expr::STATE_NONE );

  const double lx = 1.0;
  const double ly = 0.9;
  const double lz = 0.8;

  const double dx = lx / ((double) nx);
  const double dy = ly / ((double) ny);
  const double dz = lz / ((double) nz);

  const SpatialOps::Grid grid( SpatialOps::IntVec(nx, ny, nz),
                               SpatialOps::DoubleVec(lx, ly, lz) );
  Expr::ExprPatch patch(nx, ny, nz);

  // ------------------------------------------------------------------------------------------------

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  // ------------------------------------------------------------------------------------------------

  rootIDs.insert( factory.register_expression( new PlaceHolderType( xCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( yCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( zCoordTag ) ) );

  rootIDs.insert( factory.register_expression( new ConstantType( pressureTag      , pressureValue       ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( temperatureTag   , temperatureValue    ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( densityTag       , densityValue        ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( heatCapacityCpTag, heatCapacityCpValue ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( totalEnthalpyTag , totalEnthalpyValue  ) ) );

  if( doX ) rootIDs.insert( factory.register_expression( new ConstantType( xVelocityTag, xVelocityValue ) ) );
  if( doY ) rootIDs.insert( factory.register_expression( new ConstantType( yVelocityTag, yVelocityValue ) ) );
  if( doZ ) rootIDs.insert( factory.register_expression( new ConstantType( zVelocityTag, zVelocityValue ) ) );

  if( nspecies > 1){
    for( std::size_t i=0; i<nspecies; ++i ){
      rootIDs.insert( factory.register_expression( new ConstantType( massFractionTags[i], massFractionValues[i] ) ) );
      rootIDs.insert( factory.register_expression( new ConstantType( enthalpyTags[i]    , enthalpyValues[i]     ) ) );
    }
  }

  // ------------------------------------------------------------------------------------------------

  const Expr::matrix::OrdinalType xVelIdx = xMomIdx;
  const Expr::matrix::OrdinalType yVelIdx = yMomIdx;
  const Expr::matrix::OrdinalType zVelIdx = zMomIdx;

  const Expr::matrix::OrdinalType egyIdx  = ndims + 1;
  const Expr::matrix::OrdinalType tempIdx = ndims + 1;

  const Expr::matrix::OrdinalType rhoConsIdx = 0;
  const Expr::matrix::OrdinalType rhoPrimIdx = 0;

  std::vector<Expr::matrix::OrdinalType> speciesDensityIndices;
  std::vector<Expr::matrix::OrdinalType> massFracIndices;
  for( std::size_t i=0; i<nspecies - 1; ++i ){
    speciesDensityIndices.push_back( ndims + 2 + i );
    massFracIndices.push_back( ndims + 2 + i );
  }

  // ------------------------------------------------------------------------------------------------

  const std::string matrixName = "mat";
  const Expr::matrix::OrdinalType nrows = nspecies + ndims + 1 + extraRows;
  const Expr::TagList matrixTags = Expr::matrix::matrix_tags( matrixName, nrows );

  boost::shared_ptr<SparseMatrixT> zeroMatrix = boost::make_shared<SparseMatrixT>( "zero" );
  zeroMatrix->finalize();

  boost::shared_ptr<InviscidFluxJacT> inviscidFluxJacobian =
    boost::make_shared<InviscidFluxJacT>( "inviscid flux test",
                                          universalGasConstant,
                                          molecularWeights,
                                          densityTag,
                                          heatCapacityCpTag,
                                          totalEnthalpyTag,
                                          xCoordTag,
                                          yCoordTag,
                                          zCoordTag,
                                          xVelocityTag,
                                          yVelocityTag,
                                          zVelocityTag,
                                          temperatureTag,
                                          pressureTag,
                                          enthalpyTags,
                                          massFractionTags,
                                          rhoConsIdx,
                                          rhoPrimIdx,
                                          egyIdx,
                                          tempIdx,
                                          xMomIdx,
                                          yMomIdx,
                                          zMomIdx,
                                          xVelIdx,
                                          yVelIdx,
                                          zVelIdx,
                                          speciesDensityIndices,
                                          massFracIndices );

  if( operation == EMPLACE ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, inviscidFluxJacobian ) ) );
  }
  else if( operation == ADDIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix + inviscidFluxJacobian ) ) );
  }
  else if( operation == SUBIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix - inviscidFluxJacobian ) ) );
  }

  // ------------------------------------------------------------------------------------------------

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );

  grid.set_coord<SpatialOps::XDIR>( fml.field_ref<SVolFieldT>( xCoordTag ) );
  grid.set_coord<SpatialOps::YDIR>( fml.field_ref<SVolFieldT>( yCoordTag ) );
  grid.set_coord<SpatialOps::ZDIR>( fml.field_ref<SVolFieldT>( zCoordTag ) );

  tree.execute_tree();

  // ------------------------------------------------------------------------------------------------

  ScalarMatrixType ref_ax( nrows );
  ScalarMatrixType ref_ay( nrows );
  ScalarMatrixType ref_az( nrows );

  const double xCoeff = ( velocitySign == POSITIVE ) ? 1.0 / dx : -1.0 / dx;
  const double yCoeff = ( velocitySign == POSITIVE ) ? 1.0 / dy : -1.0 / dy;
  const double zCoeff = ( velocitySign == POSITIVE ) ? 1.0 / dz : -1.0 / dz;

  // make ax
  if( doX ){
    // density row
    ref_ax( rhoConsIdx, rhoPrimIdx ) = xCoeff * xVelocityValue;
    ref_ax( rhoConsIdx, xVelIdx )    = xCoeff * densityValue;

    // x-momentum row
    if( doX ){
      ref_ax( xMomIdx, rhoPrimIdx ) = xCoeff * ( xVelocityValue * xVelocityValue + pressureValue / densityValue );
      ref_ax( xMomIdx, xVelIdx )    = xCoeff * 2.0 * densityValue * xVelocityValue;
      ref_ax( xMomIdx, tempIdx )    = xCoeff * pressureValue / temperatureValue;
      for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
        ref_ax( xMomIdx, massFracIndices[i] ) = xCoeff * densityValue * universalGasConstant * temperatureValue * ( 1.0 / molecularWeights[i] - 1.0 / molecularWeights[nspecies - 1] );
    }

    // y-momentum row
    if( doY ){
      ref_ax( yMomIdx, rhoPrimIdx ) = xCoeff * xVelocityValue * yVelocityValue;
      ref_ax( yMomIdx, xVelIdx )    = xCoeff * densityValue * yVelocityValue;
      ref_ax( yMomIdx, yVelIdx )    = xCoeff * densityValue * xVelocityValue;
    }

    // z-momentum row
    if( doZ ){
      ref_ax( zMomIdx, rhoPrimIdx ) = xCoeff * xVelocityValue * zVelocityValue;
      ref_ax( zMomIdx, xVelIdx )    = xCoeff * densityValue * zVelocityValue;
      ref_ax( zMomIdx, zVelIdx )    = xCoeff * densityValue * xVelocityValue;
    }

    // energy row
    ref_ax( egyIdx, rhoPrimIdx ) = xCoeff * xVelocityValue * totalEnthalpyValue / densityValue;
    if( doX ) ref_ax( egyIdx, xVelIdx ) = xCoeff * densityValue * ( xVelocityValue * xVelocityValue + totalEnthalpyValue / densityValue );
    if( doY ) ref_ax( egyIdx, yVelIdx ) = xCoeff * densityValue * ( xVelocityValue * yVelocityValue );
    if( doZ ) ref_ax( egyIdx, zVelIdx ) = xCoeff * densityValue * ( xVelocityValue * zVelocityValue );
    ref_ax( egyIdx, tempIdx )    = xCoeff * densityValue * heatCapacityCpValue * xVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      ref_ax( egyIdx, massFracIndices[i] ) = xCoeff * densityValue * xVelocityValue * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );

    // species rows
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      const Expr::matrix::OrdinalType rhoyidx = speciesDensityIndices[i];
      const Expr::matrix::OrdinalType yidx = massFracIndices[i];

      ref_ax( rhoyidx, rhoPrimIdx ) = xCoeff * xVelocityValue * massFractionValues[i];
      ref_ax( rhoyidx, xVelIdx )    = xCoeff * densityValue * massFractionValues[i];
      ref_ax( rhoyidx, yidx    )    = xCoeff * densityValue * xVelocityValue;
    }
  }

  // make ay
  if( doY ){
    // density row
    ref_ay( rhoConsIdx, rhoPrimIdx ) = yCoeff * yVelocityValue;
    ref_ay( rhoConsIdx, yVelIdx )    = yCoeff * densityValue;

    // x-momentum row
    if( doX ){
      ref_ay( xMomIdx, rhoPrimIdx ) = yCoeff * yVelocityValue * xVelocityValue;
      ref_ay( xMomIdx, xVelIdx )    = yCoeff * densityValue * yVelocityValue;
      ref_ay( xMomIdx, yVelIdx )    = yCoeff * densityValue * xVelocityValue;
    }

    // y-momentum row
    if( doY ){
      ref_ay( yMomIdx, rhoPrimIdx ) = yCoeff * ( yVelocityValue * yVelocityValue + pressureValue / densityValue );
      ref_ay( yMomIdx, yVelIdx )    = yCoeff * 2.0 * densityValue * yVelocityValue;
      ref_ay( yMomIdx, tempIdx )    = yCoeff * pressureValue / temperatureValue;
      for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
        ref_ay( yMomIdx, massFracIndices[i] ) = yCoeff * densityValue * universalGasConstant * temperatureValue * ( 1.0 / molecularWeights[i] - 1.0 / molecularWeights[nspecies - 1] );
    }

    // z-momentum row
    if( doZ ){
      ref_ay( zMomIdx, rhoPrimIdx ) = yCoeff * yVelocityValue * zVelocityValue;
      ref_ay( zMomIdx, yVelIdx )    = yCoeff * densityValue * zVelocityValue;
      ref_ay( zMomIdx, zVelIdx )    = yCoeff * densityValue * yVelocityValue;
    }

    // energy row
    ref_ay( egyIdx, rhoPrimIdx ) = yCoeff * yVelocityValue * totalEnthalpyValue / densityValue;
    if( doX ) ref_ay( egyIdx, xVelIdx ) = yCoeff * densityValue * ( yVelocityValue * xVelocityValue );
    if( doY ) ref_ay( egyIdx, yVelIdx ) = yCoeff * densityValue * ( yVelocityValue * yVelocityValue + totalEnthalpyValue / densityValue );
    if( doZ ) ref_ay( egyIdx, zVelIdx ) = yCoeff * densityValue * ( yVelocityValue * zVelocityValue );
    ref_ay( egyIdx, tempIdx )    = yCoeff * densityValue * heatCapacityCpValue * yVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      ref_ay( egyIdx, massFracIndices[i] ) = yCoeff * densityValue * yVelocityValue * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );

    // species rows
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      const Expr::matrix::OrdinalType rhoyidx = speciesDensityIndices[i];
      const Expr::matrix::OrdinalType yidx = massFracIndices[i];

      ref_ay( rhoyidx, rhoPrimIdx ) = yCoeff * yVelocityValue * massFractionValues[i];
      ref_ay( rhoyidx, yVelIdx )    = yCoeff * densityValue * massFractionValues[i];
      ref_ay( rhoyidx, yidx    )    = yCoeff * densityValue * yVelocityValue;
    }
  }

  // make az
  if( doZ ){
    // density row
    ref_az( rhoConsIdx, rhoPrimIdx ) = zCoeff * zVelocityValue;
    ref_az( rhoConsIdx, zVelIdx )    = zCoeff * densityValue;

    // x-momentum row
    if( doX ){
      ref_az( xMomIdx, rhoPrimIdx ) = zCoeff * zVelocityValue * xVelocityValue;
      ref_az( xMomIdx, xVelIdx )    = zCoeff * densityValue * zVelocityValue;
      ref_az( xMomIdx, zVelIdx )    = zCoeff * densityValue * xVelocityValue;
    }

    // y-momentum row
    if( doY ){
      ref_az( yMomIdx, rhoPrimIdx ) = zCoeff * ( zVelocityValue * yVelocityValue );
      ref_az( yMomIdx, yVelIdx )    = zCoeff * densityValue * zVelocityValue;
      ref_az( yMomIdx, zVelIdx )    = zCoeff * densityValue * yVelocityValue;
    }

    // z-momentum row
    if( doZ ){
      ref_az( zMomIdx, rhoPrimIdx ) = zCoeff * ( zVelocityValue * zVelocityValue + pressureValue / densityValue );
      ref_az( zMomIdx, zVelIdx )    = zCoeff * 2.0 * densityValue * zVelocityValue;
      ref_az( zMomIdx, tempIdx )    = zCoeff * pressureValue / temperatureValue;
      for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
        ref_az( zMomIdx, massFracIndices[i] ) = zCoeff * densityValue * universalGasConstant * temperatureValue * ( 1.0 / molecularWeights[i] - 1.0 / molecularWeights[nspecies - 1] );
    }

    // energy row
    ref_az( egyIdx, rhoPrimIdx ) = zCoeff * zVelocityValue * totalEnthalpyValue / densityValue;
    if( doX ) ref_az( egyIdx, xVelIdx ) = zCoeff * densityValue * ( zVelocityValue * xVelocityValue );
    if( doY ) ref_az( egyIdx, yVelIdx ) = zCoeff * densityValue * ( zVelocityValue * yVelocityValue );
    if( doZ ) ref_az( egyIdx, zVelIdx ) = zCoeff * densityValue * ( zVelocityValue * zVelocityValue + totalEnthalpyValue / densityValue );
    ref_az( egyIdx, tempIdx )    = zCoeff * densityValue * heatCapacityCpValue * zVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      ref_az( egyIdx, massFracIndices[i] ) = zCoeff * densityValue * zVelocityValue * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );

    // species rows
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      const Expr::matrix::OrdinalType rhoyidx = speciesDensityIndices[i];
      const Expr::matrix::OrdinalType yidx = massFracIndices[i];

      ref_az( rhoyidx, rhoPrimIdx ) = zCoeff * zVelocityValue * massFractionValues[i];
      ref_az( rhoyidx, zVelIdx )    = zCoeff * densityValue * massFractionValues[i];
      ref_az( rhoyidx, yidx    )    = zCoeff * densityValue * zVelocityValue;
    }
  }

  auto reference = ref_ax + ref_ay + ref_az;
  if( operation == SUBIN ){
    reference.multiply_by_scale(-1.0);
  }

  // ------------------------------------------------------------------------------------------------

  TestHelper tester( false );

  for( const auto& tag : matrixTags ){
    fml.field_ref<SVolFieldT>( tag ).add_device( CPU_INDEX );
  }
  for( Expr::matrix::OrdinalType row=0; row<nrows; ++row ){
    for( Expr::matrix::OrdinalType col=0; col<nrows; ++col ){
      tester( field_equal( reference(row, col), fml.field_ref<SVolFieldT>( matrixTags[row * nrows + col] ), 1.e-8),
              boost::lexical_cast<std::string>( row ) + ", " + boost::lexical_cast<std::string>( col ) );
    }
  }
  return tester.ok();
}


int main()
{
  TestHelper tester( true );

  tester( run_inviscid_flux_test( X  , EMPLACE, 1, 0, POSITIVE ), "X   1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 1, 0, POSITIVE ), "Y   1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 1, 0, POSITIVE ), "Z   1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 1, 0, POSITIVE ), "XY  1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 1, 0, POSITIVE ), "XZ  1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 1, 0, POSITIVE ), "YZ  1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 1, 0, POSITIVE ), "XYZ 1 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 1, 2, POSITIVE ), "X   1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 1, 2, POSITIVE ), "Y   1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 1, 2, POSITIVE ), "Z   1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 1, 2, POSITIVE ), "XY  1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 1, 2, POSITIVE ), "XZ  1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 1, 2, POSITIVE ), "YZ  1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 1, 2, POSITIVE ), "XYZ 1 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 3, 0, POSITIVE ), "X   3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 3, 0, POSITIVE ), "Y   3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 3, 0, POSITIVE ), "Z   3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 3, 0, POSITIVE ), "XY  3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 3, 0, POSITIVE ), "XZ  3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 3, 0, POSITIVE ), "YZ  3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 3, 0, POSITIVE ), "XYZ 3 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 3, 2, POSITIVE ), "X   3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 3, 2, POSITIVE ), "Y   3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 3, 2, POSITIVE ), "Z   3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 3, 2, POSITIVE ), "XY  3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 3, 2, POSITIVE ), "XZ  3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 3, 2, POSITIVE ), "YZ  3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 3, 2, POSITIVE ), "XYZ 3 species 2 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 6, 0, POSITIVE ), "X   6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 6, 0, POSITIVE ), "Y   6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 6, 0, POSITIVE ), "Z   6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 6, 0, POSITIVE ), "XY  6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 6, 0, POSITIVE ), "XZ  6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 6, 0, POSITIVE ), "YZ  6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 6, 0, POSITIVE ), "XYZ 6 species 0 extrarows emplace velocity > 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 1, 0, NEGATIVE ), "X   1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 1, 0, NEGATIVE ), "Y   1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 1, 0, NEGATIVE ), "Z   1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 1, 0, NEGATIVE ), "XY  1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 1, 0, NEGATIVE ), "XZ  1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 1, 0, NEGATIVE ), "YZ  1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 1, 0, NEGATIVE ), "XYZ 1 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 1, 2, NEGATIVE ), "X   1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 1, 2, NEGATIVE ), "Y   1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 1, 2, NEGATIVE ), "Z   1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 1, 2, NEGATIVE ), "XY  1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 1, 2, NEGATIVE ), "XZ  1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 1, 2, NEGATIVE ), "YZ  1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 1, 2, NEGATIVE ), "XYZ 1 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 3, 0, NEGATIVE ), "X   3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 3, 0, NEGATIVE ), "Y   3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 3, 0, NEGATIVE ), "Z   3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 3, 0, NEGATIVE ), "XY  3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 3, 0, NEGATIVE ), "XZ  3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 3, 0, NEGATIVE ), "YZ  3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 3, 0, NEGATIVE ), "XYZ 3 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 3, 2, NEGATIVE ), "X   3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 3, 2, NEGATIVE ), "Y   3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 3, 2, NEGATIVE ), "Z   3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 3, 2, NEGATIVE ), "XY  3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 3, 2, NEGATIVE ), "XZ  3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 3, 2, NEGATIVE ), "YZ  3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 3, 2, NEGATIVE ), "XYZ 3 species 2 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( X  , EMPLACE, 6, 0, NEGATIVE ), "X   6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Y  , EMPLACE, 6, 0, NEGATIVE ), "Y   6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( Z  , EMPLACE, 6, 0, NEGATIVE ), "Z   6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XY , EMPLACE, 6, 0, NEGATIVE ), "XY  6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XZ , EMPLACE, 6, 0, NEGATIVE ), "XZ  6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( YZ , EMPLACE, 6, 0, NEGATIVE ), "YZ  6 species 0 extrarows emplace velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, EMPLACE, 6, 0, NEGATIVE ), "XYZ 6 species 0 extrarows emplace velocity < 0" );

  tester( run_inviscid_flux_test( X  , ADDIN, 1, 0, POSITIVE ), "X   1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 1, 0, POSITIVE ), "Y   1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 1, 0, POSITIVE ), "Z   1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 1, 0, POSITIVE ), "XY  1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 1, 0, POSITIVE ), "XZ  1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 1, 0, POSITIVE ), "YZ  1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 1, 0, POSITIVE ), "XYZ 1 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 1, 2, POSITIVE ), "X   1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 1, 2, POSITIVE ), "Y   1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 1, 2, POSITIVE ), "Z   1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 1, 2, POSITIVE ), "XY  1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 1, 2, POSITIVE ), "XZ  1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 1, 2, POSITIVE ), "YZ  1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 1, 2, POSITIVE ), "XYZ 1 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 3, 0, POSITIVE ), "X   3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 3, 0, POSITIVE ), "Y   3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 3, 0, POSITIVE ), "Z   3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 3, 0, POSITIVE ), "XY  3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 3, 0, POSITIVE ), "XZ  3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 3, 0, POSITIVE ), "YZ  3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 3, 0, POSITIVE ), "XYZ 3 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 3, 2, POSITIVE ), "X   3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 3, 2, POSITIVE ), "Y   3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 3, 2, POSITIVE ), "Z   3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 3, 2, POSITIVE ), "XY  3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 3, 2, POSITIVE ), "XZ  3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 3, 2, POSITIVE ), "YZ  3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 3, 2, POSITIVE ), "XYZ 3 species 2 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 6, 0, POSITIVE ), "X   6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 6, 0, POSITIVE ), "Y   6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 6, 0, POSITIVE ), "Z   6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 6, 0, POSITIVE ), "XY  6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 6, 0, POSITIVE ), "XZ  6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 6, 0, POSITIVE ), "YZ  6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 6, 0, POSITIVE ), "XYZ 6 species 0 extrarows addin velocity > 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 1, 0, NEGATIVE ), "X   1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 1, 0, NEGATIVE ), "Y   1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 1, 0, NEGATIVE ), "Z   1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 1, 0, NEGATIVE ), "XY  1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 1, 0, NEGATIVE ), "XZ  1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 1, 0, NEGATIVE ), "YZ  1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 1, 0, NEGATIVE ), "XYZ 1 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 1, 2, NEGATIVE ), "X   1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 1, 2, NEGATIVE ), "Y   1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 1, 2, NEGATIVE ), "Z   1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 1, 2, NEGATIVE ), "XY  1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 1, 2, NEGATIVE ), "XZ  1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 1, 2, NEGATIVE ), "YZ  1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 1, 2, NEGATIVE ), "XYZ 1 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 3, 0, NEGATIVE ), "X   3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 3, 0, NEGATIVE ), "Y   3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 3, 0, NEGATIVE ), "Z   3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 3, 0, NEGATIVE ), "XY  3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 3, 0, NEGATIVE ), "XZ  3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 3, 0, NEGATIVE ), "YZ  3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 3, 0, NEGATIVE ), "XYZ 3 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 3, 2, NEGATIVE ), "X   3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 3, 2, NEGATIVE ), "Y   3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 3, 2, NEGATIVE ), "Z   3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 3, 2, NEGATIVE ), "XY  3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 3, 2, NEGATIVE ), "XZ  3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 3, 2, NEGATIVE ), "YZ  3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 3, 2, NEGATIVE ), "XYZ 3 species 2 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( X  , ADDIN, 6, 0, NEGATIVE ), "X   6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , ADDIN, 6, 0, NEGATIVE ), "Y   6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , ADDIN, 6, 0, NEGATIVE ), "Z   6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XY , ADDIN, 6, 0, NEGATIVE ), "XY  6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , ADDIN, 6, 0, NEGATIVE ), "XZ  6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , ADDIN, 6, 0, NEGATIVE ), "YZ  6 species 0 extrarows addin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, ADDIN, 6, 0, NEGATIVE ), "XYZ 6 species 0 extrarows addin velocity < 0" );

  tester( run_inviscid_flux_test( X  , SUBIN, 1, 0, POSITIVE ), "X   1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 1, 0, POSITIVE ), "Y   1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 1, 0, POSITIVE ), "Z   1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 1, 0, POSITIVE ), "XY  1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 1, 0, POSITIVE ), "XZ  1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 1, 0, POSITIVE ), "YZ  1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 1, 0, POSITIVE ), "XYZ 1 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 1, 2, POSITIVE ), "X   1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 1, 2, POSITIVE ), "Y   1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 1, 2, POSITIVE ), "Z   1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 1, 2, POSITIVE ), "XY  1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 1, 2, POSITIVE ), "XZ  1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 1, 2, POSITIVE ), "YZ  1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 1, 2, POSITIVE ), "XYZ 1 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 3, 0, POSITIVE ), "X   3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 3, 0, POSITIVE ), "Y   3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 3, 0, POSITIVE ), "Z   3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 3, 0, POSITIVE ), "XY  3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 3, 0, POSITIVE ), "XZ  3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 3, 0, POSITIVE ), "YZ  3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 3, 0, POSITIVE ), "XYZ 3 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 3, 2, POSITIVE ), "X   3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 3, 2, POSITIVE ), "Y   3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 3, 2, POSITIVE ), "Z   3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 3, 2, POSITIVE ), "XY  3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 3, 2, POSITIVE ), "XZ  3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 3, 2, POSITIVE ), "YZ  3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 3, 2, POSITIVE ), "XYZ 3 species 2 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 6, 0, POSITIVE ), "X   6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 6, 0, POSITIVE ), "Y   6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 6, 0, POSITIVE ), "Z   6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 6, 0, POSITIVE ), "XY  6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 6, 0, POSITIVE ), "XZ  6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 6, 0, POSITIVE ), "YZ  6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 6, 0, POSITIVE ), "XYZ 6 species 0 extrarows subin velocity > 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 1, 0, NEGATIVE ), "X   1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 1, 0, NEGATIVE ), "Y   1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 1, 0, NEGATIVE ), "Z   1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 1, 0, NEGATIVE ), "XY  1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 1, 0, NEGATIVE ), "XZ  1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 1, 0, NEGATIVE ), "YZ  1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 1, 0, NEGATIVE ), "XYZ 1 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 1, 2, NEGATIVE ), "X   1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 1, 2, NEGATIVE ), "Y   1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 1, 2, NEGATIVE ), "Z   1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 1, 2, NEGATIVE ), "XY  1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 1, 2, NEGATIVE ), "XZ  1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 1, 2, NEGATIVE ), "YZ  1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 1, 2, NEGATIVE ), "XYZ 1 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 3, 0, NEGATIVE ), "X   3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 3, 0, NEGATIVE ), "Y   3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 3, 0, NEGATIVE ), "Z   3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 3, 0, NEGATIVE ), "XY  3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 3, 0, NEGATIVE ), "XZ  3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 3, 0, NEGATIVE ), "YZ  3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 3, 0, NEGATIVE ), "XYZ 3 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 3, 2, NEGATIVE ), "X   3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 3, 2, NEGATIVE ), "Y   3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 3, 2, NEGATIVE ), "Z   3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 3, 2, NEGATIVE ), "XY  3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 3, 2, NEGATIVE ), "XZ  3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 3, 2, NEGATIVE ), "YZ  3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 3, 2, NEGATIVE ), "XYZ 3 species 2 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( X  , SUBIN, 6, 0, NEGATIVE ), "X   6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Y  , SUBIN, 6, 0, NEGATIVE ), "Y   6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( Z  , SUBIN, 6, 0, NEGATIVE ), "Z   6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XY , SUBIN, 6, 0, NEGATIVE ), "XY  6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XZ , SUBIN, 6, 0, NEGATIVE ), "XZ  6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( YZ , SUBIN, 6, 0, NEGATIVE ), "YZ  6 species 0 extrarows subin velocity < 0" );
  tester( run_inviscid_flux_test( XYZ, SUBIN, 6, 0, NEGATIVE ), "XYZ 6 species 0 extrarows subin velocity < 0" );

  if( tester.ok() ){ std::cout << "\nPASS\n"; return 0; }
  else             { std::cout << "\nFAIL\n"; return -1;}
}

