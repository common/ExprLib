/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>
#include <algorithm>

#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>

#include <expression/ExprPatch.h>
#include <expression/matrix-assembly/EigenvalueExpression.h>
#include <expression/matrix-assembly/SparseMatrix.h>

#include "Matrix.h"

#include "../TestHelper.h"

using CellFieldType = SpatialOps::SVolField;
using EigenvalueExprT = Expr::EigenvalueExpression<CellFieldType>::Builder;

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >();

bool run_test( EigenvalueExprT::Request request )
{
  Expr::ExprPatch patch(1);
  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  const double a = 5.0;
  const double b = 2.0;
  const double c = -1.0;
  const double d = -4.0;
  const double s = 2.0;
  const double t = 3.0;

  /*
   * Test matrix [8x8]:
   * [ a,  0,  0,  0,  0,  0,  0,  0],
   * [ 0,  b,  0,  0,  0,  0,  0,  0],
   * [ 0,  0,  c,  0,  0,  0,  0,  0],
   * [ 0,  0,  0,  d,  0,  0,  0,  0],
   * [ 0,  0,  0,  0,  s,  s,  0,  0],
   * [ 0,  0,  0,  0, -s,  s,  0,  0],
   * [ 0,  0,  0,  0,  0,  0,  t,  t],
   * [ 0,  0,  0,  0,  0,  0, -t,  t]
   *
   * Eigenvalues: {a, b, c, d, -sj, +sj, -tj, +tj}
   *
   * With a=5, b=2, c=-1, d=-4, s=2, t=3 we have the following:
   *
   * All eigenvalues:          {5, 2, -1, -4, -2j, +2j, -3j, +3j}
   * Most negative real part:  -4 (d)
   * Least negative real part: -1 (c)
   * Most positive real part:   5 (a)
   * Least positive real part:  2 (b)
   * Condition number:          5 (a/std::abs(c))
   * Spectral radius:           5 (a)
   * Smallest magnitude:        1 (std::abs(c))
   * Largest imaginary part:    3 (t)
   * Smallest imaginary part:   0
   */

  const int nrows = 8;

  ASSEMBLER( Expr::matrix::SparseMatrix, CellFieldType, A )
  A->element<double>(0,0) = a;
  A->element<double>(1,1) = b;
  A->element<double>(2,2) = c;
  A->element<double>(3,3) = d;
  A->element<double>(4,4) = s;
  A->element<double>(4,5) = s;
  A->element<double>(5,4) = -s;
  A->element<double>(5,5) = s;
  A->element<double>(6,6) = t;
  A->element<double>(6,7) = t;
  A->element<double>(7,6) = -t;
  A->element<double>(7,7) = t;
  A->finalize();

  Expr::TagList realTags = { Expr::Tag( "real0", Expr::STATE_NONE ),
                             Expr::Tag( "real1", Expr::STATE_NONE ),
                             Expr::Tag( "real2", Expr::STATE_NONE ),
                             Expr::Tag( "real3", Expr::STATE_NONE ),
                             Expr::Tag( "real4", Expr::STATE_NONE ),
                             Expr::Tag( "real5", Expr::STATE_NONE ),
                             Expr::Tag( "real6", Expr::STATE_NONE ),
                             Expr::Tag( "real7", Expr::STATE_NONE ) };
  Expr::TagList imagTags = { Expr::Tag( "imag0", Expr::STATE_NONE ),
                             Expr::Tag( "imag1", Expr::STATE_NONE ),
                             Expr::Tag( "imag2", Expr::STATE_NONE ),
                             Expr::Tag( "imag3", Expr::STATE_NONE ),
                             Expr::Tag( "imag4", Expr::STATE_NONE ),
                             Expr::Tag( "imag5", Expr::STATE_NONE ),
                             Expr::Tag( "imag6", Expr::STATE_NONE ),
                             Expr::Tag( "imag7", Expr::STATE_NONE ) };
  Expr::Tag measureTag( "measure", Expr::STATE_NONE );

  if( request == EigenvalueExprT::ALL ){
    rootIDs.insert( factory.register_expression( new EigenvalueExprT( realTags, imagTags, A, nrows ) ) );
  }
  else{
    rootIDs.insert( factory.register_expression( new EigenvalueExprT( measureTag, A, nrows, request ) ) );
  }

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );
  tree.execute_tree();

  TestHelper tester( true );

  if( request == EigenvalueExprT::ALL ){

    for( const auto& r : realTags ){
      fml.field_ref<CellFieldType>( r ).add_device( CPU_INDEX );
    }
    for( const auto& i : imagTags ){
      fml.field_ref<CellFieldType>( i ).add_device( CPU_INDEX );
    }

    std::vector<std::pair<double,double>> observed;
    for( int i=0; i<nrows; ++i ){
      observed.push_back( std::make_pair( nebo_max( fml.field_ref<CellFieldType>( realTags[i] ) ),
                                          nebo_max( fml.field_ref<CellFieldType>( imagTags[i] ) ) ) );
    }

    std::vector<std::pair<double,double>> answers;
    answers.push_back( std::make_pair( a, 0.0 ) );
    answers.push_back( std::make_pair( b, 0.0 ) );
    answers.push_back( std::make_pair( c, 0.0 ) );
    answers.push_back( std::make_pair( d, 0.0 ) );
    answers.push_back( std::make_pair( s, +s ) );
    answers.push_back( std::make_pair( s, -s ) );
    answers.push_back( std::make_pair( t, +t ) );
    answers.push_back( std::make_pair( t, -t ) );

    for( const auto& a : answers ){
      const std::string aStr = boost::lexical_cast<std::string>( a.first ) + " + " + boost::lexical_cast<std::string>( a.second ) + "j";
      tester( std::find( observed.begin(), observed.end(), a ) != observed.end(), "find " + aStr );
    }
  }
  else{
    fml.field_ref<CellFieldType>( measureTag ).add_device( CPU_INDEX );

    switch( request ){
      case EigenvalueExprT::MOST_NEG_REAL_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == d, "most negative real part" );
        break;
      case EigenvalueExprT::LEAST_NEG_REAL_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == c, "least negative real part" );
        break;
      case EigenvalueExprT::MOST_POS_REAL_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == a, "most positive real part" );
        break;
      case EigenvalueExprT::LEAST_POS_REAL_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == b, "least positive real part" );
        break;
      case EigenvalueExprT::LARGEST_IMAG_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == t, "largest imaginary part" );
        break;
      case EigenvalueExprT::SMALLEST_IMAG_PART:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == 0.0, "smallest imaginary part" );
        break;
      case EigenvalueExprT::SPECTRAL_RADIUS:
      case EigenvalueExprT::LARGEST_MAG:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == a, "spectral radius / largest magnitude" );
        break;
      case EigenvalueExprT::SMALLEST_MAG:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == std::abs( c ), "smallest magnitude" );
        break;
      case EigenvalueExprT::CONDITION_NUMBER:
        tester( nebo_max( fml.field_ref<CellFieldType>( measureTag ) ) == a / std::abs( c ), "condition number" );
        break;
      case EigenvalueExprT::ALL:
        break;
      default:
        break;
    }
  }

  return tester.ok();
}


int main()
{
  TestHelper tester( true );
  tester( run_test( EigenvalueExprT::ALL ) );
  tester( run_test( EigenvalueExprT::MOST_NEG_REAL_PART ) );
  tester( run_test( EigenvalueExprT::LEAST_NEG_REAL_PART ) );
  tester( run_test( EigenvalueExprT::MOST_POS_REAL_PART ) );
  tester( run_test( EigenvalueExprT::LEAST_POS_REAL_PART ) );
  tester( run_test( EigenvalueExprT::LARGEST_IMAG_PART ) );
  tester( run_test( EigenvalueExprT::SMALLEST_IMAG_PART ) );
  tester( run_test( EigenvalueExprT::SPECTRAL_RADIUS ) );
  tester( run_test( EigenvalueExprT::LARGEST_MAG ) );
  tester( run_test( EigenvalueExprT::SMALLEST_MAG ) );
  tester( run_test( EigenvalueExprT::CONDITION_NUMBER ) );

  if( tester.ok() ){ std::cout << "\nPASS\n"; return 0; }
  else             { std::cout << "\nFAIL\n"; return -1;}
}

