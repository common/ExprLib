/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>

#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>

#include <expression/ExprPatch.h>
#include <expression/Functions.h>
#include <expression/matrix-assembly/PermutationSubMatrix.h>
#include <expression/matrix-assembly/MatrixExpression.h>

#include "Matrix.h"

#include "../TestHelper.h"


using CellFieldType = SpatialOps::SVolField;
using PermutationMatType = Expr::matrix::PermutationSubMatrix<CellFieldType>;
using PermutationPtrType = boost::shared_ptr<PermutationMatType>;
using TestMatrixType = densematrix_for_test::Matrix<double>;


bool run_test( boost::shared_ptr<Expr::matrix::AssemblerBase<CellFieldType> > assembler,
               const TestMatrixType answer )
{
  Expr::ExprPatch patch(1);
  Expr::FieldManagerList& fml = patch.field_manager_list();

  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  using MatrixAssemblerExprType = Expr::matrix::MatrixExpression<CellFieldType>::Builder;

  const Expr::matrix::OrdinalType nrows = answer.nrows();

  Expr::TagList rowTags, colTags;
  for( int i=0; i<nrows; ++i ){
    rowTags.push_back( Expr::Tag( "row" + boost::lexical_cast<std::string>( i ), Expr::STATE_NONE ) );
    colTags.push_back( Expr::Tag( "col" + boost::lexical_cast<std::string>( i ), Expr::STATE_NONE ) );
  }
  const Expr::TagList matrixTags = Expr::matrix::matrix_tags( rowTags, "-", colTags );

  Expr::ExpressionID matID = factory.register_expression( new MatrixAssemblerExprType( matrixTags, assembler ) );
  rootIDs.insert( matID );

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  assembler->trigger_sensitivities( tree );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );
  tree.execute_tree();


  TestHelper tester( false );
  for( Expr::matrix::OrdinalType flatIdx=0; flatIdx<nrows*nrows; ++flatIdx ){
    tester( field_equal( answer(flatIdx), fml.field_ref<CellFieldType>( matrixTags[flatIdx] ) ) );
  }

  if( !tester.ok() ){
    Expr::matrix::print_matrix<CellFieldType>( fml, matrixTags );
    answer.print( std::cout );
  }

  return tester.ok();
}




int main()
{
  TestHelper tester( true );



  {
    PermutationPtrType pIdenPtr = boost::make_shared<PermutationMatType>(); // built with the set_identity_row() method only
    PermutationPtrType pRmapPtr = boost::make_shared<PermutationMatType>(); // built with the set_row_map() method

    const Expr::matrix::OrdinalType nrows = 7;
    std::map<Expr::matrix::OrdinalType,Expr::matrix::OrdinalType> rMap;
    TestMatrixType answer( nrows );

    for( auto i = 2; i < nrows-2; ++i ){
      pIdenPtr->set_identity_row( i );
      rMap[i] = i;
      answer(i,i) = 1;
    }
    pRmapPtr->set_row_map( rMap );

    pIdenPtr->finalize();
    pRmapPtr->finalize();

    tester( run_test( pIdenPtr, answer ), pIdenPtr->assembler_name() );
    tester( run_test( pRmapPtr, answer ), pRmapPtr->assembler_name() );
  }

  {
    PermutationPtrType pSwapPtr1 = boost::make_shared<PermutationMatType>(); // built with the swap_rows() and set_identity_row() methods
    PermutationPtrType pSwapPtr2 = boost::make_shared<PermutationMatType>(); // built with the set_row_map() method
    PermutationPtrType pRmapPtr1 = boost::make_shared<PermutationMatType>(); // built with the swap_rows() and set_identity_row() methods
    PermutationPtrType pRmapPtr2 = boost::make_shared<PermutationMatType>(); // built with the set_row_map() method

    const Expr::matrix::OrdinalType nrows = 7;
    std::map<Expr::matrix::OrdinalType,Expr::matrix::OrdinalType> rMap1, rMap2;
    TestMatrixType answer( nrows );

    answer(0,2) = 1;
    answer(1,3) = 1;
    answer(2,0) = 1;
    answer(3,1) = 1;
    answer(4,4) = 1;

    pSwapPtr1->swap_rows( 0, 2 );
    pSwapPtr1->swap_rows( 1, 3 );
    pSwapPtr1->set_identity_row( 4 );

    pSwapPtr2->set_identity_row( 4 );
    pSwapPtr2->swap_rows( 1, 3 );
    pSwapPtr2->swap_rows( 0, 2 );

    rMap1[0] = 2;
    rMap1[1] = 3;
    rMap1[2] = 0;
    rMap1[3] = 1;
    rMap1[4] = 4;
    pRmapPtr1->set_row_map( rMap1 );

    rMap2[4] = 4;
    rMap2[3] = 1;
    rMap2[2] = 0;
    rMap2[1] = 3;
    rMap2[0] = 2;
    for( const auto& r : rMap1 ){
      pRmapPtr2->set_row_col( r.first, r.second );
    }

    pSwapPtr1->finalize();
    pSwapPtr2->finalize();
    pRmapPtr1->finalize();
    pRmapPtr2->finalize();

    tester( run_test( pSwapPtr1, answer ), pSwapPtr1->assembler_name() + ", swap ordering 1" );
    tester( run_test( pSwapPtr2, answer ), pSwapPtr2->assembler_name() + ", swap ordering 2" );
    tester( run_test( pRmapPtr1, answer ), pRmapPtr1->assembler_name() + ", row map at once" );
    tester( run_test( pRmapPtr2, answer ), pRmapPtr2->assembler_name() + ", row map by element" );
  }

  if( tester.ok() ){ std::cout << "PASS" << '\n'; return 0; }
  else             { std::cout << "FAIL" << '\n'; return -1;}
}



