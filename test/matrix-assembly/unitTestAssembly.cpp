/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>

#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>

#include <expression/ExprPatch.h>
#include <expression/Functions.h>
#include <expression/matrix-assembly/ScaledIdentityMatrix.h>
#include <expression/matrix-assembly/DiagonalMatrix.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/PermutationSubMatrix.h>
#include <expression/matrix-assembly/RowPlaceHolder.h>
#include <expression/matrix-assembly/Compounds.h>
#include <expression/matrix-assembly/MatrixExpression.h>

#include "Matrix.h"

#include "../TestHelper.h"


using CellFieldType = SpatialOps::SVolField;
using SVFType = SpatialOps::SingleValueField;

using DenseMatType = Expr::matrix::DenseSubMatrix<CellFieldType>;
using SparseMatType = Expr::matrix::SparseMatrix<CellFieldType>;
using PermutationMatType = Expr::matrix::PermutationSubMatrix<CellFieldType>;
using DiagonalMatType = Expr::matrix::DiagonalMatrix<CellFieldType>;
using ScaledIdentityMatType = Expr::matrix::ScaledIdentityMatrix<CellFieldType>;

using DensePtrType = boost::shared_ptr<Expr::matrix::DenseSubMatrix<CellFieldType> >;
using SparsePtrType = boost::shared_ptr<Expr::matrix::SparseMatrix<CellFieldType> >;
using PermutationPtrType = boost::shared_ptr<Expr::matrix::PermutationSubMatrix<CellFieldType> >;
using DiagonalPtrType = boost::shared_ptr<Expr::matrix::DiagonalMatrix<CellFieldType> >;
using ScaledIdentityPtrType = boost::shared_ptr<Expr::matrix::ScaledIdentityMatrix<CellFieldType> >;

using TestMatrixType = densematrix_for_test::Matrix<double>;

using Expr::matrix::sensitivity;


struct OperationTestSetup
{
  const Expr::Tag aTag = Expr::Tag( "a", Expr::STATE_NONE ); // field, constant, n/a
  const Expr::Tag bTag = Expr::Tag( "b", Expr::STATE_NONE ); // svf  , constant, n/a
  const Expr::Tag cTag = Expr::Tag( "c", Expr::STATE_NONE ); // field, linear  , arg:field
  const Expr::Tag eTag = Expr::Tag( "e", Expr::STATE_NONE ); // svf  , linear  , arg:svf

  double aValue;
  double bValue;
  double cSlope;
  double cIntcp;
  double cValue;
  double eSlope;
  double eIntcp;
  double eValue;

  DensePtrType M;
  SparsePtrType S;
  DiagonalPtrType D;
  PermutationPtrType P;
  ScaledIdentityPtrType Ymlt;
  ScaledIdentityPtrType Ydiv;
  ScaledIdentityPtrType I;

  const Expr::matrix::OrdinalType nrows = 6;
  TestMatrixType m;
  TestMatrixType s;
  TestMatrixType d;
  TestMatrixType p;
  TestMatrixType ymlt;
  TestMatrixType ydiv;
  TestMatrixType i;

  OperationTestSetup()
  : M( boost::make_shared<DenseMatType>( "M" ) ),
    S( boost::make_shared<SparseMatType>( "S" ) ),
    D( boost::make_shared<DiagonalMatType>( "D" ) ),
    P( boost::make_shared<PermutationMatType>( "P" ) ),
    Ymlt( boost::make_shared<ScaledIdentityMatType>( "Y*" ) ),
    Ydiv( boost::make_shared<ScaledIdentityMatType>( "Y/" ) ),
    I( boost::make_shared<ScaledIdentityMatType>( "I" ) ),
    m( nrows ),
    s( nrows ),
    d( nrows ),
    p( nrows ),
    ymlt( nrows ),
    ydiv( nrows ),
    i( nrows )
  {
    aValue = 1.0;
    bValue = 2.0;
    cSlope = 1.0;
    cIntcp = 6.0;
    eSlope = 3.0;
    eIntcp = 1.0;
    cValue = cSlope * aValue + cIntcp;
    eValue = eSlope * bValue + eIntcp;

    M->element<CellFieldType>(1,2) = aTag;
    M->element<CellFieldType>(4,3) = cTag;
    M->element<CellFieldType>(3,3) = sensitivity( cTag, aTag );
    M->element<SVFType>(1,1) = bTag;
    M->element<double>(4,2) = eValue;
    M->element<SVFType>(3,1) = sensitivity( eTag, bTag );
    M->finalize();
    m(1,2) = aValue;
    m(4,3) = cValue;
    m(3,3) = cSlope;
    m(1,1) = bValue;
    m(4,2) = eValue;
    m(3,1) = eSlope;

    S->element<CellFieldType>(4,5) = aTag;
    S->element<CellFieldType>(0,2) = cTag;
    S->element<SVFType>(1,3) = bTag;
    S->element<double>(3,4) = eValue;
    S->element<CellFieldType>(1,1) = sensitivity( cTag, aTag );
    S->element<SVFType>(1,2) = sensitivity( eTag, bTag );
    S->finalize();
    s(4,5) = aValue;
    s(0,2) = cValue;
    s(1,3) = bValue;
    s(3,4) = eValue;
    s(1,1) = cSlope;
    s(1,2) = eSlope;

    D->element<CellFieldType>(0) = aTag;
    D->element<CellFieldType>(1) = cTag;
    D->element<SVFType>(2) = bTag;
    D->element<double>(3) = eValue;
    D->element<CellFieldType>(4) = sensitivity( cTag, aTag );
    D->element<SVFType>(5) = sensitivity( eTag, bTag );
    D->finalize();
    d(0,0) = aValue;
    d(1,1) = cValue;
    d(2,2) = bValue;
    d(3,3) = eValue;
    d(4,4) = cSlope;
    d(5,5) = eSlope;

    P->swap_rows( 1, 2 );
    P->swap_rows( 3, 5 );
    P->set_identity_row( 4 );
    P->finalize();
    p(1,2) = 1; p(2,1) = 1;
    p(4,4) = 1;
    p(3,5) = 1; p(5,3) = 1;

    Ymlt->scale<CellFieldType>() = aTag;
    Ymlt->finalize();
    for( Expr::matrix::OrdinalType diagIdx = 0; diagIdx < nrows; ++diagIdx ){
      ymlt(diagIdx,diagIdx) = aValue;
    }

    Ydiv->scale<double>() = bValue;
    Ydiv->set_type( Expr::matrix::ScaledIdentityMatrix<CellFieldType>::SCALEDIVIDE );
    Ydiv->finalize();
    for( Expr::matrix::OrdinalType diagIdx = 0; diagIdx < nrows; ++diagIdx ){
      ydiv(diagIdx,diagIdx) = 1.0 / bValue;
    }

    I->finalize();
    for( Expr::matrix::OrdinalType diagIdx = 0; diagIdx < nrows; ++diagIdx ){
      i(diagIdx,diagIdx) = 1;
    }
  }
};



bool run_test( boost::shared_ptr<Expr::matrix::AssemblerBase<CellFieldType> > assembler,
               const TestMatrixType answer,
               const OperationTestSetup setup )
{
  Expr::ExprPatch patch(1);
  Expr::FieldManagerList& fml = patch.field_manager_list();

  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  using ConstantType = Expr::ConstantExpr<CellFieldType>::Builder;
  using LinearType = Expr::LinearFunction<CellFieldType>::Builder;
  using ConstantSVFType = Expr::ConstantExpr<SpatialOps::SingleValueField>::Builder;
  using LinearSVFType = Expr::LinearFunction<SpatialOps::SingleValueField>::Builder;
  using MatrixAssemblerExprType = Expr::matrix::MatrixExpression<CellFieldType>::Builder;

  const std::string matrixName = "mat";
  const Expr::TagList matrixTags = Expr::matrix::matrix_tags( matrixName, setup.nrows );

  Expr::ExpressionID aID = factory.register_expression( new ConstantType( setup.aTag, setup.aValue ) );
  Expr::ExpressionID bID = factory.register_expression( new ConstantSVFType( setup.bTag, setup.bValue ) );
  Expr::ExpressionID cID = factory.register_expression( new LinearType( setup.cTag, setup.aTag, setup.cSlope, setup.cIntcp ) );
  Expr::ExpressionID eID = factory.register_expression( new LinearSVFType( setup.eTag, setup.bTag, setup.eSlope, setup.eIntcp ) );
  Expr::ExpressionID matID = factory.register_expression( new MatrixAssemblerExprType( matrixTags, assembler ) );
  rootIDs.insert( aID );
  rootIDs.insert( bID );
  rootIDs.insert( cID );
  rootIDs.insert( eID );
  rootIDs.insert( matID );

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  assembler->trigger_sensitivities( tree );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );
  tree.execute_tree();

  TestHelper tester( false );
  for( Expr::matrix::OrdinalType flatIdx=0; flatIdx<setup.nrows*setup.nrows; ++flatIdx ){
    tester( field_equal( answer(flatIdx), fml.field_ref<CellFieldType>( matrixTags[flatIdx] ) ) );
  }

  if( !tester.ok() ){
    Expr::matrix::print_matrix<CellFieldType>( fml, matrixTags );
    answer.print( std::cout );
  }

  return tester.ok();
}

class TestHelperWithCount
{
  bool report;
  bool isokay = true;
  unsigned testcount = 0;
  unsigned passcount = 0;
  std::vector<std::string> fails;
public:
  TestHelperWithCount( const bool reportResults = true )
: report( reportResults ) {}

  void operator()( const bool result, std::string t )
  {
    testcount++;
    if( result ) passcount++;
    else fails.push_back( t );
    if( isokay ) isokay = result;
    if( report ){
      if( result ) std::cout << testcount << ":  pass " << t << std::endl;
      else         std::cout << testcount << ":  fail " << t << std::endl;
    }
  }

  bool ok() const{ return isokay; }
  bool isfailed() const{ return !isokay; }
  unsigned test_count() const{ return testcount; }
  unsigned pass_count() const{ return passcount; }
  void show_results() const
  {
    std::cout << (testcount-passcount) << "/" << testcount << " tests failed:\n";
    for( auto t : fails ){
      std::cout << "  - " << t << '\n';
    }
  }
};


int main()
{
  TestHelperWithCount tester( true );
  OperationTestSetup setup;

  boost::shared_ptr<Expr::matrix::AssemblerBase<CellFieldType> > assemblerPtr;

  // emplacement (unary operation)
  std::cout << "\n-------- UNARY OPERATIONS -------- \n";
  { assemblerPtr = setup.S;    auto b = setup.s;    tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D;    auto b = setup.d;    tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P;    auto b = setup.p;    tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt; auto b = setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv; auto b = setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I;    auto b = setup.i;    tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M;    auto b = setup.m;    tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }

  // binary operations between general-purpose matrix types
  std::cout << "\n-------- BINARY OPERATIONS -------- \n";
  { assemblerPtr = setup.M    + setup.M   ; auto b = setup.m    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.M   ; auto b = setup.s    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.M   ; auto b = setup.d    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.M   ; auto b = setup.ymlt + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.M   ; auto b = setup.ydiv + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.M   ; auto b = setup.i    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.M   ; auto b = setup.p    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.S   ; auto b = setup.m    + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.S   ; auto b = setup.s    + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.S   ; auto b = setup.d    + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.S   ; auto b = setup.ymlt + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.S   ; auto b = setup.ydiv + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.S   ; auto b = setup.i    + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.S   ; auto b = setup.p    + setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.D   ; auto b = setup.m    + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.D   ; auto b = setup.s    + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.D   ; auto b = setup.d    + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.D   ; auto b = setup.ymlt + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.D   ; auto b = setup.ydiv + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.D   ; auto b = setup.i    + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.D   ; auto b = setup.p    + setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.Ymlt; auto b = setup.m    + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.Ymlt; auto b = setup.s    + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.Ymlt; auto b = setup.d    + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.Ymlt; auto b = setup.ymlt + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.Ymlt; auto b = setup.ydiv + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.Ymlt; auto b = setup.i    + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.Ymlt; auto b = setup.p    + setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.Ydiv; auto b = setup.m    + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.Ydiv; auto b = setup.s    + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.Ydiv; auto b = setup.d    + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.Ydiv; auto b = setup.ymlt + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.Ydiv; auto b = setup.ydiv + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.Ydiv; auto b = setup.i    + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.Ydiv; auto b = setup.p    + setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.I   ; auto b = setup.m    + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.I   ; auto b = setup.s    + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.I   ; auto b = setup.d    + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.I   ; auto b = setup.ymlt + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.I   ; auto b = setup.ydiv + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.I   ; auto b = setup.i    + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.I   ; auto b = setup.p    + setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.P   ; auto b = setup.m    + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    + setup.P   ; auto b = setup.s    + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    + setup.P   ; auto b = setup.d    + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt + setup.P   ; auto b = setup.ymlt + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv + setup.P   ; auto b = setup.ydiv + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    + setup.P   ; auto b = setup.i    + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    + setup.P   ; auto b = setup.p    + setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    + setup.M   ; auto b = setup.m    + setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.M   ; auto b = setup.s    - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.M   ; auto b = setup.d    - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.M   ; auto b = setup.ymlt - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.M   ; auto b = setup.ydiv - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.M   ; auto b = setup.i    - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.M   ; auto b = setup.p    - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.S   ; auto b = setup.m    - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.S   ; auto b = setup.s    - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.S   ; auto b = setup.d    - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.S   ; auto b = setup.ymlt - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.S   ; auto b = setup.ydiv - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.S   ; auto b = setup.i    - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.S   ; auto b = setup.p    - setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.D   ; auto b = setup.m    - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.D   ; auto b = setup.s    - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.D   ; auto b = setup.d    - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.D   ; auto b = setup.ymlt - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.D   ; auto b = setup.ydiv - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.D   ; auto b = setup.i    - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.D   ; auto b = setup.p    - setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.Ymlt; auto b = setup.m    - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.Ymlt; auto b = setup.s    - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.Ymlt; auto b = setup.d    - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.Ymlt; auto b = setup.ymlt - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.Ymlt; auto b = setup.ydiv - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.Ymlt; auto b = setup.i    - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.Ymlt; auto b = setup.p    - setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.Ydiv; auto b = setup.m    - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.Ydiv; auto b = setup.s    - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.Ydiv; auto b = setup.d    - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.Ydiv; auto b = setup.ymlt - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.Ydiv; auto b = setup.ydiv - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.Ydiv; auto b = setup.i    - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.Ydiv; auto b = setup.p    - setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.I   ; auto b = setup.m    - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.I   ; auto b = setup.s    - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.I   ; auto b = setup.d    - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.I   ; auto b = setup.ymlt - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.I   ; auto b = setup.ydiv - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.I   ; auto b = setup.i    - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.I   ; auto b = setup.p    - setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.P   ; auto b = setup.m    - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    - setup.P   ; auto b = setup.s    - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    - setup.P   ; auto b = setup.d    - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt - setup.P   ; auto b = setup.ymlt - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv - setup.P   ; auto b = setup.ydiv - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    - setup.P   ; auto b = setup.i    - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    - setup.P   ; auto b = setup.p    - setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    - setup.M   ; auto b = setup.m    - setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.M   ; auto b = setup.s    * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.M   ; auto b = setup.d    * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.M   ; auto b = setup.ymlt * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.M   ; auto b = setup.ydiv * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.M   ; auto b = setup.i    * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.M   ; auto b = setup.p    * setup.m   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.S   ; auto b = setup.m    * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.S   ; auto b = setup.s    * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.S   ; auto b = setup.d    * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.S   ; auto b = setup.ymlt * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.S   ; auto b = setup.ydiv * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.S   ; auto b = setup.i    * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.S   ; auto b = setup.p    * setup.s   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.D   ; auto b = setup.m    * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.D   ; auto b = setup.s    * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.D   ; auto b = setup.d    * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.D   ; auto b = setup.ymlt * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.D   ; auto b = setup.ydiv * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.D   ; auto b = setup.i    * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.D   ; auto b = setup.p    * setup.d   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.Ymlt; auto b = setup.m    * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.Ymlt; auto b = setup.s    * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.Ymlt; auto b = setup.d    * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.Ymlt; auto b = setup.ymlt * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.Ymlt; auto b = setup.ydiv * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.Ymlt; auto b = setup.i    * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.Ymlt; auto b = setup.p    * setup.ymlt; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.Ydiv; auto b = setup.m    * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.Ydiv; auto b = setup.s    * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.Ydiv; auto b = setup.d    * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.Ydiv; auto b = setup.ymlt * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.Ydiv; auto b = setup.ydiv * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.Ydiv; auto b = setup.i    * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.Ydiv; auto b = setup.p    * setup.ydiv; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.I   ; auto b = setup.m    * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.I   ; auto b = setup.s    * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.I   ; auto b = setup.d    * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.I   ; auto b = setup.ymlt * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.I   ; auto b = setup.ydiv * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.I   ; auto b = setup.i    * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.I   ; auto b = setup.p    * setup.i   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M    * setup.P   ; auto b = setup.m    * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.S    * setup.P   ; auto b = setup.s    * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.D    * setup.P   ; auto b = setup.d    * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ymlt * setup.P   ; auto b = setup.ymlt * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.Ydiv * setup.P   ; auto b = setup.ydiv * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.I    * setup.P   ; auto b = setup.i    * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.P    * setup.P   ; auto b = setup.p    * setup.p   ; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  // ternary operations
  std::cout << "\n-------- TERNARY OPERATIONS -------- \n";
  { assemblerPtr = setup.M + ( setup.M + setup.S ); auto b = setup.m + ( setup.m + setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M - ( setup.M + setup.S ); auto b = setup.m - ( setup.m + setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M * ( setup.M + setup.S ); auto b = setup.m * ( setup.m + setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M + ( setup.M - setup.S ); auto b = setup.m + ( setup.m - setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M - ( setup.M - setup.S ); auto b = setup.m - ( setup.m - setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M * ( setup.M - setup.S ); auto b = setup.m * ( setup.m - setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M + ( setup.M * setup.S ); auto b = setup.m + ( setup.m * setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M - ( setup.M * setup.S ); auto b = setup.m - ( setup.m * setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M * ( setup.M * setup.S ); auto b = setup.m * ( setup.m * setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) + setup.M; auto b = ( setup.m + setup.s ) + setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) - setup.M; auto b = ( setup.m + setup.s ) - setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) * setup.M; auto b = ( setup.m + setup.s ) * setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) + setup.M; auto b = ( setup.m - setup.s ) + setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) - setup.M; auto b = ( setup.m - setup.s ) - setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) * setup.M; auto b = ( setup.m - setup.s ) * setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) + setup.M; auto b = ( setup.m * setup.s ) + setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) - setup.M; auto b = ( setup.m * setup.s ) - setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) * setup.M; auto b = ( setup.m * setup.s ) * setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  // special quaternary operations with prodsum and proddiff
  std::cout << "\n-------- QUATERNARY OPERATIONS -------- \n";
  { assemblerPtr = setup.M + ( setup.M * ( setup.M + setup.S ) ); auto b = setup.m + ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M - ( setup.M * ( setup.M + setup.S ) ); auto b = setup.m - ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M * ( setup.M * ( setup.M + setup.S ) ); auto b = setup.m * ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M + ( setup.M * ( setup.M - setup.S ) ); auto b = setup.m + ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M - ( setup.M * ( setup.M - setup.S ) ); auto b = setup.m - ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = setup.M * ( setup.M * ( setup.M - setup.S ) ); auto b = setup.m * ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M + setup.S ) ) + setup.M; auto b = ( setup.m * ( setup.m + setup.s ) ) + setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M + setup.S ) ) - setup.M; auto b = ( setup.m * ( setup.m + setup.s ) ) - setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M + setup.S ) ) * setup.M; auto b = ( setup.m * ( setup.m + setup.s ) ) * setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M - setup.S ) ) + setup.M; auto b = ( setup.m * ( setup.m - setup.s ) ) + setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M - setup.S ) ) - setup.M; auto b = ( setup.m * ( setup.m - setup.s ) ) - setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * ( setup.M - setup.S ) ) * setup.M; auto b = ( setup.m * ( setup.m - setup.s ) ) * setup.m; tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) * ( setup.M + setup.S ); auto b = ( setup.m + setup.s ) * ( setup.m + setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) * ( setup.M - setup.S ); auto b = ( setup.m + setup.s ) * ( setup.m - setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) * ( setup.M + setup.S ); auto b = ( setup.m - setup.s ) * ( setup.m + setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) * ( setup.M - setup.S ); auto b = ( setup.m - setup.s ) * ( setup.m - setup.s ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  // special quinary operations with prodsum and proddiff
  std::cout << "\n-------- QUINARY OPERATIONS -------- \n";
  { assemblerPtr = ( setup.M + setup.S ) + setup.M * ( setup.M + setup.S );     auto b = ( setup.m + setup.s ) + setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) + setup.M * ( setup.M - setup.S );     auto b = ( setup.m + setup.s ) + setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) + setup.M * ( setup.M + setup.S );     auto b = ( setup.m - setup.s ) + setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) + setup.M * ( setup.M - setup.S );     auto b = ( setup.m - setup.s ) + setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) + setup.M * ( setup.M + setup.S );     auto b = ( setup.m * setup.s ) + setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) + setup.M * ( setup.M - setup.S );     auto b = ( setup.m * setup.s ) + setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) - setup.M * ( setup.M + setup.S );     auto b = ( setup.m + setup.s ) - setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) - setup.M * ( setup.M - setup.S );     auto b = ( setup.m + setup.s ) - setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) - setup.M * ( setup.M + setup.S );     auto b = ( setup.m - setup.s ) - setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) - setup.M * ( setup.M - setup.S );     auto b = ( setup.m - setup.s ) - setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) - setup.M * ( setup.M + setup.S );     auto b = ( setup.m * setup.s ) - setup.m * ( setup.m + setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) - setup.M * ( setup.M - setup.S );     auto b = ( setup.m * setup.s ) - setup.m * ( setup.m - setup.s );     tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) * ( setup.M * ( setup.M + setup.S ) ); auto b = ( setup.m + setup.s ) * ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M + setup.S ) * ( setup.M * ( setup.M - setup.S ) ); auto b = ( setup.m + setup.s ) * ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) * ( setup.M * ( setup.M + setup.S ) ); auto b = ( setup.m - setup.s ) * ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M - setup.S ) * ( setup.M * ( setup.M - setup.S ) ); auto b = ( setup.m - setup.s ) * ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) * ( setup.M * ( setup.M + setup.S ) ); auto b = ( setup.m * setup.s ) * ( setup.m * ( setup.m + setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }
  { assemblerPtr = ( setup.M * setup.S ) * ( setup.M * ( setup.M - setup.S ) ); auto b = ( setup.m * setup.s ) * ( setup.m * ( setup.m - setup.s ) ); tester( run_test( assemblerPtr, b, setup ), assemblerPtr->assembler_name() ); }

  if( tester.ok() ){ std::cout << "\n-------- PASS -------- \n";}
  else             { std::cout << "\n-------- FAIL -------- \n";}
  tester.show_results();
  if( tester.ok() ){ return 0; }
  else             { return -1;}
}



