#ifndef MATRIX_H_
#define MATRIX_H_

#include <vector>
#include <iostream>
#include <ostream>
#include <cassert>

#include <expression/Tag.h>

namespace densematrix_for_test {
using OrdinalType = std::size_t;


/*
 * @class Matrix
 *
 * This class represents a 2-D square array and provides indexing methods for a
 * row-major storage in a flat vector.
 *
 * This is used for testing matrix assemblers. The +, -, and * operators are overloaded
 * for the Matrix class below to facilitate assembler testing.
 */
template<typename ScalarType>
class Matrix {
 protected:
  OrdinalType nrows_;
  std::vector<ScalarType> mat_;

 public:
  /*
   * @brief construct an empty matrix
   */
  Matrix() : nrows_(0) {}

  /*
   * @brief construct a matrix
   * @param nrows number of rows and columns in the matrix
   */
  Matrix( const OrdinalType nrows )
      : nrows_( nrows ),
        mat_( nrows * nrows, 0 )
  {}

  /*
   * @brief get the number of rows in this matrix
   */
  OrdinalType nrows() const { return nrows_; }

  /*
   * @brief non-const flat-indexing (row-major) of this matrix through the
   * parentheses operator
   * @param flatIdx the row-major index
   */
  ScalarType& operator()( const OrdinalType flatIdx ) { return mat_[flatIdx]; }

  /*
   * @brief non-const 2-D indexing of this matrix through the parentheses
   * operator
   * @param rowIdx the row index
   * @param colIdx the column index
   */
  ScalarType& operator()( const OrdinalType rowIdx, const OrdinalType colIdx ) {
    return (*this)( rowIdx * nrows_ + colIdx );
  }

  /*
   * @brief const flat-indexing (row-major) of this matrix through the
   * parentheses operator
   * @param flatIdx the row-major index
   */
  const ScalarType operator()( const OrdinalType flatIdx ) const {
    return mat_[flatIdx];
  }

  /*
   * @brief const 2-D indexing of this matrix through the parentheses
   * operator
   * @param rowIdx the row index
   * @param colIdx the column index
   */
  const ScalarType operator()( const OrdinalType rowIdx,
                               const OrdinalType colIdx ) const {
    return (*this)( rowIdx * nrows_ + colIdx );
  }

  /*
   * @brief matrix equality operator
   * @param mat matrix to compare
   */
  bool operator==( const Matrix& mat ) {
    if( mat.nrows() != nrows_ ) {
      return false;
    }
    assert( mat.nrows() == nrows_ );
    for( OrdinalType rowIdx = 0; rowIdx < nrows_; ++rowIdx ){
      for( OrdinalType colIdx = 0; colIdx < nrows_; ++colIdx ){
        if( mat(rowIdx, colIdx) != (*this)(rowIdx, colIdx) ){
          return false;
        }
      }
    }
    return true;
  }

  /*
   *
   */
  void multiply_by_scale( const double scale ){
    for( OrdinalType rowIdx = 0; rowIdx < nrows_; ++rowIdx ){
      for( OrdinalType colIdx = 0; colIdx < nrows_; ++colIdx ){
        (*this)( rowIdx * nrows_ + colIdx ) *= scale;
      }
    }
  }

  /*
   * @brief print the matrix to std::cout
   */
  void print( std::ostream& stream ) const {
    stream << "mat(" << nrows_ << "x" << nrows_ << ") = [\n";
    for( OrdinalType rowIdx = 0; rowIdx < nrows_; ++rowIdx ) {
      for( OrdinalType colIdx = 0; colIdx < nrows_; ++colIdx ) {
        stream << (*this)(rowIdx, colIdx) << " ";
      }
      stream << ( (rowIdx + 1 != nrows_) ? ',' : ']' ) << '\n';
    }
  }
};

/*
 * @brief add two matrices together
 * @param left the matrix to the left of '+'
 * @param right the matrix to the left of '+'
 */
template<typename LeftScalarType, typename RightScalarType>
Matrix<LeftScalarType> operator+( const Matrix<LeftScalarType>& left,
                                  const Matrix<RightScalarType>& right )
{
  const OrdinalType nrows = left.nrows();
  const OrdinalType rightRows = right.nrows();
  assert( nrows == rightRows );
  Matrix<LeftScalarType> sum( nrows );
  const OrdinalType numel = nrows * nrows;
  for( OrdinalType flatIdx = 0; flatIdx < numel; ++flatIdx ){
    sum(flatIdx) = left(flatIdx) + right(flatIdx);
  }
  return sum;
}

/*
 * @brief subtract one matrix from another
 * @param left the matrix to the left of '-'
 * @param right the matrix to the left of '-'
 */
template<typename LeftScalarType, typename RightScalarType>
Matrix<LeftScalarType> operator-( const Matrix<LeftScalarType>& left,
                                  const Matrix<RightScalarType>& right )
{
  const OrdinalType nrows = left.nrows();
  const OrdinalType rightRows = right.nrows();
  assert( nrows == rightRows );
  Matrix<LeftScalarType> diff( nrows );
  const OrdinalType numel = nrows * nrows;
  for( OrdinalType flatIdx = 0; flatIdx < numel; ++flatIdx ){
    diff(flatIdx) = left(flatIdx) - right(flatIdx);
  }
  return diff;
}

/*
 * @brief multiply two matrices
 * @param left the matrix to the left of '*'
 * @param right the matrix to the left of '*'
 */
template<typename LeftScalarType, typename RightScalarType>
Matrix<LeftScalarType> operator*( const Matrix<LeftScalarType>& left,
                                  const Matrix<RightScalarType>& right )
{
  const OrdinalType nrows = left.nrows();
  const OrdinalType rightRows = right.nrows();
  assert( nrows == rightRows );
  Matrix<LeftScalarType> prod( nrows );

  for( OrdinalType rowIdx = 0; rowIdx < nrows; ++rowIdx ){
    for( OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
      double temp = 0.0;
      for( OrdinalType tmpIdx = 0; tmpIdx < nrows; ++tmpIdx ){
        temp += left(rowIdx,tmpIdx) * right(tmpIdx,colIdx);
      }
      prod(rowIdx,colIdx) = temp;
    }
  }

  return prod;
}


}

#endif /* MATRIX_H_ */
