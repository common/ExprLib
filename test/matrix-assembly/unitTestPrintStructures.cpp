/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>


#include "ScaledIdentityPreAssembly.h"
#include "DiagonalPreAssembly.h"
#include "PermutationSubPreAssembly.h"
#include "SparsePreAssembly.h"
#include "DenseSubPreAssembly.h"

using Expr::Tag;

using CellFieldType = SpatialOps::SVolField;
using SingleValueType = SpatialOps::SingleValueField;

using Expr::matrix::sensitivity;
using Dense = Expr::matrix::DenseSubMatrix<CellFieldType>;
using Sparse = Expr::matrix::SparseMatrix<CellFieldType>;
using Diag = Expr::matrix::DiagonalMatrix<CellFieldType>;
using Perm = Expr::matrix::PermutationSubMatrix<CellFieldType>;
using Scale = Expr::matrix::ScaledIdentityMatrix<CellFieldType>;

#define ASSEMBLER(AssemblerT, name) \
  boost::shared_ptr<AssemblerT> name = boost::make_shared<AssemblerT>( #name );

int main( int iarg, char* carg[] )
{

  ASSEMBLER( Dense , M  )
  ASSEMBLER( Sparse, S  )
  ASSEMBLER( Diag  , D  )
  ASSEMBLER( Perm  , P  )
  ASSEMBLER( Scale , Ym )
  ASSEMBLER( Scale , Yd )
  ASSEMBLER( Scale , I  )

  const Expr::Context c = Expr::STATE_NONE;

  const Tag aTag( "a", c );
  const Tag bTag( "b", c );
  const Tag cTag( "c", c );
  const Tag dTag( "d", c );
  const Tag eTag( "e", c );
  const Tag fTag( "f", c );
  const Tag gTag( "g", c );
  const Tag hTag( "h", c );
  const Tag iTag( "i", c );
  const Tag jTag( "j", c );
  const Tag kTag( "k", c );
  const Tag lTag( "l", c );


  M->element<CellFieldType>(0,0) = aTag;
  M->element<CellFieldType>(0,1) = sensitivity( bTag, cTag );
  M->element<CellFieldType>(0,2) = cTag;
  M->element<SingleValueType>(1,0) = dTag;
  M->element<SingleValueType>(1,1) = sensitivity( eTag, fTag );
  M->element<SingleValueType>(1,2) = fTag;
  M->element<double>(2,0) = 1.0;
  M->element<double>(2,1) = 2.0;
  M->element<double>(2,2) = 3.0;
  M->finalize();

  S->element<CellFieldType>(0,0) = gTag;
  S->element<CellFieldType>(0,1) = sensitivity( hTag, iTag );
  S->element<CellFieldType>(0,2) = iTag;
  S->element<SingleValueType>(1,0) = jTag;
  S->element<SingleValueType>(1,1) = sensitivity( kTag, lTag );
  S->element<SingleValueType>(1,2) = lTag;
  S->element<double>(2,0) = 1.0;
  S->element<double>(2,1) = 2.0;
  S->element<double>(2,2) = 3.0;
  S->finalize();

  D->element<CellFieldType>(0) = aTag;
  D->element<SingleValueType>(1) = bTag;
  D->element<double>(2) = 3.0;
  D->finalize();

  P->set_row_col( 0, 1 );
  P->set_row_col( 1, 2 );
  P->set_row_col( 2, 0 );
  P->finalize();

  Ym->scale<CellFieldType>() = aTag;
  Ym->set_type( Expr::matrix::ScaledIdentityMatrix<CellFieldType>::SCALEMULTIPLY );
  Ym->finalize();

  Yd->scale<SingleValueType>() = bTag;
  Yd->set_type( Expr::matrix::ScaledIdentityMatrix<CellFieldType>::SCALEDIVIDE );
  Yd->set_double_multiplier( 3.0 );
  Yd->finalize();

  I->finalize();


  std::cout << M->assembler_name() << ": " << '\n';
  std::cout << M->get_structure();
  std::cout << "--------\n";
  std::cout << S->assembler_name() << ": " << '\n';
  std::cout << S->get_structure();
  std::cout << "--------\n";
  std::cout << D->assembler_name() << ": " << '\n';
  std::cout << D->get_structure();
  std::cout << "--------\n";
  std::cout << P->assembler_name() << ": " << '\n';
  std::cout << P->get_structure();
  std::cout << "--------\n";
  std::cout << Ym->assembler_name() << ": " << '\n';
  std::cout << Ym->get_structure();
  std::cout << "--------\n";
  std::cout << Yd->assembler_name() << ": " << '\n';
  std::cout << Yd->get_structure();
  std::cout << "--------\n";
  std::cout << I->assembler_name() << ": " << '\n';
  std::cout << I->get_structure();
  std::cout << "--------\n";



  std::cout << "PASS" << '\n';
  return 0;
}



