/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>


#include "ScaledIdentityPreAssembly.h"
#include "DiagonalPreAssembly.h"
#include "PermutationSubPreAssembly.h"
#include "SparsePreAssembly.h"
#include "DenseSubPreAssembly.h"

#include "../TestHelper.h"




int main( int iarg, char* carg[] )
{

  TestHelper tester( true );

  tester( scaledidentityttest::test_build_empty()                        , "scaled identity: build empty" );
  tester( scaledidentityttest::test_set_scale_cellfield()                , "scaled identity: insert elem: cell field" );
  tester( scaledidentityttest::test_set_scale_cellfield_sens()           , "scaled identity: insert elem: cell field sens" );
  tester( scaledidentityttest::test_set_scale_svf()                      , "scaled identity: insert elem: single-value field" );
  tester( scaledidentityttest::test_set_scale_svf_sens()                 , "scaled identity: insert elem: single-value field sens" );
  tester( scaledidentityttest::test_set_scale_double()                   , "scaled identity: insert elem: double" );
  tester( scaledidentityttest::test_throw_set_scale_cell_after_finalize(), "scaled identity: throw if insert cell after finalize" );
  tester( scaledidentityttest::test_throw_if_sens_before_finalize()      , "scaled identity: throw if trigger sensitivities before finalize" );

  tester( diagonaltest::test_build_empty()                     , "diagonal: build empty" );
  tester( diagonaltest::test_insert_element_cellfield()        , "diagonal: insert elem: cell field" );
  tester( diagonaltest::test_insert_element_cellfield_sens()   , "diagonal: insert elem: cell field sens" );
  tester( diagonaltest::test_insert_element_svf()              , "diagonal: insert elem: single-value field" );
  tester( diagonaltest::test_insert_element_svf_sens()         , "diagonal: insert elem: single-value field sens" );
  tester( diagonaltest::test_insert_element_double()           , "diagonal: insert elem: double" );
  tester( diagonaltest::test_throw_insert_cell_after_finalize(), "diagonal: throw if insert cell after finalize" );
  tester( diagonaltest::test_throw_if_sens_before_finalize()   , "diagonal: throw if trigger sensitivities before finalize" );

  tester( permutationsubtest::test_build_empty()                , "permutation: build empty" );
  tester( permutationsubtest::test_set_identity_row()           , "permutation: set identity row" );
  tester( permutationsubtest::test_swap_rows()                  , "permutation: swap rows" );
  tester( permutationsubtest::test_set_row_map()                , "permutation: set row map" );
  tester( permutationsubtest::test_throw_set_identity_finalize(), "permutation: throw if set identity row after finalize" );
  tester( permutationsubtest::test_throw_swap_rows_finalize()   , "permutation: throw if swap rows after finalize" );

  tester( sparsetest::test_build_empty()                     , "sparse: build empty" );
  tester( sparsetest::test_insert_element_cellfield()        , "sparse: insert elem: cell field" );
  tester( sparsetest::test_insert_element_cellfield_sens()   , "sparse: insert elem: cell field sens" );
  tester( sparsetest::test_insert_element_svf()              , "sparse: insert elem: single-value field" );
  tester( sparsetest::test_insert_element_svf_sens()         , "sparse: insert elem: single-value field sens" );
  tester( sparsetest::test_insert_element_double()           , "sparse: insert elem: double " );
  tester( sparsetest::test_throw_insert_cell_after_finalize(), "sparse: throw if insert cell after finalize" );
  tester( sparsetest::test_throw_if_sens_before_finalize()   , "sparse: throw if trigger sensitivities before finalize" );

  tester( densesubtest::test_build_empty()                     , "dense-sub: build empty" );
  tester( densesubtest::test_insert_element_cellfield()        , "dense-sub: insert elem: cell field" );
  tester( densesubtest::test_insert_element_cellfield_sens()   , "dense-sub: insert elem: cell field sens" );
  tester( densesubtest::test_insert_element_svf()              , "dense-sub: insert elem: single-value field" );
  tester( densesubtest::test_insert_element_svf_sens()         , "dense-sub: insert elem: single-value field sens" );
  tester( densesubtest::test_insert_element_double()           , "dense-sub: insert elem: double" );
  tester( densesubtest::test_throw_insert_cell_after_finalize(), "dense-sub: throw if insert cell after finalize" );
  tester( densesubtest::test_throw_if_sens_before_finalize()   , "dense-sub: throw if trigger sensitivities before finalize" );

  if( tester.ok() ){ std::cout << "PASS" << '\n'; return 0; }
  else             { std::cout << "FAIL" << '\n'; return -1;}
}



