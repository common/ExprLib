/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>
#include <sstream>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/ExprPatch.h>
#include <expression/ExpressionTree.h>
#include <expression/matrix-assembly/compressible-reactive-flow/DualTimeStepExpression.h>

#include "../TestHelper.h"

enum SpatialDimensions { X, Y, Z, XY, XZ, YZ, XYZ };

bool run_dual_time_step_test( const SpatialDimensions dimensions,
                              const std::size_t nspecies,
                              const double cfl,
                              const double vnn,
                              const double maxDs,
                              const double minDs )
{
  using SVolFieldT = SpatialOps::SVolField;
  using ConstantType = Expr::ConstantExpr<SVolFieldT>::Builder;
  using PlaceHolderType = Expr::PlaceHolder<SVolFieldT>::Builder;
  using DualTimeNoChemType = Expr::DualTimeStepExprNoChemistry<SVolFieldT>::Builder;

  // ------------------------------------------------------------------------------------------------

  const Expr::Tag xCoordTag( "x", Expr::STATE_NONE );
  const Expr::Tag yCoordTag( "y", Expr::STATE_NONE );
  const Expr::Tag zCoordTag( "z", Expr::STATE_NONE );

  const Expr::Tag dsTag( "ds", Expr::STATE_NONE );

  const Expr::Tag densityTag       ( "rho", Expr::STATE_NONE ); const double densityValue        = 1.0;
  const Expr::Tag heatCapacityCpTag( "cp" , Expr::STATE_NONE ); const double heatCapacityCpValue = 2.0;
  const Expr::Tag conductivityTag  ( "k"  , Expr::STATE_NONE ); const double conductivityValue   = 3.0;
  const Expr::Tag viscosityTag     ( "mu" , Expr::STATE_NONE ); const double viscosityValue      = 4.0;
  const Expr::Tag soundSpeedTag    ( "a"  , Expr::STATE_NONE ); const double soundSpeedValue     = 50.0;

  Expr::Tag xVelocityTag( "vx", Expr::STATE_NONE ); const double xVelocityValue = 5.0;
  Expr::Tag yVelocityTag( "vy", Expr::STATE_NONE ); const double yVelocityValue = 6.0;
  Expr::Tag zVelocityTag( "vz", Expr::STATE_NONE ); const double zVelocityValue = 7.0;

  const double universalGasConstant = 10.0;
  std::vector<std::string> speciesNamesRepo = {"A", "B", "C", "D", "E", "F", "G", "H"}; // up to eight species for testing

  std::vector<Expr::Tag> diffusivityTags;
  std::vector<double> diffusivityValues;
  for( std::size_t i=0; i<nspecies; ++i ){
    diffusivityTags  .push_back( Expr::Tag( "D_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
    diffusivityValues.push_back( (double) (i + 1) * 1.e-2 );
  }

  // ------------------------------------------------------------------------------------------------

  std::size_t ndims;
  bool doX = false;
  bool doY = false;
  bool doZ = false;
  int nx = 1;
  int ny = 1;
  int nz = 1;

  switch( dimensions ){
    case X:   doX = true;                         ndims = 1; nx = 16;                   break;
    case Y:               doY = true;             ndims = 1;          ny = 16;          break;
    case Z:                           doZ = true; ndims = 1;                   nz = 16; break;
    case XY:  doX = true; doY = true;             ndims = 2; nx = 8;  ny = 8;           break;
    case XZ:  doX = true;             doZ = true; ndims = 2; nx = 8;           nz = 8;  break;
    case YZ:              doY = true; doZ = true; ndims = 2;          ny = 8;  nz = 8;  break;
    case XYZ: doX = true; doY = true; doZ = true; ndims = 3; nx = 4;  ny = 4;  nz = 4;  break;
    default:
      break;
  }

  if( !doX ) xVelocityTag = Expr::Tag();
  if( !doY ) yVelocityTag = Expr::Tag();
  if( !doZ ) zVelocityTag = Expr::Tag();

  const double lx = 1.0;
  const double ly = 0.9;
  const double lz = 0.8;

  const double dx = lx / ((double) nx);
  const double dy = ly / ((double) ny);
  const double dz = lz / ((double) nz);

  const SpatialOps::Grid grid( SpatialOps::IntVec(nx, ny, nz),
                               SpatialOps::DoubleVec(lx, ly, lz) );
  Expr::ExprPatch patch(nx, ny, nz);

  // ------------------------------------------------------------------------------------------------

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  // ------------------------------------------------------------------------------------------------

  rootIDs.insert( factory.register_expression( new PlaceHolderType( xCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( yCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( zCoordTag ) ) );

  rootIDs.insert( factory.register_expression( new ConstantType( viscosityTag     , viscosityValue      ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( conductivityTag  , conductivityValue   ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( densityTag       , densityValue        ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( heatCapacityCpTag, heatCapacityCpValue ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( soundSpeedTag    , soundSpeedValue     ) ) );

  if( doX ) rootIDs.insert( factory.register_expression( new ConstantType( xVelocityTag, xVelocityValue ) ) );
  if( doY ) rootIDs.insert( factory.register_expression( new ConstantType( yVelocityTag, yVelocityValue ) ) );
  if( doZ ) rootIDs.insert( factory.register_expression( new ConstantType( zVelocityTag, zVelocityValue ) ) );

  for( std::size_t i=0; i<nspecies; ++i ){
    rootIDs.insert( factory.register_expression( new ConstantType( diffusivityTags[i], diffusivityValues[i] ) ) );
  }

  // ------------------------------------------------------------------------------------------------

  rootIDs.insert( factory.register_expression( new DualTimeNoChemType( dsTag, cfl, vnn, maxDs, minDs,
                                                                       xCoordTag, yCoordTag, zCoordTag,
                                                                       xVelocityTag, yVelocityTag, zVelocityTag,
                                                                       soundSpeedTag, conductivityTag, densityTag,
                                                                       heatCapacityCpTag, viscosityTag, diffusivityTags,
                                                                       true /* true for unit testing */ ) ) );

  // ------------------------------------------------------------------------------------------------

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );

  grid.set_coord<SpatialOps::XDIR>( fml.field_ref<SVolFieldT>( xCoordTag ) );
  grid.set_coord<SpatialOps::YDIR>( fml.field_ref<SVolFieldT>( yCoordTag ) );
  grid.set_coord<SpatialOps::ZDIR>( fml.field_ref<SVolFieldT>( zCoordTag ) );

  const double dsFirstValue = 1.e-2;
  fml.field_ref<SVolFieldT>( dsTag ) <<= dsFirstValue;

  tree.execute_tree();

  // ------------------------------------------------------------------------------------------------

  TestHelper tester( false );
  fml.field_ref<SVolFieldT>( dsTag ).add_device( CPU_INDEX );

  const double dsObserved = field_max_interior( fml.field_ref<SVolFieldT>( dsTag ) );

  double s = 0.0;
  double maxD = 0.0;

  maxD = std::max( conductivityValue / (densityValue * heatCapacityCpValue), viscosityValue / densityValue );
  for( int i=0; i<nspecies; ++i ){
    maxD = std::max( maxD, diffusivityValues[i] / densityValue );
  }

  if( doX ) s += xVelocityValue * xVelocityValue;
  if( doY ) s += yVelocityValue * yVelocityValue;
  if( doZ ) s += zVelocityValue * zVelocityValue;
  s = std::sqrt( s );

  double minDx = 1.e305;
  if( doX ) minDx = std::min( minDx, dx );
  if( doY ) minDx = std::min( minDx, dy );
  if( doZ ) minDx = std::min( minDx, dz );

  const double ds_cfl = cfl * minDx / (soundSpeedValue + s);
  const double ds_vnn = vnn * minDx * minDx / maxD;

  const double dsExpected = std::max( minDs, std::min( ds_cfl, std::min( ds_vnn, maxDs ) ) );
  tester( std::abs(dsObserved - dsExpected) < 1.e-8 );
  return tester.ok();
}


int main()
{
  TestHelper tester( true );

  const std::vector<double> maxDsValues = {1.e-2, 1.e6};
  const std::vector<double> minDsValues = {1.e0, 1.e-12};
  const std::vector<double> cflValues   = {0.1, 1., 10.};
  const std::vector<double> vnnValues   = {0.1, 1., 10.};
  const std::vector<int>    nSpecValues = {1, 3, 6};

  for( const auto& mx : maxDsValues ){
    for( const auto& mn : minDsValues ){
      for( const auto& cfl : cflValues ){
        for( const auto& vnn : vnnValues ){
          for( const auto& ns : nSpecValues ){
            std::ostringstream testStream;
            testStream << ns << " species max = " << mx << " min = " << mn << " cfl = " << cfl << " vnn = " << vnn;
            tester( run_dual_time_step_test( X  , ns, cfl, vnn, mx, mn ), "X   " + testStream.str() );
            tester( run_dual_time_step_test( Y  , ns, cfl, vnn, mx, mn ), "Y   " + testStream.str() );
            tester( run_dual_time_step_test( Z  , ns, cfl, vnn, mx, mn ), "Z   " + testStream.str() );
            tester( run_dual_time_step_test( XY , ns, cfl, vnn, mx, mn ), "XY  " + testStream.str() );
            tester( run_dual_time_step_test( XZ , ns, cfl, vnn, mx, mn ), "XZ  " + testStream.str() );
            tester( run_dual_time_step_test( YZ , ns, cfl, vnn, mx, mn ), "YZ  " + testStream.str() );
            tester( run_dual_time_step_test( XYZ, ns, cfl, vnn, mx, mn ), "XYZ " + testStream.str() );
          }
        }
      }
    }
  }

  if( tester.ok() ){ std::cout << "\nPASS\n"; return 0; }
  else             { std::cout << "\nFAIL\n"; return -1;}
}

