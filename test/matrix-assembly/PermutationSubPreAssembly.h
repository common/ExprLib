/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef PERMUTATIONSUBPREASSEMBLY_H_
#define PERMUTATIONSUBPREASSEMBLY_H_


#include <expression/matrix-assembly/PermutationSubMatrix.h>

using CellFieldType = SpatialOps::SVolField;
using SVFType = SpatialOps::SingleValueField;

using Expr::matrix::sensitivity;

namespace permutationsubtest
{
  using MatrixType = Expr::matrix::PermutationSubMatrix<CellFieldType>;

  /*
   * @brief test that we can build an empty matrix
   */
  bool test_build_empty()
  {
    try{
      MatrixType D;
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can set a row to the identity matrix row
   */
  bool test_set_identity_row()
  {
    MatrixType D;
    try{
      D.set_identity_row( 0 );
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can swap rows
   */
  bool test_swap_rows()
  {
    MatrixType D;
    try{
      D.swap_rows( 0,1 );
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can set the row map directly
   */
  bool test_set_row_map()
  {
    MatrixType D;
    std::map<Expr::matrix::OrdinalType,Expr::matrix::OrdinalType> rowMap;
    rowMap[0] = 1;
    rowMap[1] = 3;
    rowMap[2] = 2;
    rowMap[3] = 0;
    try{
      D.set_row_map( rowMap );
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test for error if we set identity after finalizing
   */
  bool test_throw_set_identity_finalize()
  {
    MatrixType D;
    D.finalize();
    try{
      D.set_identity_row( 0 );
    }
    catch( const std::exception& e ){return true;}
    return false;
  }
  /*
   * @brief test for error if we swap rows after finalizing
   */
  bool test_throw_swap_rows_finalize()
  {
    MatrixType D;
    D.finalize();
    try{
      D.swap_rows( 0, 1 );
    }
    catch( const std::exception& e ){return true;}
    return false;
  }
}





#endif /* PERMUTATIONSUBPREASSEMBLY_H_ */
