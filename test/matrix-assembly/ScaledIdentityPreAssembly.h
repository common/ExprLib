/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SCALEDIDENTITYPREASSEMBLY_H_
#define SCALEDIDENTITYPREASSEMBLY_H_

#include <expression/matrix-assembly/ScaledIdentityMatrix.h>

using CellFieldType = SpatialOps::SVolField;
using SVFType = SpatialOps::SingleValueField;

using Expr::matrix::sensitivity;

namespace scaledidentityttest
{
  using MatrixType = Expr::matrix::ScaledIdentityMatrix<CellFieldType>;

  /*
   * @brief test that we can build an empty matrix
   */
  bool test_build_empty()
  {
    try{
      MatrixType Y;
    }
    catch( const std::exception& e ){return false;}
    return true;
  }

  /*
   * @class InsertElementTags
   *
   * Just a class that builds some tags for element insertion tests.
   */
  struct InsertElementTestTags
  {
    const Expr::Tag aTag = Expr::Tag( "a", Expr::STATE_NONE );
    const Expr::Tag bTag = Expr::Tag( "b", Expr::STATE_NONE );
    const Expr::Tag cTag = Expr::Tag( "c", Expr::STATE_NONE );
    const Expr::Tag dTag = Expr::Tag( "d", Expr::STATE_NONE );
    const Expr::Tag eTag = Expr::Tag( "e", Expr::STATE_NONE );
  };
  /*
   * @brief test that we can add element: cellfield
   */
  bool test_set_scale_cellfield()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    try{
      Y.scale<CellFieldType>() = tags.aTag;
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can add element: cellfield sensitivity
   */
  bool test_set_scale_cellfield_sens()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    try{
      Y.scale<CellFieldType>() = sensitivity( tags.bTag, tags.aTag );
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can add element: svf
   */
  bool test_set_scale_svf()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    try{
      Y.scale<SpatialOps::SingleValueField>() = tags.bTag;
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can add element: svf sensitivity
   */
  bool test_set_scale_svf_sens()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    try{
      Y.scale<SpatialOps::SingleValueField>() = sensitivity( tags.dTag, tags.eTag );
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test that we can add element: double
   */
  bool test_set_scale_double()
  {
    MatrixType Y;
    try{
      Y.scale<double>() = 1.2;
    }
    catch( const std::exception& e ){return false;}
    return true;
  }
  /*
   * @brief test for error if we insert cell field after finalizing
   */
  bool test_throw_set_scale_cell_after_finalize()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    Y.scale<CellFieldType>() = tags.aTag;
    Y.finalize();
    try{
      Y.scale<CellFieldType>() = tags.aTag;
    }
    catch( const std::exception& e ){return true;}
    return false;
  }
  /*
   * @brief test for error if we trigger sensitivities before finalizing
   */
  bool test_throw_if_sens_before_finalize()
  {
    MatrixType Y;
    InsertElementTestTags tags;
    Y.scale<CellFieldType>() = tags.aTag;
    try{
      auto pairs = Y.sensitivity_pairs();
    }
    catch( const std::exception& e ){return true;}
    return false;
  }
}


#endif /* SCALEDIDENTITYPREASSEMBLY_H_ */
