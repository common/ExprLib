/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/ExprPatch.h>
#include <expression/matrix-assembly/MatrixExpression.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/Compounds.h>
#include <expression/matrix-assembly/compressible-reactive-flow/AcousticPreconditioner.h>

#include "Matrix.h"

#include "../TestHelper.h"

enum SpatialDimensions { X, Y, Z, XY, XZ, YZ, XYZ };
enum MatrixCalculation { EMPLACE, ADDIN, SUBIN };

bool run_preconditioner_test( const SpatialDimensions dimensions,
                              const MatrixCalculation operation,
                              const Expr::matrix::OrdinalType nspecies,
                              const Expr::matrix::OrdinalType extraRows )
{
  using SVolFieldT = SpatialOps::SVolField;
  using SVFT = SpatialOps::SingleValueField;
  using ConstantType = Expr::ConstantExpr<SVolFieldT>::Builder;
  using ConstantSVFType = Expr::ConstantExpr<SVFT>::Builder;
  using PlaceHolderType = Expr::PlaceHolder<SVolFieldT>::Builder;
  using MatrixExprType = Expr::matrix::MatrixExpression<SVolFieldT>::Builder;
  using PreconditionerT = Expr::matrix::AcousticPreconditionerAssembler<SVolFieldT>;
  using SparseMatrixT = Expr::matrix::SparseMatrix<SVolFieldT>;
  using ScalarMatrixType = densematrix_for_test::Matrix<double>;

  // ------------------------------------------------------------------------------------------------

  const Expr::Tag xCoordTag( "x", Expr::STATE_NONE );
  const Expr::Tag yCoordTag( "y", Expr::STATE_NONE );
  const Expr::Tag zCoordTag( "z", Expr::STATE_NONE );

  const Expr::Tag pressureTag      ( "p"  , Expr::STATE_NONE ); const double pressureValue       = 1.e5;
  const Expr::Tag temperatureTag   ( "T"  , Expr::STATE_NONE ); const double temperatureValue    = 1.e3;
  const Expr::Tag densityTag       ( "rho", Expr::STATE_NONE ); const double densityValue        = 0.1;
  const Expr::Tag heatCapacityCpTag( "cp" , Expr::STATE_NONE ); const double heatCapacityCpValue = 1.e3;
  const Expr::Tag heatCapacityCvTag( "cv" , Expr::STATE_NONE ); const double heatCapacityCvValue = 1.e3;
  const Expr::Tag totalEnergyTag   ( "et" , Expr::STATE_NONE ); const double totalEnergyValue    = 1.e6;
  const Expr::Tag viscosityTag     ( "mu" , Expr::STATE_NONE ); const double viscosityValue      = 1.e-5;
  const Expr::Tag conductivityTag  ( "k"  , Expr::STATE_NONE ); const double conductivityValue   = 0.1;
  const Expr::Tag mmwTag           ( "mmw", Expr::STATE_NONE ); const double mmwValue            = 30.;
  const Expr::Tag soundSpeedTag    ( "a"  , Expr::STATE_NONE ); const double soundSpeedValue     = 600.0;

  Expr::Tag xVelocityTag; const double xVelocityValue = 1.e0;
  Expr::Tag yVelocityTag; const double yVelocityValue = 1.e-1;
  Expr::Tag zVelocityTag; const double zVelocityValue = 1.e-2;

  const double universalGasConstant = 8.e3;
  std::vector<std::string> speciesNamesRepo = {"A", "B", "C", "D", "E", "F", "G", "H"}; // up to eight species for testing
  std::vector<double> molecularWeightsRepo = {10., 9., 8., 7., 6., 5., 4., 3.}; // up to eight species for testing

  Expr::TagList massFractionTags, energyTags, diffusivityTags;
  std::vector<double> massFractionValues, energyValues, molecularWeights, diffusivityValues;

  if( nspecies > 1 ){
    for( std::size_t i=0; i<nspecies; ++i ){
      massFractionTags  .push_back( Expr::Tag( "Y_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
      massFractionValues.push_back( (double) (i + 1) * 1.e-1 );

      energyTags      .push_back( Expr::Tag( "e_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
      energyValues    .push_back( (double) (i + 1) * 1.e6 );

      diffusivityTags  .push_back( Expr::Tag( "D_" + speciesNamesRepo[i], Expr::STATE_NONE ) );
      diffusivityValues.push_back( (double) (i + 1) * 1.e-4 );

      molecularWeights.push_back( molecularWeightsRepo[i] );
    }
  }

  // ------------------------------------------------------------------------------------------------

  Expr::matrix::OrdinalType ndims;
  bool doX = false;
  bool doY = false;
  bool doZ = false;
  int nx = 1;
  int ny = 1;
  int nz = 1;
  Expr::matrix::OrdinalType xMomIdx;
  Expr::matrix::OrdinalType yMomIdx;
  Expr::matrix::OrdinalType zMomIdx;

  switch( dimensions ){
    case X:   doX = true;                         ndims = 1; nx = 16;                   xMomIdx = 1;                           break;
    case Y:               doY = true;             ndims = 1;          ny = 16;                       yMomIdx = 1;              break;
    case Z:                           doZ = true; ndims = 1;                   nz = 16;                           zMomIdx = 1; break;
    case XY:  doX = true; doY = true;             ndims = 2; nx = 8;  ny = 8;           xMomIdx = 1; yMomIdx = 2;              break;
    case XZ:  doX = true;             doZ = true; ndims = 2; nx = 8;           nz = 8;  xMomIdx = 1;              zMomIdx = 2; break;
    case YZ:              doY = true; doZ = true; ndims = 2;          ny = 8;  nz = 8;               yMomIdx = 1; zMomIdx = 2; break;
    case XYZ: doX = true; doY = true; doZ = true; ndims = 3; nx = 4;  ny = 4;  nz = 4;  xMomIdx = 1; yMomIdx = 2; zMomIdx = 3; break;
    default:
      break;
  }

  if( doX ) xVelocityTag = Expr::Tag( "vx", Expr::STATE_NONE );
  if( doY ) yVelocityTag = Expr::Tag( "vy", Expr::STATE_NONE );
  if( doZ ) zVelocityTag = Expr::Tag( "vz", Expr::STATE_NONE );

  const double lx = 1.0;
  const double ly = 0.9;
  const double lz = 0.8;

  const double dx = lx / ((double) nx);
  const double dy = ly / ((double) ny);
  const double dz = lz / ((double) nz);

  const SpatialOps::Grid grid( SpatialOps::IntVec(nx, ny, nz),
                               SpatialOps::DoubleVec(lx, ly, lz) );
  Expr::ExprPatch patch(nx, ny, nz);

  // ------------------------------------------------------------------------------------------------

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  // ------------------------------------------------------------------------------------------------

  rootIDs.insert( factory.register_expression( new PlaceHolderType( xCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( yCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( zCoordTag ) ) );

  rootIDs.insert( factory.register_expression( new ConstantType( pressureTag      , pressureValue       ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( temperatureTag   , temperatureValue    ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( densityTag       , densityValue        ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( heatCapacityCpTag, heatCapacityCpValue ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( heatCapacityCvTag, heatCapacityCvValue ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( totalEnergyTag   , totalEnergyValue    ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( viscosityTag     , viscosityValue      ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( conductivityTag  , conductivityValue   ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( soundSpeedTag    , soundSpeedValue     ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( mmwTag           , mmwValue            ) ) );

  if( doX ) rootIDs.insert( factory.register_expression( new ConstantType( xVelocityTag, xVelocityValue ) ) );
  if( doY ) rootIDs.insert( factory.register_expression( new ConstantType( yVelocityTag, yVelocityValue ) ) );
  if( doZ ) rootIDs.insert( factory.register_expression( new ConstantType( zVelocityTag, zVelocityValue ) ) );

  if( nspecies > 1){
    for( std::size_t i=0; i<nspecies; ++i ){
      rootIDs.insert( factory.register_expression( new ConstantType( massFractionTags[i], massFractionValues[i] ) ) );
      rootIDs.insert( factory.register_expression( new ConstantType( energyTags[i]      , energyValues[i]       ) ) );
      rootIDs.insert( factory.register_expression( new ConstantType( diffusivityTags[i] , diffusivityValues[i]  ) ) );
    }
  }

  // ------------------------------------------------------------------------------------------------

  const Expr::matrix::OrdinalType xVelIdx = xMomIdx;
  const Expr::matrix::OrdinalType yVelIdx = yMomIdx;
  const Expr::matrix::OrdinalType zVelIdx = zMomIdx;

  const Expr::matrix::OrdinalType egyIdx  = ndims + 1;
  const Expr::matrix::OrdinalType tempIdx = ndims + 1;

  const Expr::matrix::OrdinalType rhoConsIdx = 0;
  const Expr::matrix::OrdinalType rhoPrimIdx = 0;

  std::vector<Expr::matrix::OrdinalType> speciesDensityIndices;
  std::vector<Expr::matrix::OrdinalType> massFracIndices;

  for( std::size_t i=0; i<nspecies - 1; ++i ){
    speciesDensityIndices.push_back( ndims + 2 + i );
    massFracIndices.push_back( ndims + 2 + i );
  }

  // ------------------------------------------------------------------------------------------------

  const std::string matrixName = "mat";
  const Expr::matrix::OrdinalType nrows = nspecies + ndims + 1 + extraRows;
  const Expr::TagList matrixTags = Expr::matrix::matrix_tags( matrixName, nrows );

  boost::shared_ptr<SparseMatrixT> zeroMatrix = boost::make_shared<SparseMatrixT>( "zero" );
  zeroMatrix->finalize();

  boost::shared_ptr<PreconditionerT> acousticPreconditioner =
    boost::make_shared<PreconditionerT>( "state transform test",
                                         universalGasConstant,
                                         molecularWeights,
                                         densityTag,
                                         heatCapacityCvTag,
                                         heatCapacityCpTag,
                                         viscosityTag,
                                         conductivityTag,
                                         totalEnergyTag,
                                         xCoordTag,
                                         yCoordTag,
                                         zCoordTag,
                                         xVelocityTag,
                                         yVelocityTag,
                                         zVelocityTag,
                                         soundSpeedTag,
                                         temperatureTag,
                                         pressureTag,
                                         mmwTag,
                                         energyTags,
                                         massFractionTags,
                                         diffusivityTags,
                                         rhoConsIdx,
                                         rhoPrimIdx,
                                         egyIdx,
                                         tempIdx,
                                         xMomIdx,
                                         yMomIdx,
                                         zMomIdx,
                                         xVelIdx,
                                         yVelIdx,
                                         zVelIdx,
                                         speciesDensityIndices,
                                         massFracIndices );

  if( operation == EMPLACE ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, acousticPreconditioner ) ) );
  }
  else if( operation == ADDIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix + acousticPreconditioner ) ) );
  }
  else if( operation == SUBIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix - acousticPreconditioner ) ) );
  }

  // ------------------------------------------------------------------------------------------------

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );

  grid.set_coord<SpatialOps::XDIR>( fml.field_ref<SVolFieldT>( xCoordTag ) );
  grid.set_coord<SpatialOps::YDIR>( fml.field_ref<SVolFieldT>( yCoordTag ) );
  grid.set_coord<SpatialOps::ZDIR>( fml.field_ref<SVolFieldT>( zCoordTag ) );

  tree.execute_tree();

  // ------------------------------------------------------------------------------------------------

  ScalarMatrixType reference( nrows );

  double s = 0.0;
  double maxD = 0.0;

  maxD = std::max( conductivityValue / (densityValue * heatCapacityCpValue), viscosityValue / densityValue );
  if( nspecies > 1 ){
    for( int i=0; i<nspecies; ++i ){
      maxD = std::max( maxD, diffusivityValues[i] / densityValue );
    }
  }

  if( doX ) s += xVelocityValue * xVelocityValue;
  if( doY ) s += yVelocityValue * yVelocityValue;
  if( doZ ) s += zVelocityValue * zVelocityValue;
  s = std::sqrt( s );

  double minDx = 1.e305;
  if( doX ) minDx = std::min( minDx, dx );
  if( doY ) minDx = std::min( minDx, dy );
  if( doZ ) minDx = std::min( minDx, dz );

  const double epsilon = 1.e-5;

  const double rhoP = densityValue / pressureValue;
  const double rhoT = - densityValue / temperatureValue;

  const double Ur = std::max( std::max( s, epsilon * soundSpeedValue ), maxD / minDx );
  const double theta = (1. / ( ( Ur * Ur ) - rhoT / ( densityValue * heatCapacityCpValue ) ) ) / rhoP;

  std::vector<double> zeta(nspecies - 1);
  std::vector<double> eps(nspecies - 1);
  for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
    zeta[i] = mmwValue * ( 1. / molecularWeights[i] - 1. / molecularWeights[nspecies - 1]);
    eps[i] = energyValues[i] - energyValues[nspecies - 1];
  }

  // density row
  reference( rhoConsIdx, rhoPrimIdx ) = theta;
  reference( rhoConsIdx, tempIdx )    = rhoT * (1. - theta);
  for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
    reference( rhoConsIdx, massFracIndices[i] ) = densityValue * (theta - 1.) * zeta[i];

  // x-momentum row
  if( doX ){
    reference( xMomIdx, rhoPrimIdx ) = theta * xVelocityValue;
    reference( xMomIdx, xVelIdx )    = densityValue;
    reference( xMomIdx, tempIdx )    = rhoT * (1. - theta) * xVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      reference( xMomIdx, massFracIndices[i] ) = xVelocityValue * densityValue * (theta - 1.) * zeta[i];
  }

  // y-momentum row
  if( doY ){
    reference( yMomIdx, rhoPrimIdx ) = theta * yVelocityValue;
    reference( yMomIdx, yVelIdx )    = densityValue;
    reference( yMomIdx, tempIdx )    = rhoT * (1. - theta) * yVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      reference( yMomIdx, massFracIndices[i] ) = yVelocityValue * densityValue * (theta - 1.) * zeta[i];
  }

  // z-momentum row
  if( doZ ){
    reference( zMomIdx, rhoPrimIdx ) = theta * zVelocityValue;
    reference( zMomIdx, zVelIdx )    = densityValue;
    reference( zMomIdx, tempIdx )    = rhoT * (1. - theta) * zVelocityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      reference( zMomIdx, massFracIndices[i] ) = zVelocityValue * densityValue * (theta - 1.) * zeta[i];
  }

  // energy row
  reference( egyIdx, rhoPrimIdx ) = totalEnergyValue * theta;
  if( doX ) reference( egyIdx, xVelIdx ) = densityValue * xVelocityValue;
  if( doY ) reference( egyIdx, yVelIdx ) = densityValue * yVelocityValue;
  if( doZ ) reference( egyIdx, zVelIdx ) = densityValue * zVelocityValue;
  reference( egyIdx, tempIdx ) = totalEnergyValue * rhoT * (1. - theta) + densityValue * heatCapacityCvValue;
  for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
    reference( egyIdx, massFracIndices[i] ) = eps[i] * densityValue + densityValue * totalEnergyValue * (theta - 1.) * zeta[i];

  // species rows
  for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
    const Expr::matrix::OrdinalType rhoyidx = speciesDensityIndices[i];
    const Expr::matrix::OrdinalType yidx = massFracIndices[i];
    const double Y = massFractionValues[i];

    reference( rhoyidx, rhoPrimIdx ) = theta * Y;
    reference( rhoyidx, tempIdx )    = Y * rhoT * (1. - theta);
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i )
      reference( rhoyidx, massFracIndices[i] ) = densityValue * (theta - 1.) * Y * zeta[i];

    reference( rhoyidx, yidx ) += densityValue;
  }

  if( operation == SUBIN ){
    reference.multiply_by_scale(-1.0);
  }

  // ------------------------------------------------------------------------------------------------

  TestHelper tester( false );

  for( const auto& tag : matrixTags ){
    fml.field_ref<SVolFieldT>( tag ).add_device( CPU_INDEX );
  }
  for( Expr::matrix::OrdinalType row=0; row<nrows; ++row ){
    for( Expr::matrix::OrdinalType col=0; col<nrows; ++col ){
      tester( field_equal( reference(row, col), fml.field_ref<SVolFieldT>( matrixTags[row * nrows + col] ), 1.e-8),
              boost::lexical_cast<std::string>( row ) + ", " + boost::lexical_cast<std::string>( col ) );
    }
  }
  return tester.ok();
}


int main()
{
  TestHelper tester( true );

  tester( run_preconditioner_test( X  , EMPLACE, 1, 0 ), "X   1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Y  , EMPLACE, 1, 0 ), "Y   1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Z  , EMPLACE, 1, 0 ), "Z   1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XY , EMPLACE, 1, 0 ), "XY  1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XZ , EMPLACE, 1, 0 ), "XZ  1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( YZ , EMPLACE, 1, 0 ), "YZ  1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XYZ, EMPLACE, 1, 0 ), "XYZ 1 species 0 extrarows emplace" );
  tester( run_preconditioner_test( X  , EMPLACE, 1, 2 ), "X   1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( Y  , EMPLACE, 1, 2 ), "Y   1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( Z  , EMPLACE, 1, 2 ), "Z   1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XY , EMPLACE, 1, 2 ), "XY  1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XZ , EMPLACE, 1, 2 ), "XZ  1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( YZ , EMPLACE, 1, 2 ), "YZ  1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XYZ, EMPLACE, 1, 2 ), "XYZ 1 species 2 extrarows emplace" );
  tester( run_preconditioner_test( X  , EMPLACE, 3, 0 ), "X   3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Y  , EMPLACE, 3, 0 ), "Y   3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Z  , EMPLACE, 3, 0 ), "Z   3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XY , EMPLACE, 3, 0 ), "XY  3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XZ , EMPLACE, 3, 0 ), "XZ  3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( YZ , EMPLACE, 3, 0 ), "YZ  3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XYZ, EMPLACE, 3, 0 ), "XYZ 3 species 0 extrarows emplace" );
  tester( run_preconditioner_test( X  , EMPLACE, 3, 2 ), "X   3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( Y  , EMPLACE, 3, 2 ), "Y   3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( Z  , EMPLACE, 3, 2 ), "Z   3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XY , EMPLACE, 3, 2 ), "XY  3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XZ , EMPLACE, 3, 2 ), "XZ  3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( YZ , EMPLACE, 3, 2 ), "YZ  3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( XYZ, EMPLACE, 3, 2 ), "XYZ 3 species 2 extrarows emplace" );
  tester( run_preconditioner_test( X  , EMPLACE, 6, 0 ), "X   6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Y  , EMPLACE, 6, 0 ), "Y   6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( Z  , EMPLACE, 6, 0 ), "Z   6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XY , EMPLACE, 6, 0 ), "XY  6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XZ , EMPLACE, 6, 0 ), "XZ  6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( YZ , EMPLACE, 6, 0 ), "YZ  6 species 0 extrarows emplace" );
  tester( run_preconditioner_test( XYZ, EMPLACE, 6, 0 ), "XYZ 6 species 0 extrarows emplace" );

  tester( run_preconditioner_test( X  , ADDIN, 1, 0 ), "X   1 species 0 extrarows addin" );
  tester( run_preconditioner_test( Y  , ADDIN, 1, 0 ), "Y   1 species 0 extrarows addin" );
  tester( run_preconditioner_test( Z  , ADDIN, 1, 0 ), "Z   1 species 0 extrarows addin" );
  tester( run_preconditioner_test( XY , ADDIN, 1, 0 ), "XY  1 species 0 extrarows addin" );
  tester( run_preconditioner_test( XZ , ADDIN, 1, 0 ), "XZ  1 species 0 extrarows addin" );
  tester( run_preconditioner_test( YZ , ADDIN, 1, 0 ), "YZ  1 species 0 extrarows addin" );
  tester( run_preconditioner_test( XYZ, ADDIN, 1, 0 ), "XYZ 1 species 0 extrarows addin" );
  tester( run_preconditioner_test( X  , ADDIN, 1, 2 ), "X   1 species 2 extrarows addin" );
  tester( run_preconditioner_test( Y  , ADDIN, 1, 2 ), "Y   1 species 2 extrarows addin" );
  tester( run_preconditioner_test( Z  , ADDIN, 1, 2 ), "Z   1 species 2 extrarows addin" );
  tester( run_preconditioner_test( XY , ADDIN, 1, 2 ), "XY  1 species 2 extrarows addin" );
  tester( run_preconditioner_test( XZ , ADDIN, 1, 2 ), "XZ  1 species 2 extrarows addin" );
  tester( run_preconditioner_test( YZ , ADDIN, 1, 2 ), "YZ  1 species 2 extrarows addin" );
  tester( run_preconditioner_test( XYZ, ADDIN, 1, 2 ), "XYZ 1 species 2 extrarows addin" );
  tester( run_preconditioner_test( X  , ADDIN, 3, 0 ), "X   3 species 0 extrarows addin" );
  tester( run_preconditioner_test( Y  , ADDIN, 3, 0 ), "Y   3 species 0 extrarows addin" );
  tester( run_preconditioner_test( Z  , ADDIN, 3, 0 ), "Z   3 species 0 extrarows addin" );
  tester( run_preconditioner_test( XY , ADDIN, 3, 0 ), "XY  3 species 0 extrarows addin" );
  tester( run_preconditioner_test( XZ , ADDIN, 3, 0 ), "XZ  3 species 0 extrarows addin" );
  tester( run_preconditioner_test( YZ , ADDIN, 3, 0 ), "YZ  3 species 0 extrarows addin" );
  tester( run_preconditioner_test( XYZ, ADDIN, 3, 0 ), "XYZ 3 species 0 extrarows addin" );
  tester( run_preconditioner_test( X  , ADDIN, 3, 2 ), "X   3 species 2 extrarows addin" );
  tester( run_preconditioner_test( Y  , ADDIN, 3, 2 ), "Y   3 species 2 extrarows addin" );
  tester( run_preconditioner_test( Z  , ADDIN, 3, 2 ), "Z   3 species 2 extrarows addin" );
  tester( run_preconditioner_test( XY , ADDIN, 3, 2 ), "XY  3 species 2 extrarows addin" );
  tester( run_preconditioner_test( XZ , ADDIN, 3, 2 ), "XZ  3 species 2 extrarows addin" );
  tester( run_preconditioner_test( YZ , ADDIN, 3, 2 ), "YZ  3 species 2 extrarows addin" );
  tester( run_preconditioner_test( XYZ, ADDIN, 3, 2 ), "XYZ 3 species 2 extrarows addin" );
  tester( run_preconditioner_test( X  , ADDIN, 6, 0 ), "X   6 species 0 extrarows addin" );
  tester( run_preconditioner_test( Y  , ADDIN, 6, 0 ), "Y   6 species 0 extrarows addin" );
  tester( run_preconditioner_test( Z  , ADDIN, 6, 0 ), "Z   6 species 0 extrarows addin" );
  tester( run_preconditioner_test( XY , ADDIN, 6, 0 ), "XY  6 species 0 extrarows addin" );
  tester( run_preconditioner_test( XZ , ADDIN, 6, 0 ), "XZ  6 species 0 extrarows addin" );
  tester( run_preconditioner_test( YZ , ADDIN, 6, 0 ), "YZ  6 species 0 extrarows addin" );
  tester( run_preconditioner_test( XYZ, ADDIN, 6, 0 ), "XYZ 6 species 0 extrarows addin" );

  tester( run_preconditioner_test( X  , SUBIN, 1, 0 ), "X   1 species 0 extrarows subin" );
  tester( run_preconditioner_test( Y  , SUBIN, 1, 0 ), "Y   1 species 0 extrarows subin" );
  tester( run_preconditioner_test( Z  , SUBIN, 1, 0 ), "Z   1 species 0 extrarows subin" );
  tester( run_preconditioner_test( XY , SUBIN, 1, 0 ), "XY  1 species 0 extrarows subin" );
  tester( run_preconditioner_test( XZ , SUBIN, 1, 0 ), "XZ  1 species 0 extrarows subin" );
  tester( run_preconditioner_test( YZ , SUBIN, 1, 0 ), "YZ  1 species 0 extrarows subin" );
  tester( run_preconditioner_test( XYZ, SUBIN, 1, 0 ), "XYZ 1 species 0 extrarows subin" );
  tester( run_preconditioner_test( X  , SUBIN, 1, 2 ), "X   1 species 2 extrarows subin" );
  tester( run_preconditioner_test( Y  , SUBIN, 1, 2 ), "Y   1 species 2 extrarows subin" );
  tester( run_preconditioner_test( Z  , SUBIN, 1, 2 ), "Z   1 species 2 extrarows subin" );
  tester( run_preconditioner_test( XY , SUBIN, 1, 2 ), "XY  1 species 2 extrarows subin" );
  tester( run_preconditioner_test( XZ , SUBIN, 1, 2 ), "XZ  1 species 2 extrarows subin" );
  tester( run_preconditioner_test( YZ , SUBIN, 1, 2 ), "YZ  1 species 2 extrarows subin" );
  tester( run_preconditioner_test( XYZ, SUBIN, 1, 2 ), "XYZ 1 species 2 extrarows subin" );
  tester( run_preconditioner_test( X  , SUBIN, 3, 0 ), "X   3 species 0 extrarows subin" );
  tester( run_preconditioner_test( Y  , SUBIN, 3, 0 ), "Y   3 species 0 extrarows subin" );
  tester( run_preconditioner_test( Z  , SUBIN, 3, 0 ), "Z   3 species 0 extrarows subin" );
  tester( run_preconditioner_test( XY , SUBIN, 3, 0 ), "XY  3 species 0 extrarows subin" );
  tester( run_preconditioner_test( XZ , SUBIN, 3, 0 ), "XZ  3 species 0 extrarows subin" );
  tester( run_preconditioner_test( YZ , SUBIN, 3, 0 ), "YZ  3 species 0 extrarows subin" );
  tester( run_preconditioner_test( XYZ, SUBIN, 3, 0 ), "XYZ 3 species 0 extrarows subin" );
  tester( run_preconditioner_test( X  , SUBIN, 3, 2 ), "X   3 species 2 extrarows subin" );
  tester( run_preconditioner_test( Y  , SUBIN, 3, 2 ), "Y   3 species 2 extrarows subin" );
  tester( run_preconditioner_test( Z  , SUBIN, 3, 2 ), "Z   3 species 2 extrarows subin" );
  tester( run_preconditioner_test( XY , SUBIN, 3, 2 ), "XY  3 species 2 extrarows subin" );
  tester( run_preconditioner_test( XZ , SUBIN, 3, 2 ), "XZ  3 species 2 extrarows subin" );
  tester( run_preconditioner_test( YZ , SUBIN, 3, 2 ), "YZ  3 species 2 extrarows subin" );
  tester( run_preconditioner_test( XYZ, SUBIN, 3, 2 ), "XYZ 3 species 2 extrarows subin" );
  tester( run_preconditioner_test( X  , SUBIN, 6, 0 ), "X   6 species 0 extrarows subin" );
  tester( run_preconditioner_test( Y  , SUBIN, 6, 0 ), "Y   6 species 0 extrarows subin" );
  tester( run_preconditioner_test( Z  , SUBIN, 6, 0 ), "Z   6 species 0 extrarows subin" );
  tester( run_preconditioner_test( XY , SUBIN, 6, 0 ), "XY  6 species 0 extrarows subin" );
  tester( run_preconditioner_test( XZ , SUBIN, 6, 0 ), "XZ  6 species 0 extrarows subin" );
  tester( run_preconditioner_test( YZ , SUBIN, 6, 0 ), "YZ  6 species 0 extrarows subin" );
  tester( run_preconditioner_test( XYZ, SUBIN, 6, 0 ), "XYZ 6 species 0 extrarows subin" );

  if( tester.ok() ){ std::cout << "\nPASS\n"; return 0; }
  else             { std::cout << "\nFAIL\n"; return -1;}
}

