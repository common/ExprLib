/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <iostream>
#include <algorithm>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FieldComparisons.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/ExprPatch.h>
#include <expression/matrix-assembly/MatrixExpression.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/compressible-reactive-flow/ViscousFluxAssembler.h>
#include <expression/matrix-assembly/Compounds.h>

#include "Matrix.h"

#include "../TestHelper.h"

enum SpatialDimensions { X, Y, Z, XY, XZ, YZ, XYZ };
enum MatrixCalculation { EMPLACE, ADDIN, SUBIN };

bool run_viscous_flux_test( const SpatialDimensions dimensions,
                            const MatrixCalculation operation,
                            const Expr::matrix::OrdinalType nspecies,
                            const Expr::matrix::OrdinalType extraRows )
{
  using SVolFieldT = SpatialOps::SVolField;
  using ConstantType = Expr::ConstantExpr<SVolFieldT>::Builder;
  using PlaceHolderType = Expr::PlaceHolder<SVolFieldT>::Builder;
  using MatrixExprType = Expr::matrix::MatrixExpression<SVolFieldT>::Builder;
  using SparseMatrixT = Expr::matrix::SparseMatrix<SVolFieldT>;
  using ViscousFluxJacT = Expr::matrix::ViscousFluxAssembler<SVolFieldT>;
  using ScalarMatrixType = densematrix_for_test::Matrix<double>;

  // ------------------------------------------------------------------------------------------------

  const Expr::Tag xCoordTag( "x", Expr::STATE_NONE );
  const Expr::Tag yCoordTag( "y", Expr::STATE_NONE );
  const Expr::Tag zCoordTag( "z", Expr::STATE_NONE );

  const Expr::Tag viscosityTag   ( "mu" , Expr::STATE_NONE ); const double viscosityValue    = 2.0;
  const Expr::Tag conductivityTag( "k"  , Expr::STATE_NONE ); const double conductivityValue = 3.0;
  const Expr::Tag densityTag     ( "rho", Expr::STATE_NONE ); const double densityValue      = 4.0;

  Expr::Tag xVelocityTag; const double xVelocityValue = 5.0;
  Expr::Tag yVelocityTag; const double yVelocityValue = 6.0;
  Expr::Tag zVelocityTag; const double zVelocityValue = 7.0;

  std::vector<std::string> speciesNames = {"A", "B", "C", "D", "E", "F", "G", "H"}; // up to eight species for testing

  Expr::TagList diffusivityTags, enthalpyTags;
  std::vector<double> diffusivityValues, enthalpyValues;
  if( nspecies > 1 ){
    for( std::size_t i=0; i<nspecies; ++i ){
      diffusivityTags  .push_back( Expr::Tag( "D_" + speciesNames[i], Expr::STATE_NONE ) );
      diffusivityValues.push_back( (double) (i + 1) * 1.e-2 );

      enthalpyTags  .push_back( Expr::Tag( "h_" + speciesNames[i], Expr::STATE_NONE ) );
      enthalpyValues.push_back( (double) (i + 1) * 1.e3 );
    }
  }

  // ------------------------------------------------------------------------------------------------

  Expr::matrix::OrdinalType ndims;
  bool doX = false;
  bool doY = false;
  bool doZ = false;
  int nx = 1;
  int ny = 1;
  int nz = 1;
  Expr::matrix::OrdinalType xMomIdx;
  Expr::matrix::OrdinalType yMomIdx;
  Expr::matrix::OrdinalType zMomIdx;

  switch( dimensions ){
    case X:   doX = true;                         ndims = 1; nx = 16;                   xMomIdx = 1;                           break;
    case Y:               doY = true;             ndims = 1;          ny = 16;                       yMomIdx = 1;              break;
    case Z:                           doZ = true; ndims = 1;                   nz = 16;                           zMomIdx = 1; break;
    case XY:  doX = true; doY = true;             ndims = 2; nx = 8;  ny = 8;           xMomIdx = 1; yMomIdx = 2;              break;
    case XZ:  doX = true;             doZ = true; ndims = 2; nx = 8;           nz = 8;  xMomIdx = 1;              zMomIdx = 2; break;
    case YZ:              doY = true; doZ = true; ndims = 2;          ny = 8;  nz = 8;               yMomIdx = 1; zMomIdx = 2; break;
    case XYZ: doX = true; doY = true; doZ = true; ndims = 3; nx = 4;  ny = 4;  nz = 4;  xMomIdx = 1; yMomIdx = 2; zMomIdx = 3; break;
    default:
      break;
  }

  if( doX ) xVelocityTag = Expr::Tag( "vx", Expr::STATE_NONE );
  if( doY ) yVelocityTag = Expr::Tag( "vy", Expr::STATE_NONE );
  if( doZ ) zVelocityTag = Expr::Tag( "vz", Expr::STATE_NONE );

  const double lx = 1.0;
  const double ly = 0.9;
  const double lz = 0.8;

  const double dx = lx / ((double) nx);
  const double dy = ly / ((double) ny);
  const double dz = lz / ((double) nz);

  const SpatialOps::Grid grid( SpatialOps::IntVec(nx, ny, nz),
                               SpatialOps::DoubleVec(lx, ly, lz) );
  Expr::ExprPatch patch(nx, ny, nz);

  // ------------------------------------------------------------------------------------------------

  Expr::FieldManagerList& fml = patch.field_manager_list();
  Expr::ExpressionFactory factory;
  std::set<Expr::ExpressionID> rootIDs;

  // ------------------------------------------------------------------------------------------------

  rootIDs.insert( factory.register_expression( new PlaceHolderType( xCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( yCoordTag ) ) );
  rootIDs.insert( factory.register_expression( new PlaceHolderType( zCoordTag ) ) );

  rootIDs.insert( factory.register_expression( new ConstantType( viscosityTag   , viscosityValue    ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( conductivityTag, conductivityValue ) ) );
  rootIDs.insert( factory.register_expression( new ConstantType( densityTag     , densityValue      ) ) );

  if( doX ) rootIDs.insert( factory.register_expression( new ConstantType( xVelocityTag, xVelocityValue ) ) );
  if( doY ) rootIDs.insert( factory.register_expression( new ConstantType( yVelocityTag, yVelocityValue ) ) );
  if( doZ ) rootIDs.insert( factory.register_expression( new ConstantType( zVelocityTag, zVelocityValue ) ) );

  if( nspecies > 1){
    for( std::size_t i=0; i<nspecies; ++i ){
      rootIDs.insert( factory.register_expression( new ConstantType( diffusivityTags[i], diffusivityValues[i] ) ) );
      rootIDs.insert( factory.register_expression( new ConstantType( enthalpyTags[i]   , enthalpyValues[i]    ) ) );
    }
  }

  // ------------------------------------------------------------------------------------------------

  const Expr::matrix::OrdinalType xVelIdx = xMomIdx;
  const Expr::matrix::OrdinalType yVelIdx = yMomIdx;
  const Expr::matrix::OrdinalType zVelIdx = zMomIdx;

  const Expr::matrix::OrdinalType egyIdx  = ndims + 1;
  const Expr::matrix::OrdinalType tempIdx = ndims + 1;

  std::vector<Expr::matrix::OrdinalType> speciesDensityIndices;
  std::vector<Expr::matrix::OrdinalType> massFracIndices;
  for( std::size_t i=0; i<nspecies - 1; ++i ){
    speciesDensityIndices.push_back( ndims + 2 + i );
    massFracIndices.push_back( ndims + 2 + i );
  }

  // ------------------------------------------------------------------------------------------------

  const std::string matrixName = "mat";
  const Expr::matrix::OrdinalType nrows = nspecies + ndims + 1 + extraRows;
  const Expr::TagList matrixTags = Expr::matrix::matrix_tags( matrixName, nrows );

  boost::shared_ptr<SparseMatrixT> zeroMatrix = boost::make_shared<SparseMatrixT>( "zero" );
  zeroMatrix->finalize();

  boost::shared_ptr<ViscousFluxJacT> viscousFluxJacobian =
    boost::make_shared<ViscousFluxJacT>( "viscous flux test",
                                         densityTag,
                                         viscosityTag,
                                         conductivityTag,
                                         xCoordTag,
                                         yCoordTag,
                                         zCoordTag,
                                         xVelocityTag,
                                         yVelocityTag,
                                         zVelocityTag,
                                         enthalpyTags,
                                         diffusivityTags,
                                         egyIdx,
                                         tempIdx,
                                         xMomIdx,
                                         yMomIdx,
                                         zMomIdx,
                                         xVelIdx,
                                         yVelIdx,
                                         zVelIdx,
                                         speciesDensityIndices,
                                         massFracIndices );

  if( operation == EMPLACE ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, viscousFluxJacobian ) ) );
  }
  else if( operation == ADDIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix + viscousFluxJacobian ) ) );
  }
  else if( operation == SUBIN ){
    rootIDs.insert( factory.register_expression( new MatrixExprType( matrixTags, zeroMatrix - viscousFluxJacobian ) ) );
  }

  // ------------------------------------------------------------------------------------------------

  Expr::ExpressionTree tree( rootIDs, factory, patch.id() );
  tree.register_fields( fml );
  tree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );

  grid.set_coord<SpatialOps::XDIR>( fml.field_ref<SVolFieldT>( xCoordTag ) );
  grid.set_coord<SpatialOps::YDIR>( fml.field_ref<SVolFieldT>( yCoordTag ) );
  grid.set_coord<SpatialOps::ZDIR>( fml.field_ref<SVolFieldT>( zCoordTag ) );

  tree.execute_tree();

  // ------------------------------------------------------------------------------------------------

  ScalarMatrixType ref_ax( nrows );
  ScalarMatrixType ref_ay( nrows );
  ScalarMatrixType ref_az( nrows );

  const double xCoeff = - 2.0 / dx / dx;
  const double yCoeff = - 2.0 / dy / dy;
  const double zCoeff = - 2.0 / dz / dz;

  const double chi_visc = 4. / 3. * viscosityValue;

  if( doX ){
    ref_ax( xMomIdx, xVelIdx ) = xCoeff * chi_visc;
    ref_ax( egyIdx, xVelIdx )  = xCoeff * chi_visc * xVelocityValue;
    ref_ax( egyIdx, tempIdx )  = - xCoeff * conductivityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      ref_ax( egyIdx, massFracIndices[i] ) = - xCoeff * densityValue * diffusivityValues[i] * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );
      ref_ax( speciesDensityIndices[i], massFracIndices[i] ) = - xCoeff * densityValue * diffusivityValues[i];
    }
    if( doY ){
      ref_ax( yMomIdx, yVelIdx ) = xCoeff * viscosityValue;
      ref_ax( egyIdx, yVelIdx )  = xCoeff * viscosityValue * yVelocityValue;
    }
    if( doZ ){
      ref_ax( zMomIdx, zVelIdx ) = xCoeff * viscosityValue;
      ref_ax( egyIdx, zVelIdx )  = xCoeff * viscosityValue * zVelocityValue;
    }
  }

  if( doY ){
    ref_ay( yMomIdx, yVelIdx ) = yCoeff * chi_visc;
    ref_ay( egyIdx, yVelIdx )  = yCoeff * chi_visc * yVelocityValue;
    ref_ay( egyIdx, tempIdx )  = - yCoeff * conductivityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      ref_ay( egyIdx, massFracIndices[i] ) = - yCoeff * densityValue * diffusivityValues[i] * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );
      ref_ay( speciesDensityIndices[i], massFracIndices[i] ) = - yCoeff * densityValue * diffusivityValues[i];
    }
    if( doX ){
      ref_ay( xMomIdx, xVelIdx ) = yCoeff * viscosityValue;
      ref_ay( egyIdx, xVelIdx )  = yCoeff * viscosityValue * xVelocityValue;
    }
    if( doZ ){
      ref_ay( zMomIdx, zVelIdx ) = yCoeff * viscosityValue;
      ref_ay( egyIdx, zVelIdx )  = yCoeff * viscosityValue * zVelocityValue;
    }
  }

  if( doZ ){
    ref_az( zMomIdx, zVelIdx ) = zCoeff * chi_visc;
    ref_az( egyIdx, zVelIdx )  = zCoeff * chi_visc * zVelocityValue;
    ref_az( egyIdx, tempIdx )  = - zCoeff * conductivityValue;
    for( Expr::matrix::OrdinalType i=0; i<nspecies - 1; ++i ){
      ref_az( egyIdx, massFracIndices[i] ) = - zCoeff * densityValue * diffusivityValues[i] * ( enthalpyValues[i] - enthalpyValues[nspecies - 1] );
      ref_az( speciesDensityIndices[i], massFracIndices[i] ) = - zCoeff * densityValue * diffusivityValues[i];
    }
    if( doX ){
      ref_az( xMomIdx, xVelIdx ) = zCoeff * viscosityValue;
      ref_az( egyIdx, xVelIdx )  = zCoeff * viscosityValue * xVelocityValue;
    }
    if( doY ){
      ref_az( yMomIdx, yVelIdx ) = zCoeff * viscosityValue;
      ref_az( egyIdx, yVelIdx )  = zCoeff * viscosityValue * yVelocityValue;
    }
  }

  auto reference = ref_ax + ref_ay + ref_az;
  if( operation == SUBIN ){
    reference.multiply_by_scale(-1.0);
  }

  // ------------------------------------------------------------------------------------------------

  TestHelper tester( false );

  for( const auto& tag : matrixTags ){
    fml.field_ref<SVolFieldT>( tag ).add_device( CPU_INDEX );
  }
  for( Expr::matrix::OrdinalType row=0; row<nrows; ++row ){
    for( Expr::matrix::OrdinalType col=0; col<nrows; ++col ){
      tester( field_equal( reference(row, col), fml.field_ref<SVolFieldT>( matrixTags[row * nrows + col] ), 1.e-8),
              boost::lexical_cast<std::string>( row ) + ", " + boost::lexical_cast<std::string>( col ) );
    }
  }
  return tester.ok();
}


int main()
{
  TestHelper tester( true );

  tester( run_viscous_flux_test( X  , EMPLACE, 1, 0 ), "X   1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Y  , EMPLACE, 1, 0 ), "Y   1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Z  , EMPLACE, 1, 0 ), "Z   1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XY , EMPLACE, 1, 0 ), "XY  1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XZ , EMPLACE, 1, 0 ), "XZ  1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( YZ , EMPLACE, 1, 0 ), "YZ  1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XYZ, EMPLACE, 1, 0 ), "XYZ 1 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( X  , EMPLACE, 1, 2 ), "X   1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( Y  , EMPLACE, 1, 2 ), "Y   1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( Z  , EMPLACE, 1, 2 ), "Z   1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XY , EMPLACE, 1, 2 ), "XY  1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XZ , EMPLACE, 1, 2 ), "XZ  1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( YZ , EMPLACE, 1, 2 ), "YZ  1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XYZ, EMPLACE, 1, 2 ), "XYZ 1 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( X  , EMPLACE, 3, 0 ), "X   3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Y  , EMPLACE, 3, 0 ), "Y   3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Z  , EMPLACE, 3, 0 ), "Z   3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XY , EMPLACE, 3, 0 ), "XY  3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XZ , EMPLACE, 3, 0 ), "XZ  3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( YZ , EMPLACE, 3, 0 ), "YZ  3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XYZ, EMPLACE, 3, 0 ), "XYZ 3 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( X  , EMPLACE, 3, 2 ), "X   3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( Y  , EMPLACE, 3, 2 ), "Y   3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( Z  , EMPLACE, 3, 2 ), "Z   3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XY , EMPLACE, 3, 2 ), "XY  3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XZ , EMPLACE, 3, 2 ), "XZ  3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( YZ , EMPLACE, 3, 2 ), "YZ  3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( XYZ, EMPLACE, 3, 2 ), "XYZ 3 species 2 extrarows emplace" );
  tester( run_viscous_flux_test( X  , EMPLACE, 6, 0 ), "X   6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Y  , EMPLACE, 6, 0 ), "Y   6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( Z  , EMPLACE, 6, 0 ), "Z   6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XY , EMPLACE, 6, 0 ), "XY  6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XZ , EMPLACE, 6, 0 ), "XZ  6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( YZ , EMPLACE, 6, 0 ), "YZ  6 species 0 extrarows emplace" );
  tester( run_viscous_flux_test( XYZ, EMPLACE, 6, 0 ), "XYZ 6 species 0 extrarows emplace" );

  tester( run_viscous_flux_test( X  , ADDIN, 1, 0 ), "X   1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Y  , ADDIN, 1, 0 ), "Y   1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Z  , ADDIN, 1, 0 ), "Z   1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XY , ADDIN, 1, 0 ), "XY  1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XZ , ADDIN, 1, 0 ), "XZ  1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( YZ , ADDIN, 1, 0 ), "YZ  1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XYZ, ADDIN, 1, 0 ), "XYZ 1 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( X  , ADDIN, 1, 2 ), "X   1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( Y  , ADDIN, 1, 2 ), "Y   1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( Z  , ADDIN, 1, 2 ), "Z   1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XY , ADDIN, 1, 2 ), "XY  1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XZ , ADDIN, 1, 2 ), "XZ  1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( YZ , ADDIN, 1, 2 ), "YZ  1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XYZ, ADDIN, 1, 2 ), "XYZ 1 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( X  , ADDIN, 3, 0 ), "X   3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Y  , ADDIN, 3, 0 ), "Y   3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Z  , ADDIN, 3, 0 ), "Z   3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XY , ADDIN, 3, 0 ), "XY  3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XZ , ADDIN, 3, 0 ), "XZ  3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( YZ , ADDIN, 3, 0 ), "YZ  3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XYZ, ADDIN, 3, 0 ), "XYZ 3 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( X  , ADDIN, 3, 2 ), "X   3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( Y  , ADDIN, 3, 2 ), "Y   3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( Z  , ADDIN, 3, 2 ), "Z   3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XY , ADDIN, 3, 2 ), "XY  3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XZ , ADDIN, 3, 2 ), "XZ  3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( YZ , ADDIN, 3, 2 ), "YZ  3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( XYZ, ADDIN, 3, 2 ), "XYZ 3 species 2 extrarows add-in" );
  tester( run_viscous_flux_test( X  , ADDIN, 6, 0 ), "X   6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Y  , ADDIN, 6, 0 ), "Y   6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( Z  , ADDIN, 6, 0 ), "Z   6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XY , ADDIN, 6, 0 ), "XY  6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XZ , ADDIN, 6, 0 ), "XZ  6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( YZ , ADDIN, 6, 0 ), "YZ  6 species 0 extrarows add-in" );
  tester( run_viscous_flux_test( XYZ, ADDIN, 6, 0 ), "XYZ 6 species 0 extrarows add-in" );

  tester( run_viscous_flux_test( X  , SUBIN, 1, 0 ), "X   1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Y  , SUBIN, 1, 0 ), "Y   1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Z  , SUBIN, 1, 0 ), "Z   1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XY , SUBIN, 1, 0 ), "XY  1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XZ , SUBIN, 1, 0 ), "XZ  1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( YZ , SUBIN, 1, 0 ), "YZ  1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XYZ, SUBIN, 1, 0 ), "XYZ 1 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( X  , SUBIN, 1, 2 ), "X   1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( Y  , SUBIN, 1, 2 ), "Y   1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( Z  , SUBIN, 1, 2 ), "Z   1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XY , SUBIN, 1, 2 ), "XY  1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XZ , SUBIN, 1, 2 ), "XZ  1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( YZ , SUBIN, 1, 2 ), "YZ  1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XYZ, SUBIN, 1, 2 ), "XYZ 1 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( X  , SUBIN, 3, 0 ), "X   3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Y  , SUBIN, 3, 0 ), "Y   3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Z  , SUBIN, 3, 0 ), "Z   3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XY , SUBIN, 3, 0 ), "XY  3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XZ , SUBIN, 3, 0 ), "XZ  3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( YZ , SUBIN, 3, 0 ), "YZ  3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XYZ, SUBIN, 3, 0 ), "XYZ 3 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( X  , SUBIN, 3, 2 ), "X   3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( Y  , SUBIN, 3, 2 ), "Y   3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( Z  , SUBIN, 3, 2 ), "Z   3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XY , SUBIN, 3, 2 ), "XY  3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XZ , SUBIN, 3, 2 ), "XZ  3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( YZ , SUBIN, 3, 2 ), "YZ  3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( XYZ, SUBIN, 3, 2 ), "XYZ 3 species 2 extrarows sub-in" );
  tester( run_viscous_flux_test( X  , SUBIN, 6, 0 ), "X   6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Y  , SUBIN, 6, 0 ), "Y   6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( Z  , SUBIN, 6, 0 ), "Z   6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XY , SUBIN, 6, 0 ), "XY  6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XZ , SUBIN, 6, 0 ), "XZ  6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( YZ , SUBIN, 6, 0 ), "YZ  6 species 0 extrarows sub-in" );
  tester( run_viscous_flux_test( XYZ, SUBIN, 6, 0 ), "XYZ 6 species 0 extrarows sub-in" );

  if( tester.ok() ){ std::cout << "\nPASS\n"; return 0; }
  else             { std::cout << "\nFAIL\n"; return -1;}
}

