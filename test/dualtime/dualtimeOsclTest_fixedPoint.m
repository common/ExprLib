clc; clear all;

dsigma = 0.01;
dt = 0.01;

c0 = 1.0;
p0 = 1.0;
phipsi0 = [c0;p0];

phipsi = phipsi0;

TOL = 1e-10;
t = 0;
tend = 1.0;
totaliter = 0;
nt = 0;
disp('- run beginning');

k = 4.0;
k11 = 1*k;
k12 = 1*k;
k21 = -2*k;
k22 = -1*k;
K = [k11,k12;k21,k22];

phivec = zeros(1,1);
psivec = zeros(1,1);
i = 0;
while t < tend
    count = 0;
    resid = TOL + 1;
    phin = phipsi;
    i = i + 1;
    while resid > TOL && count < 1e4
        
        RHS = K*phipsi;
        
        TIME = (phipsi - phin)/dt;
        
        RES = RHS - TIME;
        
        dphipsi = dsigma/(1. + dsigma / dt)*eye(2)*RES;
        
        resid = max(abs(dphipsi)./abs(phipsi));
%         disp(['iter, norm = ', num2str(count), ', ', num2str(resid)]);
        phipsi = phipsi + dphipsi;
        count = count + 1;
        
        if sum(isnan(phipsi)) > 0
            error('- NaN detected');
        end
    end
    disp(['  - time step complete in ', num2str(count),' iterations, t = ', num2str(t)]);
    phivec(i) = phipsi(1);
    psivec(i) = phipsi(2);
    nt = nt + 1;
    t = t + dt;
    totaliter = totaliter + count;
end
phivec(end)
psivec(end)
disp(['- run complete in ', num2str(totaliter), ' iterations (', num2str(totaliter/nt),' iter/step)']);
plot(1:numel(phivec),phivec,1:numel(phivec),psivec)
