\documentclass[10pt]{article}

\usepackage[margin=1in]{geometry}

\newcommand{\exprlib}{\texttt{ExprLib}}
\newcommand{\code}[1]{\texttt{#1}}

\usepackage{parskip}

\usepackage{amssymb,amsmath,mathtools,nicefrac,esint,graphicx,microtype,subcaption,url,mhchem,hyperref}
\usepackage[mathscr]{euscript}
\usepackage[usenames,dvipsnames,svgnames]{xcolor}
\usepackage[super]{nth}

\newcommand{\dder}[2]{\frac{\mathrm{d} #1}{\mathrm{d} #2}} % total derivative
\newcommand{\idder}[2]{\mathrm{d} #1/\mathrm{d} #2} % total derivative
\renewcommand{\v}[1]{\boldsymbol{#1}} % vector
\newcommand{\m}[1]{\left[\mathrm{#1}\right]} % matrix, upright with square brackets
\newcommand{\pder}[2]{\frac{\partial #1}{\partial #2}} % partial derivative
\newcommand{\ipder}[2]{\partial #1/\partial #2} % inline partial derivative
\newcommand{\tr}{\intercal} % transpose
\newcommand{\const}[1]{\Big{|}_{#1}} % stuff that is held constant in a partial derivative

\usepackage{listings}
\definecolor{listinggray}{gray}{0.9}
\definecolor{lbcolor}{rgb}{0.9,0.9,0.9}
\lstset{
backgroundcolor=\color{lbcolor},
    tabsize=2,    
%   rulecolor=,
    language=[GNU]C++,
        basicstyle=\scriptsize,
        upquote=true,
        aboveskip={1.5\baselineskip},
        columns=fixed,
        showstringspaces=false,
        extendedchars=false,
        breaklines=true,
        prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
        frame=single,
        numbers=left,
        showtabs=false,
        showspaces=false,
        showstringspaces=false,
        identifierstyle=\ttfamily,
        keywordstyle=\color[rgb]{0,0,1},
        commentstyle=\color[rgb]{0.026,0.112,0.095},
        stringstyle=\color[rgb]{0.627,0.126,0.941},
        numberstyle=\color[rgb]{0.205, 0.142, 0.73},
%        \lstdefinestyle{C++}{language=C++,style=numbers}?.
}
\lstset{
    backgroundcolor=\color{lbcolor},
    tabsize=4,
  language=C++,
  captionpos=b,
  tabsize=3,
  frame=lines,
  numbers=left,
  numberstyle=\tiny,
  numbersep=5pt,
  breaklines=true,
  showstringspaces=false,
  basicstyle=\footnotesize,
%  identifierstyle=\color{magenta},
  keywordstyle=\color[rgb]{0,0,1},
  commentstyle=\color{DarkGreen},
  stringstyle=\color{red}
  }


















\title{An \exprlib{} Example: a Lotka-Volterra Model of a Predator and Two Competing Prey Species}
\author{Mike Hansen}
\date{}

\begin{document}

\maketitle

This document demonstrates how to solve a system of ordinary differential equations (ODEs) with the block implicit dual time integration capability in the Expression Library, or \exprlib{}.
The ODEs considered here are a highly nonlinear model from ecology, of two competing prey species and a predator species.
We first discuss the system of ODEs, then the block-implicit dual time formulation and corresponding \exprlib{} code, and finally we demonstrate some results found with the python post-processing utility provided by the Expression Library.

\section{ODEs to Solve}
\subsection{Lotka-Volterra Model}
The Lotka-Volterra models of predator-prey and competitive non-predatory species are a simple model of complex ecological phenomena.
The equations are nonlinear and can present chaotic behavior.
Here we combine two non-predatory (prey) species that compete for resources with a predator species.
There can be selectivities in both the prey competition and predation model, which might be imagined as one prey species being `bulky' while the other is `evasive.'
The bulky species can out-produce and out-consume the evasive species and will dominate the landscape in absence of predation.
But the bulky species is harmed more by predation, and provides for a greater predator population.
If predation is too great, the evasive prey species will dominate, but the bulky prey species will dominate if predation is insufficient.

The governing equations for the prey species populations, $x_1$ and $x_2$, and predator population $y$ are
\begin{equation}
	\begin{aligned}
		\dder{x_1}{t} &= r_1x_1\left(1 - \frac{x_1+\alpha_{12}x_2}{K_1}\right) - \beta_1x_1y, \\
		\dder{x_2}{t} &= r_2x_2\left(1 - \frac{x_2+\alpha_{21}x_1}{K_2}\right) - \beta_2x_2y, \\
		\dder{y}{t}   &= (\eta_1x_1 + \eta_2x_2)y - \delta y.
	\end{aligned}
	\label{eqn: lv3}
\end{equation}
In the above expressions, $r_1,r_2>0$ are the intrinsic rates of growth of the prey species, while $K_1,K_2>0$ are the carrying capacities.
If $K_i\to\infty$ and there is no predation, then species $i$ would experience exponential growth, as its governing equation would reduce to $\idder{x_i}{t}=r_ix_i$ which admits the unstable solution of $x_i(t)=x_i(0)\exp(r_it)$.
If the carrying capacity is finite and there is no predation or inter-species competition, then each prey species will simply grow in logistic manner to its carrying capacity - thus $K_i$ is the maximum population of species $i$ in absence of competition or predation.

The $\alpha_{12}$ and $\alpha_{21}$ terms give rise to competition between prey for finite resources.
Observe that $x_i/K_i$ - the term that limits growth beyond $K_1$ - is replaced by $(x_i+\alpha_{ij}x_j)/K_i$.
This shows that $\alpha_{12}>0$ essentially limits the carrying capacity for species $1$ due to the consumption of resources by species $2$.
If $\alpha_{12}=\alpha_{21}=1$ then the species compete equally.
However if $\alpha_{12} = 4 = 1/\alpha_{21}$, for instance, then species $2$ effectively consumes four times the resources of species $1$.

The $\beta_1,\beta_2>0$ terms with $x_iy$ products represent loss of prey species due to predation.
The $\eta_1,\eta_2>0$ terms in the predator population equation are the gains in predator species due to predation.
Finally $\delta>0$ is the intrinsic rate of \emph{death} of predators in absence of prey.
Thus if there is no predation, the predator species goes extinct as $y\to0$ because $\idder{y}{t}=-\delta y$ has the stable solution $y(t)=y(0)\exp(-\delta t)$.

\subsection{Parameter Choices}
To choose parameters, we have two issues.
First, how to represent a `bulky' prey species and an `evasive' prey species with the given parameters in \eqref{eqn: lv3}.
And second, finding actual numbers that give interesting results.
Numbers were found by trial and error.

The distinction between the bulky and evasive prey species are as follows.
Species $1$ will be evasive, and $2$ bulky.
\begin{itemize}
	\item The evasive species has a lower intrinsic growth rate: $r_1<r_2$.
	\item The bulky species consumes more resources: $\alpha_{12}>1$, $\alpha_{21}=1/\alpha{12}<1$.
	\item Predation is more impactful on the bulky species: $\beta_2>\beta_1$, $\eta_2>\eta_1$.
	\item The evasive species has a higher carrying capacity: $K_1>K_2$.
\end{itemize}

As for exact numbers, the example parameter set and initial populations below leads to a stable equilibrium with nonzero populations of all three species.
\begin{equation}
	\text{stable nonzero equilibrium: }
	\begin{cases}
		& x_1(0) = x_2(0) = 100, y(0) = 10     , \\
		& r_1 = 1.0, K_1 = 200, \beta_1 = 0.01 , \\
		& r_2 = 4.0, K_2 = 100, \beta_2 = 0.1  , \\
		& \alpha_{12} = 1/\alpha_{21} = 4      , \\
		& \eta_1 = 0.01, \eta_2 = 0.2, \delta = 6.
	\end{cases}
\end{equation}

Another parameter set leads to extinction of the evasive species and stable, nonzero populations of the bulky prey and predator.
The only differences are the lesser impacts of predation on bulky species, with $\beta_2=0.05$ instead of $0.1$ and $\eta_2=0.1$ instead of $0.2$, in addition to the lower carrying capacity of evasive species, at $K_1=170$ instead of $200$.
\begin{equation}
	\text{extinction of evasive prey: }
	\begin{cases}
		& x_1(0) = x_2(0) = 100, y(0) = 10     , \\
		& r_1 = 1.0, K_1 = 170, \beta_1 = 0.01 , \\
		& r_2 = 4.0, K_2 = 100, \beta_2 = 0.05  , \\
		& \alpha_{12} = 1/\alpha_{21} = 4      , \\
		& \eta_1 = 0.01, \eta_2 = 0.1, \delta = 6.
	\end{cases}
\end{equation}


\subsection{Vector Form \& Jacobian Matrix}
Before solving a system of ODEs in \exprlib{} with the block-implicit method, it is wise to write the equations in vector form and understand the Jacobian matrix.
Given the solution vector $\v{U}(t)=[x_1(t),x_2(t),y(t)]^\tr$ with corresponding RHS vector $\v{R}$, the vector form of \eqref{eqn: lv3} is
\begin{equation}
	\dder{\v{U}}{t} = \v{R}(\v{U}) \quad\leftrightarrow\quad
	\dder{}{t}
	\begin{pmatrix}
		x_1 \\
		x_2 \\
		y
	\end{pmatrix}
	=
	\begin{pmatrix}
		R_1 \\
		R_2 \\
		R_3
	\end{pmatrix}
	=
	\begin{pmatrix}
		r_1x_1\left(1 - K_1^{-1}\left(x_1+\alpha_{12}x_2\right)\right) - \beta_1x_1y \\
		r_2x_2\left(1 - K_2^{-1}\left(x_2+\alpha_{21}x_1\right)\right) - \beta_2x_2y \\
		(\eta_1x_1 + \eta_2x_2)y - \delta y
	\end{pmatrix}.
\end{equation}

The Jacobian matrix is made up of partial derivatives of $\v{R}$ to $\v{U}$:
\begin{equation}
	\m{J} = \pder{\v{R}}{\v{U}} =
	\begin{bmatrix}
		\pder{R_1}{U_1}\const{u_2,u_3} & \pder{R_1}{U_2}\const{U_1,U_3} & \pder{R_1}{U_3}\const{U_1,U_2} \\
		\pder{R_2}{U_1}\const{u_2,u_3} & \pder{R_2}{U_2}\const{U_1,U_3} & \pder{R_2}{U_3}\const{U_1,U_2} \\
		\pder{R_3}{U_1}\const{u_2,u_3} & \pder{R_3}{U_2}\const{U_1,U_3} & \pder{R_3}{U_3}\const{U_1,U_2}
	\end{bmatrix}.
\end{equation}

For Equation \eqref{eqn: lv3} the derivatives with respect to a generic variable $\phi$ are as follows.
For \exprlib{} it is best to compute derivative with respect to a generic variable such as this, because it allows sensitivities to be propagated through the graph with the chain rule.
\begin{equation}
	\pder{R_1}{\phi} = r_1\left[\pder{x_1}{\phi}\left(1 - 2K_1^{-1}x_1\right) - \alpha_{12}K_1^{-1}\left(x_1\pder{x_2}{\phi}+x_2\pder{x_1}{\phi}\right)\right] - \beta_1\left(x_1\pder{y}{\phi}+y\pder{x_1}{\phi}\right),
	\label{eqn: dR1dphi}
\end{equation}
\begin{equation}
	\pder{R_2}{\phi} = r_2\left[\pder{x_2}{\phi}\left(1 - 2K_2^{-1}x_2\right) - \alpha_{21}K_2^{-1}\left(x_1\pder{x_2}{\phi}+x_2\pder{x_1}{\phi}\right)\right] - \beta_2\left(x_2\pder{y}{\phi}+y\pder{x_2}{\phi}\right),
	\label{eqn: dR2dphi}
\end{equation}
\begin{equation}
	\pder{R_3}{\phi} = \eta_1\left(x_1\pder{y}{\phi}+y\pder{x_1}{\phi}\right) + \eta_2\left(x_2\pder{y}{\phi}+y\pder{x_2}{\phi}\right) - \delta\pder{y}{\phi}.
	\label{eqn: dR3dphi}
\end{equation}





\section{Solving in \exprlib{}}

\subsection{RHS Expression and Sensitivities}
First we have to write an expression which computes the right-hand side (RHS) of the ODEs.
This is done in the \code{RhsThreeSpecies.h} file.
The \code{evaluate()} method is straightforward, and the \code{sensitivities(Tag)} method simply implements \eqref{eqn: dR1dphi}-\eqref{eqn: dR3dphi}.

\subsection{Initial Condition Setup}
To set up the initial conditions we use an \code{ExpressionTree} object.
In this case it is quite simple, as we make a tree and factory, populating the tree with constant fields with values of $x_1(0)$, $x_2(0)$, and $y(0)$.
\begin{lstlisting}
 Expr::ExpressionFactory icFactory;
 Expr::ExpressionTree icTree( icFactory, patch.id(), "tree: initial conditions" );

 typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
 icTree.insert_tree( icFactory.register_expression( new ConstantT( prey1Tag, prey1 ) ) );
 icTree.insert_tree( icFactory.register_expression( new ConstantT( prey2Tag, prey2 ) ) );
 icTree.insert_tree( icFactory.register_expression( new ConstantT( predTag, pred ) ) );
\end{lstlisting}

From here we simply execute the tree and write it to a GraphViz file for inspection if necessary.
\begin{lstlisting}
 icTree.register_fields( fml );
 icTree.bind_fields( fml );
 fml.allocate_fields( patch.field_info() );
 icTree.execute_tree();
 {
  std::ofstream f( "lotka-volterra-tree--initial-conditions.dot" );
  icTree.write_tree( f );
 }
\end{lstlisting}



\subsection{Integrator Setup}
To set up a dual time integrator you must build tags for the physical time step ($\Delta t$) and the dual time step ($\Delta\sigma$), and choose an order of accuracy in physical time.
Below we use a second order solution.
\begin{lstlisting}
 const Expr::Tag dtTag( "time step"     , Expr::STATE_NONE );
 const Expr::Tag dsTag( "dual time step", Expr::STATE_NONE );
 
 const int bdfOrder = 2;
 
 typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> IntegratorT;
 boost::shared_ptr<IntegratorT> integrator = boost::make_shared<IntegratorT>(patch.id(),
                                                                             "integrator",
                                                                             dtTag,
                                                                             dsTag,
                                                                             bdfOrder );
\end{lstlisting}

Next we set the time step expressions to constants:
\begin{lstlisting}
 typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
 integrator->set_physical_time_step_expression(new ConstantSingleValueT(dtTag, dtValue ));
 integrator->set_dual_time_step_expression    (new ConstantSingleValueT(dsTag, dsValue ));
\end{lstlisting}

\subsubsection{Building RHS Expressions}
To build the RHS expressions and any intermediates required to compute the $R_i$, we pull a factory off the integrator and register the expressions to it.
In this case we only have and RHS expression, but any other intermediates used to compute the RHS would be registered here.
\begin{lstlisting}
 typedef lv3::Rhs<FieldT>::Builder RhsT;
 Expr::ExpressionFactory& factory = integrator->factory();
 factory.register_expression( new RhsT( params, tagsE ) );
\end{lstlisting}

\subsubsection{Adding Variables}
Next we add the variables/equations to the integrator with the \code{add\_variable(\ldots)} method.
The first argument is the name of the integrated variable, the second is the RHS tag for the equation, and the final arguments are the indices of the variable and the RHS in the Jacobian matrix.
Any combination of indices may be specified so long as no two variable or RHS indices are shared (one index per variable, one per RHS) and that no index greater than $(\text{\# of equations}-1)$ is provided.
After adding all of the variables, the \code{done\_adding\_variables()} method must be called.
\begin{lstlisting}
 integrator->add_variable<FieldT>( tagsE.prey1Name   , tagsE.prey1RhsTag   , 0, 0 );
 integrator->add_variable<FieldT>( tagsE.prey2Name   , tagsE.prey2RhsTag   , 1, 1 );
 integrator->add_variable<FieldT>( tagsE.predatorName, tagsE.predatorRhsTag, 2, 2 );
 integrator->done_adding_variables();
\end{lstlisting}

\subsubsection{Jacobian Matrix}
In this case the Jacobian is formed directly through the sensitivities of the RHS expressions to the integrated solution variables.
\emph{After} we are done adding variables to the integrator, the Jacobian matrix is formed and specified.
The ASSEMBLER macro is used to declare the \code{jac} variable (pointer) to a dense matrix with elements of type \code{FieldT}.
After declaration, the elements are specified through the \code{Expr::matrix::sensitivity} method.
The \code{finalize()} method must be called on \code{jac}, and then \code{set\_jacobian(\ldots)} is called on the integrator.

A C++ lambda is used to make the Jacobian matrix assembler.
This is called each time we need to provide the Jacobian to a function such as \code{set\_jacobian} or an expression builder.
If a copy is not needed, then the lambda (namely the \code{auto jacobian = \ldots} and curly braces used to scope the lambda) may be discarded and the Jacobian itself may be provided to \code{set\_jacobian(jac)}.

\begin{lstlisting}
 auto jacobian = [&]()
 {
  ASSEMBLER( Expr::matrix::DenseSubMatrix, FieldT, jac )

  jac->element<FieldT>(0,0) = sensitivity( tagsE.prey1RhsTag   , tagsE.prey1StateTag    );
  jac->element<FieldT>(0,1) = sensitivity( tagsE.prey1RhsTag   , tagsE.prey2StateTag    );
  jac->element<FieldT>(0,2) = sensitivity( tagsE.prey1RhsTag   , tagsE.predatorStateTag );

  jac->element<FieldT>(1,0) = sensitivity( tagsE.prey2RhsTag   , tagsE.prey1StateTag    );
  jac->element<FieldT>(1,1) = sensitivity( tagsE.prey2RhsTag   , tagsE.prey2StateTag    );
  jac->element<FieldT>(1,2) = sensitivity( tagsE.prey2RhsTag   , tagsE.predatorStateTag );

  jac->element<FieldT>(2,0) = sensitivity( tagsE.predatorRhsTag, tagsE.prey1StateTag    );
  jac->element<FieldT>(2,1) = sensitivity( tagsE.predatorRhsTag, tagsE.prey2StateTag    );
  jac->element<FieldT>(2,2) = sensitivity( tagsE.predatorRhsTag, tagsE.predatorStateTag );

  jac->finalize();

  return jac;
 };
\end{lstlisting}

\subsubsection{Processing Extra Expressions}
To add an expression to the graph that is not needed to compute the RHS, for instance to visualize some complex function of the state variables, we need a special procedure of registering the expression.
In this case we are post processing eigenvalues of the Jacobian matrix, so we do the following.
\begin{lstlisting}
 typedef Expr::EigenvalueExpr<FieldT>::Builder EigT;
 integrator->get_tree().insert_tree(factory.register_expression(new EigT(...)));
\end{lstlisting}


\subsubsection{Finalizing and Running the Integration}
Once all expressions are registered and the Jacobian is set, we call the \code{prepare\_for\_integration(\ldots)} method on the integrator and execute the time and dual time loops as seen in the main executable.


\subsection{Data Output}
To write data to disc we first set up an output database and register fields to it for output.
\begin{lstlisting}
 Expr::FieldOutputDatabase db( fml, "lotka-volterra-output", true );
 
 db.request_field_output<FieldT>( tagsN.prey1StateTag    );
 db.request_field_output<FieldT>( tagsN.prey2StateTag    );
 db.request_field_output<FieldT>( tagsN.predatorStateTag );
 db.request_field_output<FieldT>( tagsE.eigenvalueTags[0] );
 db.request_field_output<FieldT>( tagsE.eigenvalueTags[1] );
 db.request_field_output<FieldT>( tagsE.eigenvalueTags[2] );
\end{lstlisting}

Then to write these fields at every physical time step we simply do the following, where \code{stepCount} is incremented every physical time step.
\begin{lstlisting}
 db.write_database( "step-" + boost::lexical_cast<std::string>( stepCount ) );
\end{lstlisting}

\subsection{Running the Executable}
When compiling \exprlib{}, be sure to set the cmake variable \texttt{ENABLE\_OUTPUT=ON}.
Compilation will builds an executable named \texttt{lotkavolterra} in the \code{\ldots/build/test/dualtime/ecology-example/} directory, in addition to the python scripts for post-processing results discussed below.
To run this executable and solve the system with default parameters, simply enter \code{./lotkavolterra}.
To see the command line options (the ODE parameters) enter \code{./lotkavolterra --help}.
This shows how one could change any number of the parameters, for example with \code{./lotkavolterra --r1=2 --K2=50}.

\section{Plotting the Results with Python}
The \code{DataLoader.py} file provides functions to load fields written by \exprlib{}, and in the \code{driver.py} file these are used to load data into vectors of the solution variables and eigenvalues over time.
Example plots of each species' populations and real parts of the eigenvalues of the Jacobian matrix are below.
\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{figures/nonzero-equilibria.png}
		\caption{Populations and eigenvalues (real part only) of the Jacobian over time for the case in which all three species find nonzero equilibrium populations.}
	\end{center}
\end{figure}
\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{figures/extinct-evasive.png}
		\caption{Populations and eigenvalues (real part only) of the Jacobian over time for the case in which evasive prey go extinct.}
	\end{center}
\end{figure}



\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{figures/nonzero-spectrum.png}
		\caption{Eigenvalues trajectories on the complex plane for the case in which all three species find nonzero equilibrium populations.}
	\end{center}
\end{figure}
\begin{figure}[htbp]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{figures/extinct-evasive-spectrum.png}
		\caption{Eigenvalues trajectories on the complex plane for the case in which evasive prey go extinct. Triangles indicate the initial condition and squares indicate the eigenvalues at the final time - the trajectories proceed in time \emph{from} the triangles \emph{to} the squares.}
	\end{center}
\end{figure}


\clearpage


\section{Debugging Matrix Assembly}
In developing implicit solution methods the Jacobian matrix is a common source of errors.
The \code{C++} file called \code{lotkaVolterraThreeSpeciesWithMatrixDebugging.cpp} demonstrates how a \code{MatrixExpression} can be used to debug matrix assembly in \code{ExprLib}.
This example only runs several time steps but it shows how to print Jacobian matrix values as time integration proceeds.
Note that (as described in the file) this requires a \code{DEBUG} build with threading off and \code{CUDA} off.



\end{document}














