/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef RHSTHREESPECIES_H_
#define RHSTHREESPECIES_H_

#include <expression/Expression.h>
#include "ModelParameters.h"
#include "TagManager.h"

namespace LotkaVolterra3
{

  using ParamsT = LotkaVolterra3::ModelParameters;
  using TagsT   = LotkaVolterra3::Tags;

  template< typename FieldT >
  class Rhs : public Expr::Expression<FieldT>
  {
  protected:
    const double& r1_   ;
    const double& r2_   ;
    const double  invK1_;
    const double  invK2_;
    const double& beta1_;
    const double& beta2_;
    const double& a12_  ;
    const double& a21_  ;
    const double& delta_;
    const double& eta1_ ;
    const double& eta2_ ;
    DECLARE_FIELD( FieldT, prey1_ )
    DECLARE_FIELD( FieldT, prey2_ )
    DECLARE_FIELD( FieldT, predator_ )

    Rhs( const ParamsT& params,
         const TagsT& tags )
    : Expr::Expression<FieldT>(),
      r1_   ( params.r1       ),
      r2_   ( params.r2       ),
      invK1_( 1.0 / params.K1 ),
      invK2_( 1.0 / params.K2 ),
      beta1_( params.beta1    ),
      beta2_( params.beta2    ),
      a12_  ( params.a12      ),
      a21_  ( params.a21      ),
      delta_( params.delta    ),
      eta1_ ( params.eta1     ),
      eta2_ ( params.eta2     )
    {
      this->set_gpu_runnable( true );
      prey1_    = this->template create_field_request<FieldT>( tags.prey1StateTag    );
      prey2_    = this->template create_field_request<FieldT>( tags.prey2StateTag    );
      predator_ = this->template create_field_request<FieldT>( tags.predatorStateTag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder
    {
    protected:
      const ParamsT params_;
      const TagsT tags_;

    public:
      Builder( const ParamsT& params,
               const TagsT& tags,
               const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( tags.rhs_tags(), nghost ),
        params_( params),
        tags_( tags )
        {}

      Expr::ExpressionBase* build() const
      {
        return new Rhs<FieldT>( params_, tags_ );
      }
    };

    ~Rhs(){}

    void evaluate()
    {
      using namespace SpatialOps;
      typename Expr::Expression<FieldT>::ValVec& rhsValues = this->get_value_vec();

      FieldT& prey1Rhs    = *rhsValues[0];
      FieldT& prey2Rhs    = *rhsValues[1];
      FieldT& predatorRhs = *rhsValues[2];

      const FieldT& x1 = prey1_   ->field_ref();
      const FieldT& x2 = prey2_   ->field_ref();
      const FieldT& y  = predator_->field_ref();

      prey1Rhs    <<= r1_ * x1 * ( 1.0 - invK1_ * ( x1 + a12_ * x2 ) ) - beta1_ * x1 * y;
      prey2Rhs    <<= r2_ * x2 * ( 1.0 - invK2_ * ( x2 + a21_ * x1 ) ) - beta2_ * x2 * y;
      predatorRhs <<= ( eta1_ * x1 + eta2_ * x2 ) * y - delta_ * y;
    }

    void sensitivity( const Expr::Tag& varTag )
    {
      const Expr::TagList& rhsTags = this->exprNames_;

      FieldT& dR1dvar = this->sensitivity_result( rhsTags[0], varTag );
      FieldT& dR2dvar = this->sensitivity_result( rhsTags[1], varTag );
      FieldT& dR3dvar = this->sensitivity_result( rhsTags[2], varTag );

      const FieldT& x1 = prey1_   ->field_ref();
      const FieldT& x2 = prey2_   ->field_ref();
      const FieldT& y  = predator_->field_ref();

      const FieldT& dx1dvar = prey1_   ->sens_field_ref( varTag );
      const FieldT& dx2dvar = prey2_   ->sens_field_ref( varTag );
      const FieldT& dydvar  = predator_->sens_field_ref( varTag );

      SpatialOps::SpatFldPtr<FieldT> x1x2sensPtr = SpatialOps::SpatialFieldStore::get<FieldT>( x1 );
      *x1x2sensPtr <<= x1 * dx2dvar + x2 * dx1dvar;
      SpatialOps::SpatFldPtr<FieldT> x1ysensPtr = SpatialOps::SpatialFieldStore::get<FieldT>( x1 );
      SpatialOps::SpatFldPtr<FieldT> x2ysensPtr = SpatialOps::SpatialFieldStore::get<FieldT>( x1 );
      *x1ysensPtr <<= x1 * dydvar + y * dx1dvar;
      *x2ysensPtr <<= x2 * dydvar + y * dx2dvar;

      dR1dvar <<= r1_ * ( dx1dvar * ( 1.0 - invK1_ * 2.0 * x1 ) - a12_ * invK1_ * *x1x2sensPtr ) - beta1_ * *x1ysensPtr;
      dR2dvar <<= r2_ * ( dx2dvar * ( 1.0 - invK2_ * 2.0 * x2 ) - a21_ * invK2_ * *x1x2sensPtr ) - beta2_ * *x2ysensPtr;
      dR3dvar <<= eta1_ * *x1ysensPtr + eta2_ * *x2ysensPtr - delta_ * dydvar;
    }


  };


}


#endif /* RHSTHREESPECIES_H_ */
