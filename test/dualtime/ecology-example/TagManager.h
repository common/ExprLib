/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TAGMANAGER_H_
#define TAGMANAGER_H_

#include <string>

namespace LotkaVolterra3
{

struct Tags
{
  Tags( const Expr::Context a_context    = Expr::STATE_NONE,
        const std::string a_prey1Name    = "prey-evasive",
        const std::string a_prey2Name    = "prey-bulky",
        const std::string a_predatorName = "predator" )
  : context     ( a_context      ),
    prey1Name   ( a_prey1Name    ),
    prey2Name   ( a_prey2Name    ),
    predatorName( a_predatorName ),
    prey1StateTag   ( Expr::Tag( prey1Name   , context ) ),
    prey2StateTag   ( Expr::Tag( prey2Name   , context ) ),
    predatorStateTag( Expr::Tag( predatorName, context ) ),
    prey1RhsTag     ( Expr::Tag( prey1Name    + "-rhs", context ) ),
    prey2RhsTag     ( Expr::Tag( prey2Name    + "-rhs", context ) ),
    predatorRhsTag  ( Expr::Tag( predatorName + "-rhs", context ) ),
    realPartEigTags ( { Expr::Tag( "real1", context ),
                        Expr::Tag( "real2", context ),
                        Expr::Tag( "real3", context ) } ),
    imagPartEigTags ( { Expr::Tag( "imag1", context ),
                        Expr::Tag( "imag2", context ),
                        Expr::Tag( "imag3", context ) } )
  {}

  const Expr::Context context = Expr::STATE_NONE;

  const std::string prey1Name    = "prey-evasive";
  const std::string prey2Name    = "prey-bulky";
  const std::string predatorName = "predator";

  const Expr::Tag prey1StateTag;
  const Expr::Tag prey2StateTag;
  const Expr::Tag predatorStateTag;

  const Expr::Tag prey1RhsTag;
  const Expr::Tag prey2RhsTag;
  const Expr::Tag predatorRhsTag;

  const Expr::TagList realPartEigTags, imagPartEigTags;

  Expr::TagList var_tags() const
  {
    return Expr::tag_list( prey1StateTag, prey2StateTag, predatorStateTag );
  }

  Expr::TagList rhs_tags() const
  {
    return Expr::tag_list( prey1RhsTag, prey2RhsTag, predatorRhsTag );
  }

};

}


#endif /* TAGMANAGER_H_ */
