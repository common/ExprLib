#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/Functions.h>
#include <expression/ExpressionTree.h>
#include <expression/ExprPatch.h>
#include <expression/FieldWriter.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/EigenvalueExpression.h>
#include <expression/matrix-assembly/MatrixExpression.h>

#include "InitialConditions.h"
#include "RhsThreeSpecies.h"

#include <iostream>
#include <fstream>

#include <boost/program_options.hpp>

namespace so = SpatialOps;
typedef so::SVolField FieldT;

namespace lv3 = LotkaVolterra3;

namespace po = boost::program_options;

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >( #name );

using Expr::matrix::sensitivity;


int main( int iarg, char* carg[] )
{
  // set up initial conditions, parameters
  lv3::ModelParameters params;
  lv3::InitialConditions ics;



  // parse command line input
  {
    po::options_description desc( "Supported Options" );
    desc.add_options()
              ( "help", "print help message" )
              ( "r1"   , po::value<double>( &(params.r1    ) )->default_value( params.r1    ) , "intrinsic growth rate - evasive prey" )
              ( "r2"   , po::value<double>( &(params.r2    ) )->default_value( params.r2    ) , "intrinsic growth rate - bulky prey" )
              ( "K1"   , po::value<double>( &(params.K1    ) )->default_value( params.K1    ) , "carrying capacity - evasive prey" )
              ( "K2"   , po::value<double>( &(params.K2    ) )->default_value( params.K2    ) , "carrying capacity - bulky prey" )
              ( "beta1", po::value<double>( &(params.beta1 ) )->default_value( params.beta1 ) , "predator interaction term - evasive prey" )
              ( "beta2", po::value<double>( &(params.beta2 ) )->default_value( params.beta2 ) , "predator interaction term - evasive prey" )
              ( "alpha", po::value<double>( &(params.a12   ) )->default_value( params.a12   ) , "prey competition term, alpha = alpha_12 = 1/alpha_21" )
              ( "eta1" , po::value<double>( &(params.eta1  ) )->default_value( params.eta1  ) , "prey interaction term - evasive prey" )
              ( "eta2" , po::value<double>( &(params.eta2  ) )->default_value( params.eta2  ) , "prey interaction term - bulky prey" )
              ( "delta", po::value<double>( &(params.delta ) )->default_value( params.delta ) , "intrinsic death rate - predator" );
    po::variables_map args;
    try{
      po::store( po::parse_command_line( iarg, carg, desc ), args );
      po::notify( args );

      if( args.count( "alpha" ) ){
        params.a21 = 1.0 / params.a12;
      }
    }
    catch( std::exception& err ){
      std::cout << "\nError parsing command line options.\n\n"
                << err.what() << "\n\n" << desc << "\n";
      return -1;
    }
    if( args.count( "help" ) ){
      std::cout << desc << "\n";
      return 1;
    }
  }



  // generate tags
  lv3::Tags tagsN( Expr::STATE_N );
  lv3::Tags tagsE( Expr::STATE_NONE );



  // make a patch, operator database, and field manager list
  Expr::ExprPatch patch( 1 );
  SpatialOps::OperatorDatabase sodb;
  Expr::FieldManagerList& fml = patch.field_manager_list();



  // use an initialization tree to set initial values
  Expr::ExpressionFactory icFactory;
  Expr::ExpressionTree icTree( icFactory, patch.id(), "tree: initial conditions" );

  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  icTree.insert_tree( icFactory.register_expression( new ConstantT( tagsN.prey1StateTag   , ics.prey1    ) ) );
  icTree.insert_tree( icFactory.register_expression( new ConstantT( tagsN.prey2StateTag   , ics.prey2    ) ) );
  icTree.insert_tree( icFactory.register_expression( new ConstantT( tagsN.predatorStateTag, ics.predator ) ) );

  icTree.compute_sensitivities( tagsN.var_tags(), tagsN.var_tags() );

  icTree.register_fields( fml );
  icTree.bind_fields( fml );
  fml.allocate_fields( patch.field_info() );
  icTree.execute_tree();
  {
    std::ofstream f( "lotka-volterra-tree--initial-conditions.dot" );
    icTree.write_tree( f );
  }



  // make a dual time integrator
  typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> IntegratorT;
  const Expr::Tag dtTag( "time step"     , Expr::STATE_NONE );
  const Expr::Tag dsTag( "dual time step", Expr::STATE_NONE );
  const int bdfOrder = 2;
  boost::shared_ptr<IntegratorT> integrator = boost::make_shared<IntegratorT>( patch.id(),
                                                                               "integrator",
                                                                               dtTag,
                                                                               dsTag,
                                                                               bdfOrder );



  // set the physical and dual time step sizes
  const double dtValue = 1e-3;
  const double dsValue = 1e16;
  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
  integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dtValue ) );
  integrator->set_dual_time_step_expression    ( new ConstantSingleValueT( dsTag, dsValue ) );



  // register expressions for RHS calculation
  typedef lv3::Rhs<FieldT>::Builder RhsT;
  Expr::ExpressionFactory& factory = integrator->factory();
  factory.register_expression( new RhsT( params, tagsE ) );



  // add variables
  const int prey1Idx    = 2; const int prey1RhsIdx    = 0;
  const int prey2Idx    = 0; const int prey2RhsIdx    = 1;
  const int predatorIdx = 1; const int predatorRhsIdx = 2;

  integrator->add_variable<FieldT>( tagsE.prey1Name   , tagsE.prey1RhsTag   , prey1Idx, prey1RhsIdx );
  integrator->add_variable<FieldT>( tagsE.prey2Name   , tagsE.prey2RhsTag   , prey2Idx, prey2RhsIdx );
  integrator->add_variable<FieldT>( tagsE.predatorName, tagsE.predatorRhsTag, predatorIdx, predatorRhsIdx );
  integrator->done_adding_variables();



  // make the jacobian
  auto jacobian = [&]()
  {
    ASSEMBLER( Expr::matrix::DenseSubMatrix, FieldT, jac )

    jac->element<FieldT>(prey1RhsIdx,prey1Idx) = sensitivity( tagsE.prey1RhsTag   , tagsE.prey1StateTag    );
    jac->element<FieldT>(prey1RhsIdx,prey2Idx) = sensitivity( tagsE.prey1RhsTag   , tagsE.prey2StateTag    );
    jac->element<FieldT>(prey1RhsIdx,predatorIdx) = sensitivity( tagsE.prey1RhsTag   , tagsE.predatorStateTag );

    jac->element<FieldT>(prey2RhsIdx,prey1Idx) = sensitivity( tagsE.prey2RhsTag   , tagsE.prey1StateTag    );
    jac->element<FieldT>(prey2RhsIdx,prey2Idx) = sensitivity( tagsE.prey2RhsTag   , tagsE.prey2StateTag    );
    jac->element<FieldT>(prey2RhsIdx,predatorIdx) = sensitivity( tagsE.prey2RhsTag   , tagsE.predatorStateTag );

    jac->element<FieldT>(predatorRhsIdx,prey1Idx) = sensitivity( tagsE.predatorRhsTag, tagsE.prey1StateTag    );
    jac->element<FieldT>(predatorRhsIdx,prey2Idx) = sensitivity( tagsE.predatorRhsTag, tagsE.prey2StateTag    );
    jac->element<FieldT>(predatorRhsIdx,predatorIdx) = sensitivity( tagsE.predatorRhsTag, tagsE.predatorStateTag );

    jac->finalize();

    return jac;
  };





  /* -- begin MATRIX DEBUGGING CODE --

   Suppose you find that your implicit method is converging slowly or not at all.
   It can be helpful to print values of the Jacobian matrix to check that it is implemented as expected.
   To do this:

   1. Build in DEBUG mode without threading and for CPU

   2. As shown below:

     2a. Print the structure of the Jacobian matrix assembler, as below.
         This will print coordinate pairs as they map to elements, such as fields, sensitivity fields, or double precision numbers.
         This allows you to check that an assembler is being constructed as expected.

     2b. Register a MatrixExpression as below that assembles the Jacobian matrix of interest, as below.
         Note that we could pass any matrix assembler to the MatrixExpression.

         Note below that we are locking fields.
         This must be done for debugging like this.
   */

  std::cout << jacobian()->get_structure() << std::endl;

  const Expr::TagList matrixDebugTags = Expr::matrix::matrix_tags( "matrix-debug", 3 );
  using MatrixAssemblerExprType = Expr::matrix::MatrixExpression<FieldT>::Builder;
  integrator->register_root_expression( new MatrixAssemblerExprType( matrixDebugTags,
                                                                     jacobian(), /* ANY assembler can be passed here */
                                                                     true        /* pass true for debugging */ ) );

  // -- end MATRIX DEBUGGING CODE --





  typedef Expr::EigenvalueExpression<FieldT>::Builder EigT;
  integrator->register_root_expression( new EigT( tagsE.realPartEigTags,
                                                  tagsE.imagPartEigTags,
                                                  jacobian(),
                                                  3 ) );

  integrator->set_jacobian( jacobian() );

  integrator->prepare_for_integration( fml, sodb, patch.field_info() );
  {
    std::ofstream f("lotka-volterra-tree--nonlinear-solve-execution.dot");
    integrator->get_tree().write_tree( f );
  }

  integrator->lock_all_execution_fields();


  // set up output of fields of interest
  Expr::FieldOutputDatabase db( fml, "lotka-volterra-output", true );
  db.request_field_output<FieldT>( tagsN.prey1StateTag    );
  db.request_field_output<FieldT>( tagsN.prey2StateTag    );
  db.request_field_output<FieldT>( tagsN.predatorStateTag );
  db.request_field_output<FieldT>( tagsE.realPartEigTags[0] );
  db.request_field_output<FieldT>( tagsE.realPartEigTags[1] );
  db.request_field_output<FieldT>( tagsE.realPartEigTags[2] );
  db.request_field_output<FieldT>( tagsE.imagPartEigTags[0] );
  db.request_field_output<FieldT>( tagsE.imagPartEigTags[1] );
  db.request_field_output<FieldT>( tagsE.imagPartEigTags[2] );



  // set up the simulation and solve!
  const unsigned maxIterPerStep = 100; // maximum nonlinear iterations per time step
  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;
  double t = 0;

  const double tend = 2.e-3; // final time

  // write out the initial condition
  db.write_database( "step-" + boost::lexical_cast<std::string>( stepCount ) );

  // run the time and nonlinear solver (dual time) loops
  std::cout << "-- running simulation...\n";
  while( t<tend ){
    dualIter = 0;
    integrator->begin_time_step();
    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;
    } while( !converged && dualIter <= maxIterPerStep );
    integrator->end_time_step();
    stepCount++;
    t += dtValue;
    db.write_database( "step-" + boost::lexical_cast<std::string>( stepCount ) );
  }
  std::cout << "-- total iteration count: " << totalIter << '\n';
  std::cout << "-- finished running Lotka-Volterra 3-species example\n";
  return 0;
}
