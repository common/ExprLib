/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef MODELPARAMETERS_H_
#define MODELPARAMETERS_H_


namespace LotkaVolterra3
{

struct ModelParameters
{
  // prey1 (evasive) parameters
  double r1    = 1.0;
  double K1    = 200.0;
  double beta1 = 0.01;

  // prey2 (bulky) parameters
  double r2    = 4.0;
  double K2    = 100.0;
  double beta2 = 0.1;

  // prey competition factors
  double a12 = 4;
  double a21 = 1.0 / a12;

  // predator parameters
  double eta1  = 0.01;
  double eta2  = 0.2;
  double delta = 6.0;
};

}


#endif /* MODELPARAMETERS_H_ */
