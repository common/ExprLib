"""
This python file shows how one might load data from an ExprLib output database.

Note that you will need to have ".../build/include/expression/util" on your PYTHONPATH
environment variable in order for this to work. This can be set in the terminal with:

export PYTHONPATH=.../build/include/expression/util:$PYTHONPATH

where .../build is your build directory.
"""

from DataLoader import DataLoader
import numpy as np
import matplotlib.pyplot as plt

baseDir = 'lotka-volterra-output'
database = DataLoader(baseDir)

fields = database.available_fields()
database.select_fields( fields )

times = database.available_times()[0:-1]
nt = len(times)
database.select_times(times)

p1 = np.zeros((nt,1))
p2 = np.zeros((nt,1))
p3 = np.zeros((nt,1))
e1 = np.zeros((nt,1))
e2 = np.zeros((nt,1))
e3 = np.zeros((nt,1))

for i, s in enumerate(times):
    p1[i] = database.get_field(s, 'prey-evasive').data()[0][0][0]
    p2[i] = database.get_field(s, 'prey-bulky').data()[0][0][0]
    p3[i] = database.get_field(s, 'predator').data()[0][0][0]
    e1[i] = database.get_field(s, 'real1').data()[0][0][0]
    e2[i] = database.get_field(s, 'real2').data()[0][0][0]
    e3[i] = database.get_field(s, 'real3').data()[0][0][0]

tf = 10.0
tvec = np.linspace(0,tf,nt)

f, axarr = plt.subplots(2, sharex=True)

axarr[0].plot(tvec,np.round(p1),label='evasive prey')
axarr[0].plot(tvec,np.round(p2),label='bulky prey')
axarr[0].plot(tvec,np.round(p3),label='predator')
axarr[0].grid(True)
axarr[0].set_ylabel('# of individuals')
axarr[0].legend()

axarr[1].plot(tvec,e1)
axarr[1].plot(tvec,e2)
axarr[1].plot(tvec,e3)
axarr[1].set_ylim([-20,20])
axarr[1].set_ylabel('Re(eigenvalue)')
axarr[1].set_xlabel('time')
axarr[1].grid(True)
plt.show()




