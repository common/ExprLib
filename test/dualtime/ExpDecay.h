/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ExpDecayRHS_Expr_h
#define ExpDecayRHS_Expr_h

#include <expression/Expression.h>

/**
 *  \class ExpDecayRHS
 *
 *  RHS for the ODE:
 *  \f[
 *  \frac{d\phi}{dt} = -k\phi.
 *  \f]
 */
template< typename FieldT >
class ExpDecayRHS
 : public Expr::Expression<FieldT>
{
  const double k_;
  DECLARE_FIELD( FieldT, phi_ )

  ExpDecayRHS( const double rateConstant,
               const Expr::Tag& phiTag )
    : Expr::Expression<FieldT>(),
      k_( rateConstant )
  {
    this->set_gpu_runnable(true);
    phi_ = this->template create_field_request<FieldT>( phiTag );
  }


public:

  class Builder : public Expr::ExpressionBuilder
  {
    const double k_;
    const Expr::Tag phiTag_;
  public:
    /**
     *  @brief Build a ExpDecayRHS expression
     *  @param rateConstant
     *  @param resultTag the tag for the value that this expression computes
     *  @param phiTag the solution variable
     */
    Builder( const double rateConstant,
             const Expr::Tag& resultTag,
             const Expr::Tag& phiTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        k_( rateConstant ),
        phiTag_( phiTag )
    {}

    Expr::ExpressionBase* build() const{ return new ExpDecayRHS<FieldT>( k_, phiTag_ ); }
  };

  ~ExpDecayRHS(){}

  void evaluate()
  {
    FieldT& result = this->value();
    result <<= -k_*phi_->field_ref();
  }

  void sensitivity( const Expr::Tag& varTag )
  {
    using namespace SpatialOps;

    const FieldT& phiSens = this->phi_->sens_field_ref( varTag );
    FieldT& thisSens = this->sensitivity_result( varTag );

    if( varTag == this->get_tag() ) thisSens <<= 1.0;
    else{
      thisSens <<= -k_ * phiSens;
    }
  }
};


#endif // ExpDecayRHS_Expr_h
