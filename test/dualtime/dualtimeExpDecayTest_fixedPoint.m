clc; clear all;

dsigma = 0.01;
dt = 0.01;

c0 = 1.0;
c = c0;

TOL = 1e-10;
t = 0;
tend = 2.0;
totaliter = 0;
nt = 0;
disp('- run beginning');

k = 1.3;

cvec = zeros(1,1);
i = 0;
while t < tend
    count = 0;
    resid = TOL + 1;
    phin = c;
    i = i + 1;
    while resid > TOL && count < 1e4
        
        RHS = -k*c;
        
        TIME = (c - phin)/dt;
        
        RES = RHS - TIME;
        
        dc = dsigma*RES / (1. + dsigma / dt);
        
        resid = max(abs(dc)/(abs(c)+sqrt(eps)));
        c = c + dc;
        count = count + 1;
        
        if sum(isnan(c)) > 0
            error('- NaN detected');
        end
    end
    cvec(i) = c;
%     disp(['  - time step complete in ', num2str(count),' iterations, t = ', num2str(t)]);
    nt = nt + 1;
    t = t + dt;
    totaliter = totaliter + count;
end
disp(['- run complete in ', num2str(totaliter), ' iterations (', num2str(totaliter/nt),' iter/step)']);
disp(c)