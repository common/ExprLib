#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>

#include <test/TestHelper.h>

#include <expression/Functions.h>
#include <expression/ExpressionTree.h>
#include <expression/ExprPatch.h>

#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include <expression/matrix-assembly/SparseMatrix.h>

#include <iostream>
#include <fstream>

namespace so = SpatialOps;

typedef so::SVolField FieldT;

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >();


template< typename FieldT >
struct DRhsDVMatrix
{
  // phiPrim = phiPrimScale_ * phi
  // psiPrim = psiPrimScale_ * psi
  const double Khh_, Khs_, Ksh_, Kss_, phiPrimScale_, psiPrimScale_;

  DRhsDVMatrix( const double Khh,
                const double Khs,
                const double Ksh,
                const double Kss,
                const double phiPrimScale,
                const double psiPrimScale )
  : Khh_( Khh ),
    Khs_( Khs ),
    Ksh_( Ksh ),
    Kss_( Kss ),
    phiPrimScale_( phiPrimScale ),
    psiPrimScale_( psiPrimScale )
  {}

  void compute_jacobian( SpatialOps::FieldMatrix<FieldT>& matrix ) const
  {
    matrix(0,0) <<= Khh_ * phiPrimScale_;
    matrix(0,1) <<= Khs_ * psiPrimScale_;
    matrix(1,0) <<= Ksh_ * phiPrimScale_;
    matrix(1,1) <<= Kss_ * psiPrimScale_;
  }
};

template< typename FieldT >
class FullRhsExprPrimitives
    : public Expr::Expression<FieldT>
{
private:
  DECLARE_FIELDS( FieldT, phiPrim_, psiPrim_ )

  const double Khh_, Khs_, Ksh_, Kss_, phiPrimScale_, psiPrimScale_;

  const Expr::Tag phiPrimTag_, psiPrimTag_;

  const DRhsDVMatrix<FieldT> drhsdv_;

  FullRhsExprPrimitives( const Expr::Tag& phiPrimTag,
                         const Expr::Tag& psiPrimTag,
                         const double Khh,  // hh = phi,phi
                         const double Khs,  // hs = phi,psi
                         const double Ksh,  // sh = psi,phi
                         const double Kss,  // ss = psi,psi
                         const double phiPrimScale,
                         const double psiPrimScale )
  : Khh_( Khh ),
    Khs_( Khs ),
    Ksh_( Ksh ),
    Kss_( Kss ),
    phiPrimScale_( phiPrimScale ),
    psiPrimScale_( psiPrimScale ),
    phiPrimTag_( phiPrimTag ),
    psiPrimTag_( psiPrimTag ),
    drhsdv_( Khh, Khs, Ksh, Kss, phiPrimScale, psiPrimScale )
  {
    this->set_gpu_runnable( true );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );
    phiPrim_ = this->template create_field_request<FieldT>( phiPrimTag );

    psiPrim_ = this->template create_field_request<FieldT>( psiPrimTag );
  }

  bool override_sensitivity() const{ return true; }

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    /**
     *  @brief Build a FullRhsExprPrimitives expression
     *  @param rhsTags, Taglist of [phiRhs tag, psiRhs tag]
     *  @param phiTag, tag for the phi variable
     *  @param psiTag, tag for the phi variable
     *  @param nghost number of ghost cells
     */
    Builder( const Expr::TagList& rhsTags,
             const Expr::Tag& phiPrimTag,
             const Expr::Tag& psiPrimTag,
             const double Khh, // hh = phi,phi
             const double Khs, // hs = phi,psi
             const double Ksh, // sh = psi,phi
             const double Kss, // ss = psi,psi
             const double phiPrimScale,
             const double psiPrimScale,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
  : ExpressionBuilder( rhsTags, nghost ),
    phiPrimTag_( phiPrimTag ),
    psiPrimTag_( psiPrimTag ),
    Khh_( Khh ),
    Khs_( Khs ),
    Ksh_( Ksh ),
    Kss_( Kss ),
    phiPrimScale_( phiPrimScale ),
    psiPrimScale_( psiPrimScale )
  {}

    Expr::ExpressionBase* build() const
    {
      return new FullRhsExprPrimitives<FieldT>( phiPrimTag_, psiPrimTag_, Khh_, Khs_, Ksh_, Kss_, phiPrimScale_, psiPrimScale_ );
    }

  private:
    const Expr::Tag phiPrimTag_;
    const Expr::Tag psiPrimTag_;
    const double Khh_, Khs_, Ksh_, Kss_, phiPrimScale_, psiPrimScale_;
  };

  ~FullRhsExprPrimitives(){}

  void evaluate()
  {
    typename Expr::Expression<FieldT>::ValVec& rhsVec = this->get_value_vec();
    const FieldT& phiPrim = phiPrim_->field_ref();
    const FieldT& psiPrim = psiPrim_->field_ref();
    *rhsVec[0] <<= Khh_ * ( phiPrim / phiPrimScale_ ) + Khs_ * ( psiPrim / psiPrimScale_ );
    *rhsVec[1] <<= Ksh_ * ( phiPrim / phiPrimScale_ ) + Kss_ * ( psiPrim / psiPrimScale_ );
  }
  void sensitivity( const Expr::Tag& tag )
  {
    const Expr::Tag& phiRhsTag = this->get_tags()[0];
    const Expr::Tag& psiRhsTag = this->get_tags()[1];

    if( tag == phiPrimTag_ ){
      std::vector<SpatialOps::SpatFldPtr<FieldT> > drdvPtrs;
      drdvPtrs.push_back( this->sensitivity_result_ptr( phiRhsTag, phiPrimTag_ ) );
      drdvPtrs.push_back( this->sensitivity_result_ptr( phiRhsTag, psiPrimTag_ ) );
      drdvPtrs.push_back( this->sensitivity_result_ptr( psiRhsTag, phiPrimTag_ ) );
      drdvPtrs.push_back( this->sensitivity_result_ptr( psiRhsTag, psiPrimTag_ ) );
      SpatialOps::FieldMatrix<FieldT> drdv( drdvPtrs );
      drhsdv_.compute_jacobian( drdv );
    }
  }
};




template<typename FieldT>
class AssemblerDummy : public Expr::Expression<FieldT>
{
protected:
  boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
  const Expr::matrix::OrdinalType nrows_;

  AssemblerDummy( boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
                  const Expr::matrix::OrdinalType nrows )
  : assembler_( assembler ),
    nrows_( nrows )
  {
    std::cout << "ASSEMBLER TEST\n----------------\n";
    assembler_->create_all_field_requests( *this );
  }

public:

  class Builder : public Expr::ExpressionBuilder
  {
  protected:
    boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
    const Expr::matrix::OrdinalType nrows_;


  public:
    Builder( boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
             const Expr::matrix::OrdinalType nrows,
             const Expr::Tag& resultTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
  : ExpressionBuilder( resultTag, nghost ),
    assembler_( assembler ),
    nrows_( nrows )
  {}

    Expr::ExpressionBase* build() const
    {
      return new AssemblerDummy<FieldT>( assembler_, nrows_ );
    }
  };

  ~AssemblerDummy(){}

  void evaluate()
  {
    typedef std::vector<SpatialOps::SpatFldPtr<FieldT> > SpatFldPtrVctr;

    FieldT& thisValue = this->value();
    SpatFldPtrVctr matPtrs;
    for( size_t i=0; i<nrows_; ++i ){
      for( size_t j=0; j<nrows_; ++j ){
        matPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( thisValue ) );
      }
    }
    SpatialOps::FieldMatrix<FieldT> mat( matPtrs );

    for( Expr::matrix::OrdinalType rowIdx=0; rowIdx < nrows_; ++rowIdx ){
      SpatialOps::FieldVector<FieldT> row( mat.row( rowIdx ) );
      assembler_->assemble( row, rowIdx, Expr::matrix::Place() );
    }

    this->value() <<= 0.0;
  }
};





bool test( const int phiPrimIdx,
           const int psiPrimIdx,
           const int phiVarIdx,
           const int phiRhsIdx,
           const int psiVarIdx,
           const int psiRhsIdx )
{

  /* ---------------------------------------------------------------
   * domain, operators, field managers, etc setup
   * ---------------------------------------------------------------
   */
  Expr::ExprPatch patch(1);
  SpatialOps::OperatorDatabase sodb;
  Expr::FieldManagerList& fml = patch.field_manager_list();

  /* ---------------------------------------------------------------
   * variable and rhs names must be made before the integrator
   * for block-implicit's chain rule assembly
   * ---------------------------------------------------------------
   */
  std::string phiName = "phi";
  std::string psiName = "psi";
  std::string phiPrimName = "phi-prim";
  std::string psiPrimName = "psi-prim";
  const Expr::Tag phiTag    ( phiName,          Expr::STATE_NONE );
  const Expr::Tag phiPrimTag( phiPrimName,      Expr::STATE_NONE );
  const Expr::Tag phiRHSTag ( phiName + "_rhs", Expr::STATE_NONE );
  const Expr::Tag psiTag    ( psiName,          Expr::STATE_NONE );
  const Expr::Tag psiPrimTag( psiPrimName,      Expr::STATE_NONE );
  const Expr::Tag psiRHSTag ( psiName + "_rhs", Expr::STATE_NONE );
  Expr::TagList rhsTags;
  rhsTags.push_back( phiRHSTag );
  rhsTags.push_back( psiRHSTag );

  const double phiPrimScale = 2.0;
  const double psiPrimScale = 3.0;

  /* ---------------------------------------------------------------
   * build integrator, set constant physical and dual time step
   * ---------------------------------------------------------------
   */
  const unsigned bdfOrder = 1;

  const double dt = 0.01;
  const double ds = 1e16;
  const Expr::Tag dtTag( "timestep"     , Expr::STATE_NONE );
  const Expr::Tag dsTag( "dual_timestep", Expr::STATE_NONE );

  typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> IntegratorT;
  boost::shared_ptr<IntegratorT> integrator = boost::make_shared<IntegratorT>( 0,
                                                                               "Block Implicit BDF",
                                                                               dtTag,
                                                                               dsTag,
                                                                               bdfOrder );

  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
  integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dt ) );
  integrator->set_dual_time_step_expression    ( new ConstantSingleValueT( dsTag, ds ) );

  /* ---------------------------------------------------------------
   * register variables and RHS at STATE_NONE to the integrator
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory& factory = integrator->factory();

  const double k   =  4.0; // oscillation frequency
  const double K11 =  1*k;
  const double K12 =  1*k;
  const double K21 = -2*k;
  const double K22 = -1*k;


  factory.register_expression( new Expr::LinearFunction<FieldT>::Builder( phiPrimTag, phiTag, phiPrimScale, 0.0 ) );
  factory.register_expression( new Expr::LinearFunction<FieldT>::Builder( psiPrimTag, psiTag, psiPrimScale, 0.0 ) );
  factory.register_expression( new FullRhsExprPrimitives<FieldT>::Builder( rhsTags, phiPrimTag, psiPrimTag, K11, K12, K21, K22, phiPrimScale, psiPrimScale ) );

  // add variables to the integrator, specifying the indices for this test
  // note: the indices don't have to be specified, nor do all or none have to be specified
  integrator->add_variable<FieldT>( phiTag.name(), rhsTags[0], phiVarIdx, phiRhsIdx );
  integrator->add_variable<FieldT>( psiTag.name(), rhsTags[1], psiVarIdx, psiRhsIdx );

  // need to call 'done_adding_variables' before indices can be obtained
  integrator->done_adding_variables();


  auto make_jacobian = [=]()
  {
    // build the Jacobian matrix of the rhs to the primitives
    ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, dRdV )
    dRdV->element<FieldT>(phiRhsIdx,phiPrimIdx) = Expr::matrix::sensitivity( phiRHSTag, phiPrimTag );
    dRdV->element<FieldT>(psiRhsIdx,phiPrimIdx) = Expr::matrix::sensitivity( psiRHSTag, phiPrimTag );
    dRdV->element<FieldT>(phiRhsIdx,psiPrimIdx) = Expr::matrix::sensitivity( phiRHSTag, psiPrimTag );
    dRdV->element<FieldT>(psiRhsIdx,psiPrimIdx) = Expr::matrix::sensitivity( psiRHSTag, psiPrimTag );
    dRdV->finalize();

    // build the transformation matrix of the primitives to the governing variables
    ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, dVdU )
    dVdU->element<double>(phiPrimIdx,phiVarIdx) = 1.0 / phiPrimScale;
    dVdU->element<double>(psiPrimIdx,psiVarIdx) = 1.0 / psiPrimScale;
    dVdU->finalize();

    // form the Jacobian matrix of the rhs to the governing variables
    return dRdV * dVdU;
  };

  // tell the integrator to use this Jacobian
  // note: we don't set a preconditioning matrix in this case, so the default identity matrix is used
  integrator->set_jacobian( make_jacobian() );

  Expr::Tag resTag( "res", Expr::STATE_NONE );
  ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, zero )
  zero->finalize();
  integrator->get_tree().insert_tree( factory.register_expression( new AssemblerDummy<FieldT>::Builder( make_jacobian(), 3, resTag ) ) );

  /* ---------------------------------------------------------------
   * final integrator prep, must happen before setting initial conditions, and tree output
   * ---------------------------------------------------------------
   */
  integrator->prepare_for_integration( fml, sodb, patch.field_info() );
  {
    std::ofstream f("dualtimeOscillationTest-integrator-tree.dot");
    integrator->get_tree().write_tree( f );
  }


  /* ---------------------------------------------------------------
   * set initial conditions with STATE_N
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory icFactory;

  const double phi0 = 1.0;
  const double psi0 = 1.0;

  const Expr::Tag phiNTag( phiName, Expr::STATE_N );
  const Expr::Tag psiNTag( psiName, Expr::STATE_N );
  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  const Expr::ExpressionID phiId = icFactory.register_expression( new ConstantT( phiNTag, phi0 ) );
  const Expr::ExpressionID psiId = icFactory.register_expression( new ConstantT( psiNTag, psi0 ) );

  std::set<Expr::ExpressionID> rootIds;
  rootIds.insert( phiId );
  rootIds.insert( psiId );
  Expr::ExpressionTree icTree( rootIds, icFactory, patch.id() );

  icTree.register_fields( fml );
  icTree.bind_fields( fml );
  icTree.execute_tree();
  {
    std::ofstream f("dualtimeOscillationTest-initialcond-tree.dot");
    icTree.write_tree( f );
  }

  TestHelper status( false );

  /* ---------------------------------------------------------------
   * set iteration counts, prep solution history file
   * ---------------------------------------------------------------
   */
  const unsigned maxIterPerStep = 100;

  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;

  /* ---------------------------------------------------------------
   * perform the integration
   * ---------------------------------------------------------------
   */
  const double tend = 1.0;

  double t = 0;
  while( t<tend ){

    dualIter = 0;
    integrator->begin_time_step();

    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;
    } while( !converged && dualIter <= maxIterPerStep );

    integrator->end_time_step();
    stepCount++;
    t += dt;
  }

  /* ---------------------------------------------------------------
   * process the test
   * ---------------------------------------------------------------
   */

  const double meanStepPerIter = ( (double) totalIter ) / ( (double) stepCount );
  const double meanStepPerIterFromMatlabCode = 2.0;
  status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "convergence rate check" );

  const FieldT& phiPredicted     = fml.field_ref<FieldT>( Expr::Tag( phiName, Expr::STATE_N ) );
  const FieldT& psiPredicted     = fml.field_ref<FieldT>( Expr::Tag( psiName, Expr::STATE_N ) );
  const double phiFromMatlabCode = -1.999662876681719;
  const double psiFromMatlabCode =  1.487205902604459;

  SpatialOps::SpatFldPtr<FieldT> tmpPhi = SpatialOps::SpatialFieldStore::get<FieldT>( phiPredicted );
  SpatialOps::SpatFldPtr<FieldT> tmpPsi = SpatialOps::SpatialFieldStore::get<FieldT>( phiPredicted );
  *tmpPhi <<= phiFromMatlabCode;
  *tmpPsi <<= psiFromMatlabCode;

  status( SpatialOps::field_equal_abs( phiPredicted, *tmpPhi, 1e-6 ), "phi value-check" );
  status( SpatialOps::field_equal_abs( psiPredicted, *tmpPsi, 1e-6 ), "psi value-check" );

  return status.ok();
}



int main( int iarg, char* carg[] )
{
  try{
    TestHelper status( true );

    status( test( 0, 1, 0, 0, 1, 1 ), "ordering (0-1), (0,0), (1,1)" );
    status( test( 0, 1, 1, 1, 0, 0 ), "ordering (0-1), (1,1), (0,0)" );

    status( test( 1, 0, 0, 0, 1, 1 ), "ordering (1-0), (0,0), (1,1)" );
    status( test( 1, 0, 1, 1, 0, 0 ), "ordering (1-0), (1,1), (0,0)" );

    status( test( 0, 1, 0, 1, 1, 0 ), "ordering (0-1), (0,1), (1,0)" );
    status( test( 1, 0, 0, 1, 1, 0 ), "ordering (1-0), (0,1), (1,0)" );

    status( test( 0, 1, 1, 0, 0, 1 ), "ordering (0-1), (1,0), (0,1)" );
    status( test( 1, 0, 1, 0, 0, 1 ), "ordering (1-0), (1,0), (0,1)" );

    if( status.ok() ){
      std::cout << "PASS\n";
      return 0;
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }

  std::cout << "FAIL\n";
  return -1;
}

