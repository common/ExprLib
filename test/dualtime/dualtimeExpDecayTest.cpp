#include <expression/dualtime/FixedPointBDFDualTimeIntegrator.h>
#include <expression/dualtime/VariableImplicitBDFDualTimeIntegrator.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>

#include <test/TestHelper.h>
#include "ExpDecay.h"
#include <expression/Functions.h>
#include <expression/ExpressionTree.h>
#include <expression/ExprPatch.h>

#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/matrix-assembly/ScaledIdentityMatrix.h>

#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

namespace po = boost::program_options;
namespace so = SpatialOps;

typedef so::SVolField FieldT;

enum IntegratorType{
  FIXED_POINT,
  VARIABLE_IMPLICIT,
  BLOCK_IMPLICIT
};

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >();


bool test( const IntegratorType integratorType )
{
  /* ---------------------------------------------------------------
   * domain, operators, field managers, etc setup
   * ---------------------------------------------------------------
   */
  Expr::ExprPatch patch(1);
  SpatialOps::OperatorDatabase sodb;
  Expr::FieldManagerList& fml = patch.field_manager_list();

  /* ---------------------------------------------------------------
   * variable and rhs names, jacobian, preconditioner
   * ---------------------------------------------------------------
   */
  std::string phiName = "phi";
  const Expr::Tag phiTag   ( phiName,          Expr::STATE_NONE );
  const Expr::Tag phiNTag  ( phiName,          Expr::STATE_N    );
  const Expr::Tag phiRHSTag( phiName + "_rhs", Expr::STATE_NONE );

  /* ---------------------------------------------------------------
   * build integrator, set constant physical and dual time step
   * ---------------------------------------------------------------
   */
  const unsigned bdfOrder = 1;
  boost::shared_ptr<Expr::DualTime::BDFDualTimeIntegrator> integrator;

  const Expr::Tag dtTag( "timestep"     , Expr::STATE_NONE );
  const Expr::Tag dsTag( "dual_timestep", Expr::STATE_NONE );

  double ds; // dual time step size

  switch( integratorType ){
    case FIXED_POINT:{
      ds = 0.01;
      integrator = boost::make_shared<Expr::DualTime::FixedPointBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                 "Fixed Point BDF",
                                                                                                 dtTag,
                                                                                                 dsTag,
                                                                                                 bdfOrder );
      break;
    }
    case VARIABLE_IMPLICIT:{
      ds = 1e16;
      integrator = boost::make_shared<Expr::DualTime::VariableImplicitBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                       "Variable Implicit BDF",
                                                                                                       dtTag,
                                                                                                       dsTag,
                                                                                                       bdfOrder );
      break;
    }
    case BLOCK_IMPLICIT:{
      ds = 1e16;
      integrator = boost::make_shared<Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                    "Block Implicit BDF",
                                                                                                    dtTag,
                                                                                                    dsTag,
                                                                                                    bdfOrder );
      break;
    }
  }

  const double dt = 0.01;
  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
  integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dt ) );
  integrator->set_dual_time_step_expression    ( new ConstantSingleValueT( dsTag, ds ) );


  /* ---------------------------------------------------------------
   * register variables and RHS at STATE_NONE to the integrator
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory& factory = integrator->factory();

  const double rateConst = 1.3;
  factory.register_expression( new ExpDecayRHS<FieldT>::Builder( rateConst, phiRHSTag, phiTag ) );
  integrator->add_variable<FieldT>( phiTag.name(), phiRHSTag );

  if( integratorType == BLOCK_IMPLICIT ){
    // because we have a generic integrator type, we need to cast to block implicit type to continue
    typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> BIType;
    auto biPtr = boost::dynamic_pointer_cast<BIType>( integrator );

    // need to call this before indices can be obtained
    biPtr->done_adding_variables();

    // these are maps from tags to indices
    std::map<Expr::Tag,int> varIndices = biPtr->variable_tag_index_map();
    std::map<Expr::Tag,int> rhsIndices = biPtr->rhs_tag_index_map();

    const int phiVarIdx = varIndices[phiTag];
    const int phiRhsIdx = rhsIndices[phiRHSTag];

    // build an assembler to compute the Jacobian matrix of the rhs to the governed variable
    ASSEMBLER( Expr::matrix::DenseSubMatrix, FieldT, jacobian )
    jacobian->element<FieldT>(phiRhsIdx,phiVarIdx) = Expr::matrix::sensitivity( phiRHSTag, phiTag );
    jacobian->finalize(); // need to finalize before using

    // build an identity matrix preconditioner
    ASSEMBLER( Expr::matrix::ScaledIdentityMatrix, FieldT, preconditioner )
    preconditioner->finalize(); // need to finalize before using

    // set the Jacobian and preconditioner assemblers
    biPtr->set_jacobian_and_preconditioner( jacobian, preconditioner );
  }

  /* ---------------------------------------------------------------
   * final integrator prep, must happen before setting initial conditions, and tree output
   * ---------------------------------------------------------------
   */
  integrator->prepare_for_integration( fml, sodb, patch.field_info() );

  integrator->lock_field<FieldT>( phiNTag );
  integrator->lock_all_execution_fields();
  integrator->copy_from_initial_condition_to_execution<FieldT>( phiNTag.name() );

  {
    std::ofstream f("dualtimeExpDecayTest-integrator-tree.dot");
    integrator->get_tree().write_tree( f );
  }

  /* ---------------------------------------------------------------
   * set initial conditions with STATE_N
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory icFactory;

  const double phi0 = 1.0;
  const Expr::ExpressionID id = icFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( phiNTag, phi0 ) );
  Expr::ExpressionTree icTree( id, icFactory, patch.id() );

  icTree.register_fields( fml );
  icTree.bind_fields( fml );
  icTree.execute_tree();

  /* ---------------------------------------------------------------
   * set iteration counts, prep solution history file
   * ---------------------------------------------------------------
   */

  const unsigned maxIterPerStep = 100;

  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;

  std::ofstream fout("dualtimeExpDecayTest-solution-history.txt");
  fout << "      t \t              c-exact \t              c\n";


  /* ---------------------------------------------------------------
   * perform the integration
   * ---------------------------------------------------------------
   */
  const double tend = 2.0;

  double t = 0;
  while( t<tend ){

    dualIter = 0;
    integrator->begin_time_step();

    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;
    } while( !converged && dualIter <= maxIterPerStep );

    integrator->end_time_step();
    stepCount++;
    t += dt;

    FieldT& phiN = fml.field_ref<FieldT>(phiNTag);
    phiN.add_device(CPU_INDEX);
    fout << std::scientific << std::setprecision(10)
         << t                      << "\t"
         << phi0*exp(-rateConst*t) << "\t"
         << phiN[0]                << std::endl;
  }
  fout.close();


  /* ---------------------------------------------------------------
   * process the test
   * ---------------------------------------------------------------
   */
  TestHelper status(false);

  if( integratorType != FIXED_POINT ){
    status( integrator->get_tree().computes_field( sens_tag( phiRHSTag, phiTag ) ),
            "Computes d phi_rhs / d phi" );
    status( integrator->get_tree().computes_field( sens_tag(phiTag,phiTag) ),
            "Computes d phi / d phi" );
  }

  const FieldT& phiPredicted      = fml.field_ref<FieldT>( phiNTag );
  const double  phiFromMatlabCode = 0.075528511873306;
  SpatialOps::SpatFldPtr<FieldT> tmp = SpatialOps::SpatialFieldStore::get<FieldT>( phiPredicted );
  *tmp <<= phiFromMatlabCode;
  status( SpatialOps::field_equal_abs( phiPredicted, *tmp, 1e-6 ), "phi value-check" );

  const double meanStepPerIter = ( (double) totalIter ) / ( (double) stepCount );
  switch( integratorType ){
    case FIXED_POINT:{
      const double meanStepPerIterFromMatlabCode = 27;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "fixed-point convergence rate check" );
      break;
    }
    case VARIABLE_IMPLICIT:{
      const double meanStepPerIterFromMatlabCode = 2;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "variable-implicit convergence rate check" );
      break;
    }
    case BLOCK_IMPLICIT:{
      const double meanStepPerIterFromMatlabCode = 2;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "block-implicit convergence rate check" );
      break;
    }
  }
  return status.ok();
}

int main( int iarg, char* carg[] )
{
  /* ---------------------------------------------------------------
   * initialization from command line options
   * ---------------------------------------------------------------
   */
  bool fixedPoint       = false;
  bool variableImplicit = false;
  bool blockImplicit    = false;

  {
    po::options_description desc("Supported Options");
    desc.add_options()
        ( "help", "print help message" )
        ( "fixed-point", "Use the fixed point dualtime integration scheme")
        ( "variable-implicit", "Use the variable implicit dualtime integration scheme")
        ( "block-implicit", "Use the block implicit dualtime integration scheme");;
    po::variables_map args;
    try{
      po::store( po::parse_command_line(iarg,carg,desc), args );
      po::notify(args);
    }
    catch( std::exception& err ){
      cout << "\nError parsing command line options.\n\n"
          << err.what() << "\n\n" << desc << "\n";
      return -1;
    }
    if( args.count("help") ){
      cout << desc << "\n";
      return 1;
    }
    fixedPoint       = args.count("fixed-point");
    variableImplicit = args.count("variable-implicit");
    blockImplicit    = args.count("block-implicit");

    if( !fixedPoint && !variableImplicit && !blockImplicit ){
      std::cout << "Error: no dualtime integration method selected.\n"
                << desc << std::endl;
        return -1;
    }
  }

  try{
    TestHelper status( true );
    if( fixedPoint       ) status( test( FIXED_POINT       ), "Fixed Point"       );
    if( variableImplicit ) status( test( VARIABLE_IMPLICIT ), "Variable Implicit" );
    if( blockImplicit    ) status( test( BLOCK_IMPLICIT    ), "Block Implicit" );

    if( status.ok() ){
      std::cout << "PASS\n";
      return 0;
    }

  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
  }
  std::cout << "FAIL\n";
  return -1;
}
