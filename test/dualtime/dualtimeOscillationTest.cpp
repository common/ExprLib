#include <expression/dualtime/FixedPointBDFDualTimeIntegrator.h>
#include <expression/dualtime/VariableImplicitBDFDualTimeIntegrator.h>
#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>

#include <test/TestHelper.h>

#include "TwoEquationLinearRhs.h"
#include <expression/Functions.h>
#include <expression/ExpressionTree.h>
#include <expression/ExprPatch.h>

#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

namespace po = boost::program_options;
namespace so = SpatialOps;

typedef so::SVolField FieldT;

enum IntegratorType{
  FIXED_POINT,
  VARIABLE_IMPLICIT,
  BLOCK_IMPLICIT
};

bool test( const IntegratorType integratorType )
{

  /* ---------------------------------------------------------------
   * domain, operators, field managers, etc setup
   * ---------------------------------------------------------------
   */
  Expr::ExprPatch patch(1);
  SpatialOps::OperatorDatabase sodb;
  Expr::FieldManagerList& fml = patch.field_manager_list();

  /* ---------------------------------------------------------------
   * variable and rhs names must be made before the integrator
   * for block-implicit's chain rule assembly
   * ---------------------------------------------------------------
   */
  std::string phiName = "phi";
  std::string psiName = "psi";
  const Expr::Tag phiTag   ( phiName,          Expr::STATE_NONE );
  const Expr::Tag phiRHSTag( phiName + "_rhs", Expr::STATE_NONE );
  const Expr::Tag psiTag   ( psiName,          Expr::STATE_NONE );
  const Expr::Tag psiRHSTag( psiName + "_rhs", Expr::STATE_NONE );

  /* ---------------------------------------------------------------
   * build integrator, set constant physical and dual time step
   * ---------------------------------------------------------------
   */
  const unsigned bdfOrder = 1;
  boost::shared_ptr<Expr::DualTime::BDFDualTimeIntegrator> integrator;

  const Expr::Tag dtTag( "timestep"     , Expr::STATE_NONE );
  const Expr::Tag dsTag( "dual_timestep", Expr::STATE_NONE );

  double ds; // dual time step size

  switch( integratorType ){
    case FIXED_POINT:{
      ds = 0.01;
      integrator = boost::make_shared<Expr::DualTime::FixedPointBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                 "Fixed Point BDF",
                                                                                                 dtTag,
                                                                                                 dsTag,
                                                                                                 bdfOrder );
      break;
    }
    case VARIABLE_IMPLICIT:{
      ds = 1e16;
      integrator = boost::make_shared<Expr::DualTime::VariableImplicitBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                       "Variable Implicit BDF",
                                                                                                       dtTag,
                                                                                                       dsTag,
                                                                                                       bdfOrder );
      break;
    }
    case BLOCK_IMPLICIT:{
      ds = 1e16;
      integrator = boost::make_shared<Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT> >( 0,
                                                                                                    "Block Implicit BDF",
                                                                                                    dtTag,
                                                                                                    dsTag,
                                                                                                    bdfOrder );
      break;
    }
  }

  const double dt   = 0.01;

  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
  integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dt ) );
  integrator->set_dual_time_step_expression    ( new ConstantSingleValueT( dsTag, ds ) );

  /* ---------------------------------------------------------------
   * register variables and RHS at STATE_NONE to the integrator
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory& factory = integrator->factory();

  const double k   =  4.0; // oscillation frequency
  const double K11 =  1*k;
  const double K12 =  1*k;
  const double K21 = -2*k;
  const double K22 = -1*k;

  factory.register_expression( new TwoEquationLinearRhs<FieldT>::Builder( K11, K12, phiRHSTag, phiTag, psiTag ) );
  factory.register_expression( new TwoEquationLinearRhs<FieldT>::Builder( K22, K21, psiRHSTag, psiTag, phiTag ) );

  integrator->add_variable<FieldT>( phiTag.name(), phiRHSTag );
  integrator->add_variable<FieldT>( psiTag.name(), psiRHSTag );

  /* ---------------------------------------------------------------
   * final integrator prep, must happen before setting initial conditions, and tree output
   * ---------------------------------------------------------------
   */
  integrator->prepare_for_integration( fml, sodb, patch.field_info() );
  {
    std::ofstream f("dualtimeOscillationTest-integrator-tree.dot");
    integrator->get_tree().write_tree( f );
  }

  /* ---------------------------------------------------------------
   * set initial conditions with STATE_N
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory icFactory;

  const double phi0 = 1.0;
  const double psi0 = 1.0;

  const Expr::Tag phiNTag( phiName, Expr::STATE_N );
  const Expr::Tag psiNTag( psiName, Expr::STATE_N );
  typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
  const Expr::ExpressionID phiId = icFactory.register_expression( new ConstantT( phiNTag, phi0 ) );
  const Expr::ExpressionID psiId = icFactory.register_expression( new ConstantT( psiNTag, psi0 ) );

  std::set<Expr::ExpressionID> rootIds;
  rootIds.insert( phiId );
  rootIds.insert( psiId );
  Expr::ExpressionTree icTree( rootIds, icFactory, patch.id() );

  icTree.register_fields( fml );
  icTree.bind_fields( fml );
  icTree.execute_tree();
  {
    std::ofstream f("dualtimeOscillationTest-initialcond-tree.dot");
    icTree.write_tree( f );
  }

  integrator->get_tree().lock_fields(fml);
  integrator->get_tree().execute_tree();

  TestHelper status( true );

  if( integratorType == VARIABLE_IMPLICIT || integratorType == BLOCK_IMPLICIT ){
    const FieldT& hh = fml.field_ref<FieldT>( sens_tag( phiTag, phiTag ) );
    const FieldT& ss = fml.field_ref<FieldT>( sens_tag( psiTag, psiTag ) );
    const FieldT& hs = fml.field_ref<FieldT>( sens_tag( phiTag, psiTag ) );
    const FieldT& sh = fml.field_ref<FieldT>( sens_tag( psiTag, phiTag ) );

    SpatialOps::SpatFldPtr<FieldT> tmp = SpatialOps::SpatialFieldStore::get<FieldT>( hh );

    *tmp <<= 1.0;
    status( SpatialOps::field_equal_abs(hh,*tmp), "dh/dh" );
    status( SpatialOps::field_equal_abs(ss,*tmp), "ds/ds" );

    *tmp <<= 0.0;
    status( SpatialOps::field_equal_abs(hs,*tmp), "dh/ds" );
    status( SpatialOps::field_equal_abs(sh,*tmp), "ds/dh" );
  }

  status( !integrator->get_tree().computes_field( phiNTag ), "doesn't compute phi (placeholder)");
  status( !integrator->get_tree().computes_field( psiNTag ), "doesn't compute psi (placeholder)");

  /* ---------------------------------------------------------------
   * set iteration counts, prep solution history file
   * ---------------------------------------------------------------
   */
  const unsigned maxIterPerStep = 100;

  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;

  /* ---------------------------------------------------------------
   * perform the integration
   * ---------------------------------------------------------------
   */
  const double tend = 1.0;

  double t = 0;
  while( t<tend ){

    dualIter = 0;
    integrator->begin_time_step();

    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;
    } while( !converged && dualIter <= maxIterPerStep );

    integrator->end_time_step();
    stepCount++;
    t += dt;
  }

  /* ---------------------------------------------------------------
   * process the test
   * ---------------------------------------------------------------
   */

  const double meanStepPerIter = ( (double) totalIter ) / ( (double) stepCount );

  switch( integratorType ){
    case FIXED_POINT:{
      const double meanStepPerIterFromMatlabCode = 30.07;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "fixed-point convergence rate check" );
      break;
    }
    case VARIABLE_IMPLICIT:{
      const double meanStepPerIterFromMatlabCode = 8.6;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "variable-implicit convergence rate check" );
      break;
    }
    case BLOCK_IMPLICIT:{
      const double meanStepPerIterFromMatlabCode = 2.0;
      status( std::abs( meanStepPerIter - meanStepPerIterFromMatlabCode ) < 1e-8, "block-implicit convergence rate check" );
      break;
    }
  }

  const FieldT& phiPredicted     = fml.field_ref<FieldT>( Expr::Tag( phiName, Expr::STATE_N ) );
  const FieldT& psiPredicted     = fml.field_ref<FieldT>( Expr::Tag( psiName, Expr::STATE_N ) );
  const double phiFromMatlabCode = -1.999662876681719;
  const double psiFromMatlabCode =  1.487205902604459;

  SpatialOps::SpatFldPtr<FieldT> tmpPhi = SpatialOps::SpatialFieldStore::get<FieldT>( phiPredicted );
  SpatialOps::SpatFldPtr<FieldT> tmpPsi = SpatialOps::SpatialFieldStore::get<FieldT>( phiPredicted );
  *tmpPhi <<= phiFromMatlabCode;
  *tmpPsi <<= psiFromMatlabCode;

  status( SpatialOps::field_equal_abs( phiPredicted, *tmpPhi, 1e-6 ), "phi value-check" );
  status( SpatialOps::field_equal_abs( psiPredicted, *tmpPsi, 1e-6 ), "psi value-check" );

  return status.ok();
}



int main( int iarg, char* carg[] )
{
  /* ---------------------------------------------------------------
   * initialization from command line options
   * ---------------------------------------------------------------
   */
  bool fixedPoint       = false;
  bool variableImplicit = false;
  bool blockImplicit    = false;

  // parse the command line options input describing the problem
  try{
    po::options_description desc("Supported Options");
    desc.add_options()
        ( "help", "print help message" )
        ( "fixed-point", "Use the fixed point dualtime integration scheme")
        ( "variable-implicit", "Use the variable implicit dualtime integration scheme")
        ( "block-implicit", "Use the block implicit dualtime integration scheme");
    po::variables_map args;

    try{
      po::store( po::parse_command_line(iarg,carg,desc), args );
      po::notify(args);
    }
    catch( std::exception& err ){
      cout << "\nError parsing command line options.\n\n"
          << err.what() << "\n\n" << desc << "\n";
      return -1;
    }
    if( args.count("help") ){
      cout << desc << "\n";
      return 1;
    }

    fixedPoint       = args.count("fixed-point");
    variableImplicit = args.count("variable-implicit");
    blockImplicit    = args.count("block-implicit");

    if( !fixedPoint && !variableImplicit && !blockImplicit ){
      std::cout << "Error: no dualtime integration method selected.\n"
                << desc << std::endl;
        return -1;
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
    return -1;
  }

  try{
    TestHelper status( true );
    if( fixedPoint       ) status( test( FIXED_POINT       ), "Fixed Point"       );
    if( variableImplicit ) status( test( VARIABLE_IMPLICIT ), "Variable Implicit" );
    if( blockImplicit    ) status( test( BLOCK_IMPLICIT    ), "Block Implicit" );

    if( status.ok() ){
      std::cout << "PASS\n";
      return 0;
    }
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
    return -1;
  }

  std::cout << "FAIL\n";
  return -1;
}

