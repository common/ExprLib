/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   heatEqn.cpp
 *  \date   Aug 4, 2015
 *  \author "James C. Sutherland"
 */

#include <iostream>
using std::cout;
using std::endl;

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/util/TimeLogger.h>

namespace so = SpatialOps;
typedef so::SVolField   VolField;
typedef so::SSurfXField FaceField;

typedef so::BasicOpTypes<VolField>::GradX      GradX;
typedef so::BasicOpTypes<VolField>::InterpC2FX InterpX;
typedef so::BasicOpTypes<VolField>::DivX       DivX;

//-- boost includes ---//
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <expression/ExprLib.h>
#include <expression/dualtime/FixedPointBDFDualTimeIntegrator.h>
#include <expression/dualtime/VariableImplicitBDFDualTimeIntegrator.h>
#include <expression/BoundaryConditionExpression.h>


#include <test/simpleHeatEqn/RHSExpr.h>
#include <test/simpleHeatEqn/Flux.h>

namespace po = boost::program_options;
using SpatialOps::IntVec;


void setup_bcs( Expr::ExpressionFactory& factory,
                const Expr::Tag& tempTag,
                const so::Grid& grid )
{
  std::vector<IntVec> xminusPts, xplusPts;
  xminusPts.push_back( IntVec( 0,              0, 0 ) );
  xplusPts .push_back( IntVec( grid.extent(0), 0, 0 ) );

  const so::GhostData ghost( IntVec(1,0,0), IntVec(1,0,0) );
  const so::BoundaryCellInfo bc = so::BoundaryCellInfo::build<FaceField>(true,true,false,false,false,false);
  const so::MemoryWindow window( so::get_window_with_ghost( grid.extent(), ghost, bc ) );

  typedef Expr::ConstantBCOpExpression<VolField,so::XDIR>::Builder XBC;
  XBC::MaskPtr xminus( new XBC::MaskType( window, bc, ghost, xminusPts ) );
  XBC::MaskPtr xplus ( new XBC::MaskType( window, bc, ghost, xplusPts  ) );

# ifdef ENABLE_CUDA
  // Masks are created on CPU so we need to explicitly transfer them to GPU
  xminus->add_consumer( GPU_INDEX );
  xplus ->add_consumer( GPU_INDEX );
# endif

  // jcs note that we need to have a better way of setting BCs on the old state.
  const Expr::Tag tempTagOld( tempTag.name(),Expr::STATE_N );
  const Expr::Tag xmTag( "xmbc", Expr::STATE_NONE );
  const Expr::Tag xpTag( "xpbc", Expr::STATE_NONE );
  const Expr::Tag xmTagDT( "xmbc-dualtime", Expr::STATE_NONE );
  const Expr::Tag xpTagDT( "xpbc-dualtime", Expr::STATE_NONE );
  factory.register_expression( new XBC( xmTag,   xminus, so::DIRICHLET, so::MINUS_SIDE, 2.0 ) );
  factory.register_expression( new XBC( xpTag,   xplus , so::DIRICHLET, so::PLUS_SIDE,  2.0 ) );
  factory.register_expression( new XBC( xmTagDT, xminus, so::DIRICHLET, so::MINUS_SIDE, 2.0 ) );
  factory.register_expression( new XBC( xpTagDT, xplus , so::DIRICHLET, so::PLUS_SIDE,  2.0 ) );

  factory.attach_modifier_expression( xmTagDT, tempTag    );
  factory.attach_modifier_expression( xpTagDT, tempTag    );
  factory.attach_modifier_expression( xmTag,   tempTagOld );
  factory.attach_modifier_expression( xpTag,   tempTagOld );
}

//===================================================================

int main( int iarg, char* carg[] )
{
  double dt, ds, tend;
  IntVec npts(10,1,1);
  so::DoubleVec length(1,1,1);

# ifdef ENABLE_THREADS
  int soThreadCount, exprThreadCount;
# endif

  // parse the command line options input describing the problem
  {
    po::options_description desc("Supported Options");
    desc.add_options()
      ( "help", "print help message" )
      ( "dt",   po::value<double>(&dt  )->default_value(1e-1), "time step size" )
      ( "tend", po::value<double>(&tend)->default_value(10), "end time" )
      ( "ds",po::value<double>(&ds)->default_value(0.05), "dual time step size" )
#     ifdef ENABLE_THREADS
      ( "tc", po::value<int>(&soThreadCount)->default_value(NTHREADS), "Number of threads for Nebo")
      ( "etc", po::value<int>(&exprThreadCount)->default_value(1), "Number of threads for ExprLib")
#     endif
      ( "nx", po::value<int>(&npts[0])->default_value(40), "nx" )
      ( "Lx", po::value<double>(&length[0])->default_value(10),"Length in x");

    po::variables_map args;
    try{
      po::store( po::parse_command_line(iarg,carg,desc), args );
      po::notify(args);
    }
    catch( std::exception& err ){
      cout << err.what()
          << "\nusage:\n" << desc << "\n";
      return 1;
    }
    if (args.count("help")) {
      cout << desc << "\n";
      return 1;
    }
  }

# ifdef ENABLE_THREADS
  cout << "ENABLE_THREADS is ON" << endl;
  SpatialOps::set_hard_thread_count( NTHREADS );
  SpatialOps::set_soft_thread_count( soThreadCount );
  Expr::set_hard_thread_count( NTHREADS );
  Expr::set_soft_thread_count( exprThreadCount );
  sleep(1);
# endif

  cout
#      ifdef ENABLE_THREADS
       << " SpatialOps NTHREADS (can set at runtime) = " << SpatialOps::get_soft_thread_count()
       << " out of " << SpatialOps::get_hard_thread_count() << endl
       << " ExprLib    NTHREADS (can set at runtime) = "
       << SpatialOps::ThreadPool::get_pool_size() << " out of "
       << SpatialOps::ThreadPool::get_pool_capacity() << endl
#      endif
       << endl;

  // build the spatial operators
  const so::Grid grid( npts, length );
  SpatialOps::OperatorDatabase sodb;
  so::build_stencils( grid, sodb );

  const Expr::Tag tempt   ( "Temperature",   Expr::STATE_NONE );
  const Expr::Tag xfluxt  ( "HeatFluxX",     Expr::STATE_NONE );
  const Expr::Tag dcoef   ( "ThermalDiff",   Expr::STATE_NONE );
  const Expr::Tag dtTag   ( "timestep",      Expr::STATE_NONE );
  const Expr::Tag dsTag   ( "dual-timestep", Expr::STATE_NONE );
  const Expr::Tag rhsTag  ( "rhs",           Expr::STATE_NONE );

  const unsigned bdfOrder = 1;
  Expr::DualTime::BDFDualTimeIntegrator* integrator = new Expr::DualTime::FixedPointBDFDualTimeIntegrator<VolField>( 0, "BasicDualTime", dtTag, dsTag, bdfOrder );

  Expr::ExpressionFactory& factory = integrator->factory();

  factory.register_expression( new Flux<GradX,InterpX>::Builder( xfluxt, tempt, dcoef ) );
  factory.register_expression( new RHSExpr<VolField>::Builder( rhsTag, xfluxt, Expr::Tag(), Expr::Tag() ) );
  factory.register_expression( new Expr::ConstantExpr<VolField>::Builder(dcoef,0.1) );

  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValue;
  integrator->set_physical_time_step_expression( new ConstantSingleValue(dtTag,dt) );
  integrator->set_dual_time_step_expression    ( new ConstantSingleValue(dsTag,ds) );

  //-- build a few coordinate fields
  const IntVec hasBC(true,true,true);
  const so::BoundaryCellInfo cellBCInfo = so::BoundaryCellInfo::build<VolField>(hasBC,hasBC);
  const so::GhostData cellGhosts(1);
  const so::MemoryWindow vwindow( so::get_window_with_ghost(npts,cellGhosts,cellBCInfo) );
  VolField xcoord( vwindow, cellBCInfo, cellGhosts, NULL );
  grid.set_coord<SpatialOps::XDIR>( xcoord );
# ifdef ENABLE_CUDA
  xcoord.add_device( GPU_INDEX );
# endif

  setup_bcs( factory, tempt, grid );

  Expr::FieldManagerList fml;

  try{
    const Expr::FieldAllocInfo ainfo( npts, 0, 0, false, false, false );

    integrator->add_variable<VolField>( tempt.name(), rhsTag );
    integrator->prepare_for_integration( fml, sodb, ainfo );
    integrator->request_timings(true);

    std::ofstream  fout("heatEqn.dot" );
    integrator->get_tree().write_tree( fout, false, true );
  }
  catch( std::exception& err ){
   std::cout << "ERROR allocating/binding fields" << std::endl
       << err.what() << std::endl;
   return -1;
  }

  //-- initialize the temperature
  {
    VolField& temp = fml.field_manager<VolField>().field_ref(Expr::Tag(tempt.name(),Expr::STATE_N));
    temp <<= sin( xcoord );
#   ifdef ENABLE_CUDA
    temp.validate_device( GPU_INDEX );
#   endif
    std::ofstream tprof("T0.dat");
    VolField::const_iterator it=temp.interior_begin();
    VolField::const_iterator ix=xcoord.interior_begin();
    for(; it!=temp.interior_end(); ++it, ++ix ){
      tprof << *ix << "\t" << *it << std::endl;
    }
  }

  try{

    const unsigned maxIterPerStep = 100;

    unsigned totalIter = 0;
    unsigned stepCount = 0;
    unsigned dualIter  = 0;
    bool     converged = false;

    double t = 0;
    while( t<tend ){

      dualIter = 0;
      integrator->begin_time_step();

      do{

        integrator->advance_dualtime( converged );

        dualIter++;
        totalIter++;

      } while( !converged && dualIter <= maxIterPerStep );

      integrator->end_time_step();
      stepCount++;
      t += dt;

    }

    {
#     ifdef ENABLE_CUDA
      VolField& temp = fml.field_ref<VolField>(tempt);
      temp.validate_device( GPU_INDEX );
#     else
      const VolField& temp = fml.field_ref<VolField>(tempt);
#     endif
      std::ofstream tprof("T.dat");
      VolField::const_iterator it=temp.interior_begin();
      VolField::const_iterator ix=xcoord.interior_begin();
      for(; it!=temp.interior_end(); ++it, ++ix ){
        tprof << *ix << "\t" << *it << std::endl;
      }
    }
    return 0;
  }
  catch( std::exception& err ){
    cout << "ERROR!" << endl
         << err.what() << endl;
  }
  return -1;

}


