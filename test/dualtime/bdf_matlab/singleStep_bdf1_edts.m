function u = singleStep_bdf1_edts(un,tn,r,h,ds,tol)

residual = tol + 1;
nv = numel(un);
uk = un;
count = 0;
while residual > tol
    rk = r(tn,uk);
    Hk = rk - (uk-un)/h;
    du = ds*Hk;
    uk = uk + du;
    residual = norm(du./(uk+tol));
    count = count + 1;
end
u = uk;
disp(count);