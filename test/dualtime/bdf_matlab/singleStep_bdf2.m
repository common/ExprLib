function u = singleStep_bdf2(un,unm1,tn,r,h,tol)

residual = tol + 1;
nv = numel(un);
uk = un;
while residual > tol
    rk = r(tn,uk);
    J = finite_difference_jacobian(r,uk,tn,rk);
    Hk = rk - (3*uk-4*un+unm1)/(2*h);
    dHdu = J - 1.5/h*eye(nv);
    du = -dHdu\Hk;
    uk = uk + du;
    residual = norm(du./(uk+tol));
end
u = uk;