function [t,u] = bdf4_edts(r,t,u0,ds,tol)
% BDF 4 scheme
%
% WARNING: ONLY VALID FOR CONSTANT TIME STEP
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-4';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

i = 2;
h = t(i) - t(i-1);
u(:,i) = singleStep_bdf1_edts(u(:,i-1),t(i-1),r,h,ds,tol);
i = 3;
h = t(i) - t(i-1);
u(:,i) = singleStep_bdf2_edts(u(:,i-1),u(:,i-2),t(i-1),r,h,ds,tol);
i = 4;
h = t(i) - t(i-1);
u(:,i) = singleStep_bdf3_edts(u(:,i-1),u(:,i-2),u(:,i-3),t(i-1),r,h,ds,tol);

%% after startup
for i = 5:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    unm1 = u(:,i-2);
    unm2 = u(:,i-3);
    unm3 = u(:,i-4);
    
    residual = tol + 1;
    uk = un;
    count = 0;
    while residual > tol
        rk = r(t(i),uk);
        Hk = rk - (25*uk-48*un+36*unm1-16*unm2+3*unm3)/(12*h);
        du = ds*Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
        count = count + 1;
    end
    u(:,i) = uk;
    disp(count);
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end