function [t,u] = bdf3_edts(r,t,u0,ds,tol)
% BDF 3 scheme
%
% WARNING: ONLY VALID FOR CONSTANT TIME STEP
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-3';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

i = 2;
h = t(i) - t(i-1);
u(:,i) = singleStep_bdf1_edts(u(:,i-1),t(i-1),r,h,ds,tol);
i = 3;
h = t(i) - t(i-1);
u(:,i) = singleStep_bdf2_edts(u(:,i-1),u(:,i-2),t(i-1),r,h,ds,tol);

%% after startup
for i = 4:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    unm1 = u(:,i-2);
    unm2 = u(:,i-3);
    
    residual = tol + 1;
    count = 0;
    uk = un;
    while residual > tol
        rk = r(t(i),uk);
        Hk = rk - (11*uk-18*un+9*unm1-2*unm2)/(6*h);
        du = ds*Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
        count = count + 1;
    end
    u(:,i) = uk;
    disp(count);
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end