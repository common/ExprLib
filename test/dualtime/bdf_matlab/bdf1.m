function [t,u] = bdf1(r,t,u0,tol)
% BDF 1
% Adams Moulton 11
% Backward Euler
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-1';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

for i = 2:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    
    residual = tol + 1;
    uk = un;
    while residual > tol
        rk = r(t(i),uk);
        J = finite_difference_jacobian(r,uk,t(i),rk);
        Hk = rk - (uk-un)/h;
        dHdu = J - 1/h*eye(nv);
        du = -dHdu\Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
    end
    u(:,i) = uk;
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end