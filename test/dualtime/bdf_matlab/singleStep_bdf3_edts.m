function u = singleStep_bdf3_edts(un,unm1,unm2,tn,r,h,ds,tol)

residual = tol + 1;
nv = numel(un);
uk = un;
count = 0;
while residual > tol
    rk = r(tn,uk);
    Hk = rk - (11*uk-18*un+9*unm1-2*unm2)/(6*h);
    du = ds*Hk;
    uk = uk + du;
    residual = norm(du./(uk+tol));
    count = count + 1;
end
u = uk;
disp(count);