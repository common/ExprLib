function [t,u] = bdf2(r,t,u0,tol)
% BDF 2 scheme
%
% WARNING: ONLY VALID FOR CONSTANT TIME STEP
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-2';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

startup = 'BDF-1';
% startup = 'TR-BDF-2';

if strcmp(startup,'BDF-1')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf1(u(:,i-1),t(i-1),r,h,tol);
elseif strcmp(startup,'TR-BDF-2')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_trbdf2(u(:,i-1),t(i-1),r,h,tol);
end

%% after startup
for i = 3:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    unm1 = u(:,i-2);
    
    residual = tol + 1;
    uk = un;
    while residual > tol
        rk = r(t(i),uk);
        J = finite_difference_jacobian(r,uk,t(i),rk);
        Hk = rk - (3*uk-4*un+unm1)/(2*h);
        dHdu = J - 1.5/h*eye(nv);
        du = -dHdu\Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
    end
    u(:,i) = uk;
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end