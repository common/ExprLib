function u = singleStep_bdf1(un,tn,r,h,tol)

residual = tol + 1;
nv = numel(un);
uk = un;
while residual > tol
    rk = r(tn,uk);
    J = finite_difference_jacobian(r,uk,tn,rk);
    Hk = rk - (uk-un)/h;
    dHdu = J - 1/h*eye(nv);
    du = -dHdu\Hk;
    uk = uk + du;
    residual = norm(du./(uk+tol));
end
u = uk;