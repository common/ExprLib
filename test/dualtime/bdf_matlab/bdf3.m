function [t,u] = bdf3(r,t,u0,tol)
% BDF 3 scheme
%
% WARNING: ONLY VALID FOR CONSTANT TIME STEP
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-3';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

startup = 'BDF-1,BDF-2';
% startup = 'TR-BDF-2,BDF-2';
% startup = 'IRKG-4';
% startup = 'IRKG-6';

if strcmp(startup,'BDF-1,BDF-2')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf1(u(:,i-1),t(i-1),r,h,tol);
    i = 3;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf2(u(:,i-1),u(:,i-2),t(i-1),r,h,tol);
elseif strcmp(startup,'TR-BDF-2,BDF-2')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_trbdf2(u(:,i-1),t(i-1),r,h,tol);
    i = 3;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf2(u(:,i-1),u(:,i-2),t(i-1),r,h,tol);
elseif strcmp(startup,'IRKG-4')
    for i = 2:3
        h = t(i) - t(i-1);
        u(:,i) = singleStep_irkg4(u(:,i-1),t(i-1),r,h,tol);
    end
elseif strcmp(startup,'IRKG-6')
    for i = 2:3
        h = t(i) - t(i-1);
        u(:,i) = singleStep_irkg6(u(:,i-1),t(i-1),r,h,tol);
    end
end

%% after startup
for i = 4:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    unm1 = u(:,i-2);
    unm2 = u(:,i-3);
    
    residual = tol + 1;
    uk = un;
    while residual > tol
        rk = r(t(i),uk);
        J = finite_difference_jacobian(r,uk,t(i),rk);
        Hk = rk - (11*uk-18*un+9*unm1-2*unm2)/(6*h);
        dHdu = J - 11/(6*h)*eye(nv);
        du = -dHdu\Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
    end
    u(:,i) = uk;
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end