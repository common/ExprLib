clc; clear all;
%%
phi0 = 1.0;
k = 1.0;
rhs = @(t,phi)-k*phi;
tf = 1.0;
h = [0.1,0.05,0.01];
%%
t = 0:h(1):tf;
[t1,phi] = bdf4_edts(rhs,t,phi0,0.1*h(1),1e-10);
error(1) = abs(phi(end) - phi0*exp(-k*tf));
%%
t = 0:h(2):tf;
[t1,phi] = bdf4_edts(rhs,t,phi0,0.1*h(2),1e-10);
error(2) = abs(phi(end) - phi0*exp(-k*tf));
%%
t = 0:h(3):tf;
[t1,phi] = bdf4_edts(rhs,t,phi0,0.1*h(3),1e-10);
error(3) = abs(phi(end) - phi0*exp(-k*tf));
%%
format long;
disp([h.' error.']);

polyfit(log10(h),log10(error),1)