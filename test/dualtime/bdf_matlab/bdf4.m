function [t,u] = bdf4(r,t,u0,tol)
% BDF 4 scheme
%
% WARNING: ONLY VALID FOR CONSTANT TIME STEP
%
% Mike Hansen
% 12/2014

integrator_name = 'BDF-4';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

startup = 'BDF-1,BDF-2,BDF-3';
% startup = 'TR-BDF-2,BDF-2,BDF-3';
% startup = 'IRKG-4';
% startup = 'IRKG-6';

if strcmp(startup,'BDF-1,BDF-2,BDF-3')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf1(u(:,i-1),t(i-1),r,h,tol);
    i = 3;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf2(u(:,i-1),u(:,i-2),t(i-1),r,h,tol);
    i = 4;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf3(u(:,i-1),u(:,i-2),u(:,i-3),t(i-1),r,h,tol);
elseif strcmp(startup,'TR-BDF-2,BDF-2,BDF-3')
    i = 2;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_trbdf2(u(:,i-1),t(i-1),r,h,tol);
    i = 3;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf2(u(:,i-1),u(:,i-2),t(i-1),r,h,tol);
    i = 4;
    h = t(i) - t(i-1);
    u(:,i) = singleStep_bdf3(u(:,i-1),u(:,i-2),u(:,i-3),t(i-1),r,h,tol);
elseif strcmp(startup,'IRKG-4')
    for i = 2:4
        h = t(i) - t(i-1);
        u(:,i) = singleStep_irkg4(u(:,i-1),t(i-1),r,h,tol);
    end
elseif strcmp(startup,'IRKG-6')
    for i = 2:4
        h = t(i) - t(i-1);
        u(:,i) = singleStep_irkg6(u(:,i-1),t(i-1),r,h,tol);
    end
end

%% after startup
for i = 5:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    unm1 = u(:,i-2);
    unm2 = u(:,i-3);
    unm3 = u(:,i-4);
    
    residual = tol + 1;
    uk = un;
    while residual > tol
        rk = r(t(i),uk);
        J = finite_difference_jacobian(r,uk,t(i),rk);
        Hk = rk - (25*uk-48*un+36*unm1-16*unm2+3*unm3)/(12*h);
        dHdu = J - 25/(12*h)*eye(nv);
        du = -dHdu\Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
    end
    u(:,i) = uk;
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end