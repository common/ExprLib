function [t,u] = bdf1_edts(r,t,u0,ds,tol)
% BDF 1
% Adams Moulton 11
% Backward Euler
%
% Mike Hansen
% 12/2014

% this uses "explicit dual time-stepping", a form of pseudo-transient
% continuation analogous to fixed-point iteration

integrator_name = 'BDF-1--EDTS';

nt = numel(t);
nv = numel(u0);
tf = t(end);

u = zeros(nv,nt);
u(:,1) = u0;

if nargin == 3
    tol = 1e-6;
end

for i = 2:nt
    h = t(i) - t(i-1);
    
    un = u(:,i-1);
    
    residual = tol + 1;
    uk = un;
    count = 0;
    while residual > tol
        rk = r(t(i),uk);
        Hk = rk - (uk-un)/h;
        du = ds*Hk;
        uk = uk + du;
        residual = norm(du./(uk+tol));
        count = count + 1;
    end
    u(:,i) = uk;
    disp(count);
    
    if ~mod(i,ceil(nt/10))
        disp(['- ',integrator_name,' : ',num2str(100*t(i)/tf,2),' %']);
    end
end