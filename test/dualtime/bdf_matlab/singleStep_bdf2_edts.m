function u = singleStep_bdf2_edts(un,unm1,tn,r,h,ds,tol)

residual = tol + 1;
nv = numel(un);
uk = un;
count = 0;
while residual > tol
    rk = r(tn,uk);
    Hk = rk - (3*uk-4*un+unm1)/(2*h);
    du = ds*Hk;
    uk = uk + du;
    residual = norm(du./(uk+tol));
    count = count + 1;
end
u = uk;
disp(count);