#include <expression/dualtime/FixedPointBDFDualTimeIntegrator.h>
#include <expression/dualtime/VariableImplicitBDFDualTimeIntegrator.h>
#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FVStaggered.h>

#include <test/TestHelper.h>
#include "ExpDecay.h"
#include <expression/Functions.h>
#include <expression/ExpressionTree.h>
#include <expression/ExprPatch.h>

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

namespace po = boost::program_options;
namespace so = SpatialOps;

typedef so::SVolField FieldT;

double bdf_error_from_dt( double dt, const unsigned designOrder )
{
  const double tend = 1.0;
  const double k    = 1.0;
  const double phi0 = 1.0;
  const double ds   = 0.1*dt;

  try{

    Expr::ExprPatch patch(1);
    SpatialOps::OperatorDatabase sodb;
    Expr::FieldManagerList& fml = patch.field_manager_list();

    const Expr::Tag dtTag    ( "timestep",      Expr::STATE_NONE );
    const Expr::Tag dsTag    ( "dual-timestep", Expr::STATE_NONE );

    Expr::DualTime::BDFDualTimeIntegrator* integrator = new Expr::DualTime::FixedPointBDFDualTimeIntegrator<FieldT>( 0, "BasicDualTime", dtTag, dsTag, designOrder );

    typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
    integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dt ) );
    integrator->set_dual_time_step_expression    ( new ConstantSingleValueT( dsTag, ds ) );

    Expr::ExpressionFactory& factory = integrator->factory();

    std::string phiName = "phi";
    const Expr::Tag phiTag   ( phiName,          Expr::STATE_NONE );
    const Expr::Tag phiRHSTag( phiName + "_rhs", Expr::STATE_NONE );
    factory.register_expression( new ExpDecayRHS<FieldT>::Builder( k, phiRHSTag, phiTag ) );

    integrator->add_variable<FieldT>( phiTag.name(), phiRHSTag );

    integrator->prepare_for_integration( fml, sodb, patch.field_info() );

    Expr::ExpressionFactory icFactory;

    const Expr::Tag phiNTag( phiName, Expr::STATE_N );
    const Expr::ExpressionID id = icFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( phiNTag, phi0 ) );
    Expr::ExpressionTree icTree( id, icFactory, patch.id() );

    icTree.register_fields( fml );
    icTree.bind_fields( fml );
    icTree.execute_tree();

    double t = 0;

    const unsigned maxIterPerStep = 100;

    unsigned totalIter = 0;
    unsigned stepCount = 0;
    unsigned dualIter  = 0;
    bool     converged = false;

    while( t < ( tend-1e-12 ) ){
      // this (tend - 1e-12) 'trick' is to prevent roundoff error from triggering an extra time step

      dualIter = 0;
      integrator->begin_time_step();

      do{

        integrator->advance_dualtime( converged );

        dualIter++;
        totalIter++;

      } while( !converged && dualIter <= maxIterPerStep );

      integrator->end_time_step();
      stepCount++;
      t += dt;
    }

    FieldT& prediction = fml.field_ref<FieldT>( Expr::Tag( phiName, Expr::STATE_N ) );
    prediction.add_device(CPU_INDEX);
    SpatialOps::SpatFldPtr<FieldT> exact = SpatialOps::SpatialFieldStore::get<FieldT>( prediction );
    *exact <<= phi0*std::exp(-k*tend);
    return SpatialOps::nebo_norm( *exact - prediction );
  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
    return -1;
  }

}

bool bdf_run_test( const double designOrder,
                   const bool check_vs_matlab = false,
                   const double matlabObservedOrder = 0.0 )
{
  typedef std::vector<double> dvec;

  const int ndt = 3;

  dvec dt(ndt);
  dvec e(ndt);

  dt[0] = 0.1;
  dt[1] = 0.05;
  dt[2] = 0.01;

  double meanlog10dt = 0.0;
  double meanlog10error = 0.0;

  for( int i=0; i<ndt; ++i ){
    e[i] = bdf_error_from_dt( dt[i], (unsigned) designOrder );
    meanlog10dt    += std::log10(dt[i])/((double) ndt);
    meanlog10error += std::log10( e[i])/((double) ndt);
  }

  double numerator = 0.0;
  double denominator = 0.0;

  for( int i=0; i<ndt; ++i ){
    const double dt_deviation = std::log10(dt[i]) - meanlog10dt;
    numerator   += dt_deviation*(std::log10(e[i]) - meanlog10error);
    denominator += dt_deviation*dt_deviation;
  }

  double observedOrder = numerator/denominator;

  if( check_vs_matlab ){
    return ( observedOrder > ( matlabObservedOrder - 0.05 ) ) && ( observedOrder < ( matlabObservedOrder + 0.1 ) );
  }
  else{
    return ( observedOrder > ( designOrder - 0.05 ) ) && ( observedOrder < ( designOrder + 0.1 ) );
  }
}



int main( int iarg, char* carg[] )
{
  TestHelper status;
  status( bdf_run_test( 1 ), "BDF-1 accuracy" );
  status( bdf_run_test( 2 ), "BDF-2 accuracy" );
  status( bdf_run_test( 3, true, 1.965900224509586 ), "BDF-3 vs Matlab code" );
  status( bdf_run_test( 4, true, 1.976953783712932 ), "BDF-4 vs Matlab code" );

  if( status.ok() ){
    std::cout << "PASS\n";
    return 0;
  }

  std::cout << "FAIL\n";
  return -1;
}
