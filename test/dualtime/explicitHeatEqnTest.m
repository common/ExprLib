clc; clear all;

dsigma = 1e-2;
dt     = 1e-2;
tend   = 10;

nx    = 40;
L     = 10;
dx    = L/(nx);
alpha = 0.1;
x     = dx/2:dx:L-dx/2;

phi0 = sin(x);
phiLBC = 2;
phiRBC = 2;

phi = phi0;
d2phidx2 = zeros(1,nx);

interior = 2:nx-1;

maxEig = 4*alpha/dx^2 + 1/dt;
maxdsigma = 2/maxEig;

disp(['- max dsigma = ', num2str(maxdsigma)]);

TOL       = 1e-10;
t         = 0;
totaliter = 0;
disp('- run beginning');
tic;
while t <= tend
  count = 0;
  resid = TOL + 1;
  phin = phi;
  while resid > TOL && count < 1e4
    
    phiL = 2*phiLBC - phi(1);
    phiR = 2*phiRBC - phi(end);
    
    d2phidx2(interior) = (phi(interior-1) + phi(interior+1) - 2*phi(interior))/dx^2;
    d2phidx2(1) = (phi(2) + phiL - 2*phi(1))/dx^2;
    d2phidx2(nx) = (phi(nx-1) + phiR - 2*phi(nx))/dx^2;
    DIFF = alpha*d2phidx2;
    
    TIME = (phi - phin)/dt;
    
    RES = DIFF - TIME;
    
    phi = phi + dsigma*RES;
    
    resid = max(RES./(phi+1e-4));
    count = count + 1;
    
    if sum(isnan(phi)) > 0
      error('- NaN detected');
    end
    fprintf('\t%1.1e\n',resid);
  end
  disp(['  - time step complete in ', num2str(count),' iterations, t = ', num2str(t), ' res = ', num2str(resid)]);
  t = t + dt;
  totaliter = totaliter + count;
end
fprintf('Took %1.1e s\n', toc);
disp(['- run complete in ', num2str(totaliter), ' iterations']);
plot(x,phi0,'-o',x,phi,'-x');
