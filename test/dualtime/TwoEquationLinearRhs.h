/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TwoEquationLinearRhs_Expr_h
#define TwoEquationLinearRhs_Expr_h

#include <expression/Expression.h>

/**
 *  \class TwoEquationLinearRhs
 *  \author Mike Hansen
 *  \date September, 2015
 *
 *  RHS for the \f\phi\f ODE:
 *  \f[
 *  \frac{d\phi}{dt} = k_1\phi + k_2\psi,
 *  \f]
 *  which is solved in a coupled fashion with a similar equation for \f$\psi\f$.
 *  The system is
 *  \f[
 *  \frac{d}{dt}
 *  \begin{pmatrix}
 *  	\phi \\
 *  	\psi
 *  \end{pmatrix}
 *  =
 *  [K]
 *  \begin{pmatrix}
 *  	\phi \\
 *  	\psi
 *  \end{pmatrix}.
 *  \f]
 *
 *  If we choose
 *  \f[
 *  K =
 *  k
 *  \begin{bmatrix}
 *  	1 & 1 \\
 *  	-1 & 1 \\
 *  \end{bmatrix},
 *  \f]
 *  then the eigenvalues are \f$\lambda_{\pm} = \pm k i\f$ and the solution is purely oscillatory.
 *  This is a challenging problem for the BDF methods because they provide damping, \f$R=1/(1+(hk)^2\leq1\f$, along the imaginary axis \f$\mathbb{I}\f$.
 *  In contrast, the Crank-Nicolson method has exactly \f$R=1.0\f$ for \f$\lambda\in\mathbb{I}\f$.
 *  In further contrast, the explicit Forward Euler method cannot stabilize any mode on \f$\mathbb{I}\f$, and cannot be used to solve this problem.
 *
 *  This equation system is constructed by building two TwoEquationLinearRhs expressions, one for \f$\phi\f$ and another for \f$\psi\f$.
 *  These are connected by the K matrix. The \f$\phi\f$ expression gets \f$K_{11},K_{12}\f$ while the \f$\psi\f$ expression gets \f$K_{21},K_{22}\f$.
 *  The \f$\phi\f$ expression must see \f$\psi\f$ as its 'CoupledVar_', and vice versa.
 *
 */
template< typename FieldT >
class TwoEquationLinearRhs
 : public Expr::Expression<FieldT>
{
  const double myK_;
  const double coupledK_;
  DECLARE_FIELD( FieldT, myVar_ )
  DECLARE_FIELD( FieldT, coupledVar_ )

  TwoEquationLinearRhs( const double myK,
                       const double coupledK,
                       const Expr::Tag& myVarTag,
                       const Expr::Tag& coupledVarTag)
    : Expr::Expression<FieldT>(),
      myK_( myK ),
      coupledK_( coupledK )
  {
    myVar_      = this->template create_field_request<FieldT>( myVarTag );
    coupledVar_ = this->template create_field_request<FieldT>( coupledVarTag );
  }


public:

  class Builder : public Expr::ExpressionBuilder
  {
    const double myK_;
    const double coupledK_;
    const Expr::Tag myVar_;
    const Expr::Tag coupledVar_;
  public:
    /**
     *  @brief Build a TwoEquationLinearRhs expression
     *  @param myK, effect of phi on phiRHS
     *  @param coupledK, effect of psi on phiRHS
     *  @param resultTag the tag for the value that this expression computes
     *  @param phiTag the solution variable
     *  @param psiTag the coupled solution variable
     */
    Builder( const double myK,
             const double coupledK,
             const Expr::Tag& resultTag,
             const Expr::Tag& myVarTag,
             const Expr::Tag& coupledVarTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        myK_( myK ),
        coupledK_( coupledK ),
        myVar_( myVarTag ),
        coupledVar_( coupledVarTag )
    {}

    Expr::ExpressionBase* build() const{ return new TwoEquationLinearRhs<FieldT>( myK_, coupledK_, myVar_, coupledVar_ ); }
  };

  ~TwoEquationLinearRhs(){}

  void evaluate()
  {
    FieldT& result = this->value();
    result <<= myK_*myVar_->field_ref() + coupledK_*coupledVar_->field_ref();
  }

  void sensitivity( const Expr::Tag& varTag )
  {
    using namespace SpatialOps;

    const FieldT& myVarSens      = this->myVar_     ->sens_field_ref( varTag );
    const FieldT& coupledVarSens = this->coupledVar_->sens_field_ref( varTag );
    FieldT& thisSens             = this->sensitivity_result( varTag );

    if( varTag == this->get_tag() ) thisSens <<= 1.0;
    else{
      thisSens <<= myK_ * myVarSens + coupledK_ * coupledVarSens;
    }

  }
};


#endif // TwoEquationLinearRhs_Expr_h


