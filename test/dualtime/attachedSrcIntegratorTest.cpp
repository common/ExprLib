#include <expression/dualtime/BlockImplicitBDFDualTimeIntegrator.h>
#include <expression/matrix-assembly/SparseMatrix.h>
#include <expression/Functions.h>
#include <expression/ExprPatch.h>

#include <expression/ExpressionTree.h>

#include <test/TestHelper.h>

#include <spatialops/structured/Grid.h>
#include <spatialops/structured/FVStaggered.h>
#include <spatialops/structured/FieldComparisons.h>

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

namespace so = SpatialOps;

namespace Sensitivity {
  template<typename FieldT>
  class ProductExpression : public Expr::Expression<FieldT> {
  private:
    DECLARE_FIELD( FieldT, expr1_ )
    DECLARE_FIELD( FieldT, expr2_ )

    ProductExpression( const Expr::Tag tag1,
                       const Expr::Tag tag2 )
          : Expr::Expression<FieldT>(){
      this->set_gpu_runnable( true );
      expr1_ = this->template create_field_request<FieldT>( tag1 );
      expr2_ = this->template create_field_request<FieldT>( tag2 );
    }

  public:
    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag tag1_, tag2_;
    public:
      Builder( const Expr::Tag productTag,
               const Expr::Tag tag1,
               const Expr::Tag tag2 ) :
            Expr::ExpressionBuilder( productTag ),
            tag1_( tag1 ),
            tag2_( tag2 ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new ProductExpression<FieldT>( tag1_, tag2_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      this->value() <<= expr1_->field_ref() * expr2_->field_ref();
    }

    void sensitivity( const Expr::Tag& var ){
      FieldT & dfdv = this->sensitivity_result( var );

      if( var == this->get_tag() ){
        dfdv <<= 1.0;
      }
      else{
        const FieldT& dfd1 = this->expr1_->sens_field_ref( var );
        const FieldT& dfd2 = this->expr2_->sens_field_ref( var );
        dfdv <<= expr1_->field_ref() * dfd2 + expr2_->field_ref() * dfd1;
      }
    }
  };

  template<typename FieldT>
  class SumOp : public Expr::Expression<FieldT> {

    DECLARE_VECTOR_OF_FIELDS( FieldT, operandList_)
    const int nOperands_;

    SumOp( const Expr::TagList operandList )
          : Expr::Expression<FieldT>(),
            nOperands_( operandList.size()){
      this->set_gpu_runnable( true );
      this->template create_field_vector_request<FieldT>( operandList, operandList_ );
    }

  public:

    class Builder : public Expr::ExpressionBuilder {
      const Expr::TagList operandList_;
    public:
      Builder( const Expr::Tag& sumTag,
               const Expr::TagList operandList )
            : Expr::ExpressionBuilder( sumTag ),
              operandList_( operandList ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new SumOp<FieldT>( operandList_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      this->value() <<= operandList_[0]->field_ref();
      for( int i = 1;i < nOperands_;++i ){
        this->value() <<= this->value() + operandList_[i]->field_ref();
      }
    }

    void sensitivity( const Expr::Tag& var ){
      FieldT & dfdv = this->sensitivity_result( var );
      if( var == this->get_tag()){
        dfdv <<= 1.0;
      }
      else{
        dfdv <<= operandList_[0]->sens_field_ref( var );
        for( int i = 1;i < nOperands_;++i ){
          dfdv <<= dfdv + operandList_[i]->sens_field_ref( var );
        }
      }
    }
  };

  template<typename FieldT>
  class DivisionExpression : public Expr::Expression<FieldT> {
    DECLARE_FIELD( FieldT, numerator_ )
    DECLARE_FIELD( FieldT, denominator_ )

    DivisionExpression( const Expr::Tag numeratorTag,
                        const Expr::Tag denominatorTag )
          : Expr::Expression<FieldT>(){
      this->set_gpu_runnable( true );
      numerator_ = this->template create_field_request<FieldT>( numeratorTag );
      denominator_ = this->template create_field_request<FieldT>( denominatorTag );
    }

  public:
    class Builder : public Expr::ExpressionBuilder {
      const Expr::Tag numeratorTag_, denominatorTag_;
    public:
      Builder( const Expr::Tag resultTag,
               const Expr::Tag numeratorTag,
               const Expr::Tag denominatorTag ) :
            Expr::ExpressionBuilder( resultTag ),
            numeratorTag_( numeratorTag ),
            denominatorTag_( denominatorTag ){}

      ~Builder(){}

      Expr::ExpressionBase* build() const{
        return new DivisionExpression<FieldT>( numeratorTag_, denominatorTag_ );
      }
    };

    void evaluate(){
      using namespace SpatialOps;
      this->value() <<= numerator_->field_ref() / denominator_->field_ref();
    }
  };
}


typedef so::SVolField FieldT;
typedef Expr::ConstantExpr<FieldT>::Builder ConstantT;
typedef Sensitivity::ProductExpression<FieldT>::Builder ProductT;
typedef Sensitivity::SumOp<FieldT>::Builder SumT;

#define ASSEMBLER(MatrixT, FieldT, name) \
  boost::shared_ptr<MatrixT<FieldT> > name = boost::make_shared<MatrixT<FieldT> >();


bool test()
{
  /* ---------------------------------------------------------------
   * domain, operators, field managers, etc setup
   * ---------------------------------------------------------------
   */
  Expr::ExprPatch patch(1);
  SpatialOps::OperatorDatabase sodb;
  Expr::FieldManagerList& fml = patch.field_manager_list();

  /* ---------------------------------------------------------------
   * variable and rhs names, jacobian, preconditioner
   * ---------------------------------------------------------------
   */
  const Expr::Tag xNTag   ( "x",      Expr::STATE_N    );
  const Expr::Tag yNTag   ( "y",      Expr::STATE_N    );
  const Expr::Tag xTag    ( "x",      Expr::STATE_NONE );
  const Expr::Tag yTag    ( "y",      Expr::STATE_NONE );
  const Expr::Tag k1t     ( "k1",     Expr::STATE_NONE );
  const Expr::Tag k2t     ( "k2",     Expr::STATE_NONE );
  const Expr::Tag k3t     ( "k3",     Expr::STATE_NONE );
  const Expr::Tag xrhsTag ( "xrhs"         , Expr::STATE_NONE );
  const Expr::Tag prodyTag( "prody"        , Expr::STATE_NONE );
  const Expr::Tag yrhsTag ( "yrhs"         , Expr::STATE_NONE );

  /* ---------------------------------------------------------------
   * build integrator, set constant physical and dual time step
   * ---------------------------------------------------------------
   */
  const unsigned bdfOrder = 1;
  typedef Expr::DualTime::BlockImplicitBDFDualTimeIntegrator<FieldT, FieldT> IntegratorType;
  const Expr::Tag dtTag( "timestep"     , Expr::STATE_NONE );
  const Expr::Tag dsTag( "dual_timestep", Expr::STATE_NONE );
  IntegratorType* integrator = new IntegratorType( 0, "BIBDF", dtTag, dsTag, bdfOrder );

  double ds = 1e2; // dual time step size

  const double dt = 10;
  typedef Expr::ConstantExpr<so::SingleValueField>::Builder ConstantSingleValueT;
  integrator->set_physical_time_step_expression( new ConstantSingleValueT( dtTag, dt ) );
  integrator->set_dual_time_step_expression( new ConstantT( dsTag, ds ) );

  /* ---------------------------------------------------------------
   * register variables and RHS at STATE_NONE to the integrator
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory& factory = integrator->factory();

  factory.register_expression( new ConstantT( k1t, -2.0 ) );
  factory.register_expression( new ConstantT( k2t, -3.0 ) );
  factory.register_expression( new ConstantT( k3t, 4.0 ) );
  factory.register_expression( new ProductT( xrhsTag, k1t, xTag ) );
  factory.register_expression( new ProductT( prodyTag, k2t, yTag ) );
  factory.register_expression( new SumT( yrhsTag, {prodyTag, k3t} ) );
  factory.attach_dependency_to_expression(prodyTag, xrhsTag, Expr::ADD_SOURCE_EXPRESSION );

  integrator->add_variable<FieldT>( xTag.name(), xrhsTag, 0, 0);
  integrator->add_variable<FieldT>( yTag.name(), yrhsTag, 1, 1);
  integrator->done_adding_variables();

  // build an assembler to compute the Jacobian matrix of the rhs to the governed variable
  ASSEMBLER( Expr::matrix::SparseMatrix, FieldT, jacobian )
  jacobian->element<FieldT>(0,0) = Expr::matrix::sensitivity( xrhsTag, xTag );
  jacobian->element<FieldT>(0,1) = Expr::matrix::sensitivity( xrhsTag, yTag );
  jacobian->element<FieldT>(1,0) = Expr::matrix::sensitivity( yrhsTag, xTag );
  jacobian->element<FieldT>(1,1) = Expr::matrix::sensitivity( yrhsTag, yTag );
  jacobian->finalize(); // need to finalize before using

  integrator->set_jacobian( jacobian);

  /* ---------------------------------------------------------------
   * final integrator prep, must happen before setting initial conditions, and tree output
   * ---------------------------------------------------------------
   */
  integrator->prepare_for_integration( fml, sodb, patch.field_info() );
  integrator->copy_from_initial_condition_to_execution<FieldT>( xNTag.name() );
  integrator->copy_from_initial_condition_to_execution<FieldT>( yNTag.name() );

  {
    std::ofstream f( "attachedSrcIntegrator-tree.dot" );
    integrator->get_tree().write_tree( f, false, true );
  }

  /* ---------------------------------------------------------------
   * set initial conditions with STATE_N
   * ---------------------------------------------------------------
   */
  Expr::ExpressionFactory initFactory;
  std::set<Expr::ExpressionID> initRoots;

  const double x0 = 1.0;
  const double y0 = 2.0;
  initRoots.insert( initFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( xNTag, x0 ) ) );
  initRoots.insert( initFactory.register_expression( new Expr::ConstantExpr<FieldT>::Builder( yNTag, y0 ) ) );
  Expr::ExpressionTree initTree( initRoots, initFactory, 0, "initialization" );

  initTree.register_fields( fml );
  initTree.bind_fields( fml );
  initTree.execute_tree();

  /* ---------------------------------------------------------------
   * set iteration counts, prep solution history file
   * ---------------------------------------------------------------
   */

  const unsigned maxIterPerStep = 100;

  unsigned totalIter = 0;
  unsigned stepCount = 0;
  unsigned dualIter  = 0;
  bool     converged = false;

  /* ---------------------------------------------------------------
   * perform the integration
   * ---------------------------------------------------------------
   */
  const double tend = 100.0;

  double t = 0;
  while( t<tend ){

    dualIter = 0;
    integrator->begin_time_step();

    do{
      integrator->advance_dualtime( converged );
      dualIter++;
      totalIter++;

      std::cout << "  iter " << std::setw( 5 ) << dualIter << "/" << std::setw( 5 ) << maxIterPerStep
                << ",  x: " << std::fixed << std::setprecision( 5 )
                << SpatialOps::nebo_max( fml.field_ref<FieldT>( xTag ))
                << ",  y: " << std::scientific << std::setprecision( 5 )
                << SpatialOps::nebo_max( fml.field_ref<FieldT>( yTag ))
                << std::endl;

    } while( !converged && dualIter <= maxIterPerStep );

    integrator->end_time_step();
    stepCount++;
    t += dt;

  }

  /* ---------------------------------------------------------------
   * process the test
   * ---------------------------------------------------------------
   */
  TestHelper status(false);

  const FieldT& xPredicted = fml.field_ref<FieldT>( xTag );
  const FieldT& yPredicted = fml.field_ref<FieldT>( yTag );
  const double xSteady = - 2.0;
  const double ySteady = 1.333333333;
  SpatialOps::SpatFldPtr<FieldT> x = SpatialOps::SpatialFieldStore::get<FieldT>( xPredicted );
  SpatialOps::SpatFldPtr<FieldT> y = SpatialOps::SpatialFieldStore::get<FieldT>( yPredicted );
  *x <<= xSteady;
  *y <<= ySteady;
  status( SpatialOps::field_equal( xPredicted, *x, 1e-6 ), "x value-check" );
  status( SpatialOps::field_equal( yPredicted, *y, 1e-6 ), "y value-check" );

  return status.ok();
}

int main()
{
  try{
    TestHelper status( true );
    status( test(), "Block Implicit" );

    if( status.ok() ){
      std::cout << "\nPASS\n";
      return 0;
    }
    else{
      std::cout << "\nFAIL\n";
      return -1;
    }

  }
  catch( std::exception& err ){
    std::cout << err.what() << std::endl;
    return -1;
  }
}
