#include <iostream>

#include <expression/ExprLib.h>
#include <expression/Functions.h>

typedef Expr::ExprPatch Patch;

using namespace std;
using namespace Expr;
namespace so = SpatialOps;

int main()
{
  Patch patch( 1 );

  ExpressionFactory factory(true);

  const Tag Atag("A",STATE_NONE);
  const Tag Btag("B",STATE_NONE);
  const Tag Ctag("C",STATE_NONE);
  const Tag Dtag("D",STATE_NONE);

  typedef ConstantExpr<SpatialOps::SingleValueField>::Builder TestExpr;

  const ExpressionID id_A = factory.register_expression( new TestExpr(Atag,1.1) );
  const ExpressionID id_B = factory.register_expression( new TestExpr(Btag,2.2) );
  const ExpressionID id_C = factory.register_expression( new TestExpr(Ctag,3.3) );
  const ExpressionID id_D = factory.register_expression( new TestExpr(Dtag,4.4) );

  factory.attach_dependency_to_expression( Btag, Atag );
  factory.attach_dependency_to_expression( Ctag, Btag );
  factory.attach_dependency_to_expression( Dtag, Atag );

  Expr::ExpressionTree tree(id_A,factory,patch.id());
  FieldManagerList& fml = patch.field_manager_list();
  Expr::FieldMgrSelector<so::SingleValueField>::type& fm = fml.field_manager<so::SingleValueField>();
  tree.register_fields( fml );
  fml.allocate_fields( patch.field_info() /*FieldInfo( patch.dim(),
                                  patch.get_n_particles(),
                                  patch.has_physical_bc_xplus(),
                                  patch.has_physical_bc_yplus(),
                                  patch.has_physical_bc_zplus() ) */);
  fm.lock_field( Atag );
  fm.lock_field( Btag );
  fm.lock_field( Ctag );
  fm.lock_field( Dtag );

  tree.bind_fields( fml );
  {
    std::ofstream f("tree.dot");
    tree.write_tree( f );
  }
  tree.execute_tree();

  typedef so::SingleValueField FieldT;
  FieldT& a = fm.field_ref( Atag );
  FieldT& b = fm.field_ref( Btag );
  FieldT& c = fm.field_ref( Ctag );
  FieldT& d = fm.field_ref( Dtag );

# ifdef ENABLE_CUDA
  a.add_device( CPU_INDEX );  a.set_device_as_active( CPU_INDEX );
  b.add_device( CPU_INDEX );  b.set_device_as_active( CPU_INDEX );
  c.add_device( CPU_INDEX );  c.set_device_as_active( CPU_INDEX );
  d.add_device( CPU_INDEX );  d.set_device_as_active( CPU_INDEX );
# endif

  cout << endl
       << "A = " << a[0] << endl
       << "B = " << b[0] << endl
       << "C = " << c[0] << endl
       << "D = " << d[0] << endl
       << endl;

  if( a[0] == 1.1+2.2+3.3+4.4 ){
    cout << "PASS" << endl;
    return 0;
  }
  else{
    cout << "FAIL - expected A=" << 1.1+2.2+3.3+4.4 << endl;
  }

  return 1;
}
