#ifndef BandC_Expr_h
#define BandC_Expr_h

#include <expression/Expression.h>


class BandC
  : public Expr::Expression<SpatialOps::SingleValueField>
{
  BandC();

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::TagList& bc ) : ExpressionBuilder(bc) {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new BandC(); }
  };

  ~BandC(){}

  void evaluate();
};

#endif // BandC_Expr_h
