#include "BandC.h"


BandC::BandC()
  : Expr::Expression<SpatialOps::SingleValueField>()
{}

//--------------------------------------------------------------------

void
BandC::evaluate()
{
  using namespace SpatialOps;
  ValVec& values = this->get_value_vec();
  SpatialOps::SingleValueField& b = *values[0];
  SpatialOps::SingleValueField& c = *values[1];
  b <<= 1.0;
  c <<= 2.0;
}

//--------------------------------------------------------------------
