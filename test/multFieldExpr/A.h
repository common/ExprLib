#ifndef A_Expr_h
#define A_Expr_h

#include <expression/ExprLib.h>


/**
 *  \class A
 */
class A
 : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELDS( SpatialOps::SingleValueField, b_, c_ )

  A( const Expr::Tag& b,
     const Expr::Tag& c );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const Expr::Tag b_, c_;
  public:
    Builder( const Expr::Tag& a, const Expr::Tag& b, const Expr::Tag& c )
    : ExpressionBuilder(a),
      b_(b), c_(c)
    {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new A(b_,c_); }
  };

  ~A(){}

  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



A::A( const Expr::Tag& b,
      const Expr::Tag& c )
  : Expr::Expression<SpatialOps::SingleValueField>()
{
  b_ = this->create_field_request<SpatialOps::SingleValueField>(b);
  c_ = this->create_field_request<SpatialOps::SingleValueField>(c);

  this->set_gpu_runnable(true);
}

//--------------------------------------------------------------------

void
A::evaluate()
{
  using namespace SpatialOps;
  SingleValueField& a = this->value();
  const SingleValueField& b = b_->field_ref();
  const SingleValueField& c = c_->field_ref();
  a <<= b + c;
}


#endif // A_Expr_h
