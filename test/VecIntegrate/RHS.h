#ifndef RHS_Expr_h
#define RHS_Expr_h

#include <expression/Expression.h>

typedef SpatialOps::SingleValueField FieldT;

/**
 *  \class RHS
 */
class RHS
 : public Expr::Expression<FieldT>
{
  const std::vector<double> freq_;

  DECLARE_FIELD( FieldT, time_ )

  RHS( const std::vector<double>& freq,
       const Expr::Tag time );

public:
  class Builder : public Expr::ExpressionBuilder
  {
    const std::vector<double> f_;
    const Expr::Tag t_;
  public:
    Builder( const Expr::TagList& names,
             const std::vector<double>& f,
             const Expr::Tag t )
    : ExpressionBuilder(names),
      f_( f ), t_(t)
    {}
    ~Builder(){}
    Expr::ExpressionBase* build() const{ return new RHS(f_,t_); }
  };

  ~RHS(){};

  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



RHS::RHS( const std::vector<double>& f,
          const Expr::Tag time )
  : Expr::Expression<FieldT>(),
    freq_( f )
{
  time_ = this->create_field_request<FieldT>( time );
  this->set_gpu_runnable( true );
}

//--------------------------------------------------------------------

void
RHS::evaluate()
{
  using namespace SpatialOps;
  ValVec& rhsvec = this->get_value_vec();
  const FieldT& time = time_->field_ref();

  assert( rhsvec.size() == freq_.size() );
  std::vector<double>::const_iterator ifreq = freq_.begin();
  for( ValVec::iterator irhs=rhsvec.begin(); irhs!=rhsvec.end(); ++irhs, ++ifreq ){
    FieldT& rhs = **irhs;
    rhs <<= sin( *ifreq * time );
  }
}

//--------------------------------------------------------------------

#endif // RHS_Expr_h
