#ifndef TemperatureRHS_h
#define TemperatureRHS_h

#include <expression/Expression.h>

template< typename DivOp >
class TemperatureRHS
  : public Expr::Expression< typename DivOp::DestFieldType >
{
  typedef typename Expr::FieldID  FieldID;

  typedef typename DivOp::SrcFieldType  FluxFieldT;
  typedef typename DivOp::DestFieldType ScalarFieldT;

  TemperatureRHS( const Expr::Tag& heatFluxTag );

  const DivOp* divOp_;

  DECLARE_FIELD( FluxFieldT, heatFlux_ )

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  virtual ~TemperatureRHS(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const;
    Builder( const Expr::Tag& trhsTag,
             const Expr::Tag& heatFluxTag );
    ~Builder(){}
  private:
    const Expr::Tag hft_;
  };
};

//--------------------------------------------------------------------
template<typename DivOp>
TemperatureRHS<DivOp>::
TemperatureRHS( const Expr::Tag& heatFluxTag )
  : Expr::Expression<ScalarFieldT>()
{
  this->set_gpu_runnable( true );
  heatFlux_ = this->template create_field_request<FluxFieldT>( heatFluxTag );
}
//--------------------------------------------------------------------
template<typename DivOp>
void
TemperatureRHS<DivOp>::
bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  divOp_ = opDB.retrieve_operator<DivOp>();
}
//--------------------------------------------------------------------
template<typename DivOp>
void
TemperatureRHS<DivOp>::
evaluate()
{
  using namespace SpatialOps;
  ScalarFieldT& result = this->value();
  const FluxFieldT& heatFlux = heatFlux_->field_ref();
  result <<= -(*divOp_)( heatFlux );
}
//--------------------------------------------------------------------

//--------------------------------------------------------------------
template<typename DivOp>
Expr::ExpressionBase*
TemperatureRHS<DivOp>::Builder::
build() const
{
  return new TemperatureRHS( hft_ );
}
//--------------------------------------------------------------------
template<typename DivOp>
TemperatureRHS<DivOp>::Builder::
Builder( const Expr::Tag& trhsTag,
         const Expr::Tag& heatFluxTag )
  : Expr::ExpressionBuilder(trhsTag),
    hft_( heatFluxTag )
{}
//--------------------------------------------------------------------

#endif
