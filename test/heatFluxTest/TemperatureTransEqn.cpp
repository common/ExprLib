#include <spatialops/structured/FVStaggered.h>

#include <expression/ExprLib.h>
#include <expression/BoundaryConditionExpression.h>

#include "TemperatureTransEqn.h"
#include "TemperatureRHS.h"
#include "HeatFluxExpr.h"
#include "GradTExpr.h"

namespace so = SpatialOps;

typedef so::SVolField    CellField;
typedef so::SSurfXField  XSideField;

typedef so::BasicOpTypes<CellField>  OpTypes;
typedef OpTypes::GradX               GradXC2F;
typedef OpTypes::DivX                DivX;
typedef OpTypes::InterpC2FX          InterpC2F;

typedef Expr::ExprPatch  PatchT;

typedef TemperatureRHS    <DivX      >    TRHS;
typedef HeatFluxExpr      <GradXC2F  >    HeatFlux_X;
typedef Expr::ConstantExpr<XSideField>    TCond;
typedef GradTExpr         <GradXC2F  >    GradT_X;

//--------------------------------------------------------------------

TemperatureTransEqn::
TemperatureTransEqn( const Expr::Tag& temperature,
                     const Expr::Tag& lambda,
                     const Expr::Tag& gradT,
                     const Expr::Tag& heatFlux,
                     Expr::ExpressionFactory& exprFactory,
                     PatchT& patch )
  : Expr::TransportEquation( temperature.name(),
                             Expr::Tag("TRHS", Expr::STATE_N) ),
    tempTag_     ( temperature ),
    thermCondTag_( lambda      ),
    gradTTag_    ( gradT       ),
    heatFluxTag_ ( heatFlux    ),

    patch_( patch )
{
  const int nghost = 1;
  exprFactory.register_expression( new HeatFlux_X::Builder(heatFluxTag_, thermCondTag_, gradTTag_, nghost) );
  exprFactory.register_expression( new TCond     ::Builder(thermCondTag_,20.0) );
  exprFactory.register_expression( new GradT_X   ::Builder(gradTTag_,    tempTag_, nghost) );
  exprFactory.set_ghosts( thermCondTag_, nghost );

  exprFactory.register_expression( new TRHS::Builder( Expr::Tag("TRHS", Expr::STATE_N), heatFlux ) );
}

//--------------------------------------------------------------------

TemperatureTransEqn::~TemperatureTransEqn(){}

//--------------------------------------------------------------------

void
TemperatureTransEqn::
setup_boundary_conditions( Expr::ExpressionFactory& factory )
{
  typedef so::SVolField  FieldT;
  typedef Expr::ConstantBCOpExpression<CellField,so::XDIR> TBCExpr;
  typedef TBCExpr::BCValFieldT BCValFieldT;

  const Expr::FieldAllocInfo& info = patch_.field_info();

  const so::GhostData ghosts(1);  // for now, we hard-code one ghost cell for all field types.
  const so::IntVec plusBC(info.bcplus[0],info.bcplus[1],info.bcplus[2]);
  const so::IntVec minusBC(plusBC);
  const so::BoundaryCellInfo bcInfo = so::BoundaryCellInfo::build<BCValFieldT>( minusBC, plusBC );
  const so::MemoryWindow mw = get_window_with_ghost( info.dim, ghosts, bcInfo );
  const so::SpatFldPtr<BCValFieldT> bcFieldProto = so::SpatialFieldStore::get_from_window<BCValFieldT>( mw, bcInfo, ghosts, CPU_INDEX );

  const so::IntVec last( mw.extent() - so::IntVec(1,1,1) );

  const double T0 = 5.0;
  const double TL = 15.0;

  const so::IntVec bc0Pt( 0, 0, 0 );
  const so::IntVec bcLPt( last[0]-2, last[1], last[2] );

  TBCExpr::MaskPtr mask0( new TBCExpr::MaskType( *bcFieldProto, std::vector<so::IntVec>(1,bc0Pt) ) );
  TBCExpr::MaskPtr maskL( new TBCExpr::MaskType( *bcFieldProto, std::vector<so::IntVec>(1,bcLPt) ) );

  const Expr::Tag bc0Tag( solnVarName_ + "_bc-", Expr::STATE_NONE );
  const Expr::Tag bcLTag( solnVarName_ + "_bc+", Expr::STATE_NONE );
  const Expr::Tag bc0TagNP1( solnVarName_ + "_bc- new", Expr::STATE_NONE );
  const Expr::Tag bcLTagNP1( solnVarName_ + "_bc+ new", Expr::STATE_NONE );

  factory.register_expression( new TBCExpr::Builder( bc0Tag,    mask0, so::DIRICHLET, so::MINUS_SIDE, T0 ) );
  factory.register_expression( new TBCExpr::Builder( bcLTag,    maskL, so::DIRICHLET, so::PLUS_SIDE,  TL ) );
  factory.register_expression( new TBCExpr::Builder( bc0TagNP1, mask0, so::DIRICHLET, so::MINUS_SIDE, T0 ) );
  factory.register_expression( new TBCExpr::Builder( bcLTagNP1, maskL, so::DIRICHLET, so::PLUS_SIDE,  TL ) );

  const Expr::Tag tempLabel( solnVarName_, Expr::STATE_N );

  factory.attach_modifier_expression( bc0Tag, tempLabel );
  factory.attach_modifier_expression( bcLTag, tempLabel );
  factory.attach_modifier_expression( bc0TagNP1, Expr::Tag( solnVarName_, Expr::STATE_NP1) );
  factory.attach_modifier_expression( bcLTagNP1, Expr::Tag( solnVarName_, Expr::STATE_NP1) );
}

//--------------------------------------------------------------------

Expr::ExpressionID
TemperatureTransEqn::
initial_condition( Expr::ExpressionFactory& exprFactory )
{
  return exprFactory.get_id( Expr::Tag( solnVarName_, Expr::STATE_N ) );
}

//--------------------------------------------------------------------
