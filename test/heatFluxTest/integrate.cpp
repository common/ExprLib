#include <iostream>
#include <sstream>

#include <expression/ExprLib.h>

#include <spatialops/structured/FVStaggered.h>
#include <spatialops/OperatorDatabase.h>
#include <spatialops/structured/FieldHelper.h>
#include <spatialops/structured/Grid.h>

#include "TemperatureTransEqn.h"

#include <test/TestHelper.h>

//====================================================================

typedef SpatialOps::SVolField    CellField;
typedef SpatialOps::SSurfXField  XSideField;

typedef SpatialOps::BasicOpTypes<CellField>  OpTypes;
typedef OpTypes::GradX      GradXC2F;
typedef OpTypes::DivX       DivX;
typedef OpTypes::InterpC2FX InterpC2F;

//====================================================================

bool
test_integrator( const Expr::TSMethod method,
                 Expr::ExpressionFactory& factory,
		 Expr::ExprPatch& patch,
		 const SpatialOps::Grid& grid )
{
  const Expr::Tag tempTag     ( "Temperature",        Expr::STATE_N    );
  const Expr::Tag thermCondTag( "ThermalConductivity",Expr::STATE_NONE );
  const Expr::Tag gradTTag    ( "dT/dx",              Expr::STATE_NONE );
  const Expr::Tag heatFluxTag ( "HeatFluxX",          Expr::STATE_NONE );

  TemperatureTransEqn tempEqn( tempTag, thermCondTag, gradTTag, heatFluxTag, factory, patch );
  tempEqn.setup_boundary_conditions( factory );

  Expr::FieldManagerList& fml = patch.field_manager_list();

  //
  // build the time-integrator and add the temperature equation to it.
  //
  Expr::TimeStepper timeIntegrator( factory, method, "integrator", patch.id() );

  timeIntegrator.add_equation<TemperatureTransEqn::FieldT>( tempEqn.solution_variable_name(),
                                                            tempEqn.get_rhs_tag(),
                                                            1 );

  timeIntegrator.finalize( fml,
                           patch.operator_database(),
                           patch.field_info() );

  {
    const std::string name = method == Expr::FORWARD_EULER ? " tree_FE.dot" : "tree_RK.dot";
    std::ofstream fout(name.c_str());
    timeIntegrator.get_tree()->write_tree(fout,false,true);
  }

  //  fml.dump_fields(cout);

  //
  // initial conditions
  //
  const Expr::Tag xtag("x",Expr::STATE_NONE);
  {
    Expr::ExpressionFactory factoryIC;

    factoryIC.register_expression( new Expr::GaussianFunction<CellField>::Builder( tempTag, xtag, 30, 20, 40, 1.0 ) );
    factoryIC.register_expression( new Expr::PlaceHolder<CellField>::Builder(xtag,1) );

    const Expr::ExpressionID icid = tempEqn.initial_condition( factoryIC );
    Expr::ExpressionTree icTree( icid, factoryIC, patch.id() );
    icTree.register_fields( fml );
    fml.allocate_fields( patch.field_info() );
    icTree.bind_fields( fml );
    icTree.bind_operators( patch.operator_database() );

    {
      std::ofstream fout("icTree.dot");
      icTree.write_tree(fout);
    }

    grid.set_coord<SpatialOps::XDIR>( fml.field_ref<CellField>( xtag ) );

    icTree.execute_tree();

    CellField& temp = fml.field_ref<CellField>(tempTag);
#   ifdef ENABLE_CUDA
    temp.add_device( CPU_INDEX );
#   endif
    SpatialOps::write_matlab( temp, "T0", false );
  }

  // integrate for a while.
  double time=0;
  const double endTime=10.0;
  const double dt = 0.01;

  // lock fields that we want to write out so that their memory isn't reused
  fml.field_manager<CellField >().lock_field(xtag);
  while( time<endTime ){
    timeIntegrator.step( dt );
    time += dt;
  }

  CellField& x    = fml.field_ref<CellField>( xtag    );
  CellField& temp = fml.field_ref<CellField>( tempTag );
# ifdef ENABLE_CUDA
  x   .add_device( CPU_INDEX );
  temp.add_device( CPU_INDEX );
  temp.set_device_as_active( CPU_INDEX ); // so that we can iterate it below
# endif

  SpatialOps::write_matlab(   x,"x",false);
  SpatialOps::write_matlab(temp,"T",false);

  double Texpected;
  if( method==Expr::FORWARD_EULER ){
    Texpected=21.1853;
  }
  else{
    Texpected=21.1871;
  }

  TestHelper status(false);
  for( CellField::const_iterator it=temp.interior_begin(); it!=temp.interior_end(); ++it ){
    //if(std::isnan(*it) ){  <- nvcc will not compile isnan... No idea why not.  The following is equivalent and compiles:
    if( *it != *it ){
      std::cout << std::endl << "***FAIL*** - detected NaN!" << std::endl;
      status( false );
    }
  }

  status( std::abs(temp[50] - Texpected ) < 1e-5 );
  if( std::abs(temp[50] - Texpected ) >= 1e-5 ){
    std::cout << "***FAIL***" << std::endl
              << " Expected T[50] = " << Texpected << std::endl
              << " Found    T[50] = " << temp[50] << std::endl;
  }

  return status.ok();
}

//====================================================================

int main()
{
  //
  // build a patch
  //
  const int npts = 100;
  Expr::ExprPatch patch(npts);

  const SpatialOps::Grid grid( SpatialOps::IntVec(npts,1,1), SpatialOps::DoubleVec(100,1,1) );
  SpatialOps::OperatorDatabase& opDB = patch.operator_database();
  SpatialOps::build_stencils( grid, opDB );

  TestHelper status(true);
  try{
    Expr::ExpressionFactory exprFactory;
    status( test_integrator( Expr::FORWARD_EULER, exprFactory, patch, grid ), "Forward Euler" );
  }
  catch( std::exception& e ){
    status( false );
    std::cout << e.what() << std::endl << std::endl;
    return -1;
  }

  try{
    Expr::ExpressionFactory exprFactory;
    status( test_integrator( Expr::SSPRK3, exprFactory, patch, grid ), "SSPRK3" );
  }
  catch( std::exception& e ){
    status( false );
    std::cout << e.what() << std::endl << std::endl;
    return -1;
  }

  if( status.ok() ){
    std::cout << "\nPASS\n";
    return 0;
  }

  std::cout << "\nFAIL\n";
  return -1;
}

//====================================================================
