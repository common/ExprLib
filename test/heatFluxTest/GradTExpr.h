#ifndef GradTExpr_h
#define GradTExpr_h

#include <expression/Expression.h>

/**
 *  @class  GradTExpr
 *  @author James C. Sutherland
 *  @date   May, 2007
 *  @brief  Calculates the temperature gradient.
 */
template< typename GradOp >
class GradTExpr
  : public Expr::Expression< typename GradOp::DestFieldType >
{
  typedef typename GradOp::SrcFieldType  ScalarFieldT; ///< the scalar field type implied by the gradient operator
  typedef typename GradOp::DestFieldType GradFieldT;   ///< the flux field type implied by the gradient operator

  const GradOp* gradOp_;

  DECLARE_FIELD( ScalarFieldT, temp_   )

  GradTExpr( const Expr::Tag& tempTag )
    : Expr::Expression<GradFieldT>()
  {
    temp_ = this->template create_field_request<ScalarFieldT>( tempTag );
#   ifdef ENABLE_CUDA
    this->set_gpu_runnable( true );
#   endif
}

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB ){
    gradOp_ = opDB.retrieve_operator<GradOp>();
  }

  void evaluate(){
    using namespace SpatialOps;
    const ScalarFieldT& temp = temp_->field_ref();
    this->value() <<= (*gradOp_)( temp );
  }


  /** @brief Constructs a GradTExpr object. */
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new GradTExpr(tempT_); }

    Builder( const Expr::Tag& gradTTag,
             const Expr::Tag& tempTag,
             const int nghost=1 )
    : Expr::ExpressionBuilder(gradTTag,nghost),
      tempT_(tempTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag tempT_;
  };

};


#endif
