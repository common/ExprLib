#ifndef HeatFluxExpr_h
#define HeatFluxExpr_h

#include <sstream>

#include <expression/Expression.h>

//====================================================================


/**
 *  @class  HeatFluxExpr
 *  @author James C. Sutherland
 *  @date   May, 2007
 *
 * @brief Implements the expression for the Fourier heat flux,
 * \f$\mathbf{q}=-\lambda\nabla T \f$
 *
 *  @par Template Parameters
 *
 *   \li \b GradOp The type of operator for use in calculating
 *   \f$\nabla T\f$.  Currently, this mandates usage of the SpatialOps
 *   library.  However, we could try to make a general interface.
 *   Right now, the concrete operator type is obtained through a
 *   singleton class in SpatialOps.
 */
template< typename GradOp >
class HeatFluxExpr
  : public Expr::Expression< typename GradOp::DestFieldType >
{
  typedef typename GradOp::SrcFieldType   ScalarFieldT; ///< scalar field type implied by the gradient operator
  typedef typename GradOp::DestFieldType  GradFieldT;   ///< the flux field type implied by the gradient operator

  DECLARE_FIELDS( GradFieldT, lambda_, gradT_  )

  HeatFluxExpr( const Expr::Tag& thermCondTag,
                const Expr::Tag& gradTTag )
    : Expr::Expression<GradFieldT>()
  {
    lambda_ = this->template create_field_request<GradFieldT>( thermCondTag );
    gradT_  = this->template create_field_request<GradFieldT>( gradTTag     );

#   ifdef ENABLE_CUDA
    this->set_gpu_runnable( true );
#   endif
  }


public:

  void evaluate()
  {
    using namespace SpatialOps;
    GradFieldT& result = this->value();
    const GradFieldT& lambda = lambda_->field_ref();
    const GradFieldT& gradT  = gradT_ ->field_ref();
    result <<= -lambda * gradT;
  }


  ~HeatFluxExpr(){}

  /**
   *  @struct Builder
   *  @brief Constructs HeatFluxExpr objects.
   */
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new HeatFluxExpr( tct_, gtt_ ); }

    Builder( const Expr::Tag& heatFluxTag,
             const Expr::Tag& thermCondTag,
             const Expr::Tag& gradTTag,
             const int nghost=1 )
      : ExpressionBuilder( heatFluxTag, nghost ),
        tct_(thermCondTag), gtt_(gradTTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag tct_, gtt_;
  };

};

#endif
