#include <expression/Expression.h>

//====================================================================

class A : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELDS( SpatialOps::SingleValueField, b_, c_, d_ )

  A( const Expr::Tag& bTag,
     const Expr::Tag& cTag,
     const Expr::Tag& dTag )
    : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);

    b_ = create_field_request<SpatialOps::SingleValueField>( bTag );
    c_ = create_field_request<SpatialOps::SingleValueField>( cTag );
    d_ = create_field_request<SpatialOps::SingleValueField>( dTag );
  }

public:

  void evaluate()
  {
    using namespace SpatialOps;
    const SpatialOps::SingleValueField& b = b_->field_ref();
    const SpatialOps::SingleValueField& c = c_->field_ref();
    const SpatialOps::SingleValueField& d = d_->field_ref();
    this->value() <<= 1.1 + b + c + d;
  }

  ~A(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const
    {
      return new A( bt_, ct_, dt_ );
    }
    Builder( const Expr::Tag& aTag,
             const Expr::Tag& bTag,
             const Expr::Tag& cTag,
             const Expr::Tag& dTag )
      : ExpressionBuilder(aTag),
        bt_(bTag), ct_(cTag), dt_(dTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag bt_, ct_, dt_;
  };

};

//====================================================================

class B : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELDS( SpatialOps::SingleValueField, e_, g_ )

  B( const Expr::Tag& etag,
     const Expr::Tag& gtag )
    : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);

    e_ = create_field_request<SpatialOps::SingleValueField>( etag );
    g_ = create_field_request<SpatialOps::SingleValueField>( gtag );
  }
public:

  void evaluate(){
    using namespace SpatialOps;
    const SpatialOps::SingleValueField& e = e_->field_ref();
    const SpatialOps::SingleValueField& g = g_->field_ref();
    this->value() <<= 2.2 + e + g;
  }

  ~B(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new B(eTag_,gTag_); }
    Builder( const Expr::Tag& bTag,
             const Expr::Tag& eTag,
             const Expr::Tag& gTag )
    : ExpressionBuilder(bTag),
      eTag_(eTag), gTag_(gTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag eTag_, gTag_;
  };

};

//====================================================================

class C : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELDS( SpatialOps::SingleValueField, f_, g_ )

  C( const Expr::Tag& fTag,
     const Expr::Tag& gTag )
  : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
    f_ = this->create_field_request<SpatialOps::SingleValueField>( fTag );
    g_ = this->create_field_request<SpatialOps::SingleValueField>( gTag );
  }

public:

  void evaluate() {
    using namespace SpatialOps;
    const SpatialOps::SingleValueField& f = f_->field_ref();
    const SpatialOps::SingleValueField& g = g_->field_ref();
    this->value() <<= 3.3 + f + g;
  }

  ~C(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const
    { return new C(fTag_,gTag_); }
    Builder( const Expr::Tag& cTag,
             const Expr::Tag& fTag,
             const Expr::Tag& gTag )
    : ExpressionBuilder(cTag),
      fTag_(fTag), gTag_(gTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag fTag_, gTag_;
  };

};

//====================================================================

class D : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELD( SpatialOps::SingleValueField, f_ )

  D( const Expr::Tag& fTag )
    : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
    f_ = this->create_field_request<SpatialOps::SingleValueField>( fTag );
  }
  const Expr::Tag fTag_;

public:

  void evaluate(){
    using namespace SpatialOps;
    const SpatialOps::SingleValueField f = f_->field_ref();
    this->value() <<= 4.4 + f;
  }

  ~D(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const
    { return new D(ft_); }
    Builder( const Expr::Tag& dTag,
             const Expr::Tag& ftag )
    : ExpressionBuilder(dTag),
      ft_(ftag) {}
    ~Builder(){}
  private:
    const Expr::Tag ft_;
  };

};

//====================================================================

class E : public Expr::Expression<SpatialOps::SingleValueField>
{
  E() : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
  }

public:
  void evaluate(){
    using namespace SpatialOps;
    this->value() <<= 5.5;
  }

  ~E(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new E(); }
    Builder( const Expr::Tag& eTag ) : ExpressionBuilder(eTag) {}
    ~Builder(){}
  private:
  };

};

//====================================================================

class F : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELDS( SpatialOps::SingleValueField, g_, h_ )

  F( const Expr::Tag& gTag,
     const Expr::Tag& hTag )
    : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
    g_ = this->create_field_request<SpatialOps::SingleValueField>( gTag );
    h_ = this->create_field_request<SpatialOps::SingleValueField>( hTag );
  }

public:

  void evaluate() {
    using namespace SpatialOps;
    this->value() <<= 6.6 + g_->field_ref() + h_->field_ref();
  }

  ~F(){}

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const
    { return new F(gTag_,hTag_); }
    Builder( const Expr::Tag& fTag,
             const Expr::Tag& gTag,
             const Expr::Tag& hTag)
    : ExpressionBuilder(fTag),
      gTag_(gTag), hTag_(hTag) {}
    ~Builder(){}
  private:
    const Expr::Tag gTag_, hTag_;
  };

};

//====================================================================

class G : public Expr::Expression<SpatialOps::SingleValueField>
{
  G() : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
  }

public:
  void evaluate() {
    using namespace SpatialOps;
    this->value() <<= 7.7;
  }

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new G(); }
    Builder( const Expr::Tag& gTag ) : ExpressionBuilder(gTag) {}
    ~Builder(){}
  private:
  };

};

//====================================================================

class H : public Expr::Expression<SpatialOps::SingleValueField>
{
  H() : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
  }

public:
  void evaluate() {
    using namespace SpatialOps;
    this->value() <<= 8.8;
  }

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new H(); }
    Builder( const Expr::Tag& hTag ) : ExpressionBuilder(hTag) {}
    ~Builder(){}
  private:
  };

};

//====================================================================

class I : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_FIELD( SpatialOps::SingleValueField, d_ )

  I( const Expr::Tag dTag )
    : Expr::Expression<SpatialOps::SingleValueField>()
  {
    this->set_gpu_runnable(true);
    d_ = this->create_field_request<SpatialOps::SingleValueField>( dTag );
  }

public:

  void evaluate() {
    using namespace SpatialOps;
    this->value() <<= 9.9 * d_->field_ref();
  }

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new I(dT_); }
    Builder( const Expr::Tag& iTag, const Expr::Tag& dTag )
    : ExpressionBuilder(iTag),
      dT_(dTag)
    {}
    ~Builder(){}
  private:
    const Expr::Tag dT_;
  };

};

//====================================================================
