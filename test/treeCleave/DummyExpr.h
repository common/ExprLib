#ifndef Dummy_Expr_h
#define Dummy_Expr_h

#include <expression/Expression.h>

/**
 *  \class Dummy
 */
class Dummy
 : public Expr::Expression<SpatialOps::SingleValueField>
{
  DECLARE_VECTOR_OF_FIELDS( SpatialOps::SingleValueField, children_ )
  Dummy( const Expr::TagList& );
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::TagList& childTags );
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& childTag );

    Expr::ExpressionBase* build() const;

  private:
    Expr::TagList childTags_;
  };
  void evaluate();
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



Dummy::Dummy( const Expr::TagList& tags )
  : Expr::Expression<SpatialOps::SingleValueField>()
{
  this->create_field_vector_request<SpatialOps::SingleValueField>( tags, children_ );
}

//--------------------------------------------------------------------

void
Dummy::evaluate()
{
  using namespace SpatialOps;
  SpatialOps::SingleValueField& result = this->value();
  result <<= 0.0;
  for( size_t i=0; i<children_.size(); ++i ){
    result <<= result + children_[i]->field_ref();
  }
}

//--------------------------------------------------------------------

Dummy::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::TagList& tags )
  : ExpressionBuilder( resultTag ),
    childTags_( tags )
{}

Dummy::
Builder::Builder( const Expr::Tag& resultTag,
                  const Expr::Tag& tag )
  : ExpressionBuilder( resultTag )
{
  childTags_.push_back(tag);
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
Dummy::
Builder::build() const
{
  return new Dummy( childTags_ );
}


#endif // Dummy_Expr_h
