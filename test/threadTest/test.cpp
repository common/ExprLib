#include "test.h"
#include <spatialops/Nebo.h>

RHSExpr::RHSExpr( const Expr::Tag& fluxTag,
                  const Expr::Tag& srcTag )
  : Expr::Expression<VolT>(),
    doFlux_( fluxTag != Expr::Tag() ),
    doSrc_ ( srcTag  != Expr::Tag() )
{
  this->set_gpu_runnable( true );

  if( doFlux_ ) flux_ = this->create_field_request<XFluxT>( fluxTag );
  if( doSrc_  ) src_  = this->create_field_request<VolT  >( srcTag  );
}

//--------------------------------------------------------------------

void
RHSExpr::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  if( doFlux_ ) div_ = opDB.retrieve_operator<XDivT>();
}

//--------------------------------------------------------------------

void
RHSExpr::evaluate()
{
  using namespace SpatialOps;
  VolT& rhs = this->value();
  if( doFlux_ ){
    const XFluxT& flux = flux_->field_ref();
    rhs <<= -(*div_)(flux);
  }
  else{
    rhs <<= 0.0;
  }
  if( doSrc_ ){
    const VolT& src = src_->field_ref();
    rhs <<= rhs + src;
  }
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
RHSExpr::Builder::
build() const
{
  return new RHSExpr( fluxTag_, srcTag_ );
}

//--------------------------------------------------------------------

RHSExpr::Builder::
Builder( const Expr::Tag& rhsTag,
         const Expr::Tag& fluxTag,
         const Expr::Tag& srcTag )
  : ExpressionBuilder(rhsTag),
    fluxTag_( fluxTag ),
    srcTag_ ( srcTag  )
{}

//--------------------------------------------------------------------


//====================================================================


//--------------------------------------------------------------------

FluxExpr::FluxExpr( const Expr::Tag& varTag,
                    const double diffCoef )
  : Expr::Expression<XFluxT>(),
    diffCoef_( diffCoef )
{
  this->set_gpu_runnable( true );
  phi_ = this->create_field_request<VolT>( varTag );
}

//--------------------------------------------------------------------

void
FluxExpr::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  grad_ = opDB.retrieve_operator<XGradT>();
}

//--------------------------------------------------------------------

void
FluxExpr::evaluate()
{
  using namespace SpatialOps;
  XFluxT& flux = value();
  const VolT& phi = phi_->field_ref();
  flux <<= -diffCoef_ * (*grad_)(phi);
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
FluxExpr::Builder::build() const
{
  return new FluxExpr( tag_, coef_ );
}

//--------------------------------------------------------------------

FluxExpr::Builder::
Builder( const Expr::Tag& fluxTag,
         const Expr::Tag& phiTag,
         const double diffCoef )
  : ExpressionBuilder(fluxTag),
    tag_ ( phiTag   ),
    coef_( diffCoef )
{}

//--------------------------------------------------------------------

//====================================================================

//--------------------------------------------------------------------

BusyWork::BusyWork( const Expr::Tag& varTag, const int nvar )
  : Expr::Expression<VolT>(),
    nvar_( nvar )
{
  this->set_gpu_runnable( true );
  phi_  = this->create_field_request<VolT>( varTag );
}

//--------------------------------------------------------------------

void
BusyWork::evaluate()
{
  using namespace SpatialOps;
  VolT& val = value();
  const VolT& phi = phi_->field_ref();

  for( int i=0; i<nvar_; ++i ){
    val <<= phi / exp(double(i));
  }
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
BusyWork::Builder::build() const
{
  return new BusyWork( tag_, nvar_ );
}

//--------------------------------------------------------------------

BusyWork::Builder::
Builder( const Expr::Tag& result, const Expr::Tag& phiTag, const int nvar )
: ExpressionBuilder(result),
  tag_( phiTag ),
  nvar_( nvar )
{}

//--------------------------------------------------------------------

//====================================================================

//--------------------------------------------------------------------

CoupledBusyWork::CoupledBusyWork( const int nvar )
  : Expr::Expression<VolT>(),
    nvar_( nvar )
{
  this->set_gpu_runnable( true );
  Expr::TagList tags;
  for( int i=0; i!=nvar_; ++i ){
    tags.push_back( Expr::Tag("var_" + boost::lexical_cast<std::string>(i), Expr::STATE_N ) );
  }
  create_field_vector_request<VolT>( tags, phi_ );
//  tmpVec_.resize( nvar, 0.0 );
}

//--------------------------------------------------------------------

void
CoupledBusyWork::evaluate()
{
  using namespace SpatialOps;
  VolT& val = value();
  val <<= 0.0;

  BOOST_FOREACH( const boost::shared_ptr<const Expr::FieldRequest<VolT> >& ptr, phi_ ){
    const VolT& phi = ptr->field_ref();
    val <<= val + exp(phi);
  }

  // NOTE: the following commented code is a mockup for point-wise calls to a
  //       third-party library.  For now, we aren't going to use this since it
  //       won't allow GPU execution via Nebo.

  //  // pack iterators into a vector
//  iterVec_.clear();
//  for( FieldVecT::const_iterator ifld=phi_.begin(); ifld!=phi_.end(); ++ifld ){
//    iterVec_.push_back( (*ifld)->begin() );
//  }
//
//  for( VolT::iterator ival=val.begin(); ival!=val.end(); ++ival ){
//    // unpack into temporary
//    tmpVec_.clear();
//    for( IterVec::const_iterator ii=iterVec_.begin(); ii!=iterVec_.end(); ++ii ){
//      tmpVec_.push_back( **ii );
//    }
//
//    double src=1.0;
//    for( std::vector<double>::const_iterator isrc=tmpVec_.begin(); isrc!=tmpVec_.end(); ++isrc ){
//      src += exp(*isrc);
//    }
//
//    *ival = src;
//
//    // advance iterators to next point
//    for( IterVec::iterator ii=iterVec_.begin(); ii!=iterVec_.end(); ++ii ){
//      ++(*ii);
//    }
//  }
}

//--------------------------------------------------------------------

Expr::ExpressionBase*
CoupledBusyWork::Builder::
build() const
{
  return new CoupledBusyWork( nvar_ );
}

//--------------------------------------------------------------------

CoupledBusyWork::Builder::
Builder( const Expr::Tag& result,
         const int nvar )
  : ExpressionBuilder(result),
    nvar_( nvar )
{}

//--------------------------------------------------------------------
