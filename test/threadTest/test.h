#ifndef test_h
#define test_h

#include "defs.h"

#include <expression/ExprLib.h>

//====================================================================

class RHSExpr : public Expr::Expression<VolT>
{
  const bool doFlux_, doSrc_;

  DECLARE_FIELD( XFluxT, flux_ )
  DECLARE_FIELD( VolT,   src_  )

  const XDivT* div_;

  RHSExpr( const Expr::Tag& fluxTag,
           const Expr::Tag& srcTag );

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const;
    Builder( const Expr::Tag& rhsTag,
             const Expr::Tag& fluxTag,
             const Expr::Tag& srcTag );
  private:
    const Expr::Tag fluxTag_, srcTag_;
  };

};

//====================================================================

class FluxExpr : public Expr::Expression<XFluxT>
{
  const double diffCoef_;
  DECLARE_FIELD( VolT, phi_ )
  const XGradT* grad_;

  FluxExpr( const Expr::Tag& var,
            const double diffCoef );

public:

  void bind_operators( const SpatialOps::OperatorDatabase& opDB );
  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const;
    Builder( const Expr::Tag& fluxTag,
             const Expr::Tag& var,
             const double diffCoef=1.0 );
  private:
    const Expr::Tag tag_;
    const double coef_;
  };
};

//====================================================================

class BusyWork : public Expr::Expression<VolT>
{
  DECLARE_FIELD( VolT, phi_ )
  const int nvar_;

  BusyWork( const Expr::Tag& var, const int nvar );

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const;
    Builder( const Expr::Tag& result,
             const Expr::Tag& var,
             const int nvar );
  private:
    const Expr::Tag tag_;
    const int nvar_;
  };

};

//====================================================================

class CoupledBusyWork : public Expr::Expression<VolT>
{
  const int nvar_;

  DECLARE_VECTOR_OF_FIELDS( VolT, phi_ )

//  typedef std::vector<VolT::const_iterator> IterVec;
//  IterVec iterVec_;
//  std::vector<double> tmpVec_;

  CoupledBusyWork( const int nvar );

public:

  void evaluate();

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const;
    Builder( const Expr::Tag& result,
             const int nvar );
  private:
    const int nvar_;
  };

};

//====================================================================

#endif
