/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   tagTest.cpp
 *  \date   Nov 16, 2015
 *  \author James C. Sutherland
 */

#include <test/TestHelper.h>
#include <expression/Tag.h>

int main()
{
  const Expr::Tag t1( "a", Expr::STATE_NONE );
  const Expr::Tag t2( "a", Expr::STATE_NONE );
  const Expr::Tag t3( "b", Expr::STATE_NONE );
  const Expr::Tag t4( t3 );

  Expr::Tag t5 = t4;

  TestHelper status(false);

  status( t1 == t2 );
  status( t1 != t3 );
  status( t4 == t3 );
  status( t5 == t3 );

  t5.reset_context( Expr::STATE_N );
  status( t5 != t3 );
  status( t5 == Expr::Tag("b",Expr::STATE_N) );

  t5.reset_name( "c" );
  status( t5 == Expr::Tag("c",Expr::STATE_N) );

  t5.reset( "b", Expr::STATE_NONE );
  status( t5 == t4 );

  if( status.ok() ){
    std::cout << "PASS\n";
    return 0;
  }
  std::cout << "FAIL\n";
  return -1;
}
