#include <stdexcept>
#include <sstream>

#include "RHS.h"

//--------------------------------------------------------------------

RHS::RHS( const Expr::TagList& ctags,
          const double k1,
          const double k2 )
  : Expr::Expression<FieldT>(),
    k1_( k1 ), k2_( k2 )
{
  this->create_field_vector_request<FieldT>( ctags, c_ );

  if( ctags.size() != 3 ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "Expected 3 species in TagList, found " << ctags.size() << std::endl;
    throw( std::runtime_error( msg.str() ) );
  }

  this->set_gpu_runnable(true);
}

//--------------------------------------------------------------------

void
RHS::evaluate()
{
  using namespace SpatialOps;

  ValVec& result = this->get_value_vec();

  const SingleValueField& cA = c_[0]->field_ref();
  const SingleValueField& cB = c_[1]->field_ref();
//  const SingleValueField& cC = c_[2]->field_ref();

  SpatFldPtr<SingleValueField> r1 = SpatialFieldStore::get<SingleValueField>(cA);
  SpatFldPtr<SingleValueField> r2 = SpatialFieldStore::get<SingleValueField>(cA);

  // calculate the rates.
  *r1 <<= k1_ * cA;
  *r2 <<= k2_ * cB;

  SingleValueField& rhsA = *(result[0]);
  SingleValueField& rhsB = *(result[1]);
  SingleValueField& rhsC = *(result[2]);

  rhsA <<= -*r1;
  rhsB <<= *r1 - *r2;
  rhsC <<= *r2;
}

//--------------------------------------------------------------------
