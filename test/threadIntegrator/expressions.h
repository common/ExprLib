#ifndef expressions_h
#define expressions_h

#include <expression/Expression.h>

//====================================================================

template<typename FieldT>
class C1 : public Expr::Expression<FieldT>
{
  const double k_;
  DECLARE_FIELD( FieldT, c1_ )

  C1( const double k )
    : Expr::Expression<FieldT>(),
      k_(k)
  {
    this->set_gpu_runnable( true );
    c1_ = this->template create_field_request<FieldT>( Expr::Tag( "C1", Expr::STATE_N ) );
  }

public:

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& rhs = this->value();
    const FieldT c1 = c1_->field_ref();
    rhs <<= -k_*c1;
  }


  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& result,
             const double k )
    : ExpressionBuilder(result),
      k_(k)
    {}
    Expr::ExpressionBase* build() const{ return new C1<FieldT>( k_ ); }
  private:
    const double k_;
  };

};


//====================================================================


template<typename FieldT>
class C2 : public Expr::Expression<FieldT>
{
  const double k_;
  DECLARE_FIELD( FieldT, c1_ )

  C2( const double k )
    : Expr::Expression<FieldT>(),
      k_(k)
  {
    this->set_gpu_runnable( true );
    c1_ = this->template create_field_request<FieldT>( Expr::Tag( "C1", Expr::STATE_N ) );
  }

public:

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& rhs = this->value();
    const FieldT c1 = c1_->field_ref();
    rhs <<= k_ * c1;
  }

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Expr::ExpressionBase* build() const{ return new C2<FieldT>( k_ ); }
    Builder( const Expr::Tag& result, const double k ) : ExpressionBuilder(result), k_(k) {}
  private:
    const double k_;
  };

};

//====================================================================

#endif // expressions_h
