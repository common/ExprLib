nebo_cuda_prep_dir()

link_directories( ${exprlib_BINARY_DIR} )

nebo_add_executable( function_test function_test.cpp )
target_link_libraries( function_test exprlib )

add_test( function_test function_test )
