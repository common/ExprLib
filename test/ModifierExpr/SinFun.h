/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SinFun_Expr_h
#define SinFun_Expr_h

#include <expression/Expression.h>

template< typename FieldT >
class SinFun
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELD( FieldT, x_ )

  SinFun( const Expr::Tag& xTag ) : Expr::Expression<FieldT>()
  {
    x_ = this->template create_field_request<FieldT>( xTag );
  }

public:

  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& xTag,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        xTag_( xTag )
    {}

    Expr::ExpressionBase* build() const{ return new SinFun<FieldT>( xTag_ ); }

  private:
    const Expr::Tag xTag_;
  };

  ~SinFun(){}

  void evaluate(){
    using namespace SpatialOps;
    this->value() <<= 2.0 * sin( x_->field_ref() );
  }
};

#endif // SinFun_Expr_h
