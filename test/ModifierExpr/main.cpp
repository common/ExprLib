#include <expression/ExprLib.h>
#include <expression/Functions.h>

#include "Modifier.h"
#include "SinFun.h"
#include <expression/ClipValue.h>
#include <test/TestHelper.h>

#include <spatialops/structured/FieldHelper.h>

using namespace std;
using namespace Expr;

#include <spatialops/structured/FVStaggeredFieldTypes.h>
#include <spatialops/structured/Grid.h>

typedef SpatialOps::SVolField FieldT;

int main()
{
  TestHelper status(true);

  ExpressionFactory factory(false);

  const SpatialOps::DoubleVec domainLength(6.3,1.0,1.0);
  const SpatialOps::IntVec npts( 50, 1, 1 );
  const SpatialOps::Grid grid( npts, domainLength );

  const Tag xtag      ( "x",      STATE_NONE );
  const Tag fxtag     ( "fx",     STATE_NONE );
  const Tag fxcliptag ( "fxclip", STATE_NONE );
  const Tag fxcliptag2( "clip2",  STATE_NONE );

  const Tag atag    ("A",     STATE_NONE);
  const Tag btag    ("B",     STATE_NONE);
  const Tag ctag    ("C",     STATE_NONE);
  const Tag amodTag1("A-mod1",STATE_NONE);
  const Tag amodTag2("A-mod2",STATE_NONE);

  IDSet roots;
  roots.insert( factory.register_expression( new ConstantExpr<FieldT>::Builder(atag,1.0) ) );
  factory.register_expression( new ConstantExpr<FieldT>::Builder(btag,2.0) );
  factory.register_expression( new ConstantExpr<FieldT>::Builder(ctag,3.0) );
  factory.register_expression( new Modifier<FieldT>::Builder(amodTag1,btag) );
  factory.register_expression( new Modifier<FieldT>::Builder(amodTag2,ctag) );

  // clipping modifier:
  roots.insert( factory.register_expression( new SinFun<FieldT>::Builder(fxtag,xtag,0) ) );
  factory.register_expression( new PlaceHolder<FieldT>::Builder( xtag, 0 ) );
  factory.register_expression( new ClipValue<FieldT>::Builder(fxcliptag, -1.0,1.0,ClipValue<FieldT>::CLIP_BOTH,    0) );
  factory.register_expression( new ClipValue<FieldT>::Builder(fxcliptag2,-0.5,0.0,ClipValue<FieldT>::CLIP_MIN_ONLY,0) );

  try{

    // this covers a bug that was identified and fixed in cases where a
    // modifier was added after an expression had been retrieved.
    factory.retrieve_expression( atag, 0, false );

    factory.attach_modifier_expression( fxcliptag,  fxtag );
    factory.attach_modifier_expression( fxcliptag2, fxtag );

    // this results in a depending on b even though it doesn't above (both are constants)
    // because the Modifier implies a dependency.
    factory.attach_modifier_expression( amodTag1, atag, 0 );
    factory.attach_modifier_expression( amodTag2, atag, 0 );

    ExpressionTree tree( roots, factory, 0, "graph" );

    FieldManagerList fml;
    tree.register_fields(fml);
    fml.allocate_fields( Expr::FieldAllocInfo( npts, 0, 0, false, false, false ) );
    {
      FieldT& x = fml.field_ref<FieldT>(xtag);
      grid.set_coord<SpatialOps::XDIR>(x);
    }
    tree.bind_fields(fml);
    {
      std::ofstream fout("graph.dot");
      tree.write_tree(fout,false,true);
    }
    tree.execute_tree();

    const FieldT& a  = fml.field_ref<FieldT>(atag);
    const FieldT& fx = fml.field_ref<FieldT>(fxtag);
#   ifdef ENABLE_CUDA
    const_cast<FieldT&>(a ).add_device( CPU_INDEX );
    const_cast<FieldT&>(fx).add_device( CPU_INDEX );
#   endif
    std::cout << *a.begin() << std::endl;
    status( *a.begin() == 6, "a=6" );
    status( nebo_max(fx)<=1.0 && nebo_min(fx)>=-0.5, "clipping" );
  }
  catch( std::exception& err ){
    cout << err.what() << endl << "\nFAIL!\n";
    return -1;
  }

  if( status.ok() ){
    cout << "\nPASS\n";
    return 0;
  }
  cout << "\nFAIL\n";
  return -1;
}
