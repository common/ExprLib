#ifndef Modifier_Expr_h
#define Modifier_Expr_h

#include <expression/Expression.h>

template< typename FieldT >
class Modifier
 : public Expr::Expression<FieldT>
{
  DECLARE_FIELD( FieldT, f_ )

  Modifier( const Expr::Tag tag )
  {
    this->set_gpu_runnable( true );
    f_ = this->template create_field_request<FieldT>(tag);
  }
public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:
    Builder( const Expr::Tag& resultTag, const Expr::Tag& tag ) : ExpressionBuilder(resultTag), tag_(tag) {}
    Expr::ExpressionBase* build() const{ return new Modifier(tag_); }
  private:
    const Expr::Tag tag_;
  };

  ~Modifier(){}

  void evaluate(){
    using namespace SpatialOps;
    FieldT& result = this->value();
    const FieldT& f = f_->field_ref();
    result <<= result + f;
  }
};


#endif // Modifier_Expr_h
