The Expression library provides support for expressing the structure of a
calculation as a directed acyclic graph, and deploying that on multiple architectures.  It has been developed by several people over the years, including:
 - [James C. Sutherland](https://sutherland.che.utah.edu)
 - Devin Robison (M.S. Student)
 - Abhishek Bagusetty (M.S. Student)

# Quick Links
 - [Doxygen Documentation](https://jenkins.multiscale.utah.edu/job/ExprLib/doxygen/)
 - [Jenkins continuous build/test](https://jenkins.multiscale.utah.edu/job/ExprLib/)
 - [Notz, P. K., Pawlowski, R. P., & Sutherland, J. C. (2012). Graph-Based Software Design for Managing Complexity and Enabling Concurrency in Multiphysics PDE Software. ACM Transactions on Mathematical Software, 39(1), 1–21. doi:10.1145/2382585.2382586](https://gitlab.multiscale.utah.edu/common/ExprLib/blob/master/doc/ACM_2012.pdf)

# Questions?
For questions, please contact [Professor Sutherland](https://sutherland.che.utah.edu/)
