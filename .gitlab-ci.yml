#----------------------------------------------------------
# to help get the yaml right, see:
#  https://gitlab.multiscale.utah.edu/ci/lint
#  http://yaml-online-parser.appspot.com/
#----------------------------------------------------------

#----------------------------------------------------------
# Prerequisites & assumptions:
#  - CMake and CTest are installed on all of the runners
#  - modern C++ compilers are available
#  - boost (and possibly cuda) installations are present
#  - doxygen is installed
#----------------------------------------------------------

stages:
  - build_test
  - docs
  
#--------------------------------------------------
#------------------- SpatialOps -------------------
.clone_spatialops      : &clone_so       git clone https://gitlab.multiscale.utah.edu/common/SpatialOps.git; cd SpatialOps; git reset --hard 41b39898909475fc9e2b2065618b7297f6b5cd56; cd ..
.conf_spatialops       : &conf_so_opt    cmake ../SpatialOps -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/install -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTS=OFF -DENABLE_EXAMPLES=OFF 
.conf_spatialops       : &conf_so_dbg    cmake ../SpatialOps -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/install -DCMAKE_BUILD_TYPE=Debug   -DENABLE_TESTS=OFF -DENABLE_EXAMPLES=OFF 
.conf_spatialops_thread: &conf_so_thread cmake ../SpatialOps -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/install -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTS=OFF -DENABLE_EXAMPLES=OFF -DENABLE_THREADS=ON -DNTHREADS=8
.conf_spatialops_gpu   : &conf_so_gpu    cmake ../SpatialOps -DCMAKE_INSTALL_PREFIX=$CI_PROJECT_DIR/install -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTS=OFF -DENABLE_EXAMPLES=OFF -DENABLE_CUDA=ON -DCUDA_NVCC_FLAGS=-std=c++11
#------------------- SpatialOps -------------------
#--------------------------------------------------


#--------------------------------------------------
#--------------------- ExprLib --------------------
.conf-opt  : &conf_opt  cmake .. -DENABLE_OUTPUT=ON -DENABLE_TESTS=ON -DSpatialOps_DIR=$CI_PROJECT_DIR/install/share -DCMAKE_BUILD_TYPE=Release
.conf-dbg  : &conf_dbg  cmake .. -DENABLE_OUTPUT=ON -DENABLE_TESTS=ON -DSpatialOps_DIR=$CI_PROJECT_DIR/install/share -DCMAKE_BUILD_TYPE=Debug  
.build_cmd : &build_cmd make -j8
.test_cmd  : &test_cmd  ctest -j4

.build:linux: &build_linux
  stage: build_test
  tags:
    - linux
  artifacts:
    name     : "BuildResults_$CI_BUILD_REF_NAME"
    when     : on_failure
    expire_in: 10 days
    paths    :
    - build/Testing
    
.build:mac: &build_mac
  <<: *build_linux
  tags:
    - mac

# define all of the unique builds that we will run
build:linux:opt: &linux_opt
  <<: *build_linux
  script:
    - mkdir build; cd build;
    - *clone_so
    - mkdir so; cd so
    - *conf_so_opt
    - make -j8 install; cd ..
    - *conf_opt
    - *build_cmd
    - *test_cmd

build:linux:dbg: &linux_dbg
  <<: *build_linux
  script: 
    - mkdir build; cd build
    - *clone_so
    - mkdir so; cd so
    - *conf_so_dbg
    - make -j8 install; cd ..
    - *conf_dbg
    - *build_cmd
    - *test_cmd

build:linux:threaded:opt:
  <<: *build_linux
  script:
    - mkdir build; cd build;
    - *clone_so
    - mkdir so; cd so
    - *conf_so_thread
    - make -j8 install; cd ..
    - *conf_opt
    - *build_cmd
    - *test_cmd

build:linux:threaded:dbg:
  <<: *build_linux
  script:
    - mkdir build; cd build;
    - *clone_so
    - mkdir so; cd so
    - *conf_so_thread
    - make -j8 install; cd ..
    - *conf_dbg
    - *build_cmd
    - *test_cmd

build:linux:gpu:
  <<: *build_linux
  tags:
    - gpu
  script:
    - mkdir build; cd build;
    - *clone_so
    - mkdir so; cd so
    - *conf_so_gpu
    - make -j8 install; cd ..
    - *conf_dbg
    - *build_cmd
    - *test_cmd

build:mac:opt: 
  <<: *build_mac
  script: 
    - mkdir build; cd build; 
    - *clone_so
    - mkdir so; cd so
    - *conf_so_opt
    - make -j8 install; cd ..
    - *conf_opt
    - *build_cmd
    - *test_cmd
    
build:mac:dbg: 
  <<: *build_mac
  script: 
    - mkdir build; cd build; 
    - *clone_so
    - mkdir so; cd so
    - *conf_so_dbg
    - make -j8 install; cd ..
    - *conf_dbg
    - *build_cmd
    - *test_cmd

build:mac:threaded:opt:
  <<: *build_mac
  script: 
    - mkdir build; cd build; 
    - *clone_so
    - mkdir so; cd so
    - *conf_so_thread
    - make -j8 install; cd ..
    - *conf_opt
    - *build_cmd
    - *test_cmd
    
build:mac:threaded:dbg:
  <<: *build_mac
  script: 
    - mkdir build; cd build; 
    - *clone_so
    - mkdir so; cd so
    - *conf_so_thread
    - make -j8 install; cd ..
    - *conf_dbg
    - *build_cmd
    - *test_cmd
    
doxygen:
  stage     : docs
  script    : doxygen Doxyfile
  artifacts :
    expire_in: 10 days
    paths:
      - doc/html
    
#--------------------- ExprLib --------------------
#--------------------------------------------------
    