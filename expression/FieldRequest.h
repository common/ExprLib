/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   FieldRequest.h
 *  \date   Jan 15, 2015
 *  \author "James C. Sutherland"
 */

#ifndef FIELDREQUEST_H_
#define FIELDREQUEST_H_

#include <boost/shared_ptr.hpp>
#include <boost/functional/hash.hpp>

#include <expression/FieldManagerList.h>
#include <expression/Context.h>

#include <spatialops/structured/SpatialFieldStore.h>
#include <spatialops/structured/GhostData.h>
#include <spatialops/util/TypeName.h>

#include <map>


namespace Expr {

  /**
   * @brief Obtain the tag that describes sensitivity of tag1 to tag2, \f$\frac{\partial t_1}{\partial t_2}\f$.
   * @param tag1 - the dependent variable
   * @param tag2 - the independent variable
   * @return the sensitivity tag.  The names and states of the original tags are included in the sensitivity's mangled name.
   */
  inline Tag sens_tag( const Tag& tag1, const Tag& tag2 )
  {
    return Tag( tag1.name() + "_" + context2str(tag1.context()) + "__sens__" + tag2.name() + "_" + context2str(tag2.context()),
                STATE_NONE );
  }

  /**
   *  \class  Expr::FieldRequest
   *  \date   Jan 15, 2015
   *  \author "James C. Sutherland"
   *  \brief Supports field description and retrieval within an Expression.
   */
  template< typename FieldT >
  class FieldRequest
  {
    const Tag tag_;
    SpatialOps::GhostData ghosts_;
    bool fieldIsSet_;
    SpatialOps::SpatFldPtr<FieldT> field_;
    TagSet sensVars_;  ///< tags for variables to compute sensitivity with respect to

    typedef std::map< Tag, SpatialOps::SpatFldPtr<FieldT> > TagFieldMap;
    TagFieldMap sensFields_;

  public:

    /**
     * \brief Create a FieldRequest.  This should not generally be called by user code.
     * @param tag the tag identifying the field
     * @param ghosts the ghost information for this field
     */
    FieldRequest( const Tag& tag,
                  const SpatialOps::GhostData& ghosts )
      : tag_( tag ), ghosts_( ghosts )
    {
      fieldIsSet_ = false;
    }

    /** \brief obtain the Tag associated with this FieldRequest */
    inline const Tag& tag() const{ return tag_; }

    inline void add_sensitivity( const Tag& var ){ sensVars_.insert(var); }

    inline void remove_sensitivity( const Tag& var ){ sensVars_.erase(var); }

    inline const TagSet& get_sensitivity() const{ return sensVars_; }

    /** \brief obtain the GhostData associated with this FieldRequest */
    inline const SpatialOps::GhostData& ghosts() const{ return ghosts_; }

    /** \brief (re)set the ghost information desired for this FieldRequest */
    inline void set_ghosts( const SpatialOps::GhostData& ng ){ ghosts_ = ng; }

    /** \brief (re)set the minimum number of ghosts required for this FieldRequest */
    inline void set_min_ghosts( const SpatialOps::GhostData& ng ){ ghosts_ = SpatialOps::max(ghosts_,ng); }

    inline const SpatialOps::GhostData& get_ghosts() const{ return ghosts_; }

    /** Invalidate access to the field pointer.  Results in an assertion if field_ref() is called. */
    inline void invalidate(){
      field_.detach();
      fieldIsSet_ = false;
    }

    /**
     * @brief Release the field associated with this FieldRequest.
     * @param fm the FieldManager that owns this field.
     */
    inline void release( FieldManagerBase& fm ){
      const bool released = fm.release_field( tag_ );
      if( released ){  // only release sensitivities if the associated field is released
        for( const Tag& sensTag: sensVars_ ){
          const Tag tag = sens_tag( tag_, sensTag );
          sensFields_[tag].detach();
          fm.release_field( tag );
        }
      }
      invalidate();
    }

    /** \brief True if this FieldRequest can have field_ref() called on it. */
    inline bool is_field_valid() const{ return fieldIsSet_; }

    /** \brief Register this field with the supplied FieldManager */
    inline void register_field( typename FieldMgrSelector<FieldT>::type& fm ){
      fm.register_field(tag_,ghosts_);
      for( const Tag& sensTag: sensVars_ ){
        const Tag tag = sens_tag( tag_, sensTag );
        fm.register_field( tag, ghosts_ );
      }
    }

    /** Obtain a const reference to the field described by this FieldRequest */
    inline const FieldT& field_ref() const{
      assert( is_field_valid() );
      return *field_;
    }

    /** Obtain a const SpatFldPtr to the field described by this FieldRequest */
    inline const SpatialOps::SpatFldPtr<FieldT> field_ptr() const{
      assert( is_field_valid() );
      return field_;
    }

    /** Obtain a const SpatFldPtr to the field described by this FieldRequest */
    inline SpatialOps::SpatFldPtr<FieldT> field_ptr(){
      assert( is_field_valid() );
      return field_;
    }

    /** Obtain a reference to the field described by this FieldRequest */
    inline FieldT& field_ref(){
      assert( is_field_valid() );
      return *field_;
    }

    inline const SpatialOps::SpatFldPtr<FieldT> sens_field_ptr( const Tag& sensTag ) const{
      assert( is_field_valid() );
      const typename TagFieldMap::const_iterator ifld = sensFields_.find( sensTag );
      if( ifld == sensFields_.end() ){
        std::ostringstream msg;
        msg << "\n\nERROR from " << __FILE__ << " : " << __LINE__
            << "\nNo sensitivity to " << sensTag << " found for field " << tag_ << std::endl
            << "\texisting sensitivities: ";
        for( const auto& entry: sensFields_ ) msg << "\t\t" << entry.first << std::endl;
        throw std::runtime_error( msg.str() );
      }
      return ifld->second;
    }

    inline const FieldT& sens_field_ref( const Tag& sensTag ) const{ return *sens_field_ptr(sensTag); }

    inline SpatialOps::SpatFldPtr<FieldT> sens_field_ptr( const Tag& sensTag ){
      assert( is_field_valid() );
      return sensFields_[sensTag];
    }

    inline FieldT& sens_field_ref( const Tag& sensTag ){ return *sens_field_ptr(sensTag); }

    /** Retrieve a pointer to the field described by this FieldRequest */
    inline void set_field( typename FieldMgrSelector<FieldT>::type& fm ){
      field_ = fm.field_ptr( tag_ );
      // reset to the number of ghosts requested by the expression or the valid ghosts, whichever is less.
      field_->reset_valid_ghosts( SpatialOps::min( ghosts_, field_->get_valid_ghost_data() ) );
//      std::cout << tag()
//          << "\n\tRequested: " << ghosts_
//          << "\n\tActual   : " << field_->get_valid_ghost_data() << std::endl;

      for( const Tag& sensTag: sensVars_ ){
        const Tag tag = sens_tag( tag_, sensTag );
        SpatialOps::SpatFldPtr<FieldT> field = fm.field_ptr( tag );
        field->reset_valid_ghosts( SpatialOps::min( ghosts_, field->get_valid_ghost_data() ) );
        sensFields_[sensTag] = field;
      }
      fieldIsSet_ = true;
    }

  };


#define DECLARE_FIELD( FieldT, FieldName )      \
    boost::shared_ptr< const Expr::FieldRequest<FieldT> > FieldName;

#define DECLARE_FIELDS2( FieldT, f1, f2 ) \
    DECLARE_FIELD( FieldT, f1 )          \
    DECLARE_FIELD( FieldT, f2 )

#define DECLARE_FIELDS3( FieldT, f1, f2, f3 ) \
    DECLARE_FIELDS2( FieldT, f1, f2 )         \
    DECLARE_FIELD( FieldT, f3 )

#define DECLARE_FIELDS4( FieldT, f1, f2, f3, f4 ) \
    DECLARE_FIELDS3( FieldT, f1, f2, f3 )         \
    DECLARE_FIELD( FieldT, f4 )

#define DECLARE_FIELDS5( FieldT, f1, f2, f3, f4, f5 ) \
    DECLARE_FIELDS4( FieldT, f1, f2, f3, f4 )         \
    DECLARE_FIELD( FieldT, f5 )

#define DECLARE_FIELDS6( FieldT, f1, f2, f3, f4, f5, f6 ) \
    DECLARE_FIELDS5( FieldT, f1, f2, f3, f4, f5 )         \
    DECLARE_FIELD( FieldT, f6 )

#define DECLARE_FIELDS7( FieldT, f1, f2, f3, f4, f5, f6, f7 ) \
    DECLARE_FIELDS6( FieldT, f1, f2, f3, f4, f5, f6 )         \
    DECLARE_FIELD( FieldT, f7 )

#define DECLARE_FIELDS8( FieldT, f1, f2, f3, f4, f5, f6, f7, f8 ) \
    DECLARE_FIELDS7( FieldT, f1, f2, f3, f4, f5, f6, f7 )         \
    DECLARE_FIELD( FieldT, f8 )

#define DECLARE_FIELDS9( FieldT, f1, f2, f3, f4, f5, f6, f7, f8, f9 ) \
    DECLARE_FIELDS8( FieldT, f1, f2, f3, f4, f5, f6, f7, f8 )         \
    DECLARE_FIELD( FieldT, f9 )

#define GET_MACRO(_1,_2,_3,_4,_5,_6,_7,_8,_9,NAME,...) NAME

  /**
   * Declare one or more fields of a given type.  Example:
   * \code
   * DECLARE_FIELDS( MyFieldType,   a, b, c )
   * DECLARE_FIELDS( YourFieldType, d )
   * \endcode
   * Results in field declarations for four fields.
   *
   * These should be paired with calls in the constructor to:
   * \code
   *  a = create_field_request<MyFieldType>( aTag );
   *  b = create_field_request<MyFieldType>( bTag );
   *  c = create_field_request<MyFieldType>( cTag );
   *  d = create_field_request<YourFieldType>( dTag );
   * \endcode
   */
#define DECLARE_FIELDS( FieldT, ... )           \
    GET_MACRO( __VA_ARGS__,       \
               DECLARE_FIELDS9,   \
               DECLARE_FIELDS8,   \
               DECLARE_FIELDS7,   \
               DECLARE_FIELDS6,   \
               DECLARE_FIELDS5,   \
               DECLARE_FIELDS4,   \
               DECLARE_FIELDS3,   \
               DECLARE_FIELDS2,   \
               DECLARE_FIELD )    \
               (FieldT, __VA_ARGS__)

  /**
   * Declare a vector of fields of a given type.  Example:
   * \code
   * DECLARE_VECTOR_OF_FIELDS( MyFieldType, fieldVec )
   * \endcode
   * Results in field declarations for four fields.
   *
   * These should be paired with calls in the constructor to:
   * \code
   *  create_field_vector_request<FieldT>( tags, fieldVec );
   * \endcode
   */
#define DECLARE_VECTOR_OF_FIELDS( FieldT, name )        \
    std::vector< boost::shared_ptr< const Expr::FieldRequest<FieldT> > > name;


  template<typename T>
  std::ostream& operator << ( std::ostream& os, const FieldRequest<T>& fr ){
    os << fr.tag() << " with ghosts: " << fr.ghosts();
    return os;
  }


  struct FieldAggregatorBase{
    virtual ~FieldAggregatorBase(){}
    virtual void invalidate() = 0;
    virtual void release( FieldManagerList& fml ) = 0;
    virtual TagList get_tags() const = 0;
    virtual void register_fields( FieldManagerList& fml ) = 0;
    virtual void set_fields( FieldManagerList& fml ) = 0;
    virtual void dump( std::ostream& os ) const = 0;
    virtual bool has_field( const Tag& ) const = 0;
    virtual void set_ghosts( const SpatialOps::GhostData ng ) = 0;
    virtual void set_min_ghosts( const SpatialOps::GhostData ng ) = 0;
    virtual SpatialOps::GhostData get_min_ghosts() const = 0;
    virtual SpatialOps::GhostData get_max_ghosts() const = 0;
    virtual void add_sensitivity( const Tag& tag ) = 0;
    virtual void remove_sensitivity( const Tag& var ) = 0;
    virtual const TagSet& get_sensitivity() const = 0;
    virtual int get_num_entries() const = 0;
  };


  /**
   * \class Expr::FieldAggregator
   * \author James C. Sutherland
   *
   */
  template<typename FieldT>
  class FieldAggregator : public FieldAggregatorBase
  {
    typedef FieldRequest<FieldT> FieldReq;

  public:
    typedef boost::shared_ptr< FieldReq > FieldReqPtr;

  private:
    typedef std::map< Tag, FieldReqPtr > FieldReqs;
    FieldReqs fieldReqs_;

  public:

    FieldAggregator(){}

    int get_num_entries() const{ return fieldReqs_.size(); }

    inline void add_sensitivity( const Tag& var ){
      for( auto& fr: fieldReqs_ ){
        fr.second->add_sensitivity( var );
      }
    }

    inline void remove_sensitivity( const Tag& var ){
      for( auto& fr: fieldReqs_ ) fr.second->remove_sensitivity(var);
    }

    inline const TagSet& get_sensitivity() const{ return fieldReqs_.begin()->second->get_sensitivity(); }

    /** \brief (re)set the ghost information */
    inline void set_ghosts( const SpatialOps::GhostData ng ){
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->set_ghosts(ng);
      }
    }

    /** \brief (re)set the minimum number of ghosts required */
    inline void set_min_ghosts( const SpatialOps::GhostData ng ){
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->set_min_ghosts(ng);
      }
    }

    inline SpatialOps::GhostData get_min_ghosts() const{
      SpatialOps::GhostData ng(100);
      for( const typename FieldReqs::value_type& vt: fieldReqs_ ){
        ng = SpatialOps::min( ng, vt.second->get_ghosts() );
      }
      return ng;
    }

    inline SpatialOps::GhostData get_max_ghosts() const{
      SpatialOps::GhostData ng(0);
      for( const typename FieldReqs::value_type& vt: fieldReqs_ ){
        ng = SpatialOps::max( ng, vt.second->get_ghosts() );
      }
      return ng;
    }

    /**
     * @brief Create a FieldRequest on this FieldAggregator and return a handle
     *        to it. Should not generally be called from user code.
     * @param tag
     * @param ghosts
     */
    inline FieldReqPtr create_field_request( const Tag& tag,
                                             const SpatialOps::GhostData ghosts )
    {
      typename FieldReqs::iterator ireq = fieldReqs_.find(tag);
      if( ireq != fieldReqs_.end() ) return ireq->second;

      FieldReqPtr frp = FieldReqPtr( new FieldReq( tag, ghosts ) );
      fieldReqs_.insert( ireq, std::make_pair(tag,frp) );
      return frp;
    }

    inline FieldReqPtr get_field_request( const Tag& tag ){
      assert( fieldReqs_.find(tag) != fieldReqs_.end() );
      return fieldReqs_[tag];
    }

    /**
     * @brief Register all of the fields on this FieldAggregator. Generally
     *        shouldn't be called from user code.
     * @param fml the FieldManagerList to register fields on.
     */
    inline void register_fields( FieldManagerList& fml ){
      typename FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->register_field( fm );
      }
    }

    /**
     * \brief populate fields from the supplied field manager. This validates
     *        the FieldRequest and makes it valid for obtaining a field.
     * \param fm the FieldManager to obtain these fields from
     */
    inline void set_fields( typename FieldMgrSelector<FieldT>::type& fm ){
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->set_field( fm );
      }
    }

    inline void set_fields( FieldManagerList& fml ){
      typename FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();
      set_fields( fm );
    }

    inline void invalidate(){
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->invalidate();
      }
    }

    inline void release( FieldManagerList& fml ){
      typename FieldMgrSelector<FieldT>::type& fm = fml.field_manager<FieldT>();
      for( typename FieldReqs::value_type& vt: fieldReqs_ ){
        vt.second->release( fm );
      }
    }

    inline TagList get_tags() const{
      TagList tags;
      for( const typename FieldReqs::value_type& vt: fieldReqs_ ){
        tags.push_back( vt.first );
      }
      return tags;
    }

    inline void dump( std::ostream& os ) const{
      for( const typename FieldReqs::value_type& vt: fieldReqs_ ) os << *vt.second << std::endl;
    }

    inline bool has_field( const Tag& tag ) const{
      return ( fieldReqs_.find( tag ) != fieldReqs_.end() );
    }

  };

  //============================================================================

  typedef std::map< std::size_t, FieldAggregatorBase* > FieldAggregators;

  template< typename FieldT >
  inline FieldAggregatorBase& get_base_aggregator( FieldAggregators& fas ){
    static boost::hash<std::string> hash;
    const std::size_t id = hash( SpatialOps::type_name<FieldT>() );
    FieldAggregators::iterator ifa = fas.find(id);
    if( ifa == fas.end() ){
      ifa = fas.insert( ifa, std::make_pair( id, new FieldAggregator<FieldT>() ) );
    }
    return *ifa->second;
  }

  template< typename FieldT >
  inline FieldAggregator<FieldT>& get_aggregator( FieldAggregators& fas ){
    return dynamic_cast<FieldAggregator<FieldT>&>( get_base_aggregator<FieldT>(fas) );
  }

  inline std::ostream& operator << ( std::ostream& os, const FieldAggregatorBase& fa ){
    fa.dump(os);
    return os;
  }

  inline std::ostream& operator << ( std::ostream& os, const FieldAggregators& fas ){
    for( const FieldAggregators::value_type& vt: fas ){
      os << *vt.second << std::endl;
    }
    return os;
  }

} /* namespace Expr */


#endif /* FIELDREQUEST_H_ */
