/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   BlockImplicitBDFDualTimeIntegrator.h
 *  \date   May 9, 2016
 *  \author mike
 */

#ifndef BLOCKIMPLICITBDFDUALTIMEINTEGRATOR_H_
#define BLOCKIMPLICITBDFDUALTIMEINTEGRATOR_H_

#include <type_traits>

#include <boost/shared_ptr.hpp>

#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/MatVecOps.h>

#include <expression/dualtime/BDFDualTimeIntegrator.h>
#include <expression/matrix-assembly/DenseSubMatrix.h>
#include <expression/matrix-assembly/PermutationSubMatrix.h>
#include <expression/matrix-assembly/ScaledIdentityMatrix.h>
#include <expression/matrix-assembly/Compounds.h>

namespace Expr{
namespace DualTime{

  /*
   * @class CounterForIndices
   *
   * This class is used to count the number of indexing objects constructed
   * to provide unique indices. This allows us to assign a unique ID to each
   * IndexTagContainer as we construct it, which facilitates unique index assignment.
   *
   * Thanks to Scott Meyers for this implementation.
   */
  template<typename T>
  class CounterForIndices {
    static std::size_t indicesCount;
  public:
    CounterForIndices(){ ++indicesCount; }
    CounterForIndices( const CounterForIndices& ) { ++indicesCount; }
    ~CounterForIndices() { --indicesCount; }
    static std::size_t get_count() { return indicesCount; }
  };

  // initialize the index type count
  template<typename T>
  std::size_t CounterForIndices<T>::indicesCount = 0;

  /*
   * @class IndexTagContainer
   *
   * This class is for temporary objects used in index setup.
   * It associates an index and a tag, and has a 'mobility' description
   * and 'arrival' status. Mobility implies whether or not the index can
   * move (float) or was specified by the user (fixed). The arrival status
   * is pending until the IndexTagContainers are managed, after which they
   * were either set manually (by user) or automatically (to fit around those
   * indices specified by the user).
   */
  class IndexTagContainer
  {
  public:
    enum Mobility { FIXED, FLOAT };
    enum Arrival { MANUAL, AUTO, PENDING };

  protected:
    CounterForIndices<IndexTagContainer> c;
    static size_t get_count() { return CounterForIndices<IndexTagContainer>::get_count();}

    Expr::Tag tag_;
    int index_;
    Mobility mobility_;
    Arrival arrival_;

  public:
    /**
     * \brief construct an IndexTagContainer with a specified index
     * \param tag the tag
     * \param index the forced index
     * \param arrival the arrival status - defaults to PENDING
     */
    IndexTagContainer( const Expr::Tag tag,
                       const int index,
                       const Arrival arrival = PENDING )
    : tag_( tag ),
      index_( index ),
      mobility_( FIXED ),
      arrival_( arrival )
    {}

    /**
     * \brief construct an IndexTagContainer without an index
     * \param tag the tag
     *
     * This constructor sets mobility to FLOAT, enabling this index to be chosen
     * to fit around those set by the user.
     */
    IndexTagContainer( const Expr::Tag tag )
    : tag_( tag ),
      index_( get_count() ),
      mobility_( FLOAT ),
      arrival_( PENDING )
    {}

    /**
     * \brief obtain the tag
     */
    inline Expr::Tag get_tag() const { return tag_; }

    /**
     * \brief obtain the index
     */
    inline int get_index() const { return index_; }

    /**
     * \brief obtain the mobility description
     */
    inline Mobility get_mobility() const { return mobility_; }

    /**
     * \brief obtain the arrival status
     */
    inline Arrival get_arrival() const { return arrival_; }


    /**
     * \brief compare two IndexTagContainers based on index and mobility
     * \param si the compared IndexTagContainer
     *
     * This leads to a map/set being sorted by both index and mobility, enabling
     * two IndexTagContainers with the same index but different mobilities
     *
     */
    inline bool operator<( const IndexTagContainer& si ) const
    {
      if( si.get_mobility() == mobility_ ){ return si.get_index() < index_; }
      else{ return true; }
    }

    /**
     * \class IndexComparator
     *
     * Compare IndexTagContainers by their index only.
     */
    class IndexComparator {
    protected:
      const int comparedIndex_;
    public:
      /**
       * \brief construct an IndexComparator with the index to compare against
       * \param index the index to compare against
       */
      IndexComparator( const int index ) : comparedIndex_( index ) {}

      /**
       * \brief compare an IndexTagContainer
       * \param si the IndexTagContainer to compare
       */
      inline bool operator() ( const IndexTagContainer& si ) const { return si.get_index() == comparedIndex_; }
    };

    /**
     * \class IndexComparator
     *
     * Compare IndexTagContainers by their tag only.
     */
    class TagComparator {
    protected:
      const Tag& comparedTag_;
    public:
      /**
       * \brief construct a TagComparator with the tag to compare against
       * \param tag the tag to compare against
       */
      TagComparator( const Tag& tag ) : comparedTag_( tag ) {}

      /**
       * \brief compare an IndexTagContainer
       * \param si the IndexTagContainer to compare
       */
      inline bool operator() ( const IndexTagContainer& si ) const { return si.get_tag() == comparedTag_; }
    };

    /**
     * \class IndexAndMobilityComparator
     *
     * Compare IndexTagContainers by their index and mobility.
     */
    class IndexAndMobilityComparator {
    protected:
      const int comparedIndex_;
      const Mobility comparedMobility_;
    public:
      /**
       * \brief construct a TagComparator with the index and mobility to compare against
       * \param index the index to compare against
       * \param mobility the mobility to compare against
       */
      IndexAndMobilityComparator( const int index, const Mobility mobility )
      : comparedIndex_( index ),
        comparedMobility_( mobility )
      {}


      /**
       * \brief compare an IndexTagContainer
       * \param si the IndexTagContainer to compare
       */
      inline bool operator() ( const IndexTagContainer& si ) const
      {
        return si.get_index() == comparedIndex_ && si.get_mobility() == comparedIndex_;
      }
    };
  };


  /**
   *  \class BlockImplicitDualTimeUpdater
   *  \author Mike A. Hansen
   *  \date May, 2016
   *  \brief Performs the dual-time update for set of variables with full local implicitness
   */
  template< typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField >
  class BlockImplicitDualTimeUpdater : public Expr::Expression<FieldT>
  {
    const unsigned nvars_;
    const Expr::TagList& oldValueTags_;

    typedef Expr::matrix::AssemblerBase<FieldT> AssemblerBaseType;
    const boost::shared_ptr<AssemblerBaseType> lhs_;

    DECLARE_FIELDS( DualTimeStepFieldT, dualTimeStep_ )

    DECLARE_VECTOR_OF_FIELDS( FieldT, residuals_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, oldValues_ )

    BlockImplicitDualTimeUpdater( const unsigned nvars,
                                  const Expr::TagList& oldValueTags,
                                  const Expr::TagList& residualTags,
                                  const Expr::Tag& dualTimeStepTag,
                                  const Expr::Tag& physicalTimeStepTag,
                                  const boost::shared_ptr<AssemblerBaseType> lhs )
      : Expr::Expression<FieldT>(),
        nvars_( nvars ),
        oldValueTags_( oldValueTags ),
        lhs_( lhs )
    {
      this->set_gpu_runnable(true);

      this->template create_field_vector_request<FieldT>( oldValueTags, oldValues_ );
      this->template create_field_vector_request<FieldT>( residualTags, residuals_ );
      dualTimeStep_ = this->template create_field_request<DualTimeStepFieldT>( dualTimeStepTag );

      lhs_->create_all_field_requests( *this );
    }

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const unsigned order_;
      const unsigned nvars_;
      const Expr::TagList oldValueTags_, residualTags_;
      const Expr::Tag dualTimeStepTag_, physicalTimeStepTag_;

      typedef Expr::matrix::AssemblerBase<FieldT> AssemblerBaseT;
      const boost::shared_ptr<AssemblerBaseT> lhs_;

    public:
      /**
       *  @brief Build a DefaultName expression
       *  @param order the order of accuracy for the time integration scheme
       *  @param nvars the number of variables in the equation block
       *  @param resultTags the updated values for the solution variables
       *  @param oldValueTags the previous values for the solution variables
       *  @param residualTags the residual values
       *  @param dualTimeStepTag the value for \f$\Delta\sigma\f$
       *  @param physicalTimeStepTag the value for \f$\Delta t\f$
       *  @param lhs a matrix assembler object for the LHS matrix
       *  @param nghost the number of ghost cells to use when building expressions in this class.
       */
      Builder( const unsigned int order,
               const unsigned int nvars,
               const Expr::TagList& resultTags,
               const Expr::TagList& oldValueTags,
               const Expr::TagList& residualTags,
               const Expr::Tag& dualTimeStepTag,
               const Expr::Tag& physicalTimeStepTag,
               const boost::shared_ptr<AssemblerBaseT> lhs,
               const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTags, nghost ),
          order_          ( order           ),
          nvars_          ( nvars           ),
          oldValueTags_   ( oldValueTags    ),
          residualTags_   ( residualTags    ),
          dualTimeStepTag_( dualTimeStepTag ),
          physicalTimeStepTag_( physicalTimeStepTag ),
          lhs_            ( lhs )
      {}

      Expr::ExpressionBase* build() const{
        return new BlockImplicitDualTimeUpdater<FieldT,DualTimeStepFieldT>( nvars_,
                                                                            oldValueTags_,
                                                                            residualTags_,
                                                                            dualTimeStepTag_,
                                                                            physicalTimeStepTag_,
                                                                            lhs_ );
      }
    };

    ~BlockImplicitDualTimeUpdater(){}

    void evaluate()
    {
      using namespace SpatialOps;

      // obtain memory for the matrix and vector
      typename Expr::Expression<FieldT>::ValVec& newValues = this->get_value_vec();
      std::vector<SpatialOps::SpatFldPtr<FieldT> > resPtrs, lhsPtrs;
      for( auto i=0; i<nvars_; ++i ){
        resPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( *newValues[0] ) );
        for( auto j=0; j<nvars_; ++j )
          lhsPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( *newValues[0] ) );
      }
      SpatialOps::FieldVector<FieldT> resVector( resPtrs );
      SpatialOps::FieldMatrix<FieldT> lhsMatrix( lhsPtrs );

      // collect residuals
      for( auto i=0; i<nvars_; ++i )
        resVector(i) <<= residuals_[i]->field_ref();

      // build the lhs matrix
      for( unsigned rowIdx=0; rowIdx < nvars_; ++rowIdx ){
        SpatialOps::FieldVector<FieldT> row( lhsMatrix.row( rowIdx ) );
        lhs_->assemble( row, rowIdx, Expr::matrix::Place() );
      }

      // do linear solve and dispatch updates
      resVector = lhsMatrix.solve( resVector );
      for( auto i=0; i<nvars_; ++i )
        *newValues[i] <<= oldValues_[i]->field_ref() - dualTimeStep_->field_ref() * resVector(i);
    }
  };

  //============================================================================

  /**
   * \class BlockImplicitDualTimeIntegrator
   * \author Mike A. Hansen
   * \date December, 2015
   *
   * \brief Implements a BlockImplicitDualTimeIntegrator with block-implicit treatment
   */
  template<typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField>
  class BlockImplicitBDFDualTimeIntegrator : public BDFDualTimeIntegrator
  {
  protected:
    typedef Expr::matrix::AssemblerBase<FieldT> AssemblerBaseType;
    typedef Expr::matrix::ScaledIdentityMatrix<FieldT> ScaledIdentityMatrixType;
    typedef Expr::matrix::PermutationSubMatrix<FieldT> PermutationMatrixType;

    boost::shared_ptr<ScaledIdentityMatrixType> physicalTime_;
    boost::shared_ptr<ScaledIdentityMatrixType> dualTime_;
    boost::shared_ptr<PermutationMatrixType   > permutation_;
    boost::shared_ptr<AssemblerBaseType       > lhs_;

    bool useDefaultJacobian_ = true;

    bool indicesFinalized_ = false;
    std::vector<std::pair<Expr::matrix::OrdinalType,Expr::matrix::OrdinalType> > indexPairs_;


    /**
     * \brief tell the execution tree to compute the needed sensitivities
     *
     * The block-implicit integration method simply asks its left-hand side
     * matrix assembler for the sensitivities it needs, which is done through the
     * trigger_sensitivities( tree ) method.
     */
    virtual void set_sensitivities() override
    {
      lhs_->trigger_sensitivities( *execTree_ );
    }

    void finalize_index_set( std::set<IndexTagContainer>& indices )
    {
      // filter the immobiles into the final set
      std::set<IndexTagContainer> immobiles;
      for( auto it = indices.begin(); it != indices.end(); ){
        if( it->get_mobility() == IndexTagContainer::FIXED ){
          immobiles.insert( IndexTagContainer( it->get_tag(), it->get_index(), IndexTagContainer::MANUAL ) );
          it = indices.erase(it);
        }
        else{
          ++it;
        }
      }
      std::set<IndexTagContainer> finalIndices = immobiles;
      // get the max index to establish the range from 0->max
      int maxIdx = -1;
      for( const auto& i : immobiles ){
        maxIdx = std::max( maxIdx, i.get_index() );
      }
      // isolate the open indices in the 0->max range
      std::set<int> openIndices;
      for( auto i=0; i<maxIdx; ++i ){
        if( std::find_if( immobiles.begin(),
                          immobiles.end(),
                          IndexTagContainer::IndexComparator(i) )== immobiles.end() ){
          openIndices.insert( i );
        }
      }
      // fit the mobiles into the open indices and append if no open indices
      auto openIndicesIter = openIndices.begin();
      for( const auto& i : indices ){
        if( !openIndices.empty() ){
          finalIndices.insert( IndexTagContainer( i.get_tag(), *openIndicesIter, IndexTagContainer::AUTO ) );
          openIndicesIter = openIndices.erase(openIndicesIter);
        }
        else{
          ++maxIdx;
          finalIndices.insert( IndexTagContainer( i.get_tag(), maxIdx, IndexTagContainer::AUTO ) );
        }
      }
      indices.clear();
      indices = finalIndices;

      // throw error if index space is too big
      if( !immobiles.empty() && !openIndices.empty() ){
        std::cout << "\n\nERROR in index comprehension!\n";
        std::cout << "Highest immobile index (specified by user) = " << maxIdx << '\n';
        std::cout << "Open indices remaining: ";
        for( auto o : openIndices ){
          std::cout << o << " ";
        }
        std::cout << "\n\n";
        std::cout << "The incomplete index-tag collection is:\n";
        for( auto i : finalIndices ){
          std::cout << "(" << i.get_index() << ", " << i.get_tag().name() << ") ";
          if( i.get_arrival() == IndexTagContainer::MANUAL ){
            if( i.get_index() == maxIdx ){
              std::cout << "**** highest fixed index ****\n";
            }
            else{
              std::cout << "user-specified, fixed\n";
            }
          }
          else{
            std::cout << "float\n";
          }
        }
        std::cout << "\n\n";
        throw std::runtime_error( "You have not provided enough variables to fill out your index space" );
      }
    }

  public:
    /**
     * @brief Construct a BlockImplicitDualTimeIntegrator
     * @param patchID the patch id we are working on
     * @param name the name for this integrator
     * @param timeStepTag the physical time step size
     * @param dualTimeStepTag the dual time step size
     * @param order the order of accuracy for the time physical time discretization (dual-time is always first order)
     * @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.  See for example usage in Wasatch
     */
    BlockImplicitBDFDualTimeIntegrator( const int patchID,
                                        const std::string name,
                                        const Tag timeStepTag,
                                        const Tag dualTimeStepTag,
                                        const unsigned order=1,
                                        const Context ukContext=Expr::STATE_NONE )
  : BDFDualTimeIntegrator( patchID, name, timeStepTag, dualTimeStepTag, true, order, ukContext ),
    physicalTime_( boost::make_shared<ScaledIdentityMatrixType>() ),
    dualTime_    ( boost::make_shared<ScaledIdentityMatrixType>() ),
    permutation_ ( boost::make_shared<PermutationMatrixType   >() )
    {}

    BlockImplicitBDFDualTimeIntegrator( ExpressionTree* execTree,
                                        ExpressionFactory* execFactory,
                                        const int patchID,
                                        const std::string name,
                                        const Tag dtTag,
                                        const Tag dsTag,
                                        const Tag timestepTag,
                                        const unsigned order=1,
                                        const Context ukContext=Expr::STATE_NONE )
    : BDFDualTimeIntegrator( execTree, execFactory, patchID, name, dtTag, dsTag, timestepTag, true, order, ukContext ),
      physicalTime_( boost::make_shared<ScaledIdentityMatrixType>() ),
      dualTime_    ( boost::make_shared<ScaledIdentityMatrixType>() ),
      permutation_ ( boost::make_shared<PermutationMatrixType   >() )
    {}


    ~BlockImplicitBDFDualTimeIntegrator(){}

    /**
     * @brief build the updater for this equation block
     *
     * This builds the expression that computes updated solution variables for the set
     * of equations registered to this integrator. For the block-implicit method,
     * all equations in a 'block' - a collection of equations - are updated through
     * a single expression because they are coupled through the linear solve.
     */
    void build_updater_expressions() override
    {
      Expr::TagList rhsTags, residualTags, oldValueTags, newValueTags;
      size_t nvars = varSpecs_.size();
      newValueTags.resize( nvars );
      oldValueTags.resize( nvars );
      residualTags.resize( nvars );

      done_adding_variables();

      BOOST_FOREACH( const VarSpecPtr var, varSpecs_ ){
        const int varIdx = var->var_index();
        const int rhsIdx = var->rhs_index();
        const std::string& varName = var->name();
        newValueTags[varIdx] = Tag( varName              , STATE_NP1  );
        oldValueTags[varIdx] = Tag( varName              , ukContext_ );
        residualTags[rhsIdx] = Tag( varName + "_residual", STATE_NONE );
      }

      if( useDefaultJacobian_ ){
        use_default_jacobian();
      }

      if( !execFactory_->have_entry( newValueTags[0] ) ){
        typedef typename BlockImplicitDualTimeUpdater<FieldT,DualTimeStepFieldT>::Builder Updater;
        Updater* update = new Updater( bdfOrder_,
                                       nvars,
                                       newValueTags,
                                       oldValueTags,
                                       residualTags,
                                       dualTimeStepTag_,
                                       timeStepTag_,
                                       lhs_ );
        this->execRootIDs_.insert( execFactory_->register_expression( update, true ) );
      }
    }


    /**
     * @brief obtains a map from variable tag to variable index
     * @param context the context of all the variable tags [default STATE_NONE]
     */
    std::map<Tag,int> variable_tag_index_map( const Expr::Context context = Expr::STATE_NONE )
    {
      if( !indicesFinalized_ ){
        throw std::runtime_error( "Can't get variable tag to index map before calling done_adding_variables() on the integrator!" );
      }
      std::map<Tag,int> map;
      for( const auto& v : varSpecs_ ){
        map[Tag( v->name(), context )] = v->var_index();
      }
      return map;
    }

    /**
     * @brief obtains a map from rhs tag to rhs index
     */
    std::map<Tag,int> rhs_tag_index_map()
    {
      if( !indicesFinalized_ ){
        throw std::runtime_error( "Can't get rhs tag to index map before calling done_adding_variables() on the integrator!" );
      }
      std::map<Tag,int> map;
      for( const auto& v : varSpecs_ ){
        map[v->rhs_tag()] = v->rhs_index();
      }
      return map;
    }

    /**
     * @brief obtains a map from variable tag to rhs tag
     * @param context the context of all the variable tags [default STATE_NONE]
     */
    std::map<Tag,Tag> variable_rhs_tag_map( const Expr::Context context = Expr::STATE_NONE )
    {
      if( !indicesFinalized_ ){
        throw std::runtime_error( "Can't get var-rhs tag-tag map before calling done_adding_variables() on the integrator!" );
      }
      std::map<Tag,Tag> map;
      for( const auto& v : varSpecs_ ){
        map[Tag( v->name(), context )] = v->rhs_tag();
      }
      return map;
    }

    /**
     * @brief sets up indices for variables and right-hand sides.
     * Call this immediately after you are done adding variables to a time integrator.
     * This must be called before setting the Jacobian/Preconditioner.
     */
    void done_adding_variables()
    {
      doneAddingVariables_ = true;
      if( !indicesFinalized_ ){
        // make index tag sets
        std::set<IndexTagContainer> varIdxTags;
        std::set<IndexTagContainer> rhsIdxTags;
        for( const auto& v : varSpecs_ ){
          const int varIdx = v->var_index();
          const int rhsIdx = v->rhs_index();

          if( varIdx == -1 ){ varIdxTags.insert( IndexTagContainer( Tag( v->name(), ukContext_ ) ) ); }
          else              { varIdxTags.insert( IndexTagContainer( Tag( v->name(), ukContext_ ), varIdx ) ); }
          if( rhsIdx == -1 ){ rhsIdxTags.insert( IndexTagContainer( v->rhs_tag() ) ); }
          else              { rhsIdxTags.insert( IndexTagContainer( v->rhs_tag(), rhsIdx ) ); }
        }
        // finalize them
        finalize_index_set( varIdxTags );
        finalize_index_set( rhsIdxTags );
        // dispatch back to variable specifications
        for( auto v : varSpecs_ ){
          const Tag varTag = Tag( v->name(), ukContext_ );
          const Tag& rhsTag = v->rhs_tag();

          const auto varIdxContainer = std::find_if( varIdxTags.begin(),
                                                     varIdxTags.end(),
                                                     IndexTagContainer::TagComparator( varTag ) );

          const auto rhsIdxContainer = std::find_if( rhsIdxTags.begin(),
                                                     rhsIdxTags.end(),
                                                     IndexTagContainer::TagComparator( rhsTag ) );

          v->var_index() = varIdxContainer->get_index();
          v->rhs_index() = rhsIdxContainer->get_index();
          indexPairs_.push_back( std::make_pair( varIdxContainer->get_index(), rhsIdxContainer->get_index() ) );
        }
        indicesFinalized_ = true;
      }
    }

    /**
     * @brief tell this integrator to use RHS sensitivities to solution variables
     *
     * This means that the $i,j$ element of the Jacobian is \f$\partial R_i / \partial U_j\f$,
     * where \f$R_i\f$ is the $i$-th element of the right-hand side vector and \f$U_j\f$ is the
     * $j$-th element of the solution variables.
     */
    void use_default_jacobian()
    {
      if( !doneAddingVariables_ ){
        throw std::runtime_error( "Can't set Jacobian before calling done_adding_variables() on the integrator!" );
      }

      std::map<Expr::Tag,int> varIndices = variable_tag_index_map( ukContext_ );
      std::map<Expr::Tag,int> rhsIndices = rhs_tag_index_map();

      boost::shared_ptr<Expr::matrix::DenseSubMatrix<FieldT> > jacobian = boost::make_shared<Expr::matrix::DenseSubMatrix<FieldT> >();

      for( const auto& v : varIndices ){
        for( const auto& r : rhsIndices ){
          jacobian->template element<FieldT>(r.second, v.second) = Expr::matrix::sensitivity( r.first, v.first );
        }
      }
      jacobian->finalize();
      set_jacobian( jacobian );
    }

    /**
     * @brief set the matrix assembler that forms the Jacobian
     * @param jacobian the assembler that builds an approximation to the
     * RHS sensitivities to the solution variables
     */
    template< typename JacobianType >
    void set_jacobian( const boost::shared_ptr<JacobianType> jacobian )
    {
      if( !doneAddingVariables_ ){
        throw std::runtime_error( "Can't set Jacobian before calling done_adding_variables() on the integrator!" );
      }

      useDefaultJacobian_ = false;

      dualTime_->set_type( Expr::matrix::ScaledIdentityMatrix<FieldT>::SCALEMULTIPLY );
      dualTime_->template scale<DualTimeStepFieldT>() = dualTimeStepTag_;
      dualTime_->finalize();

      const Expr::DualTime::BDFCoefs bdfCoeffs( bdfOrder_ );
      physicalTime_->set_type( Expr::matrix::ScaledIdentityMatrix<FieldT>::SCALEDIVIDE );
      physicalTime_->template scale<SpatialOps::SingleValueField>() = timeStepTag_;
      physicalTime_->set_double_multiplier( bdfCoeffs(0) );
      physicalTime_->finalize();

      for( const auto& p : indexPairs_ ){
        permutation_->set_row_col( p.second, p.first );
      }
      permutation_->finalize();

      lhs_ = ( jacobian - physicalTime_ * permutation_ ) * dualTime_ - permutation_;
    }

    /**
     * @brief set the matrix assemblers for the Jacobian and preconditioner matrices
     * @param jacobian the assembler that builds an approximation to the
     * RHS sensitivities to the solution variables
     * @param preconditioner the assembler that forms the preconditioner
     */
    template< typename JacobianType, typename PreconditionerType >
    void set_jacobian_and_preconditioner( const boost::shared_ptr<JacobianType> jacobian,
                                          const boost::shared_ptr<PreconditionerType> preconditioner )
    {
      if( !doneAddingVariables_ ){
        throw std::runtime_error( "Can't set Jacobian before calling done_adding_variables() on the integrator!" );
      }

      useDefaultJacobian_ = false;

      dualTime_->set_type( Expr::matrix::ScaledIdentityMatrix<FieldT>::SCALEMULTIPLY );
      dualTime_->template scale<DualTimeStepFieldT>() = dualTimeStepTag_;
      dualTime_->finalize();

      const Expr::DualTime::BDFCoefs bdfCoeffs( bdfOrder_ );
      physicalTime_->set_type( Expr::matrix::ScaledIdentityMatrix<FieldT>::SCALEDIVIDE );
      physicalTime_->template scale<SpatialOps::SingleValueField>() = timeStepTag_;
      physicalTime_->set_double_multiplier( bdfCoeffs(0) );
      physicalTime_->finalize();

      for( const auto& p : indexPairs_ ){
        permutation_->set_row_col( p.first, p.second );
      }
      permutation_->finalize();

      lhs_ = ( jacobian - physicalTime_ * permutation_ ) * dualTime_ - preconditioner;
    }

    /**
     * @brief set the matrix assemblers for the Jacobian and preconditioner matrices
     * @param lhs the assembler that builds the entire left-hand side matrix of the dual time update
     */
    template< typename LhsType >
    void set_lhs( const boost::shared_ptr<LhsType> lhs )
    {
      if( !doneAddingVariables_ ){
        throw std::runtime_error( "Can't set LHS before calling done_adding_variables() on the integrator!" );
      }

      useDefaultJacobian_ = false;

      lhs_ = lhs;
    }


  };

  //============================================================================


} // namespace DualTime
} // namespace Expr



#endif /* BLOCKIMPLICITBDFDUALTIMEINTEGRATOR_H_ */
