/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   BDFDualTimeIntegrator.h
 *  \date   May 7, 2015
 *  \author James C. Sutherland, Mike A. Hansen
 */

#ifndef BDFDUALTIMEINTEGRATOR_H_
#define BDFDUALTIMEINTEGRATOR_H_

#include <expression/dualtime/BDFResidual.h>
#include <expression/ExpressionFactory.h>
#include <expression/ExpressionTree.h>

#include <limits>
#include <cmath>
#include <string>

namespace Expr{
namespace DualTime{
  
  //==============================================================================================

  /**
   *  \class DualTimeInitializer
   *  \brief Used to initialize solution for dualtime integration
   */
  template< typename FieldT >
  class DualTimeInitializer
   : public Expr::Expression<FieldT>
  {
    DECLARE_FIELD( FieldT, old_ )
    DualTimeInitializer( const Expr::Tag& oldTag ) : Expr::Expression<FieldT>()
    {
      old_ = this->template create_field_request<FieldT>( oldTag );
    }
  public:

    class Builder : public Expr::ExpressionBuilder{
      const Expr::Tag oldTag_;
    public:
      /**
       *  @brief Build a DualTimeInitializer expression
       *  @param resultTag the tag for the value that this expression computes
       *  @param oldTag the previous value for the solution variable
       *  @param nghost the number of ghost cells to use when building expressions in this class.
       */
      Builder( const Expr::Tag& resultTag,
               const Expr::Tag& oldTag,
               const SpatialOps::GhostData nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTag, nghost ),
          oldTag_( oldTag )
      {}

      Expr::ExpressionBase* build() const{
        return new DualTimeInitializer<FieldT>( oldTag_ );
      }
    };

    ~DualTimeInitializer(){}
    void evaluate(){
      this->value() <<= old_->field_ref();
    }
  }; // class DualTimeInitializer

  
  //==============================================================================================

  /**
   *  \class FieldNorm
   *  \brief Calculates \f$\frac{\Delta\phi}{\phi+\epsilon}\f$ to determine convergence in the dual time integrator.
   */
  template< typename FieldT >
  class FieldNorm
   : public Expr::Expression< SpatialOps::SingleValueField >
  {
    DECLARE_FIELDS( FieldT, var_, varNew_ )

    FieldNorm( const Expr::Tag& varTag,
               const Expr::Tag& varNewTag )
      : Expr::Expression<SpatialOps::SingleValueField>()
    {
      this->set_gpu_runnable(true);

      var_    = this->template create_field_request<FieldT>( varTag    );
      varNew_ = this->template create_field_request<FieldT>( varNewTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const Expr::Tag varTag_, varNewTag_;
    public:
      /**
       *  @brief Build a FieldNorm expression
       *  @param resultTag the tag for the value that this expression computes
       *  @param varTag the solution variable
       *  @param varNewTag the tag for the updated value of the solution variable
       *  @param nghost the number of ghost cells
       */
      Builder( const Expr::Tag& resultTag,
               const Expr::Tag& varTag,
               const Expr::Tag& varNewTag,
               const SpatialOps::GhostData nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTag, nghost ),
          varTag_( varTag ),
          varNewTag_( varNewTag )
      {}

      Expr::ExpressionBase* build() const{
        return new FieldNorm<FieldT>( varTag_, varNewTag_ );
      }
    };  /* end of Builder class */

    ~FieldNorm(){}

    void evaluate()
    {
      SpatialOps::SingleValueField& result = this->value();
      const FieldT& var    = var_   ->field_ref();
      const FieldT& varNew = varNew_->field_ref();
      const double eps = std::sqrt( std::numeric_limits<double>::epsilon() );
      result <<= SpatialOps::field_max_interior( abs(varNew-var) / ( abs(varNew) + eps ) );
    }
    void sensitivity( const Tag& var ){
      if( var == this->get_tag() )  this->sensitivity_result(var) <<= 1.0;
      else                          this->sensitivity_result(var) <<= 0.0;
    }
  }; // class FieldNorm

  //==============================================================================================
  
  /**
   *  \class ConvergenceMeasure
   *  \brief takes the maximum value across all quantities given
   */
  class ConvergenceMeasure
   : public Expr::Expression<SpatialOps::SingleValueField >
  {
    DECLARE_VECTOR_OF_FIELDS( SpatialOps::SingleValueField, vars_ )
    ConvergenceMeasure( const Expr::TagList& varTags )
      : Expr::Expression<SpatialOps::SingleValueField >()
    {
      this->create_field_vector_request<SpatialOps::SingleValueField>( varTags, vars_ );
    }

  public:

    class Builder : public Expr::ExpressionBuilder{
      const Expr::TagList varTags_;
    public:
      /**
       *  @brief Build a ConvergenceMeasure expression
       *  @param resultTag the tag for the value that this expression computes
       *  @param varTags the variables to be included in the convergence measure
       *  @param nghost the number of ghost cells to use in the calculation
       */
      Builder( const Expr::Tag& resultTag,
               const Expr::TagList& varTags,
               const SpatialOps::GhostData nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTag, nghost ),
          varTags_( varTags )
      {}

      Expr::ExpressionBase* build() const{ return new ConvergenceMeasure( varTags_ ); }
    };  /* end of Builder class */

    ~ConvergenceMeasure(){}

    void evaluate(){
      SpatialOps::SingleValueField& result = this->value();
      result <<= 0.0;
      BOOST_FOREACH( const boost::shared_ptr<const FieldRequest<SpatialOps::SingleValueField> > var, vars_ ){
        // jcs should replace this with some nebo function that can work for GPU as well...
        result <<= max(var->field_ref(), result);
      }
    }
    void sensitivity( const Tag& var ){
      if( var == this->get_tag() )  this->sensitivity_result(var) <<= 1.0;
      else                          this->sensitivity_result(var) <<= 0.0;
    }
  }; // class ConvergenceMeasure

  //==============================================================================================
  
  /**
   *  \class BDFDualTimeIntegrator
   *  \author James C. Sutherland, Mike Hansen
   *  \date Summer, 2015
   *
   *  # Formulation
   *  Given a set of PDEs,
   *  \f[
   *    \frac{\partial \mathbf{U}}{\partial t} = \mathbf{R}(\mathbf{U}),
   *  \f]
   *  we discretize implicitly in time to find
   *  \f[
   *    T(\mathbf{U}^{n+1}) = \mathbf{R}(\mathbf{U^{n+1}}),
   *  \f]
   *  where \f$\mathbf{T}(\mathbf{U})\f$ is the (BDF) discretization.
   *  Writing the PDEs in residual form, we have
   *  \f[
   *    \mathrm{H}(\mathrm{U}) = \mathrm{R}(\mathrm{U})-\mathrm{T}(\mathrm{U}).
   *  \f]
   *
   *  We solve this problem by casting it as a new PDE system in dual-time:
   *  \f[
   *    \frac{\partial \mathrm{U}}{\partial \sigma} = \mathrm{H}(\mathrm{U}).
   *  \f]
   *  Once we solve this system to steady state (\f$\sigma\to\infty\f$) we have the
   *  solution to the PDE system at time level \f$n+1\f$.
   *
   *  ## Preconditioning
   *  The above formulation can be augmented by a preconditioner to rewrite the system as
   *  \f[
   *    [P]^{-1} \frac{\partial \mathrm{U}}{\partial \sigma} = \mathrm{H}(\mathrm{U}),
   *  \f]
   *  which alters the trajectory of the system in \f$\sigma\f$ but reaches the
   *  same value for \f$\mathrm{U}^{n+1}\f$ as \f$\sigma\to\infty\f$.
   *
   *  Of course, if we do not precondition, then \f$[P]=[I]\f$.
   *
   *  ## Explicit Dual-Time Discretization
   *  Discretizing the dual-time system using Forward Euler, we obtain
   *  \f[
   *    [A^k] \frac{\mathbf{U}^{k+1}-\mathbf{U}^k}{\Delta\sigma} = \mathbf{H}(\mathbf{U}^k),
   *  \f]
   *  where \f$k\f$ is the dual-time index and
   *  \f[
   *    [A] = [P]^{-1} + \frac{\alpha_0 \Delta\sigma}{\Delta t}[I],
   *  \f]
   *  where \f$\alpha_0\f$ is the leading coefficient from the BDF discretization.
   *  Note that if we do not have preconditioning then
   *  \f[
   *     [A] = [I] + \frac{\alpha_0 \Delta\sigma}{\Delta t}[I],
   *  \f]
   *  which is trivially inverted.
   *
   *  ## Convergence in Dual-Time
   *  As \f$\sigma\to\infty\f$ we have \f$\mathbf{U}^k \to \mathbf{U}^{n+1}\f$.
   *  More specifically (or practically), as a measure of the dual-time residual
   *  approaches zero then \f$\mathbf{U}^k \to \mathbf{U}^{n+1}\f$.
   *
   *  Each DualTimeIntegrator instance is responsible for constructing \f$[A]\f$,
   *  including the construction of the preconditioner, and performing the update
   *  of the solution variables (which may involve solution of a linear system).
   *
   *  ## Variable Naming Convention
   *  A few notes on convention here:
   *
   *  Variable | Description  | State    | Comments
   *  :------- | :----------- | :------: | :--------
   *  \f$U^{n+1}\f$ | New value of solution variable | `-`          | Never explicitly stored
   *  \f$U^{n}  \f$ | Previous t solution variable   | `STATE_N`    | Updated in place at the end of a timestep
   *  \f$U^{k+1}\f$ | New dualtime value             | `STATE_NP1`  | Updated in place at the end of a dualtime step
   *  \f$U^{k}  \f$ | Previous dualtime value        | `STATE_NONE` | Updated in place at the end of a dualtime step
   *
   */
  class BDFDualTimeIntegrator
  {
  protected:

    ExpressionFactory *execFactory_, *initFactory_;
    ExpressionTree    *execTree_   , *initTree_;

    const int patchID_;

    const SpatialOps::GhostData nghost_;  // number of ghost cells to use on residual calculation expression

    const std::string name_;

    const unsigned bdfOrder_;

    const Tag timeStepTag_, stepCountTag_, dualTimeStepTag_, convergenceTag_;

    bool isReadyForIntegration_, timeStepExprSet_, dualTimeStepExprSet_;

    std::vector<VarSpecPtr> varSpecs_;

    const bool updateRequiresSensitivites_;
    
    std::set<ExpressionID>  execRootIDs_, initRootIDs_;
    FieldManagerList* fml_;

    TagList errorNormTags_;

    const bool externalFT_; // external factory and tree
    
    // context of the kth iterate in dualtime. Specifying the context allows other codes such as Wasatch to
    // to use the BDFDualTimeIntegrator and interact with Uintah's datawarehouse approach. If not specified,
    // this will default to STATE_NONE.
    const Expr::Context ukContext_;

    double tolerance_; // nonlinear solve tolerance

    bool doneAddingVariables_ = false; // whether more variables can be added to the integrator (false) or not (true)

    /**
     * All derived classes implement this.  It is called when prepare_for_integration() is called.
     * This should set up updater expressions, etc.
     * Updaters should be loaded on the execRootIDs_ set for subsequent graph construction.
     */
    virtual void build_updater_expressions() = 0;

    /*
     * This function defaults to computing right-hand side sensitivities with respect to the nonlinear iteration variables,
     * but this is not always desired, especially in block-implicit methods where assembly can be more complicated.
     */
    virtual void set_sensitivities(){
      Expr::TagList variableTags, rhsTags;
      BOOST_FOREACH( const VarSpecPtr& var, varSpecs_ ){
        variableTags.push_back( Expr::Tag( var->name(), ukContext_ ) );
        rhsTags.push_back( var->rhs_tag() );
      }
      execTree_->compute_sensitivities( rhsTags, variableTags );
    }

  public:

    /**
     * @brief Construct a DualTimeIntegrator. This specific constructor is used when the developer
     is managing their own ExpressionTree and ExpressionFactor. In addition, this assumes that the
     dt, ds, and timestep tags are also externally set and managed. Hence, with this constructor, 
     the user should NOT call end_time_step().
     * @param execTree    A pointer to an exgternally managed ExpressionTree
     * @param execFactory A pointer to an exgternally managed ExpressionFactory
     * @param patchID the patch id we are working on
     * @param name the name for this integrator
     * @param dtTag       ExpressionTag for the physical timestep size (dt). Registered and set externally.
     * @param dsTag       ExpressionTag for the dual timestep size (dsigma). Registered and set externally.
     * @param timestepTag ExpressionTag for the timestep (counter). Registered and set externally.
     * @param computeSensitivities Enable/disable calculation of sensitivities. Depends on the BDF order.
     * @param order the order of accuracy for the time physical time discretization (dual-time is always first order)
     * @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.
     */
    BDFDualTimeIntegrator( ExpressionTree* execTree,
                           ExpressionFactory* execFactory,
                           const int patchID,
                           const std::string name,
                           const Tag dtTag,
                           const Tag dsTag,
                           const Tag timestepTag,
                           const bool computeSensitivities,
                           const unsigned order=1,
                           const Expr::Context ukContext = STATE_NONE )
    : execFactory_    ( execFactory ),
      initFactory_    ( new ExpressionFactory(false) ),
      execTree_       ( execTree ),
      initTree_       ( new ExpressionTree( *initFactory_, patchID, name + "_initializer" ) ),
      patchID_        ( patchID ), // useless in this case - but we keep track of it
      nghost_         ( DEFAULT_NUMBER_OF_GHOSTS ),  // this should be set more intelligently
      name_           ( name     ),
      bdfOrder_       ( order    ),
      timeStepTag_    ( dtTag ),
      stepCountTag_   ( timestepTag ),
      dualTimeStepTag_( dsTag ),
      convergenceTag_ ( "convergence",    STATE_NONE ),
      isReadyForIntegration_( false ),
      timeStepExprSet_      ( false ),
      dualTimeStepExprSet_  ( false ),
      updateRequiresSensitivites_( computeSensitivities ),
      fml_( NULL ),
      externalFT_(true),
      ukContext_(ukContext),
      tolerance_(1e-10)
    {}

    
    /**
     * @brief Construct a DualTimeIntegrator
     * @param patchID the patch id we are working on
     * @param name the name for this integrator
     * @param timeStepTag the physical time step size
     * @param dualTimeStepTag the dual time step size
     * @param computeSensitivities Enable/disable calculation of sensitivities. Depends on the BDF order.
     * @param order the order of accuracy for the time physical time discretization (dual-time is always first order)
     * @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.  See for example usage in Wasatch
     */
    BDFDualTimeIntegrator( const int patchID,
                           const std::string name,
                           const Tag timeStepTag,
                           const Tag dualTimeStepTag,
                           const bool computeSensitivities,
                           const unsigned order=1,
                           const Expr::Context ukContext = STATE_NONE )
    : execFactory_    ( new ExpressionFactory(false) ),
      initFactory_    ( new ExpressionFactory(false) ),
      execTree_       ( new ExpressionTree( *execFactory_, patchID, name ) ),
      initTree_       ( new ExpressionTree( *initFactory_, patchID, name + "_initializer" ) ),
      patchID_        ( patchID  ),
      nghost_         ( DEFAULT_NUMBER_OF_GHOSTS ),
      name_           ( name     ),
      bdfOrder_       ( order    ),
      timeStepTag_    ( timeStepTag ),
      stepCountTag_   ( "timestep count", STATE_NONE ),
      dualTimeStepTag_( dualTimeStepTag ),
      convergenceTag_ ( "convergence",    STATE_NONE ),
      isReadyForIntegration_( false ),
      timeStepExprSet_      ( false ),
      dualTimeStepExprSet_  ( false ),
      updateRequiresSensitivites_( computeSensitivities ),
      fml_( NULL ),
      externalFT_(false),
      ukContext_(ukContext),
      tolerance_(1e-10)
    {
      execFactory_->register_expression( new PlaceHolder<SpatialOps::SingleValueField>::Builder( stepCountTag_ ) );
    }


    virtual ~BDFDualTimeIntegrator()
    {
      // if the factories and trees are locally managed, then delete them. Otherwise
      // the external caller is responsible for deleting them.
      if (!externalFT_) {
        delete execFactory_;
        if( execTree_ ) delete execTree_;
      }
      delete initFactory_;
      if( initTree_ ) delete initTree_;
    }

    //------------------------------------------------------------------

    template< typename T >
    void prepare_for_integration()
    {
      doneAddingVariables_ = true;

      ExpressionID convergenceID;
      if (execFactory_->have_entry(convergenceTag_)) {
        convergenceID = execFactory_->get_id(convergenceTag_);
      } else {
        convergenceID = execFactory_->register_expression(new ConvergenceMeasure::Builder( convergenceTag_, errorNormTags_, 0 ) );
      }
      
      execRootIDs_.insert( convergenceID );

      build_updater_expressions();
      
      if( execRootIDs_.empty() ){
        std::ostringstream msg;
        msg << std::endl << __FILE__ << " : " << __LINE__
        << "\nError.  DualTimeIntegrator has no roots to form a graph.\n"
        << "This is likely due to the derived class not properly registering updater expressions\n\n";
        throw std::runtime_error( msg.str() );
      }
      
      execTree_->insert_tree(execRootIDs_, true);
      initTree_->insert_tree(initRootIDs_, true);

      if( updateRequiresSensitivites_ ) set_sensitivities();

      isReadyForIntegration_ = true;
    }

    //------------------------------------------------------------------

    void set_fml(FieldManagerList& fml){
      fml_ = & fml;
    }

    //------------------------------------------------------------------

    template< typename T >
    void prepare_for_integration( FieldManagerList& fml, SpatialOps::OperatorDatabase& opdb, const T& alloc )
    {
      doneAddingVariables_ = true;

      assert( timeStepExprSet_     );
      assert( dualTimeStepExprSet_ );

      set_fml( fml );

      const ExpressionID convergenceID = execFactory_->register_expression(
          new ConvergenceMeasure::Builder( convergenceTag_, errorNormTags_, 0 ) );
      execRootIDs_.insert( convergenceID );

      build_updater_expressions();

      if( execRootIDs_.empty() ){
        std::ostringstream msg;
        msg << std::endl << __FILE__ << " : " << __LINE__
            << "\nError.  DualTimeIntegrator has no roots to form a graph.\n"
            << "This is likely due to the derived class not properly registering updater expressions\n\n";
        throw std::runtime_error( msg.str() );
      }

      execTree_->insert_tree( execRootIDs_, true );
      initTree_->insert_tree( initRootIDs_, true );

      BOOST_FOREACH( VarSpecPtr varSpec, varSpecs_ ){
        const TagList oldStatesTags = generate_old_states_taglist( varSpec->name() );
        varSpec->register_variable( fml, oldStatesTags, ukContext_ );
      }

      if( updateRequiresSensitivites_ ) set_sensitivities();

      execTree_->register_fields( fml );
      initTree_->register_fields( fml );

      fml.allocate_fields( alloc );

      execTree_->bind_fields( fml );
      initTree_->bind_fields( fml );

      execTree_->bind_operators( opdb );
      initTree_->bind_operators( opdb );

      SpatialOps::SingleValueField& stepCount = fml_->field_ref<SpatialOps::SingleValueField>( stepCountTag_ );
      stepCount <<= 0.0;

      isReadyForIntegration_ = true;
    }

    //------------------------------------------------------------------
    
    /**
     * @brief Add a variable to this dual-time integrator
     * @param varName the variable to add
     * @param rhsTag the RHS for the transport equation associated with this variable
     * @param varIndex the variable index, optional, default = -1
     * @param rhsIndex the variable's rhs index, optional, default = -1
     *
     * @tparam the field type associated with the variable
     *
     * If the index parameters are left unspecified, then the index is allowed to float,
     * accomplished by the default values of -1.
     */
    template<typename FieldT>
    void add_variable( const std::string varName,
                       const Tag& rhsTag,
                       const int varIndex = -1,
                       const int rhsIndex = -1 )
    {
      if( doneAddingVariables_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << "\n"
            << "Cannot add variables to this integrator!\n"
            << "You've already called prepare_for_integration(...) or\n"
            << "done_adding_variables(...) (block-implicit).";
        throw std::runtime_error( msg.str() );
      }

      assert( !isReadyForIntegration_ );

      // avoid duplicate entries.
      BOOST_FOREACH( const VarSpecPtr& var, varSpecs_ ){
        if( var->name() == varName ){
          std::ostringstream msg;
          msg << __FILE__ << " : " << __LINE__ << "\n"
              << "Error: tried to add a duplicate variable '" << varName << "' to the DualTime integrator\n";
          throw std::runtime_error( msg.str() );
        }
      }

      const Tag varStateN   ( varName              , STATE_N    ); // previous state, U^{n}
      const Tag varThisIter ( varName              , ukContext_ ); // current iteration, U^{n,k}
      const Tag varNextIter ( varName              , STATE_NP1  ); // next iteration, U^{n,k+1}
      const Tag varResidual ( varName + "_residual", STATE_NONE ); // residual expression
      const TagList oldStatesTags = generate_old_states_taglist( varName );

      VarSpecPtr varSpec = VarSpecPtr( new VariableSpecification<FieldT>( varName, rhsTag, varIndex, rhsIndex ) );
      varSpecs_.push_back( varSpec );

      typedef typename PlaceHolder<FieldT>::Builder Placeholder;

      if( !execFactory_->have_entry(varStateN) ){
        execFactory_->register_expression( new Placeholder( varStateN  , nghost_ ) );  // previous state, U^{n}
      }
      if( !execFactory_->have_entry(varThisIter) ){
        execFactory_->register_expression( new Placeholder( varThisIter, nghost_ ) );  // current iteration, U^{n,k}
      }

      const unsigned nBackSteps = bdfOrder_  - 1;
      for( unsigned i=0; i<nBackSteps; ++i ){
        if (!execFactory_->have_entry(oldStatesTags[i])) {
          execFactory_->register_expression( new Placeholder( oldStatesTags[i], nghost_ ) ); // recent state, U^{n-i}
        }
      }

      if( initFactory_ ){
        if( !initFactory_->have_entry(varStateN) ){
          initFactory_->register_expression( new Placeholder( varStateN, nghost_ ) );
        }
      }

      if( !execFactory_->have_entry(varResidual) ){
        // register the Residual calculation expression
        typedef typename BDFResidual< FieldT, SpatialOps::SingleValueField >::Builder Residual;
        execFactory_->register_expression( new Residual( varResidual,
                                                         timeStepTag_,
                                                         stepCountTag_,
                                                         varSpec->rhs_tag(),
                                                         varName,
                                                         bdfOrder_,
                                                         ukContext_,
                                                         nghost_ ) );
      }

      const Tag normTag = Tag( varName + "_err_norm", STATE_NONE );
      if( !execFactory_->have_entry(normTag) ){
        typedef typename FieldNorm<FieldT>::Builder Norm;
        execFactory_->register_expression( new Norm( normTag,
                                                     varThisIter,
                                                     varNextIter,
                                                     nghost_ ) );
        errorNormTags_.push_back( Tag( varName+"_err_norm", STATE_NONE ) );
      }

      if( initFactory_ ){
        initRootIDs_.insert( initFactory_->register_expression(new typename DualTimeInitializer<FieldT>::Builder( varThisIter, varStateN, nghost_ ) ) );
      }
    }

    //------------------------------------------------------------------
    
    /**
     * @brief set the nonlinear solution tolerance
     * @param tolerance
     */
    inline void set_tolerance( const double tolerance )
    {
      tolerance_ = tolerance;
    }

    //------------------------------------------------------------------

    /**
     * @brief get the list of tags referring to 'other' states for a particular variable
     * @param varName the name of the variable
     *
     * 'Other' states means anything but N, NONE, and NP1. In a BDF method these are the
     * old values in physical time, N-1, N-2, etc.
     */
    Expr::TagList generate_old_states_taglist( const std::string varName )
    {
      TagList stateTags;
      const unsigned nBackSteps = bdfOrder_ - 1;
      for( unsigned i=1; i<=nBackSteps; ++i ){
        stateTags.push_back( Tag( varName + "_n-" + boost::lexical_cast<std::string>( i ) , ukContext_ ) );
      }
      return stateTags;
    }


    /**
     * @brief advance the solution forward in dual time
     * @param converged - this boolean will be set to true/false if convergence is obtained/not
     */
    inline virtual void advance_dualtime( bool& converged )
    {
      execTree_->execute_tree();

      // swap current and old iteration
      BOOST_FOREACH( VarSpecPtr var, varSpecs_ ){
        var->swap_states( *fml_, STATE_NP1, ukContext_ );
      }

      // check convergence
      converged = nebo_max(fml_->field_ref<SpatialOps::SingleValueField>( convergenceTag_ )) < tolerance_;
    }

    //------------------------------------------------------------------
    
    /**
     * @brief increment the physical time step counter used by the BDFResidual
     */
    inline void increment_step_count()
    {
      double& stepCount = fml_->field_ref<SpatialOps::SingleValueField>( stepCountTag_ )[0];
      ++stepCount;
    }

    //------------------------------------------------------------------
    
    /**
     * @brief swap states after finishing a physical time step
     */
    inline void swap_states_after_time_step()
    {
      BOOST_FOREACH( VarSpecPtr var, varSpecs_ ){
        const TagList oldStatesTags = generate_old_states_taglist( var->name() );
        var->swap_states( *fml_, oldStatesTags, ukContext_ );
      }
    }

    //------------------------------------------------------------------
    
    /**
     * @brief swap states after finishing a dual time step
     */
    inline void swap_states_after_dual_time_step()
    {
      // swap current and old iteration
      BOOST_FOREACH( VarSpecPtr var, varSpecs_ ){
        var->swap_states( *fml_, STATE_NP1, ukContext_ );
      }
    }

    //------------------------------------------------------------------
    
    /**
     * @brief prepare for a time step
     */
    inline void begin_time_step()
    {
      if( !isReadyForIntegration_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ << "\nYou must call prepare_for_integration() prior to time_advance()\n";
        throw std::runtime_error( msg.str() );
      }

      initTree_->execute_tree();
    }
    
    //------------------------------------------------------------------
    
    /**
     * @brief finish a time step
     */
    inline void end_time_step()
    {
      increment_step_count();
      swap_states_after_time_step();
    }
    
    //------------------------------------------------------------------

    /**
     * @brief set the physical time step expression
     */
    inline void set_physical_time_step_expression( ExpressionBuilder* dtExpr )
    {
      assert( !isReadyForIntegration_ );
      const ExpressionID dtID = execFactory_->register_expression( dtExpr );
      execRootIDs_.insert( dtID );
      timeStepExprSet_ = true;
    }
    
    //------------------------------------------------------------------

    /**
     * @brief set the dual time step expression
     */
    inline void set_dual_time_step_expression( ExpressionBuilder* dsExpr )
    {
      assert( !isReadyForIntegration_ );
      const ExpressionID dsID = execFactory_->register_expression( dsExpr );
      execRootIDs_.insert( dsID );
      dualTimeStepExprSet_ = true;
    }
    
    //------------------------------------------------------------------

    /**
     * @brief set the dual time step expression
     */
    inline void set_dual_time_step_expression( const Expr::ExpressionID dsID )
    {
      assert( !isReadyForIntegration_ );
      execRootIDs_.insert( dsID );
      dualTimeStepExprSet_ = true;
    }
    
    //------------------------------------------------------------------

    /**
     * @brief register an extra root expression
     * @param builder the expression builder
     */
    inline void register_root_expression( Expr::ExpressionBuilder* builder )
    {
      if( isReadyForIntegration_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ <<
            "\nYou must call register_root_expression() prior to prepare_for_integration()\n";
        throw std::runtime_error( msg.str() );
      }
      execTree_->insert_tree( execFactory_->register_expression( builder ), false );
    }

    //------------------------------------------------------------------

    /**
     * @brief lock a particular field
     * @param tag the tag of the field to be locked
     */
    template< typename VariableFieldType >
    inline void lock_field( const Expr::Tag& tag )
    {
      if( !isReadyForIntegration_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ <<
            "\nYou must call prepare_for_integration() prior to lock_field()\n";
        throw std::runtime_error( msg.str() );
      }
      typename Expr::FieldMgrSelector<VariableFieldType>::type& fm = fml_->field_manager<VariableFieldType>();
      fm.lock_field( tag );
    }

    //------------------------------------------------------------------

    /**
     * @brief lock all fields in the execution graph
     */

    inline void lock_all_execution_fields()
    {
      if( !isReadyForIntegration_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ <<
            "\nYou must call prepare_for_integration() prior to lock_all_execution_fields()\n";
        throw std::runtime_error( msg.str() );
      }
      execTree_->lock_fields( *fml_ );
    }

    //------------------------------------------------------------------

    /**
     * @brief copy a field set on STATE_N as an initial condition to the iteration state
     * @param name the name of the field tag
     *
     * This is to be called before the integration begins and after prepare_for_integration().
     * It is not called on each time step.
     *
     * Note: the iteration context is typically Expr::STATE_NONE, or the ukContext_ variable.
     */
    template< typename VariableFieldType >
    inline void copy_from_initial_condition_to_execution( const std::string name )
    {
      if( !isReadyForIntegration_ ){
        std::ostringstream msg;
        msg << __FILE__ << " : " << __LINE__ <<
            "\nYou must call prepare_for_integration() prior to copy_from_initial_condition_to_execution()\n";
        throw std::runtime_error( msg.str() );
      }
      fml_->field_ref<VariableFieldType>( Expr::Tag( name, ukContext_ ) )
          <<= fml_->field_ref<VariableFieldType>( Expr::Tag( name, Expr::STATE_N ) );
    }

    //------------------------------------------------------------------

    /**
     * This will result in timings for each node in the graph being reported to
     * a file.  The file is named using the name provided when the TimeStepper
     * is constructed, appended with '_timings.log'.
     *
     * @param setting [default true] activate/deactivate timings.
     */
    inline void request_timings( const bool setting=true ){ execTree_->request_timings(setting); }

    inline ExpressionFactory& factory(){ return *execFactory_; }

    inline const ExpressionTree& get_init_tree() const{ return *initTree_; }

    inline const ExpressionTree& get_tree() const{ return *execTree_; }

    inline ExpressionTree& get_tree() { return *execTree_; }

    inline const Tag& get_time_step_tag()      const{ return timeStepTag_; }
    inline const Tag& get_dual_time_step_tag() const{ return dualTimeStepTag_; }
  };

}
}
#endif /* BDFDUALTIMEINTEGRATOR_H_ */
