/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   BDFCoefs.h
 *  \date   Jun 9, 2015
 *  \author "James C. Sutherland"
 */

#ifndef BDFCOEFS_H_
#define BDFCOEFS_H_

#include <vector>

namespace Expr{
namespace DualTime{

  /**
   * \class BDFCoefs
   * \author James C. Sutherland
   * \date June, 2015
   * \brief Coefficients for backward-difference formulas
   *
   * \f[
   * \left( \frac{\partial \phi}{\partial t} \right)^{n+1}
   * \approx
   * \frac{1}{\Delta t}\left(\alpha_0\phi^{n+1}-\sum_{i=1}^{N_{\mathrm{BDF}}}\alpha_i \phi^{n+1-i}\right).
   * \f]
   */
  class BDFCoefs
  {
    const unsigned order_;
    std::vector<double> bdfCoef_;

  public:

    BDFCoefs( const unsigned int order ) : order_( order )
    {
      // populate BDF coefficients
      bdfCoef_.resize( order+1 );
      switch( order ){
        case 1:
          bdfCoef_[0] = 1.0;
          bdfCoef_[1] = 1.0;
          break;

        case 2:
          bdfCoef_[0] = 1.5;
          bdfCoef_[1] = 2.0;
          bdfCoef_[2] = -0.5;
          break;

        case 3:
          bdfCoef_[0] = 11.0/6.0;
          bdfCoef_[1] = 3.0;
          bdfCoef_[2] = -1.5;
          bdfCoef_[3] = 1.0/3.0;
          break;

        case 4:
          bdfCoef_[0] = 25.0/12.0;
          bdfCoef_[1] = 4.0;
          bdfCoef_[2] = -3.0;
          bdfCoef_[3] = 4.0/3.0;
          bdfCoef_[4] = -0.25;
          break;

        case 5:
          bdfCoef_[0] = 137.0/60.0;
          bdfCoef_[1] = 5.0;
          bdfCoef_[2] = -5.0;
          bdfCoef_[3] = 10.0/3.0;
          bdfCoef_[4] = -1.25;
          bdfCoef_[5] = 0.2;
          break;

        case 6:
          bdfCoef_[0] = 2.45;
          bdfCoef_[1] = 6.0;
          bdfCoef_[2] = -7.5;
          bdfCoef_[3] = 20.0/3.0;
          bdfCoef_[4] = -3.75;
          bdfCoef_[5] = 1.2;
          bdfCoef_[6] = -1.0/6.0;
          break;
        default:
          throw std::invalid_argument( "Unsupported BDF order" );
      }
    }

    /**
     * @param i the coefficient index (see formula in class description)
     * @return the value of the coefficient
     */
    inline double operator()( const unsigned i ) const{
      assert( i<=order_ );
      return bdfCoef_[i];
    }

    /**
     * @return the vector of BDF coefficients
     */
    inline const std::vector<double>& coefs() const{ return bdfCoef_; }

    /**
     * \return the order for this BDFCoefs object
     */
    inline unsigned order() const{ return order_; }
  };


} // namespace DualTime
} // namespace Expr


#endif /* BDFCOEFS_H_ */
