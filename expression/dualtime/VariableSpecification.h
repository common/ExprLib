/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   VariableSpecification.h
 *  \date   May 7, 2015
 *  \author "James C. Sutherland"
 */

#ifndef VARIABLESPECIFICATION_H
#define VARIABLESPECIFICATION_H

#include <limits>
#include <string>

#include <expression/ExprLib_Configure.h>
#include <expression/Tag.h>
#include <expression/FieldManagerList.h>

#include <iostream>

#include <boost/shared_ptr.hpp>

namespace Expr{
namespace DualTime{

  class VariableSpecificationBase{
  protected:
    const std::string name_;
    const Tag rhsTag_;

    int varIndex_;
    int rhsIndex_;

  public:
    VariableSpecificationBase( const std::string varName,
                               const Tag& rhsTag,
                               const int varIndex,
                               const int rhsIndex )
      : name_  ( varName ),
        rhsTag_( rhsTag  ),
        varIndex_( varIndex ),
        rhsIndex_( rhsIndex )
    {}

    virtual ~VariableSpecificationBase(){}

    /**
     * @brief register a variable to a field manager list and lock necessary fields
     * @param fml the field manager list
     * @param otherStateTags tags other than N, NONE, and NP1 that need to be locked
     * @param ukContext the context used for the nonlinear solver iterate, typically STATE_NONE
     *
     * The otherStateTags parameter might be the old values of a BDF integrator or
     * the stage derivatives of a DIRK integrator.
     */
    virtual void register_variable( FieldManagerList& fml,
                                           const TagList& otherStateTags,
                                           const Expr::Context ukContext ) = 0;

    /**
     * @brief swap old states
     * @param fml the field manager list
     * @param otherStateTags tags other than N, NONE, and NP1 that need to be locked
     * @param ukContext the context used for the nonlinear solver iterate, typically STATE_NONE
     *
     * The otherStateTags parameter might be the old values of a BDF integrator or
     * the stage derivatives of a DIRK integrator.
     */
    virtual void swap_states( FieldManagerList& fml,
                              const TagList& otherStateTags,
                              const Expr::Context ukContext ) = 0;

    /**
     * @brief swaps variables with the two supplied states.
     * @param fml field manager list
     * @param state1 first context to be swapped
     * @param state2 second context to be swapped
     */
    virtual void swap_states( FieldManagerList& fml,
                              const Context state1,
                              const Context state2 ) = 0;

    inline const std::string& name() const{ return name_; }

    inline const Tag& rhs_tag() const{ return rhsTag_; }

    inline int& var_index() { return varIndex_; }
    inline int& rhs_index() { return rhsIndex_; }

    inline bool operator==( const VariableSpecificationBase& other ){ return name_ == other.name_; }
  };


  template<typename T>
  class VariableSpecification : public VariableSpecificationBase
  {
  public:
    /**
     * @brief construct a variable specification
     * @param varName the name of the variable
     * @param rhsTag the tag of the equation's right-hand side
     * @param varIndex the specified index of the variable
     * @param rhsIndex the specified index of the variable's rhs
     */
    VariableSpecification( const std::string varName,
                           const Tag& rhsTag,
                           const int varIndex,
                           const int rhsIndex )
      : VariableSpecificationBase( varName, rhsTag, varIndex, rhsIndex )
    {}


    void register_variable( FieldManagerList& fml,
                            const TagList& otherStateTags,
                            const Expr::Context ukContext ) override
    {
      typename FieldMgrSelector<T>::type& fm = fml.field_manager<T>();

      const Tag varN   ( name_, Expr::STATE_N    );
      const Tag varNp1 ( name_, Expr::STATE_NP1  );
      const Tag varNone( name_, Expr::STATE_NONE );

      fm.register_field( varN,    SpatialOps::GhostData(DEFAULT_NUMBER_OF_GHOSTS) );
      fm.register_field( varNp1,  SpatialOps::GhostData(DEFAULT_NUMBER_OF_GHOSTS) );
      fm.register_field( varNone, SpatialOps::GhostData(DEFAULT_NUMBER_OF_GHOSTS) );

      fm.lock_field( varN    );
      fm.lock_field( varNp1  );
      fm.lock_field( varNone );
      for( const auto& v : otherStateTags ){
        fm.lock_field( v );
      }
    }

    void swap_states( FieldManagerList& fml,
                      const TagList& otherStateTags,
                      const Expr::Context ukContext ) override
    {
      const Tag varN   ( name_,STATE_N    );
      const Tag varNone( name_,STATE_NONE );

      const unsigned nBackSteps = otherStateTags.size();

      typename FieldMgrSelector<T>::type& fm = fml.field_manager<T>();

      if( nBackSteps > 0 ){
        if( nBackSteps > 1 ){
          // handle swapping of N-p, N-p+1, ..., N-2, N-1
          for( unsigned i=1; i<nBackSteps; ++i ){
            T& uOld   = fm.field_ref( otherStateTags[nBackSteps-i-1] );
            T& uOlder = fm.field_ref( otherStateTags[nBackSteps-i] );
            // jcs this assumes that BCs have been set as appropriate.
            uOld  .reset_valid_ghosts( uOld  .get_ghost_data() );
            uOlder.reset_valid_ghosts( uOlder.get_ghost_data() );
            uOlder.swap( uOld );
          }
        }
        // handle swapping of N-1 and N
        T& uNm1 = fm.field_ref( otherStateTags[0] );
        T& uN   = fm.field_ref( varN );
        // jcs this assumes that BCs have been set as appropriate.
        uNm1.reset_valid_ghosts( uNm1.get_ghost_data() );
        uN  .reset_valid_ghosts( uN.get_ghost_data() );
        uN.swap( uNm1 );
      }
      // all BDF methods
      // handle swapping of N and none
      T& uN    = fm.field_ref ( varN );
      T& uNone = fm.field_ref( varNone );
      // jcs this assumes that BCs have been set as appropriate.
      uN   .reset_valid_ghosts( uN   .get_ghost_data() );
      uNone.reset_valid_ghosts( uNone.get_ghost_data() );
      uN.swap( uNone );
    }

    inline void swap_states( FieldManagerList& fml,
                             const Context state1,
                             const Context state2 ) override
    {
      typename FieldMgrSelector<T>::type& fm = fml.field_manager<T>();
      T& f1 = fm.field_ref( Tag(name_,state1) );
      T& f2 = fm.field_ref( Tag(name_,state2) );
      // jcs this assumes that BCs have been set as appropriate.
      f1.reset_valid_ghosts( f1.get_ghost_data() );
      f2.reset_valid_ghosts( f2.get_ghost_data() );
      f1.swap( f2 );
    }
  };

  typedef boost::shared_ptr<VariableSpecificationBase> VarSpecPtr;


} // namespace DualTime
} // namespace Expr


#endif // VARIABLESPECIFICATION_H
