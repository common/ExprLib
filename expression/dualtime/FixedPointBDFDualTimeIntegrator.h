/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   FixedPointBDFDualTimeIntegrator.h
 *  \date   Jun 8, 2015
 *  \author "James C. Sutherland"
 */

#ifndef FIXEDPOINTBDFDUALTIMEINTEGRATOR_H_
#define FIXEDPOINTBDFDUALTIMEINTEGRATOR_H_

#include <expression/dualtime/BDFDualTimeIntegrator.h>

namespace Expr{
namespace DualTime{

  //============================================================================

  /**
   *  \class FixedPointDualTimeUpdater
   *  \author James C. Sutherland
   *  \date June, 2015
   *  \brief Performs the dual-time update for a variable without preconditioning
   *         using an explicit dual-time update.
   */
  template< typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField >
  class FixedPointDualTimeUpdater : public Expr::Expression<FieldT>
  {
    DECLARE_FIELDS( FieldT, residual_, oldValue_ )
    DECLARE_FIELDS( DualTimeStepFieldT, dualTimeStep_ )
    DECLARE_FIELD( SpatialOps::SingleValueField, timeStep_ )

    FixedPointDualTimeUpdater( const unsigned order,
                               const Expr::Tag& oldValueTag,
                               const Expr::Tag& residualTag,
                               const Expr::Tag& dualTimeStepTag,
                               const Expr::Tag& timeStepTag )
      : Expr::Expression<FieldT>()
    {
      this->set_gpu_runnable(true);

      residual_ = this->template create_field_request<FieldT>( residualTag );
      oldValue_ = this->template create_field_request<FieldT>( oldValueTag );

      dualTimeStep_ = this->template create_field_request<DualTimeStepFieldT>( dualTimeStepTag );
      timeStep_ = this->template create_field_request<SpatialOps::SingleValueField>( timeStepTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const unsigned order_;
      const Expr::Tag oldValueTag_, residualTag_, dualTimeStepTag_, timeStepTag_;
    public:
      /**
       *  @brief Build a DefaultName expression
       *  @param order the order of accuracy for the time integration scheme
       *  @param resultTag the updated value for the solution variable
       *  @param oldValueTag the previous value for the solution variable
       *  @param residualTag the residual value
       *  @param dualTimeStepTag the value for \f$\Delta\sigma\f$
       *  @param timeStepTag the value for \f$\Delta t\f$.
       *  @param nghost the number of ghost cells to use when building expressions in this class.
       */
      Builder( const unsigned int order,
               const Expr::Tag& resultTag,
               const Expr::Tag& oldValueTag,
               const Expr::Tag& residualTag,
               const Expr::Tag& dualTimeStepTag,
               const Expr::Tag& timeStepTag,
               const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTag, nghost ),
          order_          ( order           ),
          oldValueTag_    ( oldValueTag     ),
          residualTag_    ( residualTag     ),
          dualTimeStepTag_( dualTimeStepTag ),
          timeStepTag_    ( timeStepTag     )
      {}

      Expr::ExpressionBase* build() const{
        return new FixedPointDualTimeUpdater<FieldT,DualTimeStepFieldT>( order_, oldValueTag_, residualTag_, dualTimeStepTag_, timeStepTag_ );
      }
    };

    ~FixedPointDualTimeUpdater(){}

    void evaluate()
    {
      using namespace SpatialOps;

      FieldT& result = this->value();

      const FieldT&             phik = oldValue_    ->field_ref();
      const FieldT&             res  = residual_    ->field_ref();
      const DualTimeStepFieldT& dsig = dualTimeStep_->field_ref();
      const SpatialOps::SingleValueField& dt = timeStep_->field_ref();

      result <<= phik + dsig * res / ( 1 + dsig / dt );
//       result <<= phik + dsig * res;
    }
  };

  //============================================================================

  /**
   * \class FixedPointDualTimeIntegrator
   * \author James C. Sutherland
   * \date June, 2015
   *
   * \brief Implements a FixedPointDualTimeIntegrator with explicit treatment and no preconditioning.
   */
  template<typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField>
  class FixedPointBDFDualTimeIntegrator : public BDFDualTimeIntegrator
  {
    const Expr::Tag dtTag_;
  public:
    /**
     * @brief Construct a FixedPointDualTimeIntegrator
     * @param patchID the patch id we are working on
     * @param name the name for this integrator
     * @param timeStepTag the physical time step size
     * @param dualTimeStepTag the dual time step size
     * @param order the order of accuracy for the time physical time discretization (dual-time is always first order)
     * @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.  See for example usage in Wasatch
     */
    FixedPointBDFDualTimeIntegrator( const int patchID,
                                     const std::string name,
                                     const Tag timeStepTag,
                                     const Tag dualTimeStepTag,
                                     const unsigned order=1,
                                     const Context ukContext=Expr::STATE_NONE)
  : BDFDualTimeIntegrator( patchID, name, timeStepTag, dualTimeStepTag, false, order, ukContext ),
    dtTag_( timeStepTag )
    {}
    
    FixedPointBDFDualTimeIntegrator( ExpressionTree* execTree,
                                     ExpressionFactory* execFactory,
                                     const int patchID,
                                     const std::string name,
                                     const Tag dtTag,       // physical timestep size
                                     const Tag dsTag,       // dual timestep size
                                     const Tag timestepTag, // timestep counter
                                     const unsigned order=1,
                                     const Context ukContext=Expr::STATE_NONE)
    : BDFDualTimeIntegrator( execTree, execFactory, patchID, name, dtTag, dsTag, timestepTag, false, order, ukContext ),
      dtTag_( dtTag )
    {}


    ~FixedPointBDFDualTimeIntegrator(){}

    void build_updater_expressions()
    {
      typedef typename FixedPointDualTimeUpdater<FieldT,DualTimeStepFieldT>::Builder Updater;
      BOOST_FOREACH( const VarSpecPtr var, varSpecs_ ){
        const Expr::Tag updaterTag( var->name(), STATE_NP1 );
        if( !execFactory_->have_entry(updaterTag) ){
          Updater* update = new Updater( bdfOrder_,
                                        updaterTag,
                                        Tag( var->name(),             ukContext_ ),
                                        Tag( var->name()+"_residual", STATE_NONE    ),
                                        dualTimeStepTag_,
                                        dtTag_ );
          const Expr::ExpressionID np1ID = execFactory_->register_expression( update );
          this->execRootIDs_.insert( np1ID );
        }
      }
    }
  };

  //============================================================================

} // namespace DualTime
} // namespace Expr


#endif /* FIXEDPOINTBDFDUALTIMEINTEGRATOR_H_ */
