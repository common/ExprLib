/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   VariableImplicitBDFDualTimeIntegrator.h
 *  \date   Dec 17, 2015
 *  \author mike
 */

#ifndef VARIABLEIMPLICITBDFDUALTIMEINTEGRATOR_H_
#define VARIABLEIMPLICITBDFDUALTIMEINTEGRATOR_H_

#include <expression/dualtime/BDFDualTimeIntegrator.h>


namespace Expr{
namespace DualTime{

  //============================================================================

  /**
   *  \class VariableImplicitDualTimeUpdater
   *  \author Mike A. Hansen
   *  \date December, 2015
   *  \brief Performs the dual-time update for a variable without preconditioning
   *         with implicitness only with respect to itself.
   */
  template< typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField >
  class VariableImplicitDualTimeUpdater : public Expr::Expression<FieldT>
  {
    const Expr::Tag& oldValueTag_;

    DECLARE_FIELDS( FieldT, rhs_, residual_, oldValue_ )
    DECLARE_FIELDS( SpatialOps::SingleValueField, dualTimeStep_, timeStep_ )

    const double leadingBDFCoefficient_;

    VariableImplicitDualTimeUpdater( const unsigned order,
                                     const Expr::Tag& oldValueTag,
                                     const Expr::Tag& residualTag,
                                     const Expr::Tag& rhsTag,
                                     const Expr::Tag& dualTimeStepTag,
                                     const Expr::Tag& timeStepTag,
                                     const double leadingBDFCoefficient )
      : Expr::Expression<FieldT>(),
        oldValueTag_( oldValueTag ),
        leadingBDFCoefficient_( leadingBDFCoefficient )
    {
      this->set_gpu_runnable(true);

      rhs_      = this->template create_field_request<FieldT>( rhsTag      );
      residual_ = this->template create_field_request<FieldT>( residualTag );
      oldValue_ = this->template create_field_request<FieldT>( oldValueTag );

      const_cast< FieldRequest<FieldT>& >(*rhs_).add_sensitivity( oldValueTag_ );

      dualTimeStep_ = this->template create_field_request<SpatialOps::SingleValueField>( dualTimeStepTag );
      timeStep_     = this->template create_field_request<SpatialOps::SingleValueField>(     timeStepTag );
    }

  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const unsigned order_;
      const Expr::Tag oldValueTag_, residualTag_, rhsTag_, dualTimeStepTag_, timeStepTag_;
      const double leadingBDFCoefficient_;
    public:
      /**
       *  @brief Build a DefaultName expression
       *  @param order the order of accuracy for the time integration scheme
       *  @param resultTag the updated value for the solution variable
       *  @param oldValueTag the previous value for the solution variable
       *  @param residualTag the residual value
       *  @param rhsTag the right-hand side tag
       *  @param dualTimeStepTag the value for \f$\Delta\sigma\f$
       *  @param timeStepTag the value for \f$\Delta t\f$.
       *  @param nghost the number of ghost cells to use when building expressions in this class.
       */
      Builder( const unsigned int order,
               const Expr::Tag& resultTag,
               const Expr::Tag& oldValueTag,
               const Expr::Tag& residualTag,
               const Expr::Tag& rhsTag,
               const Expr::Tag& dualTimeStepTag,
               const Expr::Tag& timeStepTag,
               const double leadingBDFCoefficient,
               const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( resultTag, nghost ),
          order_          ( order           ),
          oldValueTag_    ( oldValueTag     ),
          residualTag_    ( residualTag     ),
          rhsTag_         ( rhsTag          ),
          dualTimeStepTag_( dualTimeStepTag ),
          timeStepTag_    ( timeStepTag     ),
          leadingBDFCoefficient_( leadingBDFCoefficient )
      {}

      Expr::ExpressionBase* build() const{
        return new VariableImplicitDualTimeUpdater<FieldT>( order_, oldValueTag_, residualTag_, rhsTag_, dualTimeStepTag_, timeStepTag_, leadingBDFCoefficient_ );
      }
    };

    ~VariableImplicitDualTimeUpdater(){}

    void evaluate()
    {
      using namespace SpatialOps;

      FieldT& result = this->value();

      // values
      const FieldT&             phik = oldValue_    ->field_ref();
      const FieldT&             res  = residual_    ->field_ref();
      const DualTimeStepFieldT& dsig = dualTimeStep_->field_ref();
      const SingleValueField&   dt   = timeStep_    ->field_ref();

      // sensitivities
      const FieldT& drhsdphi  = this->rhs_->sens_field_ref( oldValueTag_ );

      // lhs 'factorization' and update
      result <<= phik + dsig * res / ( 1 - dsig * ( drhsdphi - leadingBDFCoefficient_ / dt ) );
    }
  };

  //============================================================================

  /**
   * \class VariableImplicitDualTimeIntegrator
   * \author Mike A. Hansen
   * \date December, 2015
   *
   * \brief Implements a VariableImplicitDualTimeIntegrator with variable-implicit treatment and no preconditioning.
   */
  template<typename FieldT, typename DualTimeStepFieldT = SpatialOps::SingleValueField>
  class VariableImplicitBDFDualTimeIntegrator : public BDFDualTimeIntegrator
  {
  public:
    /**
     * @brief Construct a VariableImplicitDualTimeIntegrator
     * @param patchID the patch id we are working on
     * @param name the name for this integrator
     * @param timeStepTag the physical time step size
     * @param dualTimeStepTag the dual time step size
     * @param order the order of accuracy for the time physical time discretization (dual-time is always first order)
     * @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.  See for example usage in Wasatch
     */
    VariableImplicitBDFDualTimeIntegrator( const int patchID,
                                           const std::string name,
                                           const Tag timeStepTag,
                                           const Tag dualTimeStepTag,
                                           const unsigned order=1,
                                           const Context ukContext=Expr::STATE_NONE)
  : BDFDualTimeIntegrator( patchID, name, timeStepTag, dualTimeStepTag, true, order, ukContext )
    {}

    VariableImplicitBDFDualTimeIntegrator( ExpressionTree* execTree,
                                           ExpressionFactory* execFactory,
                                           const int patchID,
                                           const std::string name,
                                           const Tag dtTag,
                                           const Tag dsTag,
                                           const Tag timestepTag,
                                           const unsigned order=1,
                                           const Context ukContext=Expr::STATE_NONE)
    : BDFDualTimeIntegrator( execTree, execFactory, patchID, name, dtTag, dsTag, timestepTag, true, order, ukContext )
    {}


    ~VariableImplicitBDFDualTimeIntegrator(){}

    void build_updater_expressions()
    {
      const Expr::DualTime::BDFCoefs bdfCoeffs( bdfOrder_ );
      const double leadingBDFCoefficient = bdfCoeffs(0);

      BOOST_FOREACH( const VarSpecPtr var, varSpecs_ ){
        typedef typename VariableImplicitDualTimeUpdater<FieldT>::Builder Updater;
        Updater* update = new Updater( bdfOrder_,
                                       Tag( var->name(),             STATE_NP1  ),
                                       Tag( var->name(),             ukContext_ ),
                                       Tag( var->name()+"_residual", STATE_NONE ),
                                       var->rhs_tag(),
                                       dualTimeStepTag_,
                                       timeStepTag_,
                                       leadingBDFCoefficient );
        this->execRootIDs_.insert( execFactory_->register_expression( update ) );
      }
    }
  };

  //============================================================================

} // namespace DualTime
} // namespace Expr



#endif /* VARIABLEIMPLICITBDFDUALTIMEINTEGRATOR_H_ */
