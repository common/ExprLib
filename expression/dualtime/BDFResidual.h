/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef BDFResidual_Expr_h
#define BDFResidual_Expr_h

#include <expression/Expression.h>
#include <expression/dualtime/VariableSpecification.h>
#include <expression/dualtime/BDFCoefs.h>
#include <spatialops/structured/FieldHelper.h>
namespace Expr{
namespace DualTime{

/**
 *  \class BDFResidual
 *  \author James C. Sutherland, Mike A. Hansen
 *  \date June, 2015
 *
 *  Calculates the residual,
 *  \f[
 *   \frac{\partial \mathcal{U}}{\partial \sigma} = H( \mathcal{U} ) \equiv R(\mathcal{U}) - T(\mathcal{U})
 *  \f]
 *  where \f$R(\mathcal{U})\f$ is the RHS and \f$T(\mathcal{U})\f$
 *  is the (BDF) discrete approximation to the time derivative.
 *
 *  Assuming BDF-1 for \f$T\f$, we have
 *  \f[
 *    H(\mathcal{U}) = R(\mathcal{U}) - \frac{\mathcal{U}-U^{n}}{\Delta t},
 *  \f]
 *  where \f$U^n\f$ is the solution at physical time level \f$n\f$ and \f$\mathcal{U}\f$
 *  is the current guess to \f$U^{n+1}\f$.
 *
 */
template< typename FieldT, typename SingleValueFieldT >
class BDFResidual
 : public Expr::Expression<FieldT>
{
  const BDFCoefs bdf1_, bdf2_, bdf3_, bdf4_, bdf5_, bdf6_;
  const std::string& solnVarName_;
  const unsigned order_;
  const Context ukContext_;

  DECLARE_VECTOR_OF_FIELDS( FieldT, solnVars_ )
  DECLARE_FIELD( FieldT,            rhsTerm_  )
  DECLARE_FIELD( SingleValueFieldT, timestep_ )
  DECLARE_FIELD( SingleValueFieldT, stepCount_ )

  BDFResidual( const Expr::Tag& timestepTag,
               const Expr::Tag& stepCountTag,
               const Expr::Tag& rhsTermTag,
               const std::string& solnVarName,
               const unsigned order,
               const Context ukContext );

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const Tag timestepTag_, stepCountTag_, rhsTermTag_;
    const std::string solnVar_;
    const unsigned order_;
    const Context ukContext_;

  public:
    /**
     *  @brief Build a BDFResidual expression
     *  @param resultTag the tag for the value that this expression computes
     *  @param timestepTag the tag for the value of the timestep
     *  @param stepCountTag the tag for the time step count
     *  @param rhsTermTag the tag for the RHS
     *  @param solnVarName the name for the variable we are dealing with
     *  @param order the order of the BDF time discretization
     *  @param ukContext The Expr::Context for the k'th iterate. Defaults to STATE_NONE but could be set as needed.  See for example usage in Wasatch
     *  @param nghost the number of ghost cells to use
     */
    Builder( const Expr::Tag& resultTag,
             const Expr::Tag& timestepTag,
             const Expr::Tag& stepCountTag,
             const Expr::Tag& rhsTermTag,
             const std::string& solnVarName,
             const unsigned order,
             const Context ukContext,
             const SpatialOps::GhostData nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( resultTag, nghost ),
        timestepTag_ ( timestepTag ),
        stepCountTag_( stepCountTag ),
        rhsTermTag_  ( rhsTermTag  ),
        solnVar_     ( solnVarName ),
        order_       ( order       ),
        ukContext_   ( ukContext   )
    {}

    Expr::ExpressionBase* build() const{
      return new BDFResidual<FieldT,SingleValueFieldT>( timestepTag_, stepCountTag_, rhsTermTag_, solnVar_, order_, ukContext_ );
    }

  };

  ~BDFResidual(){}
  void evaluate_appropriate_method( const unsigned orderOrStepCountPlusOne );
  void sensitivity_appropriate_method( const unsigned orderOrStepCountPlusOne, const Tag& varTag );
  void evaluate();
  void sensitivity( const Tag& varTag ) {}
};



// ###################################################################
//
//                          Implementation
//
// ###################################################################



template< typename FieldT, typename SingleValueFieldT >
BDFResidual<FieldT,SingleValueFieldT>::
BDFResidual( const Expr::Tag& timestepTag,
             const Expr::Tag& stepCountTag,
             const Expr::Tag& rhsTermTag,
             const std::string& varName,
             const unsigned order,
             const Context ukContext)
  : Expr::Expression<FieldT>(),
    bdf1_( 1 ),  bdf2_( 2 ),  bdf3_( 3 ),
    bdf4_( 4 ),  bdf5_( 5 ),  bdf6_( 6 ),
    solnVarName_( varName ),
    order_( order ),
    ukContext_( ukContext )
{
  this->set_gpu_runnable(true);

  timestep_  = this->template create_field_request<SingleValueFieldT>( timestepTag );
  stepCount_ = this->template create_field_request<SingleValueFieldT>( stepCountTag );
  rhsTerm_   = this->template create_field_request<FieldT           >( rhsTermTag  );

  TagList stateTags;
  const unsigned nBackSteps = order - 1;
  for( unsigned i=1; i<=nBackSteps; ++i ){
    stateTags.push_back( Tag( varName + "_n-" + boost::lexical_cast<std::string>( i ) , ukContext_ ) );
  }
  stateTags.insert( stateTags.begin(), Tag( varName, STATE_N ) );
  stateTags.insert( stateTags.begin(), Tag( varName, ukContext ) );

  this->template create_field_vector_request<FieldT>( stateTags, solnVars_ );
}

//--------------------------------------------------------------------

template< typename FieldT, typename SingleValueFieldT >
void
BDFResidual<FieldT,SingleValueFieldT>::
evaluate_appropriate_method( const unsigned orderOrStepCountPlusOne )
{
  using namespace SpatialOps;

  const FieldT& rhsTerm = rhsTerm_->field_ref();
  const SingleValueFieldT& dt = timestep_->field_ref();

  FieldT& resid = this->value();

  const FieldT& uk = solnVars_[0]->field_ref();
  const FieldT& un = solnVars_[1]->field_ref();
  switch( orderOrStepCountPlusOne ){
    case 1:
      resid <<= rhsTerm - ( uk * bdf1_(0)
                          - un * bdf1_(1) ) / dt;
      break;
    case 2:
      resid <<= rhsTerm - ( solnVars_[0]->field_ref() * bdf2_(0)
                          - solnVars_[1]->field_ref() * bdf2_(1)
                          - solnVars_[2]->field_ref() * bdf2_(2) ) / dt;
      break;
    case 3:
      resid <<= rhsTerm - ( solnVars_[0]->field_ref() * bdf3_(0)
                          - solnVars_[1]->field_ref() * bdf3_(1)
                          - solnVars_[2]->field_ref() * bdf3_(2)
                          - solnVars_[3]->field_ref() * bdf3_(3) ) / dt;
      break;
    case 4:
      resid <<= rhsTerm - ( solnVars_[0]->field_ref() * bdf4_(0)
                          - solnVars_[1]->field_ref() * bdf4_(1)
                          - solnVars_[2]->field_ref() * bdf4_(2)
                          - solnVars_[3]->field_ref() * bdf4_(3)
                          - solnVars_[4]->field_ref() * bdf4_(4) ) / dt;
      break;
    case 5:
      resid <<= rhsTerm - ( solnVars_[0]->field_ref() * bdf5_(0)
                          - solnVars_[1]->field_ref() * bdf5_(1)
                          - solnVars_[2]->field_ref() * bdf5_(2)
                          - solnVars_[3]->field_ref() * bdf5_(3)
                          - solnVars_[4]->field_ref() * bdf5_(4)
                          - solnVars_[5]->field_ref() * bdf5_(5) ) / dt;
      break;
    case 6:
      resid <<= rhsTerm - ( solnVars_[0]->field_ref() * bdf6_(0)
                          - solnVars_[1]->field_ref() * bdf6_(1)
                          - solnVars_[2]->field_ref() * bdf6_(2)
                          - solnVars_[3]->field_ref() * bdf6_(3)
                          - solnVars_[4]->field_ref() * bdf6_(4)
                          - solnVars_[5]->field_ref() * bdf6_(5)
                          - solnVars_[6]->field_ref() * bdf6_(6) ) / dt;
      break;
    default:
      throw std::invalid_argument( "Unsupported BDF order" );
  }
}

//--------------------------------------------------------------------

template< typename FieldT, typename SingleValueFieldT >
void
BDFResidual<FieldT,SingleValueFieldT>::
sensitivity_appropriate_method( const unsigned orderOrStepCountPlusOne,
                                const Tag& varTag )
{
  using namespace SpatialOps;

  const FieldT& rhsSens = this->rhsTerm_->sens_field_ref( varTag );
  const SingleValueFieldT& dt = timestep_->field_ref();

  FieldT& residSens = this->sensitivity_result( varTag );

  if( varTag == this->get_tag() ){
    residSens <<= 1.0;
  }
  else{
    bool isVar = ( varTag == Tag( solnVarName_, ukContext_ ) );

    switch( orderOrStepCountPlusOne ){
      case 1:  residSens <<= rhsSens - bdf1_(0) / dt * isVar;  break;
      case 2:  residSens <<= rhsSens - bdf2_(0) / dt * isVar;  break;
      case 3:  residSens <<= rhsSens - bdf3_(0) / dt * isVar;  break;
      case 4:  residSens <<= rhsSens - bdf4_(0) / dt * isVar;  break;
      case 5:  residSens <<= rhsSens - bdf5_(0) / dt * isVar;  break;
      case 6:  residSens <<= rhsSens - bdf6_(0) / dt * isVar;  break;
      default: throw std::invalid_argument( "Unsupported BDF order" );
    }
  }
}

//--------------------------------------------------------------------

template< typename FieldT, typename SingleValueFieldT >
void
BDFResidual<FieldT,SingleValueFieldT>::
evaluate()
{
  const unsigned stepsTaken = (unsigned) stepCount_->field_ref()[0];
  if( stepsTaken >= order_-1 ) /* normal mode  */ evaluate_appropriate_method( order_         );
  else                         /* startup mode */ evaluate_appropriate_method( stepsTaken + 1 );
}

//--------------------------------------------------------------------

} // namespace DualTime
} // namespace Expr

#endif // BDFResidual_Expr_h
