/**

   \mainpage ExprLib Reference
       
    Copyright (c) 2013 The University of Utah
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.
    
   

   \subpage Installation - directions on installing ExprLib


   \section KeyClasses Key classes to pay attention to:

     - Expr::Expression - the basic class that you will derive from to
       create your own expressions.

     - Expr::ExpressionTree - a collection of Expressions that form an
       algorithm.

     For stand-alone simulations, there are several convenient classes to use:

     - Expr::ExprPatch - A convenient and light-weight class that
       provides a simple description of a structured grid.  It can be
       used with many of the other Expression library classes.

     - Expr::TimeStepper - provides a quick way to integrate systems
       of ODEs or PDEs given the variables that are being solved and
       the expressions that calculate the RHS.

     - Expr::TransportEquation - provides a basic outline for
       constructing a transport equation (PDE or ODE).

	\section Debugging flags
		
			- DEBUG_NO_FIELD_RELEASE - Defining this flag will disabled the task
				scheduler from attempting to release non-persistent fields. This may
				be helpful when debugging related errors such as attempting to access
				expression fields which have become dirty.

			- DEBUG_LOCK_ALL_FIELDS - Defining this flag will tell the expressiontree
				to automatically lock all fields during the registration phase of initial
				setup. Locked fields are not eligible to be released during execution.

  \section ExprFunctionality Extending Expression functionality (Advanced)
    There are several mechanisms to add additional functionality to an Expression:
    
      - Expr::Expression::process_after_evaluate().  This allows you to attach a
        functor to the given expression.  After evaluation, this functor is
        called with the field that the expression evaluates.  This allows you to
        perform arbitrary operations on that field.  Useful for setting simple
        boundary conditions.
        
      - Expr::ExpressionFactory::attach_dependency_to_expression().
        This allows you to add or subtract the result of some "dependent"
        expression to the result of this one.  It essentially introduces a new
        dependency and then after execution, adds/subtracts this dependency from
        the value computed.  This is primarily intended to allow addition of
        source terms to expressions.  By introducing the new dependency, the
        graph is updated as appropriate.
        
      - Expr::ExpressionFactory::attach_modifier_expression().
        This is a more sophisticated form of Expr::Expression::process_after_evaluate()
        that allows additional dependencies to be introduced into the graph.
        Rather than using a functor, this attaches a full-fledged expression.
        However, the expression does not explicitly appear in the graph. Rather,
        its induced dependencies are attributed to the parent expression, and
        this expression is triggered after evaluation of the parent expression.
        
      - Expr::Poller. A Poller can be used to provide non-blocking operations in
        a graph.  These are set through the
         Expr::ExpressionTree::add_poller()
        method. By creating a poller and associating it with an expression, we
        allow additional "conditions" to be defined that determine when an
        expression is "complete."  For example, if an expression requires
        communication to occur, then a Poller could check for the completion of
        that communication.  The advantage of using Pollers is that they will
        allow execution of other tasks to continue, with the poller being
        checked periodically to see if its condition is completed.  This results
        in automatic overlap of communication and computation.
        
*/
