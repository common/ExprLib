/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#ifndef MATRIXEXPRESSION_H_
#define MATRIXEXPRESSION_H_

#include <limits>

#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/MatVecOps.h>

#include <expression/Expression.h>
#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{


  /**
   * \class MatrixExpression
   *
   * This Expression forms a pointwise matrix into fields managed by a field manager
   * through the expression framework. If you need to write the fields of a matrix
   * out to disk, use this expression.
   */
  template< typename FieldT >
  class MatrixExpression : public Expr::Expression<FieldT>
  {
    const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
    const int nrows_;
    const bool write_output_on_debug_evaluate_;

    MatrixExpression( const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
                      const int nrows,
                      const bool write_output_on_debug_evaluate = false )
    : Expr::Expression<FieldT>(),
      assembler_( assembler ),
      nrows_( nrows ),
      write_output_on_debug_evaluate_( write_output_on_debug_evaluate )
      {
        this->set_gpu_runnable(true);
        assembler_->create_all_field_requests( *this );
      }

  public:
    struct Builder : public Expr::ExpressionBuilder
    {
    private:
      const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
      const int nrows_;
      const bool write_output_on_debug_evaluate_;

    public:
      /**
       * \brief build a matrix expression to assemble a matrix
       * \param matrixTags the tags for the matrix fields
       * \param assembler the matrix assembler
       * \param write_output_on_debug_evaluate whether or not to write output in the evaluate method for debugging
       *
       * The number of tags must be a square number, this is checked in debug mode.
       */
      Builder( const Expr::TagList& matrixTags,
               const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
               const bool write_output_on_debug_evaluate = false )
    : Expr::ExpressionBuilder( matrixTags  ),
      assembler_( assembler ),
      nrows_( std::sqrt( matrixTags.size() ) ),
      write_output_on_debug_evaluate_( write_output_on_debug_evaluate )
      {
        assert( std::sqrt( matrixTags.size() ) == static_cast<int>( std::sqrt( matrixTags.size() ) ) );
      }

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new MatrixExpression<FieldT>( assembler_, nrows_, write_output_on_debug_evaluate_ );
      }
    };

    void evaluate()
    {
      using namespace SpatialOps;

      typename Expr::Expression<FieldT>::ValVec& elements = this->get_value_vec();
      SpatialOps::FieldMatrix<FieldT> matrix( elements );

      for( Expr::matrix::OrdinalType rowIdx=0; rowIdx < nrows_; ++rowIdx ){
        SpatialOps::FieldVector<FieldT> row( matrix.row( rowIdx ) );
        assembler_->assemble( row, rowIdx, Expr::matrix::Place() );
      }

#ifndef NDEBUG
#ifndef ENABLE_CUDA
#ifndef ENABLE_THREADS
      if( write_output_on_debug_evaluate_ ){

        std::cout << " - inside MatrixExpression with debug output on, assembler = " << assembler_->assembler_name() << '\n';

        std::vector<typename FieldT::const_iterator> elementFieldIterators;
        for( size_t i = 0; i < nrows_ * nrows_; ++i ){
          elementFieldIterators.push_back(elements[i]->begin());
        }

        const SpatialOps::MemoryWindow mw = matrix(0, 0).window_with_ghost();
        const size_t nx = mw.extent(0);
        const size_t ny = mw.extent(1);
        const size_t nz = mw.extent(2);

        std::cout << " grid points: nx = " << nx << ", ny = " << ny << ", nz = " << nz << std::endl;
        std::cout << "--------------------------------------------------------------------------------\n";


        for( size_t z = 0; z < nz; ++z ){
          for( size_t y = 0; y < ny; ++y ){
            for( size_t x = 0; x < nx; ++x ){
              std::cout << " grid point [" << x << ", " << y << ", " << z << "] :\n";
              std::cout << "----------\n           ";
              for( size_t i = 0; i < nrows_; ++i ){
                std::cout << "col " << std::setw(4) << i << std::setw(4) << "";
              }
              std::cout << '\n';
              for( size_t i = 0; i < nrows_; ++i ){
                std::cout << " row " << std::setw(4) << i << ": ";
                for( size_t j = 0; j < nrows_; ++j ) {
                  const double value = *elementFieldIterators[i * nrows_ + j];
                  if( std::abs( value ) > 1.e-14 ){
                    std::cout << std::scientific << std::setprecision(2) << std::setw(12) << value;
                  }
                  else{
                    std::cout << std::setw(12) << "";
                  }
                  ++(elementFieldIterators[i * nrows_ + j]);
                }
                std::cout << std::endl;
              }
              std::cout << "--------------------------------------------------------------------------------\n";
            }
          }
        }
      }
#endif
#endif
#endif
    }
  };

} // namespace matrix
}


#endif /* MATRIXEXPRESSION_H_ */
