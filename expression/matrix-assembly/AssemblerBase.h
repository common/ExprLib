/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ASSEMBLERBASE_H_
#define ASSEMBLERBASE_H_

#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <spatialops/structured/MatVecFields.h>

#include <expression/ExpressionTree.h>

#include <expression/matrix-assembly/MapUtilities.h>
#include <expression/matrix-assembly/Operations.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class: AssemblerBase
   *
   * This class is the base for all matrix assemblers - the compositional units in
   * matrix assembly. This base class stores a vector of required Handles and the
   * number of those Handles (and getters). It defines fundamental types of field,
   * vector, and matrix objects, as well as for ordinals and Handle-field maps.
   *
   * This class has a pure virtual method, assemble, which is defined for an Operation
   * argument of type 'Place.' This method should never be called unless from a
   * derived class that overrides the method.
   *
   * The assemble method takes three parameters:
   *
   * 1. The row to be modified/assembled in-place. This must be a VectorPtrType,
   * which is a vector of field pointers.
   *
   * 2. The index of the row being modified.
   *
   * 3. The mode of operation, Place, AddIn, SubIn, and RMult (see below).
   *
   * To use an assembler to assemble a matrix, simply build the assembler, build
   * the matrix to be assembled, and call assemble(...,Place) in a loop over each
   * row.
   *
   * All derived classes must be template classes. If they are to be used in an
   * emplacement operation, they must override the assemble(...,Place) method.
   * If they are never to be used in emplacement, for instance a state
   * transformation assembler that is only used for right multiplication, then do
   * not override the assemble(...,Place) method! This will provide compile-time
   * safety on compositions of assemblers.
   *
   * There are four operation types defined between assemblers:
   *
   * 1. Place - emplacing elements into a matrix:
   * \code assemble(..., Place) \endcode
   *
   * 2. AddIn - adding elements into a matrix:
   * \code assemble(..., AddIn) \endcode
   *
   * 3. SubIn - subtracting elements from a matrix:
   * \code assemble(..., SubIn) \endcode
   *
   * 4. RMult - performing right multiplication on a matrix:
   * \code assemble(..., RMult) \endcode
   *
   * These operations must be provided on a row-wise basis, meaning a reference to
   * a matrix row is the input and must be modified in-place to provide the row
   * after the operation. To assemble a matrix, the Place operation may be used on
   * a pointer/reference of AssemblerBase type.
   */
  template<typename ElementFieldType>
  class AssemblerBase
  {
  protected:
    Expr::matrix::HandlePtrListType<ElementFieldType> requiredHandles_;

    bool isFinalized_ = false;

    const std::string name_ = "unnamed assembler";

    void check_finalized() const
    {
      if( !isFinalized_ ){
        throw std::runtime_error( "Matrix assembler was not finalized before assembly!" );
      }
    }

  public:
    /**
     * Common types used by derived classes.
     */
    using ElementType = ElementFieldType;
    using VectorType = typename SpatialOps::FieldVector<ElementType>;

    /**
     * \brief construct an empty AssemblerBase
     */
    AssemblerBase() {}

    /**
     * \brief construct a named AssemblerBase
     */
    AssemblerBase( const std::string name ) : name_( name ) {}

    /**
     * \brief empty virtual destructor for AssemblerBase
     */
    virtual ~AssemblerBase() {}

    /**
     * \brief finalize this assembler
     *
     * MUST BE CALLED before an assembler can be used
     */
    virtual void finalize()
    {
      isFinalized_ = true;
    }

    /**
     * \brief bool if this assembler is finalized or not
     */
    bool is_finalized() const
    {
      return isFinalized_;
    }

    /**
     * \brief obtain name of this type of assembler
     *
     * This returns the type of assembler plus the provided name given in construction.
     * For example: "SparseMatrix( John )" or "Summation< SparseMatrix( Hello ), DenseSubMatrix( World ) >"
     */
    virtual std::string assembler_name() const = 0;

    /**
     * \brief obtain a string representing this matrix assembler's structure
     *
     * For example, for a sparse matrix might output the following:
     *
     * (0,1) -> a
     * (1,2) -> b
     * (3,1) -> #2
     * ...
     *
     * to indicate that element (0,1) has the tag with name "a", (1,2) has
     * the tag with name "b", and that (3,1) is a double with value of 2.0.
     */
    virtual std::string get_structure() const
    {
      return "Assembler: " + name_ + ": get_structure() method unimplemented!";
    }

    /**
     * \brief set the required Handles of this assembler
     * \param Handles the required Handles
     *
     * Handles are used to associated an assembler element with an actual field through a tag.
     * This method sets the unique collection of handles that this assembler requires.
     * This method is called from compound assemblers, where a union of required handles of
     * the component assemblers (say, A and B in the A+B compound) is required by the compound.
     */
    void set_required_handles( const Expr::matrix::HandlePtrListType<ElementFieldType>& Handles ){
      requiredHandles_ = Handles;
    }

    /**
     * \brief set the required Handle of this assembler
     * \param Handle a single required Handle pointer
     *
     * Handles are used to associated an assembler element with an actual field through a tag.
     * This method inserts a handle into the unique collection of handles that this assembler requires.
     * This method is used by assemblers such as ScaledIdentityMatrix that require only a single handle.
     */
    void set_required_handles( const Expr::matrix::HandlePtrType<ElementFieldType>& Handle ){
      requiredHandles_.insert( Handle );
    }

    /**
     * \brief create field requests on the owning Expression
     * \param expr the Expression, provided as (*this), in which this Handle is being used
     *
     * This function should only be called from within the constructor of the Expression
     * in which the matrix is assembled (for instance, to assemble a matrix and
     * compute eigenvalues or do a linear solve).
     */
    template<typename ExprFieldT>
    void create_all_field_requests( Expr::Expression<ExprFieldT>& expr )
    {
#ifdef SHOW_EXPRLIB_MATRIX_DIAGNOSTICS
      std::cout << "Matrix assembly: creating field requests...\n";
      std::cout << "  Assembler: " << assembler_name() << '\n';
#endif
      for( auto handle : requiredHandles_ ){
#ifdef SHOW_EXPRLIB_MATRIX_DIAGNOSTICS
        std::cout << "- creating field request with handle: " << handle->description() << "\n";
#endif
        if( handle->is_initialized() ){
          handle->create_field_request( expr );
        }
      }
    }

    /**
     * \brief get a reference to the list of required Handles for this assembler object
     *
     * Handles are used to associated an assembler element with an actual field through a tag.
     * This method returns a collection of unique handles that the assembler requires.
     */
    Expr::matrix::HandlePtrListType<ElementFieldType>& required_handles()
    {
      this->check_finalized();
      return requiredHandles_;
    }

    /**
     * \brief obtain a pair of taglists, the first being sensitivity functions and the
     * second being sensitivity variables.
     *
     * The sensitivity function and sensitivity variable are the
     * numerator and denominator in a partial derivative, respectively:
     *
     * \f$\frac{\partial\text{function}}{\partial\text{variable}}\f$
     */
    std::pair<Expr::TagList,Expr::TagList> sensitivity_pairs() const
    {
      this->check_finalized();
      std::set<Expr::Tag> sensFunctions, sensVariables;
      for( auto handle : requiredHandles_ ){
        if( handle->has_sensitivity() ){
          sensFunctions.insert( handle->field_tag() );
          sensVariables.insert( handle->sens_var_tag() );
        }
      }
      const Expr::TagList funList( sensFunctions.begin(), sensFunctions.end() );
      const Expr::TagList varList( sensVariables.begin(), sensVariables.end() );
      return std::make_pair( funList, varList );
    }

    /*
     * \brief trigger sensitivity calculations
     * \param tree an ExpressionTree on which we compute the sensitivities
     */
    void trigger_sensitivities( Expr::ExpressionTree& tree ) const
    {
      std::pair<Expr::TagList,Expr::TagList> sensPairs = sensitivity_pairs();
      tree.compute_sensitivities( sensPairs.first, sensPairs.second );
    }

    /**
     * \brief assemble into a matrix row by emplacement
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode    the mode of operation, in this case it must be a Place() object
     * (literally just use \code Place() \endcode here)
     *
     */
    virtual void assemble( VectorType& row,
                           const OrdinalType rowIdx,
                           const Expr::matrix::Place mode ) const =0;

  };

}
}


#endif /* ASSEMBLERBASE_H_ */
