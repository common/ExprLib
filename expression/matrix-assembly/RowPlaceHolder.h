/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ROWPLACEHOLDER_H_
#define ROWPLACEHOLDER_H_

#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{
  /**
   * \class RowPlaceHolder
   *
   * This class serves as a place-holder of a dense row for compound operations
   * that must respect a certain order, such as a product of an assembler and a
   * summation. The summation must be evaluated row-wise first, and a
   * RowPlaceHolder is used to store the result. A RowPlaceHolder can do
   * emplacement, addition, and subtraction. It cannot be used to RMult, because
   * it can not know anything about columns.
   */
  template <typename ElementFieldT>
  class RowPlaceHolder : public AssemblerBase<ElementFieldT> {
  protected:
    const SpatialOps::FieldVector<ElementFieldT>& row_;

    void check_bounds( const Expr::matrix::OrdinalType nrows ) const
    {
      const Expr::matrix::OrdinalType thisNRows = row_.elements();
      if( thisNRows != nrows ){
        const std::string nrowsStr = boost::lexical_cast<std::string>( nrows );
        const std::string thisRowSize = boost::lexical_cast<std::string>( thisNRows );
        throw std::runtime_error( "Error! Matrix with " + nrowsStr + " rows is not big enough to hold "
                                  "a RowPlaceHolder with size of " + thisRowSize + "." );
      }
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = RowPlaceHolder<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct a RowPlaceHolder from a vector
     */
    RowPlaceHolder( const SpatialOps::FieldVector<ElementFieldT>& row )
    : AssemblerBase<ElementFieldT>(), row_(row)
    {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "RowPlaceHolder";
    }

    /**
     * \brief emplace this row
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Place mode ) const override
    {
      const OrdinalType nrows = row.elements();
      check_bounds( nrows );
      for (OrdinalType colIdx = 0; colIdx < nrows; ++colIdx) {
        row(colIdx) <<= row_(colIdx);
      }
    }

    /**
     * \brief add this row
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const AddIn mode ) const
    {
      const OrdinalType nrows = row.elements();
      check_bounds( nrows );
      for (OrdinalType colIdx = 0; colIdx < nrows; ++colIdx) {
        row(colIdx) <<= row(colIdx) + row_(colIdx);
      }
    }

    /**
     * \brief subtract this row
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const SubIn mode ) const
    {
      const OrdinalType nrows = row.elements();
      check_bounds( nrows );
      for (OrdinalType colIdx = 0; colIdx < nrows; ++colIdx) {
        row(colIdx) <<= row(colIdx) - row_(colIdx);
      }
    }
  };

} // namespace matrix
}

#endif /** ROWPLACEHOLDER_H_ */
