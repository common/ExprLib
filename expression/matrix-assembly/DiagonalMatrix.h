/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef DIAGONALMATRIX_H_
#define DIAGONALMATRIX_H_

#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{

  /**
   * \class DiagonalMatrix
   *
   * This class represents a diagonal matrix. It maps from ordinals (diagonal
   * indices) to tags of fields representing the diagonal elements. This class has
   * all operations defined - Place, AddIn, SubIn, and RMult.
   *
   * A DiagonalMatrix is constructed without arguments, and elements should be
   * added with the parentheses operator, as M(i) = aTag; where i is the diagonal
   * index, and aTag is the field tag that goes in element (i,i).
   *
   * The finalize() function MUST BE CALLED before a DiagonalMatrix can be used.
   *
   * An example construction and filling procedure is:
   *
   * \code
   * DiagonalMatrix<FieldT> D;
   * D(0) = aTag;
   * D(1) = bTag;
   * D.finalize();
   * \endcode
   */
  template<typename ElementFieldT>
  class DiagonalMatrix : public AssemblerBase<ElementFieldT>
  {
  protected:
    using DiagonalMapType    = Expr::matrix::OrdinalHandleMapType<ElementFieldT               >;
    using DiagonalMapSVFType = Expr::matrix::OrdinalHandleMapType<SpatialOps::SingleValueField>;

    DiagonalMapType diagonalMap_;

    Expr::matrix::OrdinalType minMatrixSize_ = 0;

    void check_bounds( const Expr::matrix::OrdinalType nrows ) const
    {
      if( minMatrixSize_ > nrows ){
        const std::string nrowsStr = boost::lexical_cast<std::string>( nrows );
        const std::string minMatrixSizeStr = boost::lexical_cast<std::string>( minMatrixSize_ );
        throw std::runtime_error( "Error! Matrix with " + nrowsStr + " rows is not big enough to hold "
                                  "a diagonal matrix with minimum size of " + minMatrixSizeStr + "." );
      }
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = DiagonalMatrix<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct an empty diagonal matrix
     */
    DiagonalMatrix() {}

    /*
     * \brief construct a named object
     */
    DiagonalMatrix( const std::string name ) : AssemblerBase<ElementFieldT>( name ) {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "DiagonalMatrix( " + AssemblerBase<ElementFieldT>::name_ + " )";
    }

    /**
     * \brief show the structure of this sparse matrix
     *
     * Prints initialized index pairs and the handles associated with them.
     */
    std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      for( const auto& d : diagonalMap_ ){
        const Expr::matrix::OrdinalType diagIdx = d.first;
        const std::string diagIdxStr = boost::lexical_cast<std::string>( diagIdx );
        const auto h = d.second;
        structure += "  (" + diagIdxStr + ", " + diagIdxStr + ") -> " + h->description() + "\n";
      }
      return structure;
    }

    /**
     * \brief set the tag or value associated with an element in this matrix, identified by diagonal index
     * \param diagonalOrdinal the diagonal index
     *
     * This gives access to a Handle by reference, so one should write,
     *
     * \code
     * M->element<FieldType>(i) = aTag
     * \endcode,
     *
     * for instance, to set the ith diagonal element to the field associated with aTag.
     *
     * The specified type can be one of three things:
     *
     * 1. ElementType - the type of the elementary fields of the matrix
     * 2. SingleValueField
     * 3. double - in this case a double must be provided on the RHS of the assignment expression, instead of a tag.
     */
    template<typename T>
    Expr::matrix::Handle<ElementType>& element( const Expr::matrix::OrdinalType diagonalOrdinal )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "DiagonalMatrix is finalized! Can't insert into it!" );
      }
      const auto& iter = diagonalMap_.find( diagonalOrdinal );
      if( std::is_same<T,ElementType>::value ){
        if( iter == diagonalMap_.end() ){
          diagonalMap_[diagonalOrdinal] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::ELEMENTTYPE );
        }
        else{
          std::cout << "WARNING! Diagonal matrix element, (" << diagonalOrdinal << "), being overridden!\n";
        }
        return *diagonalMap_[diagonalOrdinal];
      }
      else if( std::is_same<T,SpatialOps::SingleValueField>::value ){
        if( iter == diagonalMap_.end() ){
          diagonalMap_[diagonalOrdinal] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::SINGLEVALUEFIELDTYPE );
        }
        else{
          std::cout << "WARNING! Diagonal matrix element, (" << diagonalOrdinal << "), being overridden!\n";
        }
        return *diagonalMap_[diagonalOrdinal];
      }
      else if( std::is_same<T,double>::value ){
        if( iter == diagonalMap_.end() ){
          diagonalMap_[diagonalOrdinal] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::DOUBLETYPE );
        }
        else{
          std::cout << "WARNING! Diagonal matrix element, (" << diagonalOrdinal << "), being overridden!\n";
        }
        return *diagonalMap_[diagonalOrdinal];
      }
      else{
        throw std::runtime_error( "matrix element must be an ElementType, SingleValueField, or double!" );
      }
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      AssemblerBase<ElementFieldT>::finalize();
      AssemblerBase<ElementFieldT>::set_required_handles( Expr::matrix::extract_unique_values( diagonalMap_ ) );

      for( const auto& m : diagonalMap_ ){
        minMatrixSize_ = std::max( minMatrixSize_, m.first );
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();
      check_bounds( nrows );

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      const auto& rowIdxIter = diagonalMap_.find( rowIdx );
      if( rowIdxIter != diagonalMap_.end() ) {
        const auto h = rowIdxIter->second;
        if( h->is_double() ){ row(rowIdx) <<= h->double_value();  }
        else{
          if( h->is_svf() ) { row(rowIdx) <<= h->svf_field();     }
          else              { row(rowIdx) <<= h->element_field(); }
        }
      }
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      const auto& rowIdxIter = diagonalMap_.find( rowIdx );
      if( rowIdxIter != diagonalMap_.end() ) {
        const auto h = rowIdxIter->second;
        if( h->is_double() ){ row(rowIdx) <<= row(rowIdx) + h->double_value();  }
        else{
          if( h->is_svf() ) { row(rowIdx) <<= row(rowIdx) + h->svf_field();     }
          else              { row(rowIdx) <<= row(rowIdx) + h->element_field(); }
        }
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      const auto& rowIdxIter = diagonalMap_.find( rowIdx );
      if( rowIdxIter != diagonalMap_.end() ) {
        const auto h = rowIdxIter->second;
        if( h->is_double() ){ row(rowIdx) <<= row(rowIdx) - h->double_value();  }
        else{
          if( h->is_svf() ) { row(rowIdx) <<= row(rowIdx) - h->svf_field();     }
          else              { row(rowIdx) <<= row(rowIdx) - h->element_field(); }
        }
      }
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();
      check_bounds( nrows );

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        const auto& colIdxIter = diagonalMap_.find( colIdx );
        const auto h = colIdxIter->second;
        if( colIdxIter != diagonalMap_.end() ){
          if( h->is_double() ){ row(colIdx) <<= row(colIdx) * h->double_value();  }
          else{
            if( h->is_svf() ) { row(colIdx) <<= row(colIdx) * h->svf_field();     }
            else              { row(colIdx) <<= row(colIdx) * h->element_field(); }
          }
        }
        else{
          row(colIdx) <<= 0.0;
        }
      }
    }

  };

} // namespace matrix
}

#endif /** DIAGONALMATRIX_H_ */
