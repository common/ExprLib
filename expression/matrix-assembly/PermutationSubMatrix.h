/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef PERMUTATIONSUBMATRIX_H_
#define PERMUTATIONSUBMATRIX_H_

#include <limits>
#include <expression/matrix-assembly/AssemblerBase.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class PermutationSubMatrix
   *
   * A permutation matrix is found by swapping rows of an identity matrix.
   * Every row has one nonzero element whose value is one.
   * Every column has one nonzero element whose value is one.
   *
   * Permutation matrices are very sparse, with N nonzero elements for a matrix with N rows.
   * They are square matrices by definition.
   *
   * This class is capable of all four operations: emplacement, addition-in,
   * subtraction-from, and right multiplication.
   *
   * The finalize() function MUST BE CALLED before a PermutationSubMatrix can be used.
   *
   */
  template<typename ElementFieldT>
  class PermutationSubMatrix : public AssemblerBase<ElementFieldT>
  {
  protected:
    using MapType = std::map<Expr::matrix::OrdinalType,Expr::matrix::OrdinalType>;
    MapType rowMap_;
    MapType colMap_;

    Expr::matrix::OrdinalType nrows_     = 0;
    Expr::matrix::OrdinalType finalRow_  = 0;
    Expr::matrix::OrdinalType anchorRow_ = std::numeric_limits<int>::max();

    void check_bounds( const Expr::matrix::OrdinalType nrows ) const
    {
      if( ( anchorRow_ + nrows_ > nrows ) ){
        const std::string nrowsStr = boost::lexical_cast<std::string>( nrows );
        const std::string anchorRowStr = boost::lexical_cast<std::string>( anchorRow_ );
        const std::string finalRowStr = boost::lexical_cast<std::string>( anchorRow_ + nrows_ );
        const std::string anchorColStr = boost::lexical_cast<std::string>( anchorRow_ );
        const std::string finalColStr = boost::lexical_cast<std::string>( anchorRow_ + nrows_ );
        throw std::runtime_error( "Error! Matrix with " + nrowsStr + " rows is not big enough to hold "
                                  "a permutation matrix spanning rows " + anchorRowStr + " to " + finalRowStr +
                                  " and cols " + anchorRowStr + " to " + finalRowStr + ".");
      }
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = PermutationSubMatrix<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct an empty permutation sub-matrix
     */
    PermutationSubMatrix() {}

    /*
     * \brief construct a named object
     */
    PermutationSubMatrix( const std::string name ) : AssemblerBase<ElementFieldT>( name ) {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "PermutationSubMatrix( " + AssemblerBase<ElementFieldT>::name_ + " )";
    }

    /**
     * \brief show the structure of this sparse matrix
     *
     * Prints initialized index pairs and the handles associated with them.
     */
    std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      for( const auto& r : rowMap_ ){
        const Expr::matrix::OrdinalType rowIdx = r.first;
        const std::string rowIdxStr = boost::lexical_cast<std::string>( rowIdx );
        const std::string colIdxStr = boost::lexical_cast<std::string>( r.second );
        structure += "  (" + rowIdxStr + ", " + colIdxStr + ") -> 1\n";
      }
      return structure;
    }

    /**
     * \brief add un-permuted row
     * \param rowIdx the row for which matrix(row,row) = 1
     */
    void set_identity_row( const Expr::matrix::OrdinalType rowIdx )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "PermutationSubMatrix is finalized! Can't set identity row!" );
      }
      anchorRow_ = std::min( anchorRow_, rowIdx );
      finalRow_ = std::max( finalRow_, rowIdx );
      nrows_ = finalRow_ - anchorRow_ + 1;
      rowMap_[rowIdx] = rowIdx;
    }

    /**
     * \brief swap two rows to permute the identity matrix
     * \param rowIdx1 the first row
     * \param rowIdx2 the second row
     */
    void swap_rows( const Expr::matrix::OrdinalType rowIdx1,
                    const Expr::matrix::OrdinalType rowIdx2 )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "PermutationSubMatrix is finalized! Can't swap rows!" );
      }
      anchorRow_ = std::min( anchorRow_, std::min( rowIdx1, rowIdx2 ) );
      finalRow_ = std::max( finalRow_, std::max( rowIdx1, rowIdx2 ) );
      nrows_ = finalRow_ - anchorRow_ + 1;

      if( rowMap_.find(rowIdx1) == rowMap_.end() ){
        rowMap_[rowIdx1] = rowIdx1;
      }
      if( rowMap_.find(rowIdx2) == rowMap_.end() ){
        rowMap_[rowIdx2] = rowIdx2;
      }

      const Expr::matrix::OrdinalType row1Loc = rowMap_[rowIdx1];
      const Expr::matrix::OrdinalType row2Loc = rowMap_[rowIdx2];

      rowMap_[rowIdx2] = row1Loc;
      rowMap_[rowIdx1] = row2Loc;
    }

    /**
     * \brief construct the permutation matrix directly from a row mapping
     * \param rowMap a mapping from rows to columns. rowIdx->colIdx (colIdx is location of the 1)
     *
     * This will override the entire row map of the permutation matrix!
     * If you swap rows before calling this, the swaps will be overridden.
     */
    void set_row_map( const MapType& rowMap )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "PermutationSubMatrix is finalized! Can't set row map!" );
      }

      for( const auto& r : rowMap ){
        auto rowIdx1 = r.first;
        auto rowIdx2 = r.second;

        anchorRow_ = std::min( anchorRow_, std::min( rowIdx1, rowIdx2 ) );
        finalRow_ = std::max( finalRow_, std::max( rowIdx1, rowIdx2 ) );
        nrows_ = finalRow_ - anchorRow_ + 1;
      }
      rowMap_ = rowMap;
    }

    /**
     * \brief add a 1 element by row and column index
     * \param rowIdx the row index
     * \param colIdx the column index
     *
     * Note that you can't add multiple 1s to the same row.
     * This function overrides any previous column index associated with the row.
     * No error is thrown if an element is overridden.
     */
    void set_row_col( const Expr::matrix::OrdinalType rowIdx,
                      const Expr::matrix::OrdinalType colIdx )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "PermutationSubMatrix is finalized! Can't set row, col 1!" );
      }

      anchorRow_ = std::min( anchorRow_, std::min( rowIdx, colIdx ) );
      finalRow_ = std::max( finalRow_, std::max( rowIdx, colIdx ) );
      rowMap_[rowIdx] = colIdx;
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      AssemblerBase<ElementFieldT>::finalize();

      for( Expr::matrix::OrdinalType diagIdx = anchorRow_; diagIdx < finalRow_; ++diagIdx ){
        if( rowMap_.find(diagIdx) == rowMap_.end() ){
          rowMap_[diagIdx] = diagIdx;
        }
      }
      for( const auto& r : rowMap_ ){
        colMap_[r.second] = r.first;
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType rowSize = row.elements();
      check_bounds( rowSize );

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        row(colIdx) <<= 0.0;
      }
      if( rowIdx >= anchorRow_ && rowIdx < finalRow_+1 ){
        row(rowMap_.at( rowIdx )) <<= 1.0;
      }
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      if( rowIdx >= anchorRow_ && rowIdx < finalRow_+1 ){
        const Expr::matrix::OrdinalType colIdx = rowMap_.at( rowIdx );
        row(colIdx) <<= row(colIdx) + 1.0;
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      if( rowIdx >= anchorRow_ && rowIdx < finalRow_+1 ){
        const Expr::matrix::OrdinalType colIdx = rowMap_.at( rowIdx );
        row(colIdx) <<= row(colIdx) - 1.0;
      }
    }

    /**
     * \brief right-multiplication assembly
     * \param row     a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType rowSize = row.elements();
      check_bounds( rowSize );

      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( rowSize );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        leftRow(colIdx) <<= row(colIdx);
      }

      for( Expr::matrix::OrdinalType colIdxToAssign = 0; colIdxToAssign < rowSize; ++colIdxToAssign ){
        row(colIdxToAssign) <<= 0.0;
      }
      for( Expr::matrix::OrdinalType colIdxToAssign = anchorRow_; colIdxToAssign < finalRow_+1; ++colIdxToAssign ){
        row(colIdxToAssign) <<= leftRow(colMap_.at( colIdxToAssign ));
      }
    }

  };

} // namespace matrix
}

#endif /** PERMUTATIONSUBMATRIX_H_ */
