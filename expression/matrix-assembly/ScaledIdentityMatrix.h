/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SCALEDIDENTITYMATRIX_H_
#define SCALEDIDENTITYMATRIX_H_

#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{


  /**
   * \class ScaledIdentityMatrix
   *
   * This class represents a scaled identity matrix. That is, a matrix with zeros
   * everywhere but the major diagonal, and whose diagonal elements are the same.
   * A tag is provided for the diagoanl scaling field, and you may provide a ScalingType
   * (enumeration) that determines if the scale is a multiplication (default) or
   * division. The scale type defaults to multiplication while the tag defaults to
   * an empty tag, which results in an unscaled identity matrix. This class has
   * all operations defined - Place, AddIn, SubIn, and RMult.
   *
   * A ScaledIdentityMatrix is constructed without arguments, and the diagonal scale
   * and scaling type (* or /) are set, respectively, by two methods shown below.
   * The finalize() method must be called before a ScaledIdentityMatrix can be used.
   *
   * \code
   * ScaledIdentityMatrix<FieldT> Y;
   * Y.set_scale( aTag ); // or Y.set_single_value_field_scale( aTag );
   * Y.set_type( Expr::matrix::SCALEMULTIPLY ); // or use SCALEDIVIDE
   * Y.finalize();
   * \endcode
   *
   * This code means that \f$Y_{i,i} = a \f$. If division is used instead then the result is
   * \f$Y_{i,i} = 1/a \f$. If \code set_scale \endcode is never called then the result is
   * an identity matrix with no scaling, \f$Y_{i,i} = 1 \f$.
   */
  template<typename ElementFieldT>
  class ScaledIdentityMatrix : public AssemblerBase<ElementFieldT>
  {
  public:
    /**
     * \enum ScalingType
     *
     * Type of scaling performed on the diagonal of a ScaledIdentityMatrix.
     */
    enum ScalingType
    {
      SCALEMULTIPLY,
      SCALEDIVIDE
    };

  protected:
    using HandlePtrType = Expr::matrix::HandlePtrType<ElementFieldT>;

    HandlePtrType scale_;
    bool hasFieldScale_      = false;
    ScalingType scalingType_ = SCALEMULTIPLY;
    double baseScaling_      = 1.0; // identity

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = ScaledIdentityMatrix<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct an empty scaled identity matrix
     */
    ScaledIdentityMatrix() {}

    /*
     * \brief construct a named object
     */
    ScaledIdentityMatrix( const std::string name ) : AssemblerBase<ElementFieldT>( name ) {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "ScaledIdentityMatrix( " + AssemblerBase<ElementFieldT>::name_ + " )";
    }

    /**
     * \brief show the structure of this sparse matrix
     *
     * Prints initialized index pairs and the handles associated with them.
     */
    std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";

      std::string baseScalingStr = boost::lexical_cast<std::string>( baseScaling_ );

      if( hasFieldScale_ ){
        if( scalingType_ == SCALEMULTIPLY ){
          structure += "  scale: " + baseScalingStr + " * " + scale_->description() + "\n";
        }
        else{
          structure += "  scale: " + baseScalingStr + " / " + scale_->description() + "\n";
        }
      }
      else{
        if( baseScaling_ == 1.0 ){
          structure += "  Identity\n";
        }
        else{
          structure += "  scale: " + baseScalingStr + " \n";
        }
      }
      return structure;
    }

    /**
     * \brief get the field Handle associated with the diagonal scale
     */
    template<typename T>
    Expr::matrix::Handle<ElementType>& scale()
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "ScaledIdentityMatrix is finalized! Can't set scale!" );
      }
      if( hasFieldScale_ ){
        throw std::runtime_error( "ScaledIdentityMatrix scale has already been set. Cannot reset!" );
      }
      if( std::is_same<T,ElementType>::value ){
        scale_ = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::ELEMENTTYPE );
        hasFieldScale_ = true;
        return *scale_;
      }
      else if( std::is_same<T,SpatialOps::SingleValueField>::value ){
        scale_ = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::SINGLEVALUEFIELDTYPE );
        hasFieldScale_ = true;
        return *scale_;
      }
      else if( std::is_same<T,double>::value ){
        scale_ = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::DOUBLETYPE );
        hasFieldScale_ = true;
        return *scale_;
      }
      else{
        throw std::runtime_error( "matrix element must be an ElementType, SingleValueField, or double!" );
      }
    }

    /**
     * \brief set the type of scaling in this matrix
     * \param type the type of scaling used, SCALEMULTIPLY or SCALEDIVIDE
     */
    void set_type( const ScalingType& type )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "ScaledIdentityMatrix is finalized! Can't set scaling type!" );
      }
      else{
        if( type == SCALEMULTIPLY || type == SCALEDIVIDE ){
          scalingType_ = type;
        }
        else{
          throw std::runtime_error( "Scaling type must be SCALEMULTIPLY or SCALEDIVIDE - incorrect type given!" );
        }
      }
    }

    /**
     * \brief set the base multiplier (a double) on this matrix
     * \param baseScaling the new multiplier
     */
    void set_double_multiplier( const double baseScaling )
    {
      baseScaling_ = baseScaling;
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      AssemblerBase<ElementFieldT>::finalize();
      if( hasFieldScale_ ){
          AssemblerBase<ElementFieldT>::set_required_handles( scale_ );
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      if( hasFieldScale_ ){
        if( scalingType_ == SCALEMULTIPLY ){
          if( scale_->is_double() ){ row(rowIdx) <<= baseScaling_ * scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= baseScaling_ * scale_->svf_field();     }
            else                   { row(rowIdx) <<= baseScaling_ * scale_->element_field(); }
          }
        }
        else{
          if( scale_->is_double() ){ row(rowIdx) <<= baseScaling_ / scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= baseScaling_ / scale_->svf_field();     }
            else                   { row(rowIdx) <<= baseScaling_ / scale_->element_field(); }
          }
        }
      }
      else{
        row(rowIdx) <<= baseScaling_;
      }
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      this->check_finalized();

      if( hasFieldScale_ ){
        if( scalingType_ == SCALEMULTIPLY ){
          if( scale_->is_double() ){ row(rowIdx) <<= row(rowIdx) + baseScaling_ * scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= row(rowIdx) + baseScaling_ * scale_->svf_field();     }
            else                   { row(rowIdx) <<= row(rowIdx) + baseScaling_ * scale_->element_field(); }
          }
        }
        else{
          if( scale_->is_double() ){ row(rowIdx) <<= row(rowIdx) + baseScaling_ / scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= row(rowIdx) + baseScaling_ / scale_->svf_field();     }
            else                   { row(rowIdx) <<= row(rowIdx) + baseScaling_ / scale_->element_field(); }
          }
        }
      }
      else{
        row(rowIdx) <<= row(rowIdx) + baseScaling_;
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      this->check_finalized();

      if( hasFieldScale_ ){
        if( scalingType_ == SCALEMULTIPLY ){
          if( scale_->is_double() ){ row(rowIdx) <<= row(rowIdx) - baseScaling_ * scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= row(rowIdx) - baseScaling_ * scale_->svf_field();     }
            else                   { row(rowIdx) <<= row(rowIdx) - baseScaling_ * scale_->element_field(); }
          }
        }
        else{
          if( scale_->is_double() ){ row(rowIdx) <<= row(rowIdx) - baseScaling_ / scale_->double_value();  }
          else{
            if( scale_->is_svf() ) { row(rowIdx) <<= row(rowIdx) - baseScaling_ / scale_->svf_field();     }
            else                   { row(rowIdx) <<= row(rowIdx) - baseScaling_ / scale_->element_field(); }
          }
        }
      }
      else{
        row(rowIdx) <<= row(rowIdx) - baseScaling_;
      }
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();

      if( hasFieldScale_ ){
        if( scalingType_ == SCALEMULTIPLY ){
          if( scale_->is_double() ){
            for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
              row(colIdx) <<= row(colIdx) * baseScaling_ * scale_->double_value();
            }
          }
          else{
            if( scale_->is_svf() ) {
              for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
                row(colIdx) <<= row(colIdx) * baseScaling_ * scale_->svf_field();
              }
            }
            else{
              for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
                row(colIdx) <<= row(colIdx) * baseScaling_ * scale_->element_field();
              }
            }
          }
        }
        else{
          if( scale_->is_double() ){
            for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
              row(colIdx) <<= row(colIdx) * baseScaling_ / scale_->double_value();
            }
          }
          else{
            if( scale_->is_svf() ) {
              for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
                row(colIdx) <<= row(colIdx) * baseScaling_ / scale_->svf_field();
              }
            }
            else{
              for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
                row(colIdx) <<= row(colIdx) * baseScaling_ / scale_->element_field();
              }
            }
          }
        }
      }
      else{
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          row(colIdx) <<= row(colIdx) * baseScaling_;
        }
      }
    }

  };


} // namespace matrix
}


#endif /** SCALEDIDENTITYMATRIX_H_ */
