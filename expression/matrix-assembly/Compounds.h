/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef COMPOUNDS_H_
#define COMPOUNDS_H_

#include <expression/matrix-assembly/RowPlaceHolder.h>

namespace Expr
{
namespace matrix
{
  /**
   * \class Summation
   *
   * This compound assembler class is a summation of assembler types (possibly
   * compounds themselves). This class supports emplacement, addition, and
   * subtraction. Right-multiplication by a summation is enabled by the ProdSum
   * compound assembler below, which behaves in a special manner.
   *
   * Through the Summation and ProdSum compounds, a summation may be used in
   * emplacement, addition, subtraction, and right multiplication.
   */
  template <typename LeftAssemblerT, typename RightAssemblerT>
  class Summation : public AssemblerBase<typename LeftAssemblerT::ElementType> {
  protected:
    const boost::shared_ptr<LeftAssemblerT > left_;
    const boost::shared_ptr<RightAssemblerT> right_;

  public:
    /**
     * MatrixAssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = Summation<LeftAssemblerT,RightAssemblerT>;
    /**
     * types from AssemblerBase templated on the left assembler type
     */
    using ElementType = typename AssemblerBase<typename LeftAssemblerT::ElementType>::ElementType;
    using VectorType  = typename AssemblerBase<typename LeftAssemblerT::ElementType>::VectorType;

    /**
     * \brief construct a summation object from two assemblers. This should not be
     * called from user code. Summations should be made only with the '+'
     * operator overloaded below.
     * \param left the assembler object on the left of '+'
     * \param right the assembler object on the right of '+'
     */
    Summation( const boost::shared_ptr<LeftAssemblerT > left,
               const boost::shared_ptr<RightAssemblerT> right )
    : AssemblerBase<typename LeftAssemblerT::ElementType>(),
      left_( left ),
      right_( right )
    {
      this->set_required_handles( union_handle_lists( left_->required_handles(), right_->required_handles() ) );
      this->finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "Summation< " + left_->assembler_name() + ", " + right_->assembler_name() + " >";
    }

    /**
     * \brief obtain a string representing this matrix assembler's structure
     */
    virtual std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      structure += left_->get_structure() + "\n" + right_->get_structure();
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      left_ ->assemble( row, rowIdx, Place() );
      right_->assemble( row, rowIdx, AddIn() );
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      left_ ->assemble( row, rowIdx, AddIn() );
      right_->assemble( row, rowIdx, AddIn() );
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      left_ ->assemble( row, rowIdx, SubIn() );
      right_->assemble( row, rowIdx, SubIn() );
    }
  };

  /**
   * \class Difference
   *
   * This compound assembler class is a difference of assembler types (possibly
   * compounds themselves). This class supports emplacement, addition, and
   * subtraction. Right-multiplication by a summation is enabled by the ProdDiff
   * compound assembler below, which behaves in a special manner.
   *
   * Through the Difference and ProdDiff compounds, a summation may be used in
   * emplacement, addition, subtraction, and right multiplication.
   */
  template <typename LeftAssemblerT, typename RightAssemblerT>
  class Difference : public AssemblerBase<typename LeftAssemblerT::ElementType>{
  protected:
    const boost::shared_ptr<LeftAssemblerT > left_;
    const boost::shared_ptr<RightAssemblerT> right_;

  public:
    /**
     * MatrixAssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = Difference<LeftAssemblerT,RightAssemblerT>;
    /**
     * types from AssemblerBase templated on the left assembler type
     */
    using ElementType = typename AssemblerBase<typename LeftAssemblerT::ElementType>::ElementType;
    using VectorType  = typename AssemblerBase<typename LeftAssemblerT::ElementType>::VectorType;

    /**
     * \brief construct a difference object from two assemblers. This should not be
     * called from user code. Differences should be made only with the '-'
     * operator overloaded below.
     * \param left the assembler object on the left of '-'
     * \param right the assembler object on the right of '-'
     */
    Difference( const boost::shared_ptr<LeftAssemblerT > left,
                const boost::shared_ptr<RightAssemblerT> right )
    : AssemblerBase<typename LeftAssemblerT::ElementType>(),
      left_ ( left  ),
      right_( right )
    {
      this->set_required_handles( union_handle_lists( left_->required_handles(), right_->required_handles() ) );
      this->finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "Difference< " + left_->assembler_name() + ", " + right_->assembler_name() + " >";
    }

    /**
     * \brief obtain a string representing this matrix assembler's structure
     */
    virtual std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      structure += left_->get_structure() + "\n" + right_->get_structure();
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      left_ ->assemble( row, rowIdx, Place() );
      right_->assemble( row, rowIdx, SubIn() );
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      left_ ->assemble( row, rowIdx, AddIn() );
      right_->assemble( row, rowIdx, SubIn() );
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      left_ ->assemble( row, rowIdx, SubIn() );
      right_->assemble( row, rowIdx, AddIn() );
    }
  };

  /**
   * \class Product
   *
   * This compound assembler class is a product of assembler types (possibly
   * compounds themselves). This class supports emplacement, addition, subtraction,
   * and right multiplication.
   */
  template <typename LeftAssemblerT, typename RightAssemblerT>
  class Product : public AssemblerBase<typename LeftAssemblerT::ElementType> {
  protected:
    const boost::shared_ptr<LeftAssemblerT > left_;
    const boost::shared_ptr<RightAssemblerT> right_;

  public:
    /**
     * MatrixAssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = Product<LeftAssemblerT,RightAssemblerT>;
    /**
     * types from AssemblerBase templated on the left assembler type
     */
    using ElementType = typename AssemblerBase<typename LeftAssemblerT::ElementType>::ElementType;
    using VectorType  = typename AssemblerBase<typename LeftAssemblerT::ElementType>::VectorType;

    /**
     * \brief construct a difference object from two assemblers. This should not be
     * called from user code. Differences should be made only with the '-'
     * operator overloaded below.
     * \param left the assembler object on the left of '-'
     * \param right the assembler object on the right of '-'
     */
    Product( const boost::shared_ptr<LeftAssemblerT > left,
             const boost::shared_ptr<RightAssemblerT> right )
    : AssemblerBase<typename LeftAssemblerT::ElementType>(),
      left_ ( left  ),
      right_( right )
    {
      this->set_required_handles( union_handle_lists( left_->required_handles(), right_->required_handles() ) );
      this->finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "Product< " + left_->assembler_name() + ", " + right_->assembler_name() + " >";
    }

    /**
     * \brief obtain a string representing this matrix assembler's structure
     */
    virtual std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      structure += left_->get_structure() + "\n" + right_->get_structure();
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      left_ ->assemble( row, rowIdx, Place() );
      right_->assemble( row, rowIdx, RMult() );
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> productRow( tempRowPtrs );
      left_->assemble( productRow, rowIdx, Expr::matrix::Place() );
      right_->assemble( productRow, rowIdx, Expr::matrix::RMult() );
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( productRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::AddIn() );
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> productRow( tempRowPtrs );
      left_->assemble( productRow, rowIdx, Expr::matrix::Place() );
      right_->assemble( productRow, rowIdx, Expr::matrix::RMult() );
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( productRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::SubIn() );
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const Expr::matrix::OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      left_ ->assemble( row, rowIdx, RMult() );
      right_->assemble( row, rowIdx, RMult() );
    }
  };


  /**
   * \class ProdSum
   *
   * This compound assembler class is a product of an assembler type and a
   * summation type.
   *
   * Through the Summation and ProdSum compounds, a summation may be used in
   * emplacement, addition, subtraction, and right multiplication.
   */
  template <typename LeftAssemblerT,
  typename RightLeftAssemblerT,
  typename RightRightAssemblerT>
  class ProdSum : public AssemblerBase<typename LeftAssemblerT::ElementType> {
  protected:
    const boost::shared_ptr<LeftAssemblerT> left_;
    const boost::shared_ptr<Expr::matrix::Summation<RightLeftAssemblerT, RightRightAssemblerT> > right_;

  public:
    /**
     * MatrixAssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = ProdSum<LeftAssemblerT,RightLeftAssemblerT,RightRightAssemblerT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<typename LeftAssemblerT::ElementType>::ElementType;
    using VectorType =  typename AssemblerBase<typename LeftAssemblerT::ElementType>::VectorType;

    /**
     * \brief construct a product-summation object from an assembler on the left
     * and a summation assembler on the right
     * operator overloaded below.
     * \param left the assembler object on the left of '*'
     * \param right the summation assembler on the right of '*'
     */
    ProdSum( const boost::shared_ptr<LeftAssemblerT> left,
             const boost::shared_ptr<Summation<RightLeftAssemblerT, RightRightAssemblerT> > right )
    : AssemblerBase<typename LeftAssemblerT::ElementType>(),
      left_ ( left  ),
      right_( right )
    {
      this->set_required_handles( union_handle_lists( left_->required_handles(), right_->required_handles() ) );
      this->finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "ProdSum< " + left_->assembler_name() + ", " + right_->assembler_name() + " >";
    }

    /**
     * \brief obtain a string representing this matrix assembler's structure
     */
    virtual std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      structure += left_->get_structure() + "\n" + right_->get_structure();
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::Place() );
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::AddIn() );
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::SubIn() );
    }

    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > thisRowPtrs;
      thisRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        thisRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> thisRow( thisRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        thisRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      for( Expr::matrix::OrdinalType rowIdx = 0; rowIdx < nrows; ++rowIdx )
      {
        this->assemble( thisRow, rowIdx, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + row(rowIdx) * thisRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::Place() );
    }

  };


  /**
   * \class ProdDiff
   *
   * This compound assembler class is a product of an assembler type and a
   * difference type.
   *
   * Through the Difference and ProdDiff compounds, a difference may be used in
   * emplacement, addition, subtraction, and right multiplication.
   */
  template <typename LeftAssemblerT,
            typename RightLeftAssemblerT,
            typename RightRightAssemblerT>
  class ProdDiff : public AssemblerBase<typename LeftAssemblerT::ElementType> {
  protected:
    const boost::shared_ptr<LeftAssemblerT> left_;
    const boost::shared_ptr<Expr::matrix::Difference<RightLeftAssemblerT, RightRightAssemblerT> > right_;

  public:
    /**
     * MatrixAssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = ProdDiff<LeftAssemblerT,RightLeftAssemblerT,RightRightAssemblerT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<typename LeftAssemblerT::ElementType>::ElementType;
    using VectorType  = typename AssemblerBase<typename LeftAssemblerT::ElementType>::VectorType;

    /**
     * \brief construct a product-difference object from an assembler on the left
     * and a difference assembler on the right
     * operator overloaded below.
     * \param left the assembler object on the left of '*'
     * \param right the difference assembler on the right of '*'
     */
    ProdDiff( const boost::shared_ptr<LeftAssemblerT> left,
              const boost::shared_ptr<Difference<RightLeftAssemblerT, RightRightAssemblerT> > right )
    : AssemblerBase<typename LeftAssemblerT::ElementType>(),
      left_ ( left  ),
      right_( right )
    {
      this->set_required_handles( union_handle_lists( left_->required_handles(), right_->required_handles() ) );
      this->finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "ProdDiff< " + left_->assembler_name() + ", " + right_->assembler_name() + " >";
    }

    /**
     * \brief obtain a string representing this matrix assembler's structure
     */
    virtual std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      structure += left_->get_structure() + "\n" + right_->get_structure();
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::Place() );
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::AddIn() );
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= 0.0;
        leftRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      left_->assemble( leftRow, rowIdx, Expr::matrix::Place() );
      for( Expr::matrix::OrdinalType rowIdxLooped = 0; rowIdxLooped < nrows; ++rowIdxLooped ){
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRow(colIdx) <<= 0.0;
        }
        right_->assemble( tempRow, rowIdxLooped, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + leftRow(rowIdxLooped) * tempRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::SubIn() );
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      const Expr::matrix::OrdinalType nrows = row.elements();
      std::vector<SpatialOps::SpatFldPtr<ElementType> > thisRowPtrs;
      thisRowPtrs.reserve( nrows );
      std::vector<SpatialOps::SpatFldPtr<ElementType> > rsltRowPtrs;
      rsltRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        thisRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        rsltRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> thisRow( thisRowPtrs );
      SpatialOps::FieldVector<ElementType> rsltRow( rsltRowPtrs );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        thisRow(colIdx) <<= 0.0;
        rsltRow(colIdx) <<= 0.0;
      }

      for( Expr::matrix::OrdinalType rowIdx = 0; rowIdx < nrows; ++rowIdx )
      {
        this->assemble( thisRow, rowIdx, Expr::matrix::Place() );
        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          rsltRow(colIdx) <<= rsltRow(colIdx) + row(rowIdx) * thisRow(colIdx);
        }
      }
      Expr::matrix::RowPlaceHolder<ElementType> rowHolder( rsltRow );
      rowHolder.assemble( row, rowIdx, Expr::matrix::Place() );
    }

  };




  /**
   * Magic to effectively limit the scope of the operators below to matrix assembly objects
   */
  template <typename T1, typename T2>
  struct DefinedIfSameType;
  template <typename T1>
  struct DefinedIfSameType<T1, T1>
  {
    typedef T1 AssemblerType;
  };


  /**
   * \brief operator '+' overloaded to produce a summation object
   * \param left the assembler object on the left of '+'
   * \param right the assembler object on the right of '+'
   */
  template<typename LeftAssemblerT,
           typename RightAssemblerT>
  boost::shared_ptr<
  Summation<typename DefinedIfSameType<typename  LeftAssemblerT::AssemblerType,  LeftAssemblerT>::AssemblerType,
            typename DefinedIfSameType<typename RightAssemblerT::AssemblerType, RightAssemblerT>::AssemblerType> >
  operator+( const boost::shared_ptr<LeftAssemblerT > left,
             const boost::shared_ptr<RightAssemblerT> right )
  {
    return boost::make_shared<Summation<LeftAssemblerT, RightAssemblerT> >( left, right );
  }

  /**
   * \brief operator '-' overloaded to produce a summation object
   * \param left the assembler object on the left of '-'
   * \param right the assembler object on the right of '-'
   */
  template<typename LeftAssemblerT,
           typename RightAssemblerT>
  boost::shared_ptr<
  Difference<typename DefinedIfSameType<typename  LeftAssemblerT::AssemblerType,  LeftAssemblerT>::AssemblerType,
             typename DefinedIfSameType<typename RightAssemblerT::AssemblerType, RightAssemblerT>::AssemblerType> >
  operator-( const boost::shared_ptr<LeftAssemblerT> left,
             const boost::shared_ptr<RightAssemblerT> right )
  {
    return boost::make_shared<Difference<LeftAssemblerT, RightAssemblerT> >( left, right );
  }

  /**
   * \brief operator '*' overloaded to produce a product object
   * \param left the assembler object on the left of '*'
   * \param right the assembler object on the right of '*'
   */
  template<typename LeftAssemblerT,
           typename RightAssemblerT>
  boost::shared_ptr<
  Product<typename DefinedIfSameType<typename  LeftAssemblerT::AssemblerType,  LeftAssemblerT>::AssemblerType,
          typename DefinedIfSameType<typename RightAssemblerT::AssemblerType, RightAssemblerT>::AssemblerType> >
  operator*( const boost::shared_ptr<LeftAssemblerT> left,
             const boost::shared_ptr<RightAssemblerT> right )
  {
    return boost::make_shared<Product<LeftAssemblerT, RightAssemblerT> >( left, right );
  }

  /**
   * \brief operator '*' overloaded to produce a ProdSum object
   * \param left the assembler object on the left of '*'
   * \param right the summation assembler on the right of '*'
   */
  template<typename LeftAssemblerT,
           typename RightLeftAssemblerT,
           typename RightRightAssemblerT>
  boost::shared_ptr<
  ProdSum<typename DefinedIfSameType<typename       LeftAssemblerT::AssemblerType,       LeftAssemblerT>::AssemblerType,
          typename DefinedIfSameType<typename  RightLeftAssemblerT::AssemblerType,  RightLeftAssemblerT>::AssemblerType,
          typename DefinedIfSameType<typename RightRightAssemblerT::AssemblerType, RightRightAssemblerT>::AssemblerType> >
  operator*( const boost::shared_ptr<LeftAssemblerT> left,
             const boost::shared_ptr<Expr::matrix::Summation<RightLeftAssemblerT, RightRightAssemblerT> > right )
  {
    return boost::make_shared<ProdSum<LeftAssemblerT, RightLeftAssemblerT, RightRightAssemblerT> >( left, right );
  }

  /**
   * \brief operator '*' overloaded to produce a ProdDiff object
   * \param left the assembler object on the left of '*'
   * \param right the difference assembler on the right of '*'
   */
  template<typename LeftAssemblerT,
           typename RightLeftAssemblerT,
           typename RightRightAssemblerT>
  boost::shared_ptr<
  ProdDiff<typename DefinedIfSameType<typename       LeftAssemblerT::AssemblerType,       LeftAssemblerT>::AssemblerType,
           typename DefinedIfSameType<typename  RightLeftAssemblerT::AssemblerType,  RightLeftAssemblerT>::AssemblerType,
           typename DefinedIfSameType<typename RightRightAssemblerT::AssemblerType, RightRightAssemblerT>::AssemblerType> >
  operator*( const boost::shared_ptr<LeftAssemblerT> left,
             const boost::shared_ptr<Expr::matrix::Difference<RightLeftAssemblerT, RightRightAssemblerT> > right )
  {
    return boost::make_shared<ProdDiff<LeftAssemblerT, RightLeftAssemblerT, RightRightAssemblerT> >( left, right );
  }

}
}

#endif /** COMPOUNDS_H_ */
