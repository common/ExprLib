/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef SPARSEMATRIX_H_
#define SPARSEMATRIX_H_

#include <expression/matrix-assembly/AssemblerBase.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class SparseMatrix
   *
   * This class is a generic sparse matrix that is capable of all four operations:
   * emplacement, addition-in, subtraction-from, and right multiplication. It should
   * be used when a matrix is sparse - DenseSubMatrix is the other general-purpose matrix
   * and should be used if matrices are expected to be full.
   *
   * A SparseMatrix is constructed without arguments, and elements should be added
   * with the parentheses operator, as M(i,j) = aTag; where i is the row index, j
   * is the column index, and aTag is the field tag that goes in element (i,j).
   *
   * The finalize() function MUST BE CALLED before a SparseMatrix can be used.
   *
   * An example construction and filling procedure is:
   *
   * \code
   * SparseMatrix<FieldT> M;
   * M(0,0) = aTag;
   * M(1,0) = bTag;
   * M(1,1) = cTag;
   * M.finalize();
   * \endcode
   */
  template<typename ElementFieldT>
  class SparseMatrix : public AssemblerBase<ElementFieldT>
  {
  protected:
    using OrdinalPairType               = Expr::matrix::OrdinalPairType;
    using OrdinalPairHandleMapFieldType = Expr::matrix::OrdinalPairHandleMapType<ElementFieldT>;
    using OrdinalRowColMapFieldType     = Expr::matrix::OrdinalRowColMapType<ElementFieldT>;

    OrdinalPairHandleMapFieldType pairFieldMap_;
    OrdinalRowColMapFieldType rowFieldMap_;
    OrdinalRowColMapFieldType colFieldMap_;

    Expr::matrix::OrdinalType minMatrixSize_ = 0;

    void check_bounds( const Expr::matrix::OrdinalType nrows ) const
    {
      if( minMatrixSize_ > nrows ){
        const std::string nrowsStr = boost::lexical_cast<std::string>( nrows );
        const std::string minMatrixSizeStr = boost::lexical_cast<std::string>( minMatrixSize_ );
        throw std::runtime_error( "Error! Matrix with " + nrowsStr + " rows is not big enough to hold "
                                  "a sparse matrix with minimum size of " + minMatrixSizeStr + "." );
      }
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = SparseMatrix<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct an empty sparse matrix
     */
    SparseMatrix() {}

    /*
     * \brief construct a named object
     */
    SparseMatrix( const std::string name ) : AssemblerBase<ElementFieldT>( name ) {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "SparseMatrix( " + AssemblerBase<ElementFieldT>::name_ + " )";
    }

    /**
     * \brief print the matrix mapping
     */
    void print_map()
    {
      for( const auto& e : pairFieldMap_ ){
        std::cout << "(" << e.first.first << ", " << e.first.second << ") -> " << e.second->description() << '\n';
      }
    }

    /**
     * \brief set the tag or value associated with an element in this matrix, identified by row and column index
     * \param rowIdx the row index
     * \param colIdx the column index
     *
     * This gives access to a Handle by reference, so one should write,
     *
     * \code
     * M->element<FieldType>(i,j) = aTag
     * \endcode,
     *
     * for instance, to set the i,j element to the field associated with aTag.
     *
     * The specified type can be one of three things:
     *
     * 1. ElementType - the type of the elementary fields of the matrix
     * 2. SingleValueField
     * 3. double - in this case a double must be provided on the RHS of the assignment expression, instead of a tag.
     */
    template<typename T>
    Expr::matrix::Handle<ElementType>& element( const Expr::matrix::OrdinalType rowIdx,
                                          const Expr::matrix::OrdinalType colIdx )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "SparseMatrix is finalized! Can't insert into it!" );
      }
      const OrdinalPairType pair = std::make_pair( rowIdx, colIdx );
      const auto& iter = pairFieldMap_.find( pair );
      if( std::is_same<T,ElementType>::value ){
        if( iter == pairFieldMap_.end() ){
          pairFieldMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::ELEMENTTYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *pairFieldMap_[pair];
      }
      else if( std::is_same<T,SpatialOps::SingleValueField>::value ){
        if( iter == pairFieldMap_.end() ){
          pairFieldMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::SINGLEVALUEFIELDTYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *pairFieldMap_[pair];
      }
      else if( std::is_same<T,double>::value ){
        if( iter == pairFieldMap_.end() ){
          pairFieldMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::DOUBLETYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *pairFieldMap_[pair];
      }
      else{
        throw std::runtime_error( "matrix element must be an ElementType, SingleValueField, or double!" );
      }
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      AssemblerBase<ElementFieldT>::finalize();
      AssemblerBase<ElementFieldT>::set_required_handles( Expr::matrix::extract_unique_values( pairFieldMap_ ) );

      rowFieldMap_ = Expr::matrix::rowify_element_map( pairFieldMap_ );
      colFieldMap_ = Expr::matrix::colify_element_map( pairFieldMap_ );

      for( const auto& m : pairFieldMap_ ){
        minMatrixSize_ = std::max( minMatrixSize_, std::max( m.first.first, m.first.second ) );
      }

      pairFieldMap_.clear();
    }

    /**
     * \brief show the structure of this sparse matrix
     *
     * Prints initialized index pairs and the handles associated with them.
     */
    std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      for( const auto& r : rowFieldMap_ ){
        const Expr::matrix::OrdinalType rowIdx = r.first;
        const std::string rowIdxStr = boost::lexical_cast<std::string>( rowIdx );
        for( const auto& c : r.second ){
          const auto h = c.second;
          const std::string colIdxStr = boost::lexical_cast<std::string>( c.first );
          structure += "  (" + rowIdxStr + ", " + colIdxStr + ") -> " + h->description() + "\n";
        }
      }
      return structure;
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();
      check_bounds( nrows );

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      const auto& r = rowFieldMap_.find( rowIdx );
      if( r != rowFieldMap_.end() ){
        for( const auto& c : r->second ){
          const auto h = c.second;
          if( h->is_double() ){ row(c.first) <<= h->double_value();  }
          else{
            if( h->is_svf() ) { row(c.first) <<= h->svf_field();     }
            else              { row(c.first) <<= h->element_field(); }
          }
        }
      }
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      const auto& rowIdxIter = rowFieldMap_.find( rowIdx );
      if( rowIdxIter != rowFieldMap_.end() ){
        for( const auto& c : rowIdxIter->second ){
          const auto h = c.second;
          if( h->is_double() ){ row(c.first) <<= row(c.first) + h->double_value();  }
          else{
            if( h->is_svf() ) { row(c.first) <<= row(c.first) + h->svf_field();     }
            else              { row(c.first) <<= row(c.first) + h->element_field(); }
          }
        }
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      const auto& rowIdxIter = rowFieldMap_.find( rowIdx );
      if( rowIdxIter != rowFieldMap_.end() ){
        for( const auto& c : rowIdxIter->second ){
          const auto h = c.second;
          if( h->is_double() ){ row(c.first) <<= row(c.first) - h->double_value();  }
          else{
            if( h->is_svf() ) { row(c.first) <<= row(c.first) - h->svf_field();     }
            else              { row(c.first) <<= row(c.first) - h->element_field(); }
          }
        }
      }
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType nrows = row.elements();
      check_bounds( nrows );

      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::SpatFldPtr<ElementType> tempPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= row(colIdx);
      }


      for( Expr::matrix::OrdinalType colIdxToAssign = 0; colIdxToAssign < nrows; ++colIdxToAssign ){
        *tempPtr <<= 0.0;

        const auto& colIdxIter = colFieldMap_.find( colIdxToAssign );
        if( colIdxIter != colFieldMap_.end() ){
          for( const auto& rowToIter : colIdxIter->second ){
            const auto h = rowToIter.second;
            if( h->is_double() ){ *tempPtr <<= *tempPtr + tempRow(rowToIter.first) * h->double_value();  }
            else{
              if( h->is_svf() ) { *tempPtr <<= *tempPtr + tempRow(rowToIter.first) * h->svf_field();     }
              else              { *tempPtr <<= *tempPtr + tempRow(rowToIter.first) * h->element_field(); }
            }
          }
        }
        row(colIdxToAssign) <<= *tempPtr;
      }
    }

  };

} // namespace matrix
}

#endif /** SPARSEMATRIX_H_ */
