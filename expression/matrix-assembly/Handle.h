/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef HANDLE_H_
#define HANDLE_H_

#include <map>

#include <expression/Tag.h>
#include <expression/FieldManager.h>
#include <expression/FieldRequest.h>

#include <spatialops/structured/FVStaggeredFieldTypes.h>  // SpatialOps::SingleValueField

namespace Expr
{
namespace matrix
{

  /**
   * \class Handle
   *
   * A Handle is used by a matrix assembler to access a field and or a field's sensitivities.
   * Handles should never appear in user code, only in matrix assemblers.
   * If you aren't writing a custom matrix assembler, and you work with Handles,
   * then you're probably doing something quite wrong.
   *
   * In case you do have to deal with Handles...
   *
   * 1. Make a Handle with the empty constructor!
   *    Often shared pointers are used for Handles, so use \code make_shared<Handle>() \endcode with no arguments.
   *
   * 2. Assign/initialize a Handle with the assignment operator and a tag or tag pair.
   *    Assignment to a tag means the Handle will provide the field associated with the tag,
   *    while assignment to a tag pair (a,b) means that the Handle will provide the sensitivity
   *    field of a with respect to b.
   *
   * 3. A Handle's field request must be made from within the Expression that owns the matrix assembler
   *    that owns the handle. This is accomplished with the \code create_field_request( Expression expr ) \endcode
   *    function called directly off the handle itself. The owning expression is passed as \code (*this) \endcode.
   *
   * 4. A field may be grabbed from a Handle with the \code field() \endcode method. If the Handle was
   *    made with a tag pair then it is a 'sensitivity handle' and it will return a sensitivity field.
   *    Otherwise it returns the field asssociated with the tag to which it was assigned.
   *
   * 5. Whether or not a Handle has been assigned to a tag may be checked with the \code is_initialized() \endcode method.
   *    The \code is_field_attached() \endcode method tells whether or not the Handle's field request has been created.
   *    A Handle cannot be used if it has not been assigned and the field cannot be obtained if the request has not been created.
   */
  template<typename FieldT>
  class Handle
  {
  public:
    /**
     * \enum FieldType
     */
    enum FieldType { ELEMENTTYPE, SINGLEVALUEFIELDTYPE, DOUBLETYPE };

  protected:
    Expr::Tag fieldTag_;
    Expr::Tag sensVarTag_;
    bool isSens_              = false;
    bool isInitialized_       = false;
    bool fieldRequestCreated_ = false;
    bool isSingleValueField_ = false;

    using FieldRequestPtr = boost::shared_ptr< const Expr::FieldRequest<FieldT> >;
    FieldRequestPtr fieldRequest_;

    using FieldRequestSVFPtr = boost::shared_ptr< const Expr::FieldRequest<SpatialOps::SingleValueField> >;
    FieldRequestSVFPtr fieldRequestSVF_;

    double value_ = 0.0;
    bool isDouble_    = false;
    bool isDoubleSet_ = false;

    const FieldType fieldType_;

    template< typename T >
    void check_for_inf_or_nan_debug_only( const T& field ) const
    {
#ifndef NDEBUG
#ifndef ENABLE_CUDA
      check_for_inf_or_nan_debug_only( nebo_sum_interior( field ) );
#endif
#endif
    }

    void check_for_inf_or_nan_debug_only( const double& value ) const
    {
#ifndef NDEBUG
#ifndef ENABLE_CUDA
      if( std::isnan( value ) ){
        std::ostringstream msg;
        msg << std::endl
            << "Error in matrix assembly, NaN found in Handle with description: " << description() << std::endl;
        throw std::runtime_error( msg.str() );
      }
      if( std::isinf( value ) ){
        std::ostringstream msg;
        msg << std::endl
            << "Error in matrix assembly, Inf found in Handle with description: " << description() << std::endl;
        throw std::runtime_error( msg.str() );
      }
#endif
#endif
    }



  public:
    /**
     * \brief make an empty Handle of a particular type
     * \param fieldType the type of Handle, specified as a FieldType enumeration
     */
    Handle( const FieldType fieldType )
    : fieldType_( fieldType )
    {
      switch( fieldType ){
        case SINGLEVALUEFIELDTYPE: isSingleValueField_ = true; break;
        case DOUBLETYPE:           isDouble_           = true; break;
        case ELEMENTTYPE:                                      break;
      }
    }

    /**
     * \brief assign a function tag to this Handle
     * \param funTag the tag being assigned in
     *
     * This sets the Handle to a value-only (no sensitivity) field Handle
     */
    void operator=( const Expr::Tag& funTag )
    {
      fieldTag_ = funTag;
      isSens_ = false;
      isInitialized_ = true;
    }

    /**
     * \brief assign a function tag and variable tag to this Handle
     * \param funVarPair a pair of functionTag, variableTag in this order
     *
     * This sets the Handle to a sensitivity field Handle
     */
    void operator=( const std::pair<Expr::Tag,Expr::Tag>& funVarPair )
    {
      fieldTag_ = funVarPair.first;
      sensVarTag_ = funVarPair.second;
      isSens_ = true;
      isInitialized_ = true;
    }

    /**
     * \brief assign a double-precision number to this Handle
     * \param value the double-precision number
     *
     * This sets the Handle to a value-only double handle
     */
    void operator=( const double value )
    {
      value_ = value;
      isInitialized_ = true;
      isDoubleSet_ = true;
    }

    /**
     * \brief create this Handle's field request in an Expression
     * \param expr the Expression, provided as (*this), in which this Handle is being used
     */
    template<typename ExprFieldT>
    void create_field_request( Expr::Expression<ExprFieldT>& expr )
    {
      if( !isDouble_ ){
        if( !isSingleValueField_ ){
          fieldRequest_ = expr.template create_field_request<FieldT>( fieldTag_ );
          if( isSens_ ){
            const_cast< Expr::FieldRequest<FieldT>& >( *fieldRequest_ ).add_sensitivity( sensVarTag_ );
          }
          fieldRequestCreated_ = true;
        }
        else{
          fieldRequestSVF_ = expr.template create_field_request<SpatialOps::SingleValueField>( fieldTag_ );
          if( isSens_ ){
            const_cast< Expr::FieldRequest<SpatialOps::SingleValueField>& >( *fieldRequestSVF_ ).add_sensitivity( sensVarTag_ );
          }
          fieldRequestCreated_ = true;
        }
      }
    }

    /**
     * \brief obtain a field reference to this Handle's field, for element field types
     */
    const FieldT& element_field() const
    {
      if( !isInitialized_ ){
        throw std::runtime_error( "Handle.element_field() called without being initialized!" );
      }
      else{
        if( !isDouble_ ){
          if( !fieldRequestCreated_ ){
            throw std::runtime_error( "Handle.element_field() called before the field request was created!" );
          }
          else{
            if( !isSens_ ){
              check_for_inf_or_nan_debug_only( fieldRequest_->field_ref() );
              return fieldRequest_->field_ref();
            }
            else{
              check_for_inf_or_nan_debug_only( fieldRequest_->sens_field_ref( sensVarTag_ ) );
              return fieldRequest_->sens_field_ref( sensVarTag_ );
            }
          }
        }
      }
      check_for_inf_or_nan_debug_only( fieldRequest_->field_ref() );
      return fieldRequest_->field_ref();
    }

    /**
     * \brief obtain a field reference to this Handle's field, for single value field types
     */
    const SpatialOps::SingleValueField& svf_field() const
    {
      if( !isInitialized_ ){
        throw std::runtime_error( "Handle.svf_field() called without being initialized!" );
      }
      else{
        if( !isDouble_ ){
          if( !fieldRequestCreated_ ){
            throw std::runtime_error( "Handle.svf_field() called before the field request was created!" );
          }
          else{
            if( !isSens_ ){
              check_for_inf_or_nan_debug_only( fieldRequestSVF_->field_ref() );
              return fieldRequestSVF_->field_ref();
            }
            else{
              check_for_inf_or_nan_debug_only( fieldRequestSVF_->field_ref() );
              return fieldRequestSVF_->sens_field_ref( sensVarTag_ );
            }
          }
        }
      }
      check_for_inf_or_nan_debug_only( fieldRequest_->field_ref() );
      return fieldRequestSVF_->field_ref();
    }

    /**
     * \brief return the double associated with this Handle, if it is a double Handle
     */
    double double_value() const
    {
      if( !isInitialized_ ){
        throw std::runtime_error( "Handle.double_value() called without being initialized!" );
      }
      else{
        if( isDouble_ ){
          check_for_inf_or_nan_debug_only( value_ );
          return value_;
        }
        else{
          throw std::runtime_error( "Handle.double_value() called on a non-double Handle!" );
        }
      }
    }

    /**
     * \brief get the name associated with this Handle's field tag
     */
    std::string field_name() const { return fieldTag_.name(); }

    /**
     * \brief get a one-line description of this handle
     */
    std::string description() const
    {
      if( !isInitialized_ ){
        return "[]";
      }
      else{
        if( isDouble_ ){
          return "double: " + boost::lexical_cast<std::string>( value_ );
        }
        else{
          if( isSens_ ){
            return "d " + fieldTag_.name() + " / d " + sensVarTag_.name();
          }
          else{
            return fieldTag_.name();
          }
        }
      }
    }

    /**
     * \brief get boolean if Handle has a sensitivity (true) or is value-only (false)
     */
    bool has_sensitivity() const { return isSens_; }

    /**
     * \brief get the Handle's field tag
     */
    Expr::Tag field_tag() const { return fieldTag_; }

    /**
     * \brief get the Handle's sensitivity variable tag
     */
    Expr::Tag sens_var_tag() const{ return sensVarTag_; }

    /**
     * \brief whether or not the Handle has been assigned to a tag (true) or not (false)
     */
    bool is_initialized() const { return isInitialized_; }

    /**
     * \brief whether or not the Handle has a double precision number (true) or a field (false)
     */
    bool is_double() const { return isDouble_; }

    /**
     * \brief whether or not the Handle holds a single value field (true) or not (false)
     */
    bool is_svf() const { return isSingleValueField_; }

    /**
     * \brief whether or not the field has been created (true) or not (false)
     */
    bool is_field_attached() const { return fieldRequestCreated_; }

    /**
     * \brief change this Handle to a svf holder
     */
    void set_to_svf()
    {
      if( isInitialized_ ){
        throw std::runtime_error( "Handle has already been initialized. Cannot set to SVF!" );
      }
      fieldType_ = SINGLEVALUEFIELDTYPE;
    }

    /**
     * \brief change this Handle to a double holder
     */
    void set_to_double()
    {
      if( isInitialized_ ){
        throw std::runtime_error( "Handle has already been initialized. Cannot set to double!" );
      }
      fieldType_ = DOUBLETYPE;
    }
  };

} // namespace matrix
}

#endif /** HANDLE_H_ */
