/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef EXPRLIB_INVISCIDFLUXASSEMBLER_H
#define EXPRLIB_INVISCIDFLUXASSEMBLER_H


#include <spatialops/structured/IntVec.h>
#include <expression/matrix-assembly/CustomAssembler.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class InviscidFluxAssembler
   *
   */
  template<typename SVolFieldT>
  class InviscidFluxAssembler : public CustomAssembler<SVolFieldT>
  {
  protected:
    using OrdinalType = Expr::matrix::OrdinalType;
    using OrdinalVecType = std::vector<OrdinalType>;
    using HandleType = Expr::matrix::Handle<SVolFieldT>;
    using HandlePtrType = boost::shared_ptr<HandleType>;
    using HandlePtrVecType = std::vector<HandlePtrType>;
    using HandlePtrSetType = std::set<HandlePtrType>;
    using CustomAssemblerType = Expr::matrix::CustomAssembler<SVolFieldT>;

    using IntVec = SpatialOps::IntVec;
    const IntVec noOffsetPoint_ = IntVec(1, 1, 1);
    const IntVec xOffsetPoint_  = IntVec(2, 1, 1);
    const IntVec yOffsetPoint_  = IntVec(1, 2, 1);
    const IntVec zOffsetPoint_  = IntVec(1, 1, 2);

    const bool doX_ = false;
    const bool doY_ = false;
    const bool doZ_ = false;
    const bool doSpecies_ = false;

    const double universalGasConstant_;

    const OrdinalType numberSpecies_;
    const OrdinalType densityConservedIdx_;
    const OrdinalType densityPrimitiveIdx_;
    const OrdinalType rhoEgyIndex_;
    const OrdinalType temperatureIndex_;
    const OrdinalType xMomIndex_, yMomIndex_, zMomIndex_;
    const OrdinalType xVelIndex_, yVelIndex_, zVelIndex_;

    std::vector<double> invMolecularWeights_;

    OrdinalVecType speciesDensityIndices_;
    OrdinalVecType massFracIndices_;

    HandlePtrVecType enthalpies_, massFractions_;

    HandlePtrType xCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType yCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType zCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType density_        = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType pressure_       = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType temperature_    = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType heatCapacityCp_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType totalVolumetricEnthalpy_  = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType xVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType yVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType zVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    void check_indices()
    {
      std::set<OrdinalType> conservedIndices_, primitiveIndices_;
      OrdinalType conservedCount = 0;
      OrdinalType primitiveCount = 0;
      conservedIndices_.insert( densityConservedIdx_ ); primitiveIndices_.insert( densityPrimitiveIdx_ ); conservedCount++; primitiveCount++;
      if( doX_ ) { conservedIndices_.insert( xMomIndex_ ); primitiveIndices_.insert( xVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doY_ ) { conservedIndices_.insert( yMomIndex_ ); primitiveIndices_.insert( yVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doZ_ ) { conservedIndices_.insert( zMomIndex_ ); primitiveIndices_.insert( zVelIndex_ ); conservedCount++; primitiveCount++; }
      conservedIndices_.insert( rhoEgyIndex_ ); primitiveIndices_.insert( temperatureIndex_ ); conservedCount++; primitiveCount++;
      if( doSpecies_ ){
        for( OrdinalType i=0; i<numberSpecies_ - 1; ++i ){
          conservedIndices_.insert( speciesDensityIndices_[i] );
          primitiveIndices_.insert( massFracIndices_[i] );
          conservedCount++;
          primitiveCount++;
        }
      }
      assert( conservedIndices_.size() == conservedCount );
      assert( primitiveIndices_.size() == primitiveCount );
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = InviscidFluxAssembler<SVolFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename Expr::matrix::AssemblerBase<SVolFieldT>::ElementType;
    using VectorType  = typename Expr::matrix::AssemblerBase<SVolFieldT>::VectorType;

    using CustomAssemblerType::assemble;

    /**
     * @brief build an inviscid flux matrix assembler
     *
     * @param assemblerName          A string identifier for the assembler
     * @param universalGasConstant   The universal gas constant
     * @param molecularWeights       A vector of species molecular weights
     * @param densityTag             Tag for the mixture mass density
     * @param heatCapacityCpTag      Tag for the mixture specific heat capacity at constant pressure
     * @param totalEnthalpyTag       Tag for the mixture volumetric total enthalpy
     * @param xTag                   Tag for the x-coordinate of volume fields
     * @param yTag                   Tag for the y-coordinate of volume fields
     * @param zTag                   Tag for the z-coordinate of volume fields
     * @param vxTag                  Tag for the x-velocity (set to an empty tag if there is no x-velocity)
     * @param vyTag                  Tag for the y-velocity (set to an empty tag if there is no y-velocity)
     * @param vzTag                  Tag for the z-velocity (set to an empty tag if there is no z-velocity)
     * @param pressureTag            Tag for the pressure
     * @param temperatureTag         Tag for the temperature
     * @param enthalpyTags           Vector of tags for the specific species enthalpies (pass an empty vector if the fluid is not a mixture)
     * @param massFractionTags       Vector of tags for the species mass fractions (pass an empty vector if the fluid is not a mixture)
     * @param densityConservedIndex  Index of the density in the conserved variable vector
     * @param densityPrimitiveIndex  Index of the density in the primitive variable vector
     * @param energyIndex            Index of the energy in the conserved variable vector
     * @param temperatureIndex       Index of the temperature in the primitive variable vector
     * @param xMomIndex              Index of the x-momentum in the conserved variable vector (only applies if x-velocity tag is non-empty)
     * @param yMomIndex              Index of the y-momentum in the conserved variable vector (only applies if y-velocity tag is non-empty)
     * @param zMomIndex              Index of the z-momentum in the conserved variable vector (only applies if z-velocity tag is non-empty)
     * @param xVelIndex              Index of the x-velocity in the primitive variable vector (only applies if x-velocity tag is non-empty)
     * @param yVelIndex              Index of the y-velocity in the primitive variable vector (only applies if y-velocity tag is non-empty)
     * @param zVelIndex              Index of the z-velocity in the primitive variable vector (only applies if z-velocity tag is non-empty)
     * @param speciesDensityIndices  Vector of indices of the species densities in the conserved variable vector
     * @param massFractionIndices    Vector of indices of the species mass fractions in the primitive variable vector
     */
    InviscidFluxAssembler( const std::string assemblerName,
                           const double universalGasConstant,
                           const std::vector<double>& molecularWeights,
                           const Expr::Tag& densityTag,
                           const Expr::Tag& heatCapacityCpTag,
                           const Expr::Tag& totalEnthalpyTag,
                           const Expr::Tag& xTag,
                           const Expr::Tag& yTag,
                           const Expr::Tag& zTag,
                           const Expr::Tag& vxTag,
                           const Expr::Tag& vyTag,
                           const Expr::Tag& vzTag,
                           const Expr::Tag& temperatureTag,
                           const Expr::Tag& pressureTag,
                           const Expr::TagList& enthalpyTags,
                           const Expr::TagList& massFractionTags,
                           const OrdinalType densityConservedIndex,
                           const OrdinalType densityPrimitiveIndex,
                           const OrdinalType energyIndex,
                           const OrdinalType temperatureIndex,
                           const OrdinalType xMomIndex,
                           const OrdinalType yMomIndex,
                           const OrdinalType zMomIndex,
                           const OrdinalType xVelIndex,
                           const OrdinalType yVelIndex,
                           const OrdinalType zVelIndex,
                           const OrdinalVecType& speciesDensityIndices,
                           const OrdinalVecType& massFractionIndices )
      : CustomAssembler<SVolFieldT>( assemblerName ),
        doX_( vxTag != Expr::Tag() ),
        doY_( vyTag != Expr::Tag() ),
        doZ_( vzTag != Expr::Tag() ),
        doSpecies_( massFractionTags.size() ),
        universalGasConstant_( universalGasConstant ),
        numberSpecies_( massFractionTags.size() ),
        densityConservedIdx_( densityConservedIndex ),
        densityPrimitiveIdx_( densityPrimitiveIndex ),
        rhoEgyIndex_( energyIndex ),
        temperatureIndex_( temperatureIndex ),
        xMomIndex_( xMomIndex ),
        yMomIndex_( yMomIndex ),
        zMomIndex_( zMomIndex ),
        xVelIndex_( xVelIndex ),
        yVelIndex_( yVelIndex ),
        zVelIndex_( zVelIndex )
    {
      *xCoord_ = xTag;
      *yCoord_ = yTag;
      *zCoord_ = zTag;

      *density_        = densityTag;
      *pressure_       = pressureTag;
      *temperature_    = temperatureTag;
      *heatCapacityCp_ = heatCapacityCpTag;

      *totalVolumetricEnthalpy_ = totalEnthalpyTag;

      if( doX_ ) *xVelocity_ = vxTag;
      if( doY_ ) *yVelocity_ = vyTag;
      if( doZ_ ) *zVelocity_ = vzTag;

      assert( massFractionTags.size() == enthalpyTags.size() );
      assert( massFractionTags.size() == molecularWeights.size() );
      assert( speciesDensityIndices.size() == massFractionIndices.size() );

      if( doSpecies_ ){
        for( std::size_t i=0; i<numberSpecies_; ++i ){
          enthalpies_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *enthalpies_[i] = enthalpyTags[i];

          massFractions_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *massFractions_[i] = massFractionTags[i];

          invMolecularWeights_.push_back( 1. / molecularWeights[i] );
        }

        for( std::size_t i=0; i<numberSpecies_ - 1; ++i ){
          speciesDensityIndices_.push_back( speciesDensityIndices[i] );
          massFracIndices_.push_back( massFractionIndices[i] );
        }
      }

      check_indices();

      finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "InviscidFluxAssembler( " + Expr::matrix::AssemblerBase<SVolFieldT>::name_ + " )";
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      Expr::matrix::AssemblerBase<SVolFieldT>::finalize();

      CustomAssemblerType::rowMap_.insert( std::make_pair( densityConservedIdx_, OrdinalVecType() ) );
      CustomAssemblerType::rowMap_.insert( std::make_pair( rhoEgyIndex_, OrdinalVecType() ) );
      if( doX_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( xMomIndex_, OrdinalVecType() ) );
      if( doY_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( yMomIndex_, OrdinalVecType() ) );
      if( doZ_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( zMomIndex_, OrdinalVecType() ) );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_.insert( std::make_pair( speciesDensityIndices_[i], OrdinalVecType() ) );
        }
      }

      CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( densityPrimitiveIdx_ );
      if( doX_ ) CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( xVelIndex_ );
      if( doY_ ) CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( yVelIndex_ );
      if( doZ_ ) CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( zVelIndex_ );

      CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( densityPrimitiveIdx_ );
      CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( temperatureIndex_ );
      if( doX_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( xVelIndex_ );
      if( doY_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( yVelIndex_ );
      if( doZ_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( zVelIndex_ );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( massFracIndices_[i] );
        }
      }

      if( doX_ ){
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( temperatureIndex_ );
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( xVelIndex_ );
        if( doY_ ) CustomAssemblerType::rowMap_[xMomIndex_].push_back( yVelIndex_ );
        if( doZ_ ) CustomAssemblerType::rowMap_[xMomIndex_].push_back( zVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[xMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doY_ ){
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( temperatureIndex_ );
        if( doX_ ) CustomAssemblerType::rowMap_[yMomIndex_].push_back( xVelIndex_ );
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( yVelIndex_ );
        if( doZ_ ) CustomAssemblerType::rowMap_[yMomIndex_].push_back( zVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[yMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doZ_ ){
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( temperatureIndex_ );
        if( doX_ ) CustomAssemblerType::rowMap_[zMomIndex_].push_back( xVelIndex_ );
        if( doY_ ) CustomAssemblerType::rowMap_[zMomIndex_].push_back( yVelIndex_ );
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( zVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[zMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( densityPrimitiveIdx_ );
          if( doX_ ) CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( xVelIndex_ );
          if( doY_ ) CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( yVelIndex_ );
          if( doZ_ ) CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( zVelIndex_ );
          for( OrdinalType j=0; j < numberSpecies_ - 1; ++j ){
            CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( massFracIndices_[j] );
          }
        }
      }

      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( xCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( yCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( zCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( density_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( pressure_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( temperature_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( totalVolumetricEnthalpy_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( heatCapacityCp_ );
      if( doX_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( xVelocity_ );
      if( doY_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( yVelocity_ );
      if( doZ_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( zVelocity_ );
      if( doSpecies_ ){
        for( const auto& enthalpy : enthalpies_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( enthalpy );
        }
        for( const auto& fraction : massFractions_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( fraction );
        }
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const OrdinalType nrows = row.elements();

      for( OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      const double dx = xCoord_->element_field()(xOffsetPoint_) - xCoord_->element_field()(noOffsetPoint_);
      const double dy = yCoord_->element_field()(yOffsetPoint_) - yCoord_->element_field()(noOffsetPoint_);
      const double dz = zCoord_->element_field()(zOffsetPoint_) - zCoord_->element_field()(noOffsetPoint_);

      const double invDx = doX_ ? 1. / dx : 0.;
      const double invDy = doY_ ? 1. / dy : 0.;
      const double invDz = doZ_ ? 1. / dz : 0.;

      SpatialOps::SpatFldPtr<ElementType> xCoeffPtr       = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> yCoeffPtr       = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> zCoeffPtr       = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> velDotCoeffsPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ); *velDotCoeffsPtr <<= 0.0;

      if( doX_ ) *xCoeffPtr <<= cond( xVelocity_->element_field() > 0., invDx )( -invDx );
      if( doY_ ) *yCoeffPtr <<= cond( yVelocity_->element_field() > 0., invDy )( -invDy );
      if( doZ_ ) *zCoeffPtr <<= cond( zVelocity_->element_field() > 0., invDz )( -invDz );

      if( doX_ ) *velDotCoeffsPtr <<= *velDotCoeffsPtr + ( *xCoeffPtr * xVelocity_->element_field() );
      if( doY_ ) *velDotCoeffsPtr <<= *velDotCoeffsPtr + ( *yCoeffPtr * yVelocity_->element_field() );
      if( doZ_ ) *velDotCoeffsPtr <<= *velDotCoeffsPtr + ( *zCoeffPtr * zVelocity_->element_field() );

      bool isSpeciesRow = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), rowIdx ) != speciesDensityIndices_.end();

      if( rowIdx == densityConservedIdx_ ){
        row(densityPrimitiveIdx_) <<= *velDotCoeffsPtr;
        const SVolFieldT& rho = density_->element_field();
        if( doX_ ) row(xVelIndex_) <<= rho * *xCoeffPtr;
        if( doY_ ) row(yVelIndex_) <<= rho * *yCoeffPtr;
        if( doZ_ ) row(zVelIndex_) <<= rho * *zCoeffPtr;
      }
      else if( rowIdx == rhoEgyIndex_ ){
        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& ht  = totalVolumetricEnthalpy_->element_field();

        row(densityPrimitiveIdx_) <<= ht / rho * *velDotCoeffsPtr;
        row(temperatureIndex_) <<= rho * heatCapacityCp_->element_field() * *velDotCoeffsPtr;

        if( doX_ ) row(xVelIndex_) <<= rho * ( xVelocity_->element_field() * *velDotCoeffsPtr + ht / rho * *xCoeffPtr );
        if( doY_ ) row(yVelIndex_) <<= rho * ( yVelocity_->element_field() * *velDotCoeffsPtr + ht / rho * *yCoeffPtr );
        if( doZ_ ) row(zVelIndex_) <<= rho * ( zVelocity_->element_field() * *velDotCoeffsPtr + ht / rho * *zCoeffPtr );

        if( doSpecies_ ){
          const SVolFieldT& hn = enthalpies_[numberSpecies_-1]->element_field();
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const SVolFieldT& hi = enthalpies_[i]->element_field();
            row(massFracIndices_[i]) <<= rho * (hi - hn) * *velDotCoeffsPtr;
          }
        }

      }
      else if( rowIdx == xMomIndex_ && doX_ ){
        const SVolFieldT& vx  = xVelocity_->element_field();
        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& p   = pressure_->element_field();
        const SVolFieldT& T   = temperature_->element_field();

        row(densityPrimitiveIdx_) <<= vx * *velDotCoeffsPtr + p / rho * *xCoeffPtr;
        row(temperatureIndex_   ) <<= p / T * *xCoeffPtr;

        if( doX_ ) row(xVelIndex_) <<= rho * vx * *xCoeffPtr;
        if( doY_ ) row(yVelIndex_) <<= rho * vx * *yCoeffPtr;
        if( doZ_ ) row(zVelIndex_) <<= rho * vx * *zCoeffPtr;

        row(xVelIndex_) <<= row(xVelIndex_) + rho * *velDotCoeffsPtr;

        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            row(massFracIndices_[i]) <<= rho * universalGasConstant_ * T * ( invMolecularWeights_[i] - invMolecularWeights_[numberSpecies_ - 1] ) * *xCoeffPtr;
          }
        }
      }
      else if( rowIdx == yMomIndex_ && doY_ ){
        const SVolFieldT& vy  = yVelocity_->element_field();
        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& p   = pressure_->element_field();
        const SVolFieldT& T   = temperature_->element_field();

        row(densityPrimitiveIdx_) <<= vy * *velDotCoeffsPtr + p / rho * *yCoeffPtr;
        row(temperatureIndex_   ) <<= p / T * *yCoeffPtr;

        if( doX_ ) row(xVelIndex_) <<= rho * vy * *xCoeffPtr;
        if( doY_ ) row(yVelIndex_) <<= rho * vy * *yCoeffPtr;
        if( doZ_ ) row(zVelIndex_) <<= rho * vy * *zCoeffPtr;

        row(yVelIndex_) <<= row(yVelIndex_) + rho * *velDotCoeffsPtr;

        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            row(massFracIndices_[i]) <<= rho * universalGasConstant_ * T * ( invMolecularWeights_[i] - invMolecularWeights_[numberSpecies_ - 1] ) * *yCoeffPtr;
          }
        }
      }
      else if( rowIdx == zMomIndex_ && doZ_ ){
        const SVolFieldT& vz  = zVelocity_->element_field();
        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& p   = pressure_->element_field();
        const SVolFieldT& T   = temperature_->element_field();

        row(densityPrimitiveIdx_) <<= vz * *velDotCoeffsPtr + p / rho * *zCoeffPtr;
        row(temperatureIndex_   ) <<= p / T * *zCoeffPtr;

        if( doX_ ) row(xVelIndex_) <<= rho * vz * *xCoeffPtr;
        if( doY_ ) row(yVelIndex_) <<= rho * vz * *yCoeffPtr;
        if( doZ_ ) row(zVelIndex_) <<= rho * vz * *zCoeffPtr;

        row(zVelIndex_) <<= row(zVelIndex_) + rho * *velDotCoeffsPtr;

        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            row(massFracIndices_[i]) <<= rho * universalGasConstant_ * T * ( invMolecularWeights_[i] - invMolecularWeights_[numberSpecies_ - 1] ) * *zCoeffPtr;
          }
        }
      }
      else if( isSpeciesRow && doSpecies_ ){
        const auto& speciesIteratorFound = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), rowIdx );
        const OrdinalType speciesVectorIndex = speciesIteratorFound - speciesDensityIndices_.begin();

        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& Y   = massFractions_[speciesVectorIndex]->element_field();

        row(densityPrimitiveIdx_) <<= Y * *velDotCoeffsPtr;

        if( doX_ ) row(xVelIndex_) <<= rho * Y * *xCoeffPtr;
        if( doY_ ) row(yVelIndex_) <<= rho * Y * *yCoeffPtr;
        if( doZ_ ) row(zVelIndex_) <<= rho * Y * *zCoeffPtr;

        row(massFracIndices_[speciesVectorIndex]) <<= rho * *velDotCoeffsPtr;
      }
    }

  };

} // namespace matrix
} // namespace Expr

#endif //EXPRLIB_INVISCIDFLUXASSEMBLER_H
