/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef EXPRLIB_ACOUSTICPRECONDITIONER_H
#define EXPRLIB_ACOUSTICPRECONDITIONER_H


#include <spatialops/structured/IntVec.h>
#include <expression/matrix-assembly/CustomAssembler.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class AcousticPreconditionerAssembler
   *
   */
  template<typename SVolFieldT>
  class AcousticPreconditionerAssembler : public CustomAssembler<SVolFieldT>
  {
  protected:
    using OrdinalType = Expr::matrix::OrdinalType;
    using OrdinalVecType = std::vector<OrdinalType>;
    using HandleType = Expr::matrix::Handle<SVolFieldT>;
    using HandlePtrType = boost::shared_ptr<HandleType>;
    using HandlePtrVecType = std::vector<HandlePtrType>;
    using HandlePtrSetType = std::set<HandlePtrType>;
    using CustomAssemblerType = Expr::matrix::CustomAssembler<SVolFieldT>;

    using IntVec = SpatialOps::IntVec;
    const IntVec noOffsetPoint_ = IntVec(1, 1, 1);
    const IntVec xOffsetPoint_  = IntVec(2, 1, 1);
    const IntVec yOffsetPoint_  = IntVec(1, 2, 1);
    const IntVec zOffsetPoint_  = IntVec(1, 1, 2);

    const bool doX_ = false;
    const bool doY_ = false;
    const bool doZ_ = false;
    const bool doSpecies_ = false;

    const double universalGasConstant_;

    const OrdinalType numberSpecies_;
    const OrdinalType densityConservedIdx_;
    const OrdinalType densityPrimitiveIdx_;
    const OrdinalType rhoEgyIndex_;
    const OrdinalType temperatureIndex_;
    const OrdinalType xMomIndex_, yMomIndex_, zMomIndex_;
    const OrdinalType xVelIndex_, yVelIndex_, zVelIndex_;

    std::vector<double> invMolecularWeights_;

    OrdinalVecType speciesDensityIndices_;
    OrdinalVecType massFracIndices_;

    HandlePtrVecType energies_, massFractions_, diffusivities_;

    HandlePtrType xCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType yCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType zCoord_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType mmw_            = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType density_        = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType soundSpeed_     = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType pressure_       = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType temperature_    = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType heatCapacityCv_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType heatCapacityCp_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType totalEnergy_    = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType viscosity_    = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType conductivity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType xVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType yVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType zVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    void check_indices()
    {
      std::set<OrdinalType> conservedIndices_, primitiveIndices_;
      OrdinalType conservedCount = 0;
      OrdinalType primitiveCount = 0;
      conservedIndices_.insert( densityConservedIdx_ ); primitiveIndices_.insert( densityPrimitiveIdx_ ); conservedCount++; primitiveCount++;
      if( doX_ ) { conservedIndices_.insert( xMomIndex_ ); primitiveIndices_.insert( xVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doY_ ) { conservedIndices_.insert( yMomIndex_ ); primitiveIndices_.insert( yVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doZ_ ) { conservedIndices_.insert( zMomIndex_ ); primitiveIndices_.insert( zVelIndex_ ); conservedCount++; primitiveCount++; }
      conservedIndices_.insert( rhoEgyIndex_ ); primitiveIndices_.insert( temperatureIndex_ ); conservedCount++; primitiveCount++;
      if( doSpecies_ ){
        for( OrdinalType i=0; i<numberSpecies_ - 1; ++i ){
          conservedIndices_.insert( speciesDensityIndices_[i] );
          primitiveIndices_.insert( massFracIndices_[i] );
          conservedCount++;
          primitiveCount++;
        }
      }
      assert( conservedIndices_.size() == conservedCount );
      assert( primitiveIndices_.size() == primitiveCount );
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = AcousticPreconditionerAssembler<SVolFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename Expr::matrix::AssemblerBase<SVolFieldT>::ElementType;
    using VectorType  = typename Expr::matrix::AssemblerBase<SVolFieldT>::VectorType;

    using CustomAssemblerType::assemble;

    /**
     * @brief build an acoustic preconditioning matrix assembler
     *
     * @param assemblerName          A string identifier for the assembler
     * @param universalGasConstant   The universal gas constant
     * @param molecularWeights       A vector of species molecular weights
     * @param densityTag             Tag for the mixture mass density
     * @param heatCapacityCvTag      Tag for the mixture specific heat capacity at constant volume
     * @param heatCapacityCpTag      Tag for the mixture specific heat capacity at constant pressure
     * @param viscosityTag           Tag for the kinematic viscosity
     * @param conductivityTag        Tag for the thermal conductivity
     * @param totalEnergyTag         Tag for the mixture specific total energy
     * @param xTag                   Tag for the x-coordinate of volume fields
     * @param yTag                   Tag for the y-coordinate of volume fields
     * @param zTag                   Tag for the z-coordinate of volume fields
     * @param vxTag                  Tag for the x-velocity (set to an empty tag if there is no x-velocity)
     * @param vyTag                  Tag for the y-velocity (set to an empty tag if there is no y-velocity)
     * @param vzTag                  Tag for the z-velocity (set to an empty tag if there is no z-velocity)
     * @param sosTag                 Tag for the speed of sound
     * @param pressureTag            Tag for the pressure
     * @param temperatureTag         Tag for the temperature
     * @param mmwTag                 Tag for the mixture molecular weight
     * @param energyTags             Vector of tags for the specific species energies (pass an empty vector if the fluid is not a mixture)
     * @param massFractionTags       Vector of tags for the species mass fractions (pass an empty vector if the fluid is not a mixture)
     * @param diffusivityTags        Vector of tags for the species mixture-averaged diffusion coefficients (pass an empty vector if the fluid is not a mixture)
     * @param densityConservedIndex  Index of the density in the conserved variable vector
     * @param densityPrimitiveIndex  Index of the density in the primitive variable vector
     * @param energyIndex            Index of the energy in the conserved variable vector
     * @param temperatureIndex       Index of the temperature in the primitive variable vector
     * @param xMomIndex              Index of the x-momentum in the conserved variable vector (only applies if x-velocity tag is non-empty)
     * @param yMomIndex              Index of the y-momentum in the conserved variable vector (only applies if y-velocity tag is non-empty)
     * @param zMomIndex              Index of the z-momentum in the conserved variable vector (only applies if z-velocity tag is non-empty)
     * @param xVelIndex              Index of the x-velocity in the primitive variable vector (only applies if x-velocity tag is non-empty)
     * @param yVelIndex              Index of the y-velocity in the primitive variable vector (only applies if y-velocity tag is non-empty)
     * @param zVelIndex              Index of the z-velocity in the primitive variable vector (only applies if z-velocity tag is non-empty)
     * @param speciesDensityIndices  Vector of indices of the species densities in the conserved variable vector
     * @param massFractionIndices    Vector of indices of the species mass fractions in the primitive variable vector
     */
    AcousticPreconditionerAssembler( const std::string assemblerName,
                                     const double universalGasConstant,
                                     const std::vector<double>& molecularWeights,
                                     const Expr::Tag& densityTag,
                                     const Expr::Tag& heatCapacityCvTag,
                                     const Expr::Tag& heatCapacityCpTag,
                                     const Expr::Tag& viscosityTag,
                                     const Expr::Tag& conductivityTag,
                                     const Expr::Tag& totalEnergyTag,
                                     const Expr::Tag& xTag,
                                     const Expr::Tag& yTag,
                                     const Expr::Tag& zTag,
                                     const Expr::Tag& vxTag,
                                     const Expr::Tag& vyTag,
                                     const Expr::Tag& vzTag,
                                     const Expr::Tag& sosTag,
                                     const Expr::Tag& temperatureTag,
                                     const Expr::Tag& pressureTag,
                                     const Expr::Tag& mmwTag,
                                     const Expr::TagList& energyTags,
                                     const Expr::TagList& massFractionTags,
                                     const Expr::TagList& diffusivityTags,
                                     const OrdinalType densityConservedIndex,
                                     const OrdinalType densityPrimitiveIndex,
                                     const OrdinalType energyIndex,
                                     const OrdinalType temperatureIndex,
                                     const OrdinalType xMomIndex,
                                     const OrdinalType yMomIndex,
                                     const OrdinalType zMomIndex,
                                     const OrdinalType xVelIndex,
                                     const OrdinalType yVelIndex,
                                     const OrdinalType zVelIndex,
                                     const OrdinalVecType& speciesDensityIndices,
                                     const OrdinalVecType& massFractionIndices )
      : CustomAssembler<SVolFieldT>( assemblerName ),
        doX_( vxTag != Expr::Tag() ),
        doY_( vyTag != Expr::Tag() ),
        doZ_( vzTag != Expr::Tag() ),
        doSpecies_( massFractionTags.size() ),
        universalGasConstant_( universalGasConstant ),
        numberSpecies_( massFractionTags.size() ),
        densityConservedIdx_( densityConservedIndex ),
        densityPrimitiveIdx_( densityPrimitiveIndex ),
        rhoEgyIndex_( energyIndex ),
        temperatureIndex_( temperatureIndex ),
        xMomIndex_( xMomIndex ),
        yMomIndex_( yMomIndex ),
        zMomIndex_( zMomIndex ),
        xVelIndex_( xVelIndex ),
        yVelIndex_( yVelIndex ),
        zVelIndex_( zVelIndex )
    {
      *xCoord_ = xTag;
      *yCoord_ = yTag;
      *zCoord_ = zTag;

      *density_        = densityTag;
      *mmw_            = mmwTag;
      *soundSpeed_     = sosTag;
      *pressure_       = pressureTag;
      *temperature_    = temperatureTag;
      *heatCapacityCv_ = heatCapacityCvTag;
      *heatCapacityCp_ = heatCapacityCpTag;
      *viscosity_      = viscosityTag;
      *conductivity_   = conductivityTag;
      *totalEnergy_    = totalEnergyTag;

      if( doX_ ) *xVelocity_ = vxTag;
      if( doY_ ) *yVelocity_ = vyTag;
      if( doZ_ ) *zVelocity_ = vzTag;

      assert( massFractionTags.size() == energyTags.size() );
      assert( massFractionTags.size() == diffusivityTags.size() );
      assert( massFractionTags.size() == molecularWeights.size() );
      assert( speciesDensityIndices.size() == massFractionIndices.size() );

      if( doSpecies_ ){
        for( OrdinalType i=0; i<numberSpecies_; ++i ){
          energies_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *energies_[i] = energyTags[i];

          massFractions_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *massFractions_[i] = massFractionTags[i];

          diffusivities_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *diffusivities_[i] = diffusivityTags[i];

          invMolecularWeights_.push_back( 1. / molecularWeights[i] );
        }

        for( OrdinalType i=0; i<numberSpecies_ - 1; ++i ){
          speciesDensityIndices_.push_back( speciesDensityIndices[i] );
          massFracIndices_.push_back( massFractionIndices[i] );
        }
      }

      check_indices();

      finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "AcousticPreconditioner( " + Expr::matrix::AssemblerBase<SVolFieldT>::name_ + " )";
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      Expr::matrix::AssemblerBase<SVolFieldT>::finalize();

      CustomAssemblerType::rowMap_.insert( std::make_pair( densityConservedIdx_, OrdinalVecType() ) );
      CustomAssemblerType::rowMap_.insert( std::make_pair( rhoEgyIndex_, OrdinalVecType() ) );
      if( doX_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( xMomIndex_, OrdinalVecType() ) );
      if( doY_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( yMomIndex_, OrdinalVecType() ) );
      if( doZ_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( zMomIndex_, OrdinalVecType() ) );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_.insert( std::make_pair( speciesDensityIndices_[i], OrdinalVecType() ) );
        }
      }

      CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( densityPrimitiveIdx_ );
      CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( temperatureIndex_ );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[densityConservedIdx_].push_back( massFracIndices_[i] );
        }
      }

      CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( densityPrimitiveIdx_ );
      CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( temperatureIndex_ );
      if( doX_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( xVelIndex_ );
      if( doY_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( yVelIndex_ );
      if( doZ_ ) CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( zVelIndex_ );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[rhoEgyIndex_].push_back( massFracIndices_[i] );
        }
      }

      if( doX_ ){
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( temperatureIndex_ );
        CustomAssemblerType::rowMap_[xMomIndex_].push_back( xVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[xMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doY_ ){
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( temperatureIndex_ );
        CustomAssemblerType::rowMap_[yMomIndex_].push_back( yVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[yMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doZ_ ){
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( temperatureIndex_ );
        CustomAssemblerType::rowMap_[zMomIndex_].push_back( zVelIndex_ );
        if( doSpecies_ ){
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            CustomAssemblerType::rowMap_[zMomIndex_].push_back( massFracIndices_[i] );
          }
        }
      }

      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( densityPrimitiveIdx_ );
          CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( temperatureIndex_ );
          for( OrdinalType j=0; j < numberSpecies_ - 1; ++j ){
            CustomAssemblerType::rowMap_[speciesDensityIndices_[i]].push_back( massFracIndices_[j] );
          }
        }
      }

      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( xCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( yCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( zCoord_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( mmw_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( density_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( soundSpeed_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( pressure_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( temperature_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( totalEnergy_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( heatCapacityCv_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( heatCapacityCp_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( viscosity_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( conductivity_ );
      if( doX_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( xVelocity_ );
      if( doY_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( yVelocity_ );
      if( doZ_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( zVelocity_ );
      if( doSpecies_ ){
        for( const auto& diffusivity : diffusivities_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( diffusivity );
        }
        for( const auto& fraction : massFractions_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( fraction );
        }
        for( const auto& energy : energies_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( energy );
        }
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const OrdinalType nrows = row.elements();

      for( OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      const double dx = xCoord_->element_field()(xOffsetPoint_) - xCoord_->element_field()(noOffsetPoint_);
      const double dy = yCoord_->element_field()(yOffsetPoint_) - yCoord_->element_field()(noOffsetPoint_);
      const double dz = zCoord_->element_field()(zOffsetPoint_) - zCoord_->element_field()(noOffsetPoint_);

      const double invDx = doX_ ? 1. / dx : 0.;
      const double invDy = doY_ ? 1. / dy : 0.;
      const double invDz = doZ_ ? 1. / dz : 0.;
      const double invMinDx = std::max( invDx, std::max( invDy, invDz ) );

      SpatialOps::SpatFldPtr<ElementType> thetaPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> speedPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> maxDPtr  = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> rhoPPtr  = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      SpatialOps::SpatFldPtr<ElementType> rhoTPtr  = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );

      const double epsilon = 1.e-5;

      SVolFieldT& theta = *thetaPtr;
      SVolFieldT& speed = *speedPtr;
      SVolFieldT& rhoP  = *rhoPPtr;
      SVolFieldT& rhoT  = *rhoTPtr;
      SVolFieldT& maxD  = *maxDPtr;

      const SVolFieldT& rho = density_->element_field();
      const SVolFieldT& T   = temperature_->element_field();
      const SVolFieldT& cp  = heatCapacityCp_->element_field();
      const SVolFieldT& cv  = heatCapacityCv_->element_field();
      const SVolFieldT& a   = soundSpeed_->element_field();
      const SVolFieldT& p   = pressure_->element_field();
      const SVolFieldT& mmw = mmw_->element_field();

      rhoP <<= rho / p;
      rhoT <<= - rho / T;

      speed <<= 0.0;
      if( doX_ ) speed <<= speed + square( xVelocity_->element_field() );
      if( doY_ ) speed <<= speed + square( yVelocity_->element_field() );
      if( doZ_ ) speed <<= speed + square( zVelocity_->element_field() );
      speed <<= sqrt( speed );

      maxD <<= max( conductivity_->element_field() / (rho * cp),
                    viscosity_->element_field() / rho );

      if( doSpecies_ ){
        for( int i=0; i<diffusivities_.size(); ++i ){
          maxD <<= max( maxD, diffusivities_[i]->element_field() / rho );
        }
      }

      // first compute the reference velocity (use theta as a temporary)
      theta <<= max( max( speed,
                          epsilon * a ),
                     maxD * invMinDx );
      // then compute the value of theta = Theta / rho_p from the reference velocity
      theta <<= (1. / ( square( theta ) - rhoT / ( rho * cp ) ) ) / rhoP;

      bool isSpeciesRow = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), rowIdx ) != speciesDensityIndices_.end();

      if( rowIdx == densityConservedIdx_ ){
        row(densityPrimitiveIdx_) <<= theta;
        row(temperatureIndex_)    <<= (1. - theta) * rhoT;

        if( doSpecies_ ){
          const double invMn = invMolecularWeights_[numberSpecies_-1];
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const double invMi = invMolecularWeights_[i];
            row(massFracIndices_[i]) <<= (theta - 1.) * rho * mmw * ( invMi - invMn );
          }
        }
      }
      else if( rowIdx == rhoEgyIndex_ ){
        const SVolFieldT& et = totalEnergy_->element_field();

        row(densityPrimitiveIdx_) <<= theta * et;
        row(temperatureIndex_) <<= (1. - theta) * rhoT * et + rho * cv;

        if( doX_ ) row(xVelIndex_) <<= rho * xVelocity_->element_field();
        if( doY_ ) row(yVelIndex_) <<= rho * yVelocity_->element_field();
        if( doZ_ ) row(zVelIndex_) <<= rho * zVelocity_->element_field();

        if( doSpecies_ ){
          const double invMn = invMolecularWeights_[numberSpecies_-1];
          const SVolFieldT& en = energies_[numberSpecies_-1]->element_field();
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const double invMi = invMolecularWeights_[i];
            const SVolFieldT& ei = energies_[i]->element_field();
            row(massFracIndices_[i]) <<= rho * (ei - en) + rho * et * (theta - 1.) * mmw * ( invMi - invMn );
          }
        }

      }
      else if( rowIdx == xMomIndex_ && doX_ ){
        const SVolFieldT& vx  = xVelocity_->element_field();

        row(densityPrimitiveIdx_) <<= theta * vx;
        row(temperatureIndex_   ) <<= rhoT * (1. - theta) * vx;

        row(xVelIndex_) <<= rho;

        if( doSpecies_ ){
          const double invMn = invMolecularWeights_[numberSpecies_-1];
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const double invMi = invMolecularWeights_[i];
            row(massFracIndices_[i]) <<= (theta - 1.) * rho * vx * mmw * ( invMi - invMn );
          }
        }
      }
      else if( rowIdx == yMomIndex_ && doY_ ){
        const SVolFieldT& vy  = yVelocity_->element_field();

        row(densityPrimitiveIdx_) <<= theta * vy;
        row(temperatureIndex_   ) <<= rhoT * (1. - theta) * vy;

        row(yVelIndex_) <<= rho;

        if( doSpecies_ ){
          const double invMn = invMolecularWeights_[numberSpecies_-1];
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const double invMi = invMolecularWeights_[i];
            row(massFracIndices_[i]) <<= (theta - 1.) * rho * vy * mmw * ( invMi - invMn );
          }
        }
      }
      else if( rowIdx == zMomIndex_ && doZ_ ){
        const SVolFieldT& vz  = zVelocity_->element_field();

        row(densityPrimitiveIdx_) <<= theta * vz;
        row(temperatureIndex_   ) <<= rhoT * (1. - theta) * vz;

        row(zVelIndex_) <<= rho;

        if( doSpecies_ ){
          const double invMn = invMolecularWeights_[numberSpecies_-1];
          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const double invMi = invMolecularWeights_[i];
            row(massFracIndices_[i]) <<= (theta - 1.) * rho * vz * mmw * ( invMi - invMn );
          }
        }
      }
      else if( isSpeciesRow && doSpecies_ ){
        const auto& speciesIteratorFound = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), rowIdx );
        const OrdinalType speciesVectorIndex = speciesIteratorFound - speciesDensityIndices_.begin();

        const SVolFieldT& Y = massFractions_[speciesVectorIndex]->element_field();

        row(densityPrimitiveIdx_) <<= theta * Y;
        row(temperatureIndex_   ) <<= rhoT * (1. - theta) * Y;

        const double invMn = invMolecularWeights_[numberSpecies_-1];
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          const double invMi = invMolecularWeights_[i];
          row(massFracIndices_[i]) <<= (theta - 1.) * rho * Y * mmw * ( invMi - invMn );
        }

        row(massFracIndices_[speciesVectorIndex]) <<= row(massFracIndices_[speciesVectorIndex]) + rho;
      }
    }
  };

} // namespace matrix
} // namespace Expr


#endif //EXPRLIB_ACOUSTICPRECONDITIONER_H
