/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef EXPRLIB_STATETRANSFORMASSEMBLER_H
#define EXPRLIB_STATETRANSFORMASSEMBLER_H


#include <spatialops/structured/IntVec.h>
#include <expression/matrix-assembly/CustomAssembler.h>


namespace Expr
{
namespace matrix
{

  /**
   * \class StateTransformAssembler
   *
   */
  template<typename SVolFieldT>
  class StateTransformAssembler : public CustomAssembler<SVolFieldT>
  {
  protected:
    using OrdinalType = Expr::matrix::OrdinalType;
    using OrdinalVecType = std::vector<OrdinalType>;
    using HandleType = Expr::matrix::Handle<SVolFieldT>;
    using HandlePtrType = boost::shared_ptr<HandleType>;
    using HandlePtrVecType = std::vector<HandlePtrType>;
    using HandlePtrSetType = std::set<HandlePtrType>;
    using CustomAssemblerType = Expr::matrix::CustomAssembler<SVolFieldT>;

    const bool doX_ = false;
    const bool doY_ = false;
    const bool doZ_ = false;
    const bool doSpecies_ = false;

    const OrdinalType numberSpecies_;
    const OrdinalType densityConservedIdx_;
    const OrdinalType densityPrimitiveIdx_;
    const OrdinalType rhoEgyIndex_;
    const OrdinalType temperatureIndex_;
    const OrdinalType xMomIndex_, yMomIndex_, zMomIndex_;
    const OrdinalType xVelIndex_, yVelIndex_, zVelIndex_;

    OrdinalVecType speciesDensityIndices_;
    OrdinalVecType massFracIndices_;

    HandlePtrVecType massFractions_, energies_;

    HandlePtrType density_        = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType heatCapacityCv_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType totalEnergy_    = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    HandlePtrType xVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType yVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );
    HandlePtrType zVelocity_ = boost::make_shared<HandleType>( HandleType::ELEMENTTYPE );

    void check_indices()
    {
      std::set<OrdinalType> conservedIndices_, primitiveIndices_;
      OrdinalType conservedCount = 0;
      OrdinalType primitiveCount = 0;
      conservedIndices_.insert( densityConservedIdx_ ); primitiveIndices_.insert( densityPrimitiveIdx_ ); conservedCount++; primitiveCount++;
      if( doX_ ) { conservedIndices_.insert( xMomIndex_ ); primitiveIndices_.insert( xVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doY_ ) { conservedIndices_.insert( yMomIndex_ ); primitiveIndices_.insert( yVelIndex_ ); conservedCount++; primitiveCount++; }
      if( doZ_ ) { conservedIndices_.insert( zMomIndex_ ); primitiveIndices_.insert( zVelIndex_ ); conservedCount++; primitiveCount++; }
      conservedIndices_.insert( rhoEgyIndex_ ); primitiveIndices_.insert( temperatureIndex_ ); conservedCount++; primitiveCount++;
      if( doSpecies_ ){
        for( OrdinalType i=0; i<numberSpecies_ - 1; ++i ){
          conservedIndices_.insert( speciesDensityIndices_[i] );
          primitiveIndices_.insert( massFracIndices_[i] );
          conservedCount++;
          primitiveCount++;
        }
      }
      assert( conservedIndices_.size() == conservedCount );
      assert( primitiveIndices_.size() == primitiveCount );
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = StateTransformAssembler<SVolFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename Expr::matrix::AssemblerBase<SVolFieldT>::ElementType;
    using VectorType  = typename Expr::matrix::AssemblerBase<SVolFieldT>::VectorType;

    using CustomAssemblerType::assemble;

    /**
     * @brief build a state transform matrix assembler
     *
     * @param assemblerName          A string identifier for the assembler
     * @param densityTag             Tag for the mixture mass density
     * @param heatCapacityCvTag      Tag for the mixture specific heat capacity at constant volume
     * @param totalEnergyTag         Tag for the mixture specific total energy
     * @param vxTag                  Tag for the x-velocity (set to an empty tag if there is no x-velocity)
     * @param vyTag                  Tag for the y-velocity (set to an empty tag if there is no y-velocity)
     * @param vzTag                  Tag for the z-velocity (set to an empty tag if there is no z-velocity)
     * @param energyTags             Vector of tags for the specific species energies (pass an empty vector if the fluid is not a mixture)
     * @param massFractionTags       Vector of tags for the species mass fractions (pass an empty vector if the fluid is not a mixture)
     * @param densityConservedIndex  Index of the density in the conserved variable vector
     * @param densityPrimitiveIndex  Index of the density in the primitive variable vector
     * @param energyIndex            Index of the energy in the conserved variable vector
     * @param temperatureIndex       Index of the temperature in the primitive variable vector
     * @param xMomIndex              Index of the x-momentum in the conserved variable vector (only applies if x-velocity tag is non-empty)
     * @param yMomIndex              Index of the y-momentum in the conserved variable vector (only applies if y-velocity tag is non-empty)
     * @param zMomIndex              Index of the z-momentum in the conserved variable vector (only applies if z-velocity tag is non-empty)
     * @param xVelIndex              Index of the x-velocity in the primitive variable vector (only applies if x-velocity tag is non-empty)
     * @param yVelIndex              Index of the y-velocity in the primitive variable vector (only applies if y-velocity tag is non-empty)
     * @param zVelIndex              Index of the z-velocity in the primitive variable vector (only applies if z-velocity tag is non-empty)
     * @param speciesDensityIndices  Vector of indices of the species densities in the conserved variable vector
     * @param massFractionIndices    Vector of indices of the species mass fractions in the primitive variable vector
     */
    StateTransformAssembler( const std::string assemblerName,
                             const Expr::Tag& densityTag,
                             const Expr::Tag& heatCapacityCvTag,
                             const Expr::Tag& totalEnergyTag,
                             const Expr::Tag& vxTag,
                             const Expr::Tag& vyTag,
                             const Expr::Tag& vzTag,
                             const Expr::TagList& energyTags,
                             const Expr::TagList& massFractionTags,
                             const OrdinalType densityConservedIndex,
                             const OrdinalType densityPrimitiveIndex,
                             const OrdinalType energyIndex,
                             const OrdinalType temperatureIndex,
                             const OrdinalType xMomIndex,
                             const OrdinalType yMomIndex,
                             const OrdinalType zMomIndex,
                             const OrdinalType xVelIndex,
                             const OrdinalType yVelIndex,
                             const OrdinalType zVelIndex,
                             const OrdinalVecType& speciesDensityIndices,
                             const OrdinalVecType& massFractionIndices )
      : CustomAssembler<SVolFieldT>( assemblerName ),
        doX_( vxTag != Expr::Tag() ),
        doY_( vyTag != Expr::Tag() ),
        doZ_( vzTag != Expr::Tag() ),
        doSpecies_( massFractionTags.size() ),
        numberSpecies_( massFractionTags.size() ),
        densityConservedIdx_( densityConservedIndex ),
        densityPrimitiveIdx_( densityPrimitiveIndex ),
        rhoEgyIndex_( energyIndex ),
        temperatureIndex_( temperatureIndex ),
        xMomIndex_( xMomIndex ),
        yMomIndex_( yMomIndex ),
        zMomIndex_( zMomIndex ),
        xVelIndex_( xVelIndex ),
        yVelIndex_( yVelIndex ),
        zVelIndex_( zVelIndex )
    {
      *density_        = densityTag;
      *heatCapacityCv_ = heatCapacityCvTag;
      *totalEnergy_    = totalEnergyTag;

      if( doX_ ) *xVelocity_ = vxTag;
      if( doY_ ) *yVelocity_ = vyTag;
      if( doZ_ ) *zVelocity_ = vzTag;

      assert( massFractionTags.size() == energyTags.size() );
      assert( speciesDensityIndices.size() == massFractionIndices.size() );

      if( doSpecies_ ){
        for( OrdinalType i=0; i<numberSpecies_; ++i ){
          energies_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *energies_[i] = energyTags[i];

          massFractions_.push_back( boost::make_shared<HandleType>( HandleType::ELEMENTTYPE ) );
          *massFractions_[i] = massFractionTags[i];
        }

        for( OrdinalType i=0; i<numberSpecies_ - 1; ++i ){
          speciesDensityIndices_.push_back( speciesDensityIndices[i] );
          massFracIndices_.push_back( massFractionIndices[i] );
        }
      }

      check_indices();

      finalize();
    }

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "StateTransformAssembler( " + Expr::matrix::AssemblerBase<SVolFieldT>::name_ + " )";
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      Expr::matrix::AssemblerBase<SVolFieldT>::finalize();

      CustomAssemblerType::rowMap_.insert( std::make_pair( densityPrimitiveIdx_, OrdinalVecType() ) );
      CustomAssemblerType::rowMap_.insert( std::make_pair( temperatureIndex_, OrdinalVecType() ) );
      if( doX_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( xVelIndex_, OrdinalVecType() ) );
      if( doY_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( yVelIndex_, OrdinalVecType() ) );
      if( doZ_ ) CustomAssemblerType::rowMap_.insert( std::make_pair( zVelIndex_, OrdinalVecType() ) );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_.insert( std::make_pair( massFracIndices_[i], OrdinalVecType() ) );
        }
      }

      CustomAssemblerType::rowMap_[densityPrimitiveIdx_].push_back( densityConservedIdx_ );

      CustomAssemblerType::rowMap_[temperatureIndex_].push_back( densityConservedIdx_ );
      CustomAssemblerType::rowMap_[temperatureIndex_].push_back( rhoEgyIndex_ );
      if( doX_ ) CustomAssemblerType::rowMap_[temperatureIndex_].push_back( xMomIndex_ );
      if( doY_ ) CustomAssemblerType::rowMap_[temperatureIndex_].push_back( yMomIndex_ );
      if( doZ_ ) CustomAssemblerType::rowMap_[temperatureIndex_].push_back( zMomIndex_ );
      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[temperatureIndex_].push_back( speciesDensityIndices_[i] );
        }
      }

      if( doX_ ){
        CustomAssemblerType::rowMap_[xVelIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[xVelIndex_].push_back( xMomIndex_ );
      }

      if( doY_ ){
        CustomAssemblerType::rowMap_[yVelIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[yVelIndex_].push_back( yMomIndex_ );
      }

      if( doZ_ ){
        CustomAssemblerType::rowMap_[zVelIndex_].push_back( densityPrimitiveIdx_ );
        CustomAssemblerType::rowMap_[zVelIndex_].push_back( zMomIndex_ );
      }

      if( doSpecies_ ){
        for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
          CustomAssemblerType::rowMap_[massFracIndices_[i]].push_back( densityConservedIdx_ );
          CustomAssemblerType::rowMap_[massFracIndices_[i]].push_back( speciesDensityIndices_[i] );
        }
      }

      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( heatCapacityCv_ );
      Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( density_ );
      if( doX_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( xVelocity_ );
      if( doY_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( yVelocity_ );
      if( doZ_ ) Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( zVelocity_ );
      if( doSpecies_ ){
        for( const auto& fraction : massFractions_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( fraction );
        }
        for( const auto& energy : energies_ ){
          Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( energy );
        }
      }
      else{
        Expr::matrix::AssemblerBase<SVolFieldT>::set_required_handles( totalEnergy_ );
      }
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const OrdinalType nrows = row.elements();

      for( OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        row(colIdx) <<= 0.0;
      }

      bool isSpeciesRow = std::find( massFracIndices_.begin(), massFracIndices_.end(), rowIdx ) != massFracIndices_.end();

      if( rowIdx == temperatureIndex_ ){
        const SVolFieldT& rho = density_->element_field();
        const SVolFieldT& cv  = heatCapacityCv_->element_field();

        row(rhoEgyIndex_) <<= 1 / ( rho * cv );

        if( doX_ ) { row(xMomIndex_) <<= - xVelocity_->element_field() / ( rho * cv ); }
        if( doY_ ) { row(yMomIndex_) <<= - yVelocity_->element_field() / ( rho * cv ); }
        if( doZ_ ) { row(zMomIndex_) <<= - zVelocity_->element_field() / ( rho * cv ); }

        if( doSpecies_ ){
          const SVolFieldT& en = energies_[numberSpecies_-1]->element_field();

          row(densityConservedIdx_) <<= - en;
          if( doX_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + 0.5 * xVelocity_->element_field() * xVelocity_->element_field(); }
          if( doY_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + 0.5 * yVelocity_->element_field() * yVelocity_->element_field(); }
          if( doZ_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + 0.5 * zVelocity_->element_field() * zVelocity_->element_field(); }
          row(densityConservedIdx_) <<= row(densityConservedIdx_) / ( rho * cv );

          for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
            const SVolFieldT& ei = energies_[i]->element_field();
            row(speciesDensityIndices_[i]) <<= - (ei - en) / ( rho * cv );
          }
        }
        else{
          row(densityConservedIdx_) <<= - totalEnergy_->element_field();
          if( doX_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + xVelocity_->element_field() * xVelocity_->element_field(); }
          if( doY_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + yVelocity_->element_field() * yVelocity_->element_field(); }
          if( doZ_ ) { row(densityConservedIdx_) <<= row(densityConservedIdx_) + zVelocity_->element_field() * zVelocity_->element_field(); }
          row(densityConservedIdx_) <<= row(densityConservedIdx_) / ( rho * cv );
        }

      }
      else if( rowIdx == densityPrimitiveIdx_ ){
        row(densityConservedIdx_) <<= 1.0;
      }
      else if( rowIdx == xVelIndex_ && doX_ ){
        row(xMomIndex_)           <<= 1. / density_->element_field();
        row(densityConservedIdx_) <<= - xVelocity_->element_field() / density_->element_field();
      }
      else if( rowIdx == yVelIndex_ && doY_ ){
        row(yMomIndex_)           <<= 1. / density_->element_field();
        row(densityConservedIdx_) <<= - yVelocity_->element_field() / density_->element_field();
      }
      else if( rowIdx == zVelIndex_ && doZ_ ){
        row(zMomIndex_)           <<= 1. / density_->element_field();
        row(densityConservedIdx_) <<= - zVelocity_->element_field() / density_->element_field();
      }
      else if( isSpeciesRow && doSpecies_ ){
        const auto& speciesIteratorFound = std::find( massFracIndices_.begin(), massFracIndices_.end(), rowIdx );
        const OrdinalType speciesVectorIndex = speciesIteratorFound - massFracIndices_.begin();
        row(speciesDensityIndices_[speciesVectorIndex]) <<= 1. / density_->element_field();
        row(densityConservedIdx_)                       <<= - massFractions_[speciesVectorIndex]->element_field() / density_->element_field();
      }
    }

    /**
     * \brief multiplication-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const OrdinalType nrows = row.elements();

      std::vector<SpatialOps::SpatFldPtr<ElementType> > tempRowPtrs;
      tempRowPtrs.reserve( nrows );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );
      SpatialOps::SpatFldPtr<ElementType> tempPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        tempRow(colIdx) <<= row(colIdx);
      }

      for( Expr::matrix::OrdinalType colIdxToAssign = 0; colIdxToAssign < nrows; ++colIdxToAssign ){

        bool isSpeciesCol = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), colIdxToAssign ) != speciesDensityIndices_.end();

        if( colIdxToAssign == densityConservedIdx_ ){
          const SVolFieldT& rho = density_->element_field();
          const SVolFieldT& cv = heatCapacityCv_->element_field();
          *tempPtr <<= tempRow(densityPrimitiveIdx_);
          if( doX_ ) *tempPtr <<= *tempPtr - tempRow(xVelIndex_) * xVelocity_->element_field() / rho;
          if( doY_ ) *tempPtr <<= *tempPtr - tempRow(yVelIndex_) * yVelocity_->element_field() / rho;
          if( doZ_ ) *tempPtr <<= *tempPtr - tempRow(zVelIndex_) * zVelocity_->element_field() / rho;
          if( doSpecies_ ){
            *tempPtr <<= *tempPtr - energies_[numberSpecies_-1]->element_field() * tempRow(temperatureIndex_) / ( rho * cv );
            if( doX_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * 0.5 * xVelocity_->element_field() * xVelocity_->element_field() / ( rho * cv );
            if( doY_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * 0.5 * yVelocity_->element_field() * yVelocity_->element_field() / ( rho * cv );
            if( doZ_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * 0.5 * zVelocity_->element_field() * zVelocity_->element_field() / ( rho * cv );

            for( OrdinalType i=0; i < numberSpecies_ - 1; ++i ){
              *tempPtr <<= *tempPtr - massFractions_[i]->element_field() / rho * tempRow(massFracIndices_[i]);
            }
          }
          else{
            *tempPtr <<= *tempPtr - totalEnergy_->element_field() * tempRow(temperatureIndex_) / ( rho * cv );
            if( doX_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * xVelocity_->element_field() * xVelocity_->element_field() / ( rho * cv );
            if( doY_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * yVelocity_->element_field() * yVelocity_->element_field() / ( rho * cv );
            if( doZ_ ) *tempPtr <<= *tempPtr + tempRow(temperatureIndex_) * zVelocity_->element_field() * zVelocity_->element_field() / ( rho * cv );
          }
        }
        else if( colIdxToAssign == xMomIndex_ && doX_ ){
          *tempPtr <<= ( tempRow(xVelIndex_) - xVelocity_->element_field() / heatCapacityCv_->element_field() * tempRow(temperatureIndex_) ) / density_->element_field();
        }
        else if( colIdxToAssign == yMomIndex_ && doY_ ){
          *tempPtr <<= ( tempRow(yVelIndex_) - yVelocity_->element_field() / heatCapacityCv_->element_field() * tempRow(temperatureIndex_) ) / density_->element_field();
        }
        else if( colIdxToAssign == zMomIndex_ && doZ_ ){
          *tempPtr <<= ( tempRow(zVelIndex_) - zVelocity_->element_field() / heatCapacityCv_->element_field() * tempRow(temperatureIndex_) ) / density_->element_field();
        }
        else if( colIdxToAssign == rhoEgyIndex_ ){
          *tempPtr <<= tempRow(temperatureIndex_) / ( density_->element_field() * heatCapacityCv_->element_field() );
        }
        else if( isSpeciesCol && doSpecies_ ){
          const auto& speciesIteratorFound = std::find( speciesDensityIndices_.begin(), speciesDensityIndices_.end(), colIdxToAssign );
          const OrdinalType speciesVectorIndex = speciesIteratorFound - speciesDensityIndices_.begin();
          const SVolFieldT& ei  = energies_[speciesVectorIndex]->element_field();
          const SVolFieldT& en  = energies_[numberSpecies_-1]->element_field();
          const SVolFieldT& cv  = heatCapacityCv_->element_field();
          const SVolFieldT& rho = density_->element_field();
          *tempPtr <<= ( tempRow(massFracIndices_[speciesVectorIndex]) - (ei - en) / cv * tempRow(temperatureIndex_) ) / rho;
        }
        else{
          *tempPtr <<= 0.0;
        }
        row(colIdxToAssign) <<= *tempPtr;
      }
    }

  };

} // namespace matrix
} // namespace Expr

#endif //EXPRLIB_STATETRANSFORMASSEMBLER_H
