/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef EXPRLIB_DUALTIMESTEPEXPRESSION_H
#define EXPRLIB_DUALTIMESTEPEXPRESSION_H

#include <expression/Expression.h>

namespace Expr
{

  template< typename FieldT >
  class DualTimeStepExprNoChemistry
    : public Expr::Expression<FieldT>
  {
    const double cfl_, vnn_, maxDs_, minDs_;
    const bool doX_, doY_, doZ_, doSpecies_;
    DECLARE_FIELDS( FieldT, a_, k_, rho_, cp_, mu_ )
    DECLARE_FIELDS( FieldT, vx_, vy_, vz_ )
    DECLARE_FIELDS( FieldT, x_, y_, z_ )
    DECLARE_VECTOR_OF_FIELDS( FieldT, diffCoeffs_ )

    using IntVec = SpatialOps::IntVec;
    const IntVec noOffsetPoint_ = IntVec(1, 1, 1);
    const IntVec xOffsetPoint_  = IntVec(2, 1, 1);
    const IntVec yOffsetPoint_  = IntVec(1, 2, 1);
    const IntVec zOffsetPoint_  = IntVec(1, 1, 2);

    DualTimeStepExprNoChemistry( const double cfl,
                                 const double vnn,
                                 const double maxDs,
                                 const double minDs,
                                 const Expr::Tag& xTag,
                                 const Expr::Tag& yTag,
                                 const Expr::Tag& zTag,
                                 const Expr::Tag& vxTag,
                                 const Expr::Tag& vyTag,
                                 const Expr::Tag& vzTag,
                                 const Expr::Tag& aTag,
                                 const Expr::Tag& kTag,
                                 const Expr::Tag& rhoTag,
                                 const Expr::Tag& cpTag,
                                 const Expr::Tag& muTag,
                                 const Expr::TagList& dTags )
      : Expr::Expression<FieldT>(),
        cfl_( cfl ),
        vnn_( vnn ),
        maxDs_( maxDs ),
        minDs_( minDs ),
        doX_( vxTag != Expr::Tag() ),
        doY_( vyTag != Expr::Tag() ),
        doZ_( vzTag != Expr::Tag() ),
        doSpecies_( !dTags.size() )
    {
      this->set_gpu_runnable(false);
      x_ = this->template create_field_request<FieldT>( xTag );
      y_ = this->template create_field_request<FieldT>( yTag );
      z_ = this->template create_field_request<FieldT>( zTag );

      if( doX_ ) vx_ = this->template create_field_request<FieldT>( vxTag );
      if( doY_ ) vy_ = this->template create_field_request<FieldT>( vyTag );
      if( doZ_ ) vz_ = this->template create_field_request<FieldT>( vzTag );

      a_   = this->template create_field_request<FieldT>( aTag );
      k_   = this->template create_field_request<FieldT>( kTag );
      rho_ = this->template create_field_request<FieldT>( rhoTag );
      cp_  = this->template create_field_request<FieldT>( cpTag );
      mu_  = this->template create_field_request<FieldT>( muTag );

      if( doSpecies_ )
        this->template create_field_vector_request<FieldT>( dTags, diffCoeffs_ );
    }


  public:

    class Builder : public Expr::ExpressionBuilder
    {
      const double cfl_, vnn_, maxDs_, minDs_;
      const Expr::Tag xTag_, yTag_, zTag_, vxTag_, vyTag_, vzTag_, aTag_, kTag_, rhoTag_, cpTag_, muTag_;
      const Expr::TagList dTags_;
    public:
      /**
       * @brief build a dual time step expression for local variation by acoustic and diffusive time scales
       *
       * @param dsTag       The dual time step tag (to be computed by this expression)
       * @param cfl         The local acoustic CFL number (acoustic wave stability constraints suggest cfl < 1)
       * @param vnn         The local von Neumann number (diffusive stability constraints suggest vnn < 1)
       * @param maxDs       The largest allowable value of the dual time step
       * @param minDs       The smallest allowable value of the dual time step
       * @param xTag        Tag for the x-coordinate of volume fields
       * @param yTag        Tag for the y-coordinate of volume fields
       * @param zTag        Tag for the z-coordinate of volume fields
       * @param vxTag       Tag for the x-velocity (set to an empty tag if there is no x-velocity)
       * @param vyTag       Tag for the y-velocity (set to an empty tag if there is no y-velocity)
       * @param vzTag       Tag for the z-velocity (set to an empty tag if there is no z-velocity)
       * @param aTag        Tag for the speed of sound
       * @param kTag        Tag for the thermal conductivity
       * @param rhoTag      Tag for the mixture mass density
       * @param cpTag       Tag for the mixture specific heat capacity at constant pressure
       * @param muTag       Tag for the kinematic viscosity
       * @param dTags       Vector of tags for the species mixture-averaged diffusion coefficients
       * @param nghost      The number of ghost points (optional, default DEFAULT_NUMBER_OF_GHOSTS)
       */
      Builder( const Expr::Tag& dsTag,
               const double cfl,
               const double vnn,
               const double maxDs,
               const double minDs,
               const Expr::Tag& xTag,
               const Expr::Tag& yTag,
               const Expr::Tag& zTag,
               const Expr::Tag& vxTag,
               const Expr::Tag& vyTag,
               const Expr::Tag& vzTag,
               const Expr::Tag& aTag,
               const Expr::Tag& kTag,
               const Expr::Tag& rhoTag,
               const Expr::Tag& cpTag,
               const Expr::Tag& muTag,
               const Expr::TagList& dTags,
               const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
        : ExpressionBuilder( dsTag, nghost ),
          cfl_( cfl ),
          vnn_( vnn ),
          maxDs_( maxDs ),
          minDs_( minDs ),
          xTag_( xTag ),
          yTag_( yTag ),
          zTag_( zTag ),
          vxTag_( vxTag ),
          vyTag_( vyTag ),
          vzTag_( vzTag ),
          aTag_( aTag ),
          kTag_( kTag ),
          rhoTag_( rhoTag ),
          cpTag_( cpTag ),
          muTag_( muTag ),
          dTags_( dTags )
      {}

      Expr::ExpressionBase* build() const{ return new DualTimeStepExprNoChemistry<FieldT>( cfl_, vnn_,
                                                                                           maxDs_, minDs_,
                                                                                           xTag_, yTag_, zTag_,
                                                                                           vxTag_, vyTag_, vzTag_,
                                                                                           aTag_, kTag_, rhoTag_, cpTag_, muTag_, dTags_ ); }
    };

    ~DualTimeStepExprNoChemistry(){}

    void evaluate()
    {
      using namespace SpatialOps;

      FieldT& ds = this->value();

      const FieldT& a = a_->field_ref();
      const FieldT& k = k_->field_ref();
      const FieldT& rho = rho_->field_ref();
      const FieldT& cp = cp_->field_ref();
      const FieldT& mu = mu_->field_ref();

      SpatialOps::SpatFldPtr<FieldT> speedPtr = SpatialOps::SpatialFieldStore::get<FieldT>( ds );
      SpatialOps::SpatFldPtr<FieldT> maxDPtr = SpatialOps::SpatialFieldStore::get<FieldT>( ds );

      FieldT& s = *speedPtr;
      s <<= 0.0;

      FieldT& maxD = *maxDPtr;
      maxD <<= max( k / (rho * cp),
                    mu / rho );

      if( doSpecies_ ){
        for( size_t i=0; i<diffCoeffs_.size(); ++i ){
          maxD <<= max( maxD, diffCoeffs_[i]->field_ref() / rho );
        }
      }

      if( doX_ ) s <<= s + vx_->field_ref() * vx_->field_ref();
      if( doY_ ) s <<= s + vy_->field_ref() * vy_->field_ref();
      if( doZ_ ) s <<= s + vz_->field_ref() * vz_->field_ref();
      s <<= sqrt( s );

      double minDx = 1.e305;
      if( doX_ ) minDx = std::min( minDx, x_->field_ref()(xOffsetPoint_) - x_->field_ref()(noOffsetPoint_) );
      if( doY_ ) minDx = std::min( minDx, y_->field_ref()(yOffsetPoint_) - y_->field_ref()(noOffsetPoint_) );
      if( doZ_ ) minDx = std::min( minDx, z_->field_ref()(zOffsetPoint_) - z_->field_ref()(noOffsetPoint_) );

      ds <<= max( minDs_, min( cfl_ * minDx / (a + s), min( vnn_ * minDx * minDx / maxD, maxDs_ ) ) );
    }
  };

}

#endif //EXPRLIB_DUALTIMESTEPEXPRESSION_H
