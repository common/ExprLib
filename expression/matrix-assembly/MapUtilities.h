/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef MAPUTILITIES_H_
#define MAPUTILITIES_H_

#include <map>
#include <set>
#include <vector>
#include <cassert>

#include <boost/shared_ptr.hpp>

#include <expression/matrix-assembly/Handle.h>

namespace Expr
{
namespace matrix {

  /**
   * Below are defined some types for ordinals, pairs, and maps.
   * These are useful in defining sparse matrix structures.
   * There are also functions for transforming maps, and
   * extracting keys and values.
   *
   * As an example, consider the sparse matrix below:
   *
   * S = [1, 0, 0, 2;
   *      0, 3, 0, 0;
   *      0, 0, 0, 0;
   *      0, 4, 5, 0];
   *
   * An Ordinal is an index. The 1st row has a row ordinal 0, 2nd 1, etc.
   * The 4th column has a column ordinal of 3, etc.
   *
   * An OrdinalPair is a pair of indices. The (0,3) element of S is 2,
   * and the (3,2) element is 5. The first is the row, second is the column.
   *
   * A sparse matrix is most easiliy envisioned as a map between ordinal pairs
   * and elements (in this case, handles representing fields that are the elements).
   * In this representation, S is the following map:
   *
   * S as an OrdinalPair-Element Map:
   *   [0,0] -> 1
   *   [0,3] -> 2
   *   [1,1] -> 3
   *   [3,1] -> 4
   *   [3,2] -> 5
   *
   * This representation is clear and convenient, but it's not very useful for
   * arithmetic operations where we only want to iterate over a particular subset
   * of elements, perhaps a single row or a single column at once. For these purposes,
   * we introduce the row and column maps. These map from an ordinal, say a row or
   * column index, to a map between ordinals and elements.
   *
   * A row map describes a row. A column map describes a column.
   *
   * A row map goes from column ordinal to element.
   * A column map goes from row ordinal to element.
   *
   * The first row of S has the row map: 0->1, 3->2.
   * The fourth row of S has the row map: 1->4, 2->5.
   * The third row of S has an empty row map.
   *
   * The first column of S has the column map: 0->1.
   * The second column of S has the column map: 1->3, 3->4.
   *
   * To quickly iterate on rows, we may 'rowify' the pair-element map.
   * This gives a map between row ordinal and row map. For S:
   *
   * 'Rowified' S map:
   *   0 -> {0->1, 3->2}
   *   1 -> {1->3}
   *   3 -> {1->4, 2->5}
   *
   * Note that there is no entry for the third row.
   *
   * The 'colified' S map is the transpose:
   *   0 -> {0->1}
   *   1 -> {1->3, 3->4}
   *   2 -> {3->5}
   *   3 -> {0->2}
   *
   * The purpose of this file is to provide convenient access to such transformations
   * and define consistent fundamental types.
   */

  /**
   * \typedef OrdinalType
   *
   * OrdinalType is the type used to represent indices in matrices and vectors
   */
  typedef int OrdinalType;

  /**
   * \typedef OrdinalPairType
   *
   * A pair of two OrdinalTypes, as (i,j) for 2-D matrix indexing.
   */
  typedef typename std::pair<OrdinalType,OrdinalType> OrdinalPairType;

  /**
   * \typedef HandlePtrType
   *
   * An alias for a pointer to a Handle type
   */
  template<typename FieldType>
  using HandlePtrType = boost::shared_ptr<Expr::matrix::Handle<FieldType> >;

  /**
   * \typedef HandlePtrListType
   *
   * A collection of unique Handle pointers
   */
  template<typename FieldType>
  using HandlePtrListType = std::set<HandlePtrType<FieldType> >;

  /**
   * \typedef OrdinalHandlePtrMapType
   *
   * A map between ordinals and Handle pointers. Useful for diagonal matrix or row/col maps.
   */
  template<typename FieldType>
  using OrdinalHandleMapType = std::map<OrdinalType, HandlePtrType<FieldType> >;

  /**
   * \typedef OrdinalPairHandleMapType
   *
   * A map between ordinal pairs and Handle pointers. Useful for describing a sparse matrix.
   */
  template<typename FieldType>
  using OrdinalPairHandleMapType = std::map<OrdinalPairType, HandlePtrType<FieldType> >;

  /**
   * \typedef OrdinalRowColMapType
   *
   * A map between an ordinal to a map between ordinals and Handle pointers.
   * Useful for iterating over rows and columns of a sparse matrix.
   */
  template<typename FieldType>
  using OrdinalRowColMapType = std::map<OrdinalType, OrdinalHandleMapType<FieldType> >;

  /**
   * \brief combine vectors of HandlePtr types
   * \param left the first vector
   * \param right the second vector
   */
  template<typename FieldType>
  HandlePtrListType<FieldType> union_handle_lists( const HandlePtrListType<FieldType> left,
                                                   const HandlePtrListType<FieldType> right )
  {
    HandlePtrListType<FieldType> retval( left );
    for( const auto& r : right ){
      retval.insert( r );
    }
    return retval;
  }

  /**
   * \brief make a row-major list of tags representing a matrix as basename-row-col
   * \param basename the name that prefixes the row and column indices
   * \param nrows the number of rows in the matrix
   * \param context the context of the tags, default Expr::STATE_NONE
   *
   * For example,
   *
   * \code matrix_tags( "M", 2 ) \endcode
   *
   * would produce {M-0-0, M-0-1, M-1-0, M-1-1}.
   */
  inline
  Expr::TagList matrix_tags( const std::string basename,
                             const int nrows,
                             const Expr::Context context = Expr::STATE_NONE )
  {
    Expr::TagList matrixTags;
    for( Expr::matrix::OrdinalType rowIdx=0; rowIdx<nrows; ++rowIdx ){
      for( Expr::matrix::OrdinalType colIdx=0; colIdx<nrows; ++colIdx ){
        const std::string pairStr = boost::lexical_cast<std::string>( rowIdx ) +
                                    "-" +
                                    boost::lexical_cast<std::string>( colIdx );
        matrixTags.push_back( Expr::Tag( basename + "-" + pairStr , context ) );
      }
    }
    return matrixTags;
  }


  /**
   * \brief make a row-major list of tags representing a matrix as pre-mid-post where
   * pre and post come from a Cartesian product of vectors of strings
   * \param rowList the row identifier vector of strings
   * \param midStr the number of rows in the matrix
   * \param colList the column identifier vector of strings
   * \param context the context of the tags, default Expr::STATE_NONE
   *
   * For example,
   *
   * \code
   * using StrVecT = std::vector<std::string>;
   *
   * StrVecT ab = {"a", "b"};
   * StrVecT cd = {"c", "d"};
   *
   * matrix_tags( ab, "-sens-", cd )
   * \endcode
   *
   * would produce {a-sens-c, a-sens-d, b-sens-c, b-sens-d}.
   */
  inline
  Expr::TagList matrix_tags( const std::vector<std::string>& rowList,
                             const std::string midStr,
                             const std::vector<std::string>& colList,
                             const Expr::Context context = Expr::STATE_NONE )
  {
    assert( rowList.size() == colList.size() );
    const int nrows = rowList.size();

    Expr::TagList matrixTags;
    for( int i=0; i<nrows; ++i ){
      for( int j=0; j<nrows; ++j ){
        matrixTags.push_back( Expr::Tag( rowList[i] + midStr + colList[j] , context ) );
      }
    }
    return matrixTags;
  }

  /**
   * \brief make a row-major list of tags representing a matrix as pre-mid-post where
   * pre and post come from a cartesian product of tag lists
   * \param rowList the row identifier TagList
   * \param midStr the number of rows in the matrix
   * \param colList the column identifier TagList
   * \param context the context of the tags, default Expr::STATE_NONE
   *
   * For example,
   *
   * \code
   * Expr::TagList ab = {Expr::Tag("a",Expr::STATE_NONE), Expr::Tag("b",Expr::STATE_NONE)};
   * Expr::TagList cd = {Expr::Tag("c",Expr::STATE_NONE), Expr::Tag("d",Expr::STATE_NONE)};
   *
   * matrix_tags( ab, "-sens-", cd )
   * \endcode
   *
   * would produce {a-sens-c, a-sens-d, b-sens-c, b-sens-d}.
   */
  inline
  Expr::TagList matrix_tags( const Expr::TagList& rowList,
                             const std::string midStr,
                             const Expr::TagList& colList,
                             const Expr::Context context = Expr::STATE_NONE )
  {
    using StrVecT = std::vector<std::string>;
    StrVecT rows, cols;

    const int nrows = rowList.size();

    rows.resize( nrows );
    cols.resize( nrows );

    for( int i=0; i<nrows; ++i ){
      rows[i] = rowList[i].name();
      cols[i] = colList[i].name();
    }

    return matrix_tags( rows, midStr, cols, context );
  }

  /**
   * \brief build a pair of tags designated for a sensitivity field
   * \param funTag the 'function' tag - see below
   * \param varTag the 'variable' tag - see below
   *
   * The sensitivity function and sensitivity variable are the
   * numerator and denominator in a partial derivative, respectively:
   *
   * \f$\frac{\partial\mathrm{function}}{\partial\mathrm{variable}}\f$
   *
   * You can read this as: "sensitivity of 'fun' to 'var'"
   */
  inline std::pair<Expr::Tag,Expr::Tag> sensitivity( const Expr::Tag& funTag,
                                                     const Expr::Tag& varTag )
  {
    return std::pair<Expr::Tag,Expr::Tag>( funTag, varTag );
  }


  /**
   * \brief print out the first element of a matrix of fields in a pretty way
   * \param fml field manager list
   * \param matrixTags the taglist made with Expr::matrix::make_matrix_tags
   *
   * NOTE: this does nothing when CUDA is enabled because it directly subscripts a field.
   * If print_field() is used for CUDA support, then pretty-printing like this can't happen...
   */
  template<typename FieldT>
  void print_matrix( Expr::FieldManagerList& fml,
                     const Expr::TagList& matrixTags )
  {
  #ifndef ENABLE_CUDA
    const Expr::matrix::OrdinalType nrows = static_cast<Expr::matrix::OrdinalType>( std::sqrt( matrixTags.size() ) );
    for( Expr::matrix::OrdinalType rowIdx = 0; rowIdx < nrows; ++rowIdx ){
      std::cout << ( (rowIdx != 0) ? ' ' : '[' );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
        const Expr::matrix::OrdinalType flatIdx = rowIdx * nrows + colIdx;
        std::cout << fml.field_ref<FieldT>( matrixTags[flatIdx] )[0] << ( (colIdx + 1 != nrows) ? " " : "" );
      }
      std::cout << ( (rowIdx + 1 != nrows) ? ',' : ']' ) << '\n';
    }
  #endif
  }

  /**
   * \brief extract the keys of a map into a set
   * \param inputMap the map
   */
  template<typename MapType>
  std::set<typename MapType::key_type> extract_unique_keys( const MapType& inputMap)
  {
    std::set<typename MapType::key_type> keys;
    for( const auto& element : inputMap ){
      keys.insert( element.first );
    }
    return keys;
  }

  /**
   * \brief extract the values of a map into a set
   * \param inputMap the map
   */
  template<typename MapType>
  std::set<typename MapType::mapped_type> extract_unique_values( const MapType& inputMap)
  {
    std::set<typename MapType::mapped_type> values;
    for( const auto& element : inputMap ){
      values.insert( element.second );
    }

    return values;
  }

  /**
   * \brief determine if a container contains a particular element
   * \param element the element of interest
   * \param container the container potentially containing it
   */
  template< typename ElementType, typename ContainerType >
  inline bool contains_element( ElementType element, ContainerType container )
  {
    return std::find( container.begin(), container.end(), element ) != container.end();
  }

  /**
   * \brief convert a pair-handle map to a row-row map map
   * \param elementMap the map between pairs and handles
   *
   * 'rowify' means taking a map between (i,j) pairs and Handles, and converting it
   * to a map from between row index and a column index-Handle map. This way we can
   * iterate over columns of active rows one at a time without having to consider
   * the entire map and do comparisons of pairs.
   *
   * This is useful for emplacement, addition, and subtraction of matrix rows.
   *
   * Note that only non-empty rows are provided in the resultant map.
   */
  template<typename FieldType>
  OrdinalRowColMapType<FieldType>
  rowify_element_map( const OrdinalPairHandleMapType<FieldType>& elementMap )
  {
    std::set<OrdinalType> activeRows;
    OrdinalRowColMapType<FieldType> activeRowMaps;

    for( const auto& m : elementMap ){
      activeRows.insert( m.first.first );
    }
    for( const auto& row : activeRows ){
      OrdinalHandleMapType<FieldType> rowMap;
      for( const auto& m : elementMap ){
        if( m.first.first == row ){
          rowMap[m.first.second] = m.second;
        }
      }
      activeRowMaps[row] = rowMap;
    }
    return activeRowMaps;
  }

  /**
   * \brief convert a pair-tag map to a column-column map map
   * \param elementMap the map between pairs and handles
   *
   * 'colify' means taking a map between (i,j) pairs and Handles, and converting it
   * to a map from between column index and a row index-Handle map. This way we can
   * iterate over rows of active columns one at a time without having to consider
   * the entire map and do comparisons of pairs.
   *
   * This is useful for right-multiplication when we need fast access to columns.
   *
   * Note that only non-empty columns are provided in the resultant map.
   */
  template<typename FieldType>
  OrdinalRowColMapType<FieldType>
  colify_element_map( const OrdinalPairHandleMapType<FieldType>& elementMap )
  {
    std::set<OrdinalType> activeCols;
    OrdinalRowColMapType<FieldType> activeColMaps;

    for( const auto& m : elementMap ){
      activeCols.insert( m.first.second );
    }
    for( const auto& col : activeCols ){
      OrdinalHandleMapType<FieldType> colMap;
      for( const auto& m : elementMap ){
        if (m.first.second == col) {
          colMap[m.first.first] = m.second;
        }
      }
      activeColMaps[col] = colMap;
    }
    return activeColMaps;
  }

} // namespace matrix
}

#endif /** MAPUTILITIES_H_ */
