//
// Created by Mike Hansen on 5/7/18.
//

#ifndef EXPRLIB_CUSTOMASSEMBLER_H
#define EXPRLIB_CUSTOMASSEMBLER_H

#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{

  /**
   * \class: CustomAssembler
   *
   * This class specializes the general matrix assembler class (AssemblerBase)
   * so that one can avoid writing redundant addition-in and subtraction-in
   * assembly in custom matrix assembly classes.
   *
   * This class defines the addition-in and subtraction-in assembly methods
   * by doing emplacement into a temporary row followed by sparse arithmetic.
   * This requires that the rowMap_ member be specified in derived classes.
   * The rowMap_ maps row indices to vectors of nonzero column indices.
   */
  template<typename ElementFieldType>
  class CustomAssembler : public AssemblerBase<ElementFieldType>
  {
  public:
    using OrdinalType = Expr::matrix::OrdinalType;
    using OrdinalVecType = std::vector<OrdinalType>;
    using ElementType = typename AssemblerBase<ElementFieldType>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldType>::VectorType;

  protected:
    std::map<OrdinalType, OrdinalVecType> rowMap_;

  public:
    /**
     * \brief construct an empty CustomAssembler
     */
    CustomAssembler() : AssemblerBase<ElementFieldType>() {}

    /**
     * \brief construct a named CustomAssembler
     */
    CustomAssembler( const std::string name ) : AssemblerBase<ElementFieldType>( name ) {}

    /**
     * \brief empty virtual destructor for CustomAssembler
     */
    virtual ~CustomAssembler() {}

    /**
     * \brief assemble into a matrix row by emplacement
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode    the mode of operation, in this case it must be a Place() object
     * (literally just use \code Place() \endcode here)
     */
    virtual void assemble( VectorType& row,
                           const OrdinalType rowIdx,
                           const Expr::matrix::Place mode ) const override
    {}

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    virtual void assemble( VectorType& row,
                           const OrdinalType rowIdx,
                           const Expr::matrix::AddIn mode ) const
    {
      if( rowMap_.count( rowIdx ) > 0 ){
        // emplace into a temporary
        const Expr::matrix::OrdinalType nrows = row.elements();
        std::vector<SpatialOps::SpatFldPtr<ElementFieldType> > tempRowPtrs;
        tempRowPtrs.reserve( nrows );

        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        }
        SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );

        this->assemble( tempRow, rowIdx, Expr::matrix::Place() );

        // now do sparse add-in (dense add-in would be a clean one-liner with a RowPlaceHolder object, but less efficient)
        for( OrdinalType colIdx : rowMap_.at(rowIdx) ){
          row(colIdx) <<= row(colIdx) + tempRow(colIdx);
        }
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    virtual void assemble( VectorType& row,
                           const OrdinalType rowIdx,
                           const Expr::matrix::SubIn mode ) const
    {
      if( rowMap_.count( rowIdx ) > 0 ){
        // emplace into a temporary
        const Expr::matrix::OrdinalType nrows = row.elements();
        std::vector<SpatialOps::SpatFldPtr<ElementFieldType> > tempRowPtrs;
        tempRowPtrs.reserve( nrows );

        for( Expr::matrix::OrdinalType colIdx = 0; colIdx < nrows; ++colIdx ){
          tempRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
        }
        SpatialOps::FieldVector<ElementType> tempRow( tempRowPtrs );

        this->assemble( tempRow, rowIdx, Expr::matrix::Place() );

        // now do sparse add-in (dense add-in would be a clean one-liner with a RowPlaceHolder object, but less efficient)
        for( OrdinalType colIdx : rowMap_.at(rowIdx) ){
          row(colIdx) <<= row(colIdx) - tempRow(colIdx);
        }
      }
    }
  };
}
}

#endif //EXPRLIB_CUSTOMASSEMBLER_H
