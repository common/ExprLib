/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef DENSESUBMATRIX_H_
#define DENSESUBMATRIX_H_

#include <limits>
#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{
namespace matrix
{

  /**
   * \class DenseSubMatrix
   *
   * This class is a generic dense sub-matrix that is capable of all four operations:
   * emplacement, addition-in, subtraction-from, and right multiplication. It should
   * be used when a sub-matrix is dense - SparseMatrix is the other general-purpose matrix
   * and should be used if there are no sub-matrices ('blocks') that are fairly full.
   *
   * A DenseSubMatrix need not be square.
   *
   * A DenseSubMatrix is constructed without arguments, and elements should be added
   * with the parentheses operator, as M(i,j) = aTag; where i is the row index, j
   * is the column index, and aTag is the field tag that goes in element (i,j).
   *
   * The finalize() function MUST BE CALLED before a DenseMatrix can be used.
   *
   * An example construction and filling procedure is:
   *
   * \code
   * DenseMatrix<FieldT> M;
   * M(0,0) = aTag;
   * M(1,0) = bTag;
   * M(1,1) = cTag;
   * M.finalize();
   * \endcode
   */
  template<typename ElementFieldT>
  class DenseSubMatrix : public AssemblerBase<ElementFieldT>
  {
  protected:
    std::vector<Expr::matrix::HandlePtrType<ElementFieldT> > handles_;
    Expr::matrix::OrdinalPairHandleMapType<ElementFieldT> initMap_;

    Expr::matrix::OrdinalType nrows_ = 0;
    Expr::matrix::OrdinalType ncols_ = 0;
    Expr::matrix::OrdinalType anchorRow_ = std::numeric_limits<Expr::matrix::OrdinalType>::max();
    Expr::matrix::OrdinalType anchorCol_ = std::numeric_limits<Expr::matrix::OrdinalType>::max();
    Expr::matrix::OrdinalType finalRow_ = 0;
    Expr::matrix::OrdinalType finalCol_ = 0;

    Expr::matrix::OrdinalType numElements_ = 0;
    Expr::matrix::OrdinalType numNonZeros_ = 0;

    double nonZeroFraction_ = 0.0;

    void check_bounds( const Expr::matrix::OrdinalType nrows ) const
    {
      if( ( finalRow_ > nrows ) ||
          ( finalCol_ > nrows ) ){
        const std::string nrowsStr = boost::lexical_cast<std::string>( nrows );
        const std::string anchorRowStr = boost::lexical_cast<std::string>( anchorRow_ );
        const std::string finalRowStr = boost::lexical_cast<std::string>( finalRow_ );
        const std::string anchorColStr = boost::lexical_cast<std::string>( anchorCol_ );
        const std::string finalColStr = boost::lexical_cast<std::string>( finalCol_ );
        throw std::runtime_error( "Error! Matrix with " + nrowsStr + " rows is not big enough to hold "
                                  "a dense matrix spanning rows " + anchorRowStr + " to " + finalRowStr +
                                  " and cols " + anchorColStr + " to " + finalColStr + ".");
      }
    }

  public:
    /**
     * AssemblerType enables usage of this assembler type in compound assemblers (operations)
     */
    using AssemblerType = DenseSubMatrix<ElementFieldT>;
    /**
     * types from AssemblerBase
     */
    using ElementType = typename AssemblerBase<ElementFieldT>::ElementType;
    using VectorType  = typename AssemblerBase<ElementFieldT>::VectorType;

    /**
     * \brief construct an empty dense sub-matrix
     */
    DenseSubMatrix(){}

    /*
     * \brief construct a named object
     */
    DenseSubMatrix( const std::string name ) : AssemblerBase<ElementFieldT>( name ) {}

    /**
     * \brief obtain name of this type of assembler
     */
    std::string assembler_name() const override
    {
      return "DenseSubMatrix( " + AssemblerBase<ElementFieldT>::name_ + " )";
    }

    /**
     * \brief show the structure of this sparse matrix
     *
     * Prints initialized index pairs and the handles associated with them.
     */
    std::string get_structure() const override
    {
      std::string structure = assembler_name() + ":\n";
      for( Expr::matrix::OrdinalType rowIdx = anchorRow_; rowIdx < finalRow_+1; ++rowIdx ){
        const std::string rowIdxStr = boost::lexical_cast<std::string>( rowIdx );
        for( Expr::matrix::OrdinalType colIdx = anchorCol_; colIdx < finalCol_+1; ++colIdx ){
          const std::string colIdxStr = boost::lexical_cast<std::string>( colIdx );
          const Expr::matrix::OrdinalType flatIdx = ( rowIdx - anchorRow_ ) * ncols_ + ( colIdx - anchorCol_ );
          structure += "  (" + rowIdxStr + ", " + colIdxStr + ") -> " + handles_[flatIdx]->description() + "\n";
        }
      }
      return structure;
    }

    /**
     * \brief set the tag or value associated with an element in this matrix, identified by row and column index
     * \param rowIdx the row index
     * \param colIdx the column index
     *
     * This gives access to a Handle by reference, so one should write,
     *
     * \code M->element<FieldType>(i,j) = aTag \endcode,
     *
     * for instance, to set the i,j element to the field associated with aTag.
     *
     * The specified type can be one of three things:
     *
     * 1. ElementType - the type of the elementary fields of the matrix
     * 2. SingleValueField
     * 3. double - in this case a double must be provided on the RHS of the assignment expression, instead of a tag.
     */
    template<typename T>
    Expr::matrix::Handle<ElementType>& element( const Expr::matrix::OrdinalType rowIdx,
                                          const Expr::matrix::OrdinalType colIdx )
    {
      if( this->is_finalized() ){
        throw std::runtime_error( "SparseMatrix is finalized! Can't insert into it!" );
      }
      const OrdinalPairType pair = std::make_pair( rowIdx, colIdx );
      const auto& iter = initMap_.find( pair );
      if( std::is_same<T,ElementType>::value ){
        if( iter == initMap_.end() ){
          initMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::ELEMENTTYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *initMap_[pair];
      }
      else if( std::is_same<T,SpatialOps::SingleValueField>::value ){
        if( iter == initMap_.end() ){
          initMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::SINGLEVALUEFIELDTYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *initMap_[pair];
      }
      else if( std::is_same<T,double>::value ){
        if( iter == initMap_.end() ){
          initMap_[pair] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::DOUBLETYPE );
        }
        else{
          std::cout << "WARNING! Matrix element, (" << rowIdx << ", " << colIdx << "), being overridden!\n";
        }
        return *initMap_[pair];
      }
      else{
        throw std::runtime_error( "matrix element must be an ElementType, SingleValueField, or double!" );
      }
    }

    /**
     * \brief finalize the matrix and prevent further entries from being added,
     * and set required tags. MUST BE CALLED PRIOR TO ASSEMBLY! If the matrix is
     * not finalized, then a runtime error will be thrown in assembly.
     */
    void finalize() override
    {
      AssemblerBase<ElementFieldT>::finalize();

      for( const auto& m : initMap_ ){
        numNonZeros_++;
        const Expr::matrix::OrdinalType rowIdx = m.first.first;
        const Expr::matrix::OrdinalType colIdx = m.first.second;

        if( rowIdx < anchorRow_ ) anchorRow_ = rowIdx;
        if( rowIdx > finalRow_  ) finalRow_  = rowIdx;
        if( colIdx < anchorCol_ ) anchorCol_ = colIdx;
        if( colIdx > finalCol_  ) finalCol_  = colIdx;
      }

      nrows_ = finalRow_ - anchorRow_ + 1;
      ncols_ = finalCol_ - anchorCol_ + 1;
      numElements_ = nrows_ * ncols_;
      nonZeroFraction_ = static_cast<double>( numNonZeros_ ) / static_cast<double>( numElements_ );

      handles_.resize( numElements_ );
      for( Expr::matrix::OrdinalType rowIdx = anchorRow_; rowIdx < finalRow_+1; ++rowIdx ){
        for( Expr::matrix::OrdinalType colIdx = anchorCol_; colIdx < finalCol_+1; ++colIdx ){
          const Expr::matrix::OrdinalType flatIdx = ( rowIdx - anchorRow_ ) * ncols_ + ( colIdx - anchorCol_ );
          const Expr::matrix::OrdinalPairType pair = std::make_pair( rowIdx, colIdx );
          const auto& mapIter = initMap_.find( pair );
          if( mapIter != initMap_.end() ){
            handles_[flatIdx] = mapIter->second;
            initMap_.erase( mapIter );
          }
          else{
            handles_[flatIdx] = boost::make_shared<Expr::matrix::Handle<ElementType> >( Expr::matrix::Handle<ElementType>::ELEMENTTYPE );
          }
        }
      }
      initMap_.clear();

      std::set<HandlePtrType<ElementType> > uniqueHandles;
      for( auto v : handles_ ){
        uniqueHandles.insert( v );
      }
      AssemblerBase<ElementFieldT>::set_required_handles( uniqueHandles );
    }

    /**
     * \brief emplacement assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a Place() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::Place mode ) const override
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType rowSize = row.elements();
      check_bounds( rowSize );

      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        row(colIdx) <<= 0.0;
      }
      if( rowIdx >= anchorRow_ && rowIdx <= finalRow_ ){
        for( Expr::matrix::OrdinalType colIdx = anchorCol_; colIdx < finalCol_+1; ++colIdx ){
          const Expr::matrix::OrdinalType flatIdx = ( rowIdx - anchorRow_ ) * ncols_ + ( colIdx - anchorCol_ );
          if( handles_[flatIdx]->is_initialized() ){
            if( handles_[flatIdx]->is_double() ){ row(colIdx) <<= handles_[flatIdx]->double_value();  }
            else{
              if( handles_[flatIdx]->is_svf() ) { row(colIdx) <<= handles_[flatIdx]->svf_field();     }
              else                              { row(colIdx) <<= handles_[flatIdx]->element_field(); }
            }
          }
          else{
            row(colIdx) <<= 0.0;
          }
        }
      }
    }

    /**
     * \brief addition-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an AddIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::AddIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      if( rowIdx >= anchorRow_ && rowIdx <= finalRow_ ){
        for( Expr::matrix::OrdinalType colIdx = anchorCol_; colIdx < finalCol_+1; ++colIdx ){
          const Expr::matrix::OrdinalType flatIdx = ( rowIdx - anchorRow_ ) * ncols_ + ( colIdx - anchorCol_ );
          if( handles_[flatIdx]->is_initialized() ){
            if( handles_[flatIdx]->is_double() ){ row(colIdx) <<= row(colIdx) + handles_[flatIdx]->double_value();  }
            else{
              if( handles_[flatIdx]->is_svf() ) { row(colIdx) <<= row(colIdx) + handles_[flatIdx]->svf_field();     }
              else                              { row(colIdx) <<= row(colIdx) + handles_[flatIdx]->element_field(); }
            }
          }
        }
      }
    }

    /**
     * \brief subtraction-in assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - a SubIn() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::SubIn mode ) const
    {
      this->check_finalized();
      check_bounds( row.elements() );

      if( rowIdx >= anchorRow_ && rowIdx <= finalRow_ ){
        for( Expr::matrix::OrdinalType colIdx = anchorCol_; colIdx < finalCol_+1; ++colIdx ){
          const Expr::matrix::OrdinalType flatIdx = ( rowIdx - anchorRow_ ) * ncols_ + ( colIdx - anchorCol_ );
          if( handles_[flatIdx]->is_initialized() ){
            if( handles_[flatIdx]->is_double() ){ row(colIdx) <<= row(colIdx) - handles_[flatIdx]->double_value();  }
            else{
              if( handles_[flatIdx]->is_svf() ) { row(colIdx) <<= row(colIdx) - handles_[flatIdx]->svf_field();     }
              else                              { row(colIdx) <<= row(colIdx) - handles_[flatIdx]->element_field(); }
            }
          }
        }
      }
    }

    /**
     * \brief right-multiplication assembly
     * \param row    a vector of field pointers representing a matrix row
     * \param rowIdx the index of the row being assembled
     * \param mode   the mode of operation - an RMult() object constructed here
     */
    void assemble( VectorType& row,
                   const OrdinalType rowIdx,
                   const Expr::matrix::RMult mode ) const
    {
      this->check_finalized();
      const Expr::matrix::OrdinalType rowSize = row.elements();
      check_bounds( rowSize );

      std::vector<SpatialOps::SpatFldPtr<ElementType> > leftRowPtrs;
      leftRowPtrs.reserve( rowSize );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        leftRowPtrs.push_back( SpatialOps::SpatialFieldStore::get<ElementType>( row(0) ) );
      }
      SpatialOps::FieldVector<ElementType> leftRow( leftRowPtrs );
      SpatialOps::SpatFldPtr<ElementType> tempPtr = SpatialOps::SpatialFieldStore::get<ElementType>( row(0) );
      for( Expr::matrix::OrdinalType colIdx = 0; colIdx < rowSize; ++colIdx ){
        leftRow(colIdx) <<= row(colIdx);
      }

      for( Expr::matrix::OrdinalType colIdxToAssign = 0; colIdxToAssign < anchorCol_; ++colIdxToAssign ){
        row(colIdxToAssign) <<= 0.0;
      }
      for( Expr::matrix::OrdinalType colIdxToAssign = finalCol_+1; colIdxToAssign < rowSize; ++colIdxToAssign ){
        row(colIdxToAssign) <<= 0.0;
      }
      for( Expr::matrix::OrdinalType colIdxToAssign = anchorCol_; colIdxToAssign < finalCol_+1; ++colIdxToAssign ){
        *tempPtr <<= 0.0;
        for( Expr::matrix::OrdinalType rowIdxToIter = anchorRow_; rowIdxToIter < finalRow_+1; ++rowIdxToIter ){
          const Expr::matrix::OrdinalType flatIdx = ( rowIdxToIter - anchorRow_ ) * ncols_ + colIdxToAssign - anchorCol_;
          if( handles_[flatIdx]->is_initialized() ){
            if( handles_[flatIdx]->is_double() ){ *tempPtr <<= *tempPtr + handles_[flatIdx]->double_value() * leftRow(rowIdxToIter);  }
            else{
              if( handles_[flatIdx]->is_svf() ) { *tempPtr <<= *tempPtr + handles_[flatIdx]->svf_field() * leftRow(rowIdxToIter);     }
              else                              { *tempPtr <<= *tempPtr + handles_[flatIdx]->element_field() * leftRow(rowIdxToIter); }
            }
          }
        }
        row(colIdxToAssign) <<= *tempPtr;
      }
    }

  };

} // namespace matrix
}

#endif /** DENSESUBMATRIX_H_ */
