/*
 * The MIT License
 *
 * Copyright (c) 2016-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#ifndef EIGENVALUEEXPR_H_
#define EIGENVALUEEXPR_H_

#include <limits>

#include <spatialops/structured/MatVecFields.h>
#include <spatialops/structured/MatVecOps.h>

#include <expression/Expression.h>
#include <expression/matrix-assembly/AssemblerBase.h>

namespace Expr
{

  template<typename FieldType>
  std::vector<FieldType> union_vectors( const std::vector<FieldType>& left,
                                        const std::vector<FieldType>& right )
  {
    std::vector<FieldType> retval( left );
    retval.reserve( right.size() );
    for( const auto& r : right ){
      retval.push_back( r );
    }
    return retval;
  }

  /**
   * \class EigenvaluesExpr
   *
   * This Expression computes a subset of eigenvalues of a dense, pointwise matrix.
   * One may request the following subsets:
   *
   * - all of the eigenvalues:         ALL
   * - the largest negative real part: MOST_NEG_REAL_PART
   * - the largest positive real part: MOST_POS_REAL_PART
   * - the largest imaginary part:     LARGEST_IMAG_PART
   * - the largest negative real part: LEAST_NEG_REAL_PART
   * - the largest positive real part: LEAST_POS_REAL_PART
   * - the largest imaginary part:     SMALLEST_IMAG_PART
   * - the largest magnitude:          SMALLEST_MAG
   * - the largest magnitude:          SPECTRAL_RADIUS or LARGEST_MAG
   * - the condition number:           CONDITION_NUMBER
   *
   * If requesting the largest of any part or magnitude, the answer will be bounded
   * by zero on one side. For instance, if all the eigenvalues are negative, then the
   * largest positive real part will be zero - not the 'least negative' value.
   *
   * The enumeration is scoped by Expr::EigenvaluesExpr<FieldT>::Builder.
   * This means if you make a typedef for Expr::EigenvaluesExpr<FieldT>::Builder, as
   * is commonly done to register expressions to a factory, then you can use that
   * typedef to scope the eigenvalue request.
   */
  template< typename FieldT >
  class EigenvalueExpression : public Expr::Expression<FieldT>
  {
  public:
    struct Builder : public Expr::ExpressionBuilder
    {
    public:
      enum Request
      {
        ALL,
        MOST_NEG_REAL_PART,
        MOST_POS_REAL_PART,
        LEAST_NEG_REAL_PART,
        LEAST_POS_REAL_PART,
        LARGEST_IMAG_PART,
        SMALLEST_IMAG_PART,
        LARGEST_MAG,
        SMALLEST_MAG,
        SPECTRAL_RADIUS,
        CONDITION_NUMBER
      };

    private:
      const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
      const int nrows_;
      const Request request_;

    public:
      /**
       * \brief build an eigenvalues expression to compute all of the eigenvalues
       * \param realTags the tags for the resultant real parts of eigenvalues
       * \param imagTags the tags for the resultant imag parts of eigenvalues
       * \param assembler the matrix assembler
       * \param nrows the number of rows in the matrix
       *
       * The number of rows must match the size of the lists of tags.
       * This is checked in debug.
       */
      Builder( const Expr::TagList& realTags,
               const Expr::TagList& imagTags,
               const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
               const int nrows )
    : Expr::ExpressionBuilder( union_vectors( realTags, imagTags )  ),
      assembler_( assembler ),
      nrows_( nrows ),
      request_( ALL )
      {
        assert( nrows_ == realTags.size() );
        assert( nrows_ == imagTags.size() );
      }

      /**
       * \brief build an eigenvalues expression to compute a single eigenvalue/norm
       * \param resultTag the tag of the eigenvalue/norm field
       * \param assembler the matrix assembler
       * \param nrows the number of rows in the matrix
       * \param request the type of eigenvalue/norm requested
       */
      Builder( const Expr::Tag& resultTag,
               const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
               const int nrows,
               const Request request )
      : Expr::ExpressionBuilder( resultTag ),
        assembler_( assembler ),
        nrows_( nrows ),
        request_( request )
      {}

      ~Builder(){}
      Expr::ExpressionBase* build() const{
        return new EigenvalueExpression<FieldT>( assembler_, nrows_, request_ );
      }
    };

    private:

    const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler_;
    const int nrows_;
    const typename Builder::Request request_;

    EigenvalueExpression( const boost::shared_ptr<Expr::matrix::AssemblerBase<FieldT> > assembler,
                          const int nvars,
                          const typename Builder::Request request )
    : Expr::Expression<FieldT>(),
      assembler_( assembler ),
      nrows_( nvars ),
      request_( request )
      {
      this->set_gpu_runnable(true);
      assembler_->create_all_field_requests( *this );
      }

    public:
    void evaluate()
    {
      using namespace SpatialOps;

      typename Expr::Expression<FieldT>::ValVec& eigs = this->get_value_vec();
      typedef std::vector<SpatialOps::SpatFldPtr<FieldT> > SpatFldPtrVctr;
      SpatFldPtrVctr realPartPtrs, imagPartPtrs, matrixPtrs;
      for( int i=0; i<nrows_; ++i ){
        realPartPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( *eigs[0] ) );
        imagPartPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( *eigs[0] ) );
        for( int j=0; j<nrows_; ++j ){
          matrixPtrs.push_back( SpatialOps::SpatialFieldStore::get<FieldT>( *eigs[0] ) );
        }
      }

      SpatialOps::FieldMatrix<FieldT> matrix( matrixPtrs );
      for( Expr::matrix::OrdinalType rowIdx=0; rowIdx < nrows_; ++rowIdx ){
        SpatialOps::FieldVector<FieldT> row( matrix.row( rowIdx ) );
        assembler_->assemble( row, rowIdx, Expr::matrix::Place() );
      }

      SpatialOps::FieldVector<FieldT> realPart( realPartPtrs );
      SpatialOps::FieldVector<FieldT> imagPart( imagPartPtrs );
      matrix.eigenvalues( realPart, imagPart );

      if( request_ == Builder::ALL ){
        typename Expr::Expression<FieldT>::ValVec& eigs = this->get_value_vec();
        for( int i=0; i<nrows_; ++i ){
          *eigs[i] <<= realPart(i);
          *eigs[nrows_+i] <<= imagPart(i);
        }
      }
      else{
        const double largeNumber = std::numeric_limits<double>::max();
        SpatialOps::SpatFldPtr<FieldT> smallestPtr = SpatialOps::SpatialFieldStore::get<FieldT>( *eigs[0] );
        FieldT& result = this->value();
        switch( request_ ){
          case Builder::MOST_NEG_REAL_PART:
            result <<= 0.0;
            for( int i=0; i<nrows_; ++i ){
              result <<= min( result, min( realPart(i), 0.0 ) );
            }
            break;

          case Builder::MOST_POS_REAL_PART:
            result <<= 0.0;
            for( int i=0; i<nrows_; ++i ){
              result <<= max( result, max( realPart(i), 0.0 ) );
            }
            break;

          case Builder::LEAST_NEG_REAL_PART:
            result <<= -largeNumber;
            for( int i=0; i<nrows_; ++i ){
              result <<= max( result, cond( realPart(i) > 0.0, -largeNumber )( realPart(i) ) );
            }
            break;

          case Builder::LEAST_POS_REAL_PART:
            result <<= largeNumber;
            for( int i=0; i<nrows_; ++i ){
              result <<= min( result, cond( realPart(i) < 0.0, largeNumber )( realPart(i) ) );
            }
            break;

          case Builder::LARGEST_IMAG_PART:
            result <<= 0.0;
            for( int i=0; i<nrows_; ++i ){
              result <<= max( result, abs( imagPart(i) ) );
            }
            break;

          case Builder::SMALLEST_IMAG_PART:
            result <<= largeNumber;
            for( int i=0; i<nrows_; ++i ){
              result <<= min( result, abs( imagPart(i) ) );
            }
            break;

          case Builder::SMALLEST_MAG:
            result <<= largeNumber;
            for( int i=0; i<nrows_; ++i ){
              result <<= min( result, realPart(i) * realPart(i) + imagPart(i) * imagPart(i) );
            }
            result <<= sqrt( result );
            break;

          case Builder::SPECTRAL_RADIUS:
          case Builder::LARGEST_MAG:
            result <<= 0.0;
            for( int i=0; i<nrows_; ++i ){
              result <<= max( result, realPart(i) * realPart(i) + imagPart(i) + imagPart(i) );
            }
            result <<= sqrt( result );
            break;

          case Builder::CONDITION_NUMBER:
            result <<= 0.0;
            *smallestPtr <<= largeNumber;
            for( int i=0; i<nrows_; ++i ){
              result <<= max( result, realPart(i) * realPart(i) + imagPart(i) * imagPart(i) );
              *smallestPtr <<= min( *smallestPtr, realPart(i) * realPart(i) + imagPart(i) * imagPart(i) );
            }
            result <<= sqrt( result ) / sqrt( *smallestPtr );
            break;

          default:
            break;
        }
      }


    }
  };

}

#endif /* EIGENVALUEEXPR_H_ */
