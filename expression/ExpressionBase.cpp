/*
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#include <set>
#include <stdexcept>
#include <sstream>

#include <expression/ExprFwd.h>
#include <expression/ExpressionBase.h>
#include <expression/ExprDeps.h>
#include <expression/FieldRequest.h>

namespace Expr{

//#define DEBUG_CUDA_HANDLES

//--------------------------------------------------------------------

ExpressionBase::
ExpressionBase( FieldAggregatorBase* fab )
  : myFieldAggregator_( fab ),
    cleaveFromParents_ (false),
    cleaveFromChildren_(false),
    exprGpuRunnable_   (false)
{
  fmlID_ = DEFAULT_FML_ID;
# ifdef ENABLE_CUDA
  deviceID_ = 0;
  cudaStreamAlreadyExists_ = false;
  cudaStream_ = NULL;
# endif
}

//--------------------------------------------------------------------

#ifdef ENABLE_CUDA

void ExpressionBase::create_cuda_stream( int deviceIndex )
{
# ifndef NDEBUG
  if( !IS_GPU_INDEX(deviceIndex) ){
    std::ostringstream msg;
    msg << "Invalid device Index : " << deviceIndex << " found while creating stream for expression : " << this->get_tags()[0]
        << "at " << __FILE__ << " : " << __LINE__ << std::endl;
    throw(std::runtime_error(msg.str()));
  }
# endif

  // Stream created for the first time
  if( !cudaStreamAlreadyExists_ ){
    deviceID_ = deviceIndex;
    cudaSetDevice(deviceID_);
    cudaError err;
    if (cudaSuccess != (err = cudaStreamCreate( &cudaStream_ ))) {
      std::ostringstream msg;
      msg << "Failed to create stream for expression : " << this->get_tags()[0] << std::endl
          << " at " << __FILE__ << " : " << __LINE__ << std::endl;
      msg << "\t - " << cudaGetErrorString(err);
      throw(std::runtime_error(msg.str()));
    }
    cudaStreamAlreadyExists_ = true;
  }

  // Stream already exists but there is a change in device context
  else if( cudaStreamAlreadyExists_ && (deviceID_ != deviceIndex) ){
    cudaSetDevice(deviceID_);
    cudaError err;
    if (cudaSuccess != (err = cudaStreamDestroy( cudaStream_ ))) {
      std::ostringstream msg;
      msg << "Failed to destroy existing stream for expression : " << this->get_tags()[0] << std::endl
          << " at " << __FILE__ << " : " << __LINE__ << std::endl;
      msg << "\t - " << cudaGetErrorString(err);
      throw(std::runtime_error(msg.str()));
    }
    cudaStream_ = NULL;

    deviceID_ = deviceIndex;
    cudaSetDevice(deviceID_);
    if (cudaSuccess != (err = cudaStreamCreate( &cudaStream_ ))){
      std::ostringstream msg;
      msg << "Failed to create stream for expression : " << this->get_tags()[0] << std::endl
          << " at " << __FILE__ << " : " << __LINE__ << std::endl;
      msg << "\t - " << cudaGetErrorString(err);
      throw(std::runtime_error(msg.str()));
    }
    cudaStreamAlreadyExists_ = true;
  }

  // For other cases
  else{
    return;
  }

# ifdef DEBUG_CUDA_HANDLES
  std::cout << "Creating CudaStream with context : " << deviceID_
            << ", for expr : " << this->get_tags()[0] << " & streamID : " << cudaStream_ << std::endl;
# endif
}

//--------------------------------------------------------------------

cudaStream_t ExpressionBase::get_cuda_stream()
{
  return cudaStream_;
}

#endif

//--------------------------------------------------------------------

ExpressionBase::~ExpressionBase()
{
  delete myFieldAggregator_;

# ifdef ENABLE_CUDA

# ifdef DEBUG_CUDA_HANDLES
  std::cout << "Destroying Stream with context : " << deviceID_
            << ", for expr " << this->get_tags()[0] << " & streamID : " << cudaStream_ << std::endl;
# endif
    // set the device context before invoking CUDA API
    cudaSetDevice(deviceID_);
    cudaError err;
    if( cudaStream_ != NULL ){
      if( cudaSuccess != (err = cudaStreamDestroy( cudaStream_ )) ){
        std::ostringstream msg;
        msg << "Failed to destroy stream, at " << __FILE__ << " : " << __LINE__
            << std::endl;
        msg << "\t - " << cudaGetErrorString(err);
        throw(std::runtime_error(msg.str()));
      }
      cudaStream_ = NULL;
    }
# endif
}

//--------------------------------------------------------------------

void ExpressionBase::set_gpu_runnable( const bool b )
{
  exprGpuRunnable_ = b;
}

//--------------------------------------------------------------------

void
ExpressionBase::
base_advertise_dependents( ExprDeps& exprDeps,
                           FieldAggregatorBase*& fieldDeps )
{
  // Ensure that this expression has been associated with a field.
  // This should be automatically done when the expression is
  // constructed by the factory.
  if( exprNames_.empty() ){
    std::ostringstream msg;
    msg << "ERROR!  An expression exists that has not been associated with any fields!" << std::endl
        << "       " << __FILE__ << " : " << __LINE__ << std::endl;
    throw std::runtime_error( msg.str() );
  }

  for( ExtraSrcTerms::iterator isrc=srcTerms_.begin(); isrc!=srcTerms_.end(); ++isrc ){
    exprDeps.requires_expression( isrc->srcExpr->get_tags() );
  }

  // NOTE: modifiers do not call base_advertise_dependents called because we
  // already have this information. Of course, perhaps we want to attach source
  // expressions on the modifiers as well?  Hmmmm...
  for( ExpressionBase* modifier: modifiers_ ){
    for( const FieldAggregators::value_type& vt: modifier->fieldAggregators_ ){
      exprDeps.requires_expression( vt.second->get_tags() );
      // propagate ghost information to modifier dependents.
      vt.second->set_min_ghosts( myGhosts_ );
    }
#   ifndef NDEBUG
    if( modifier->srcTerms_.size() > 0 ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "ERROR: Modifier expressions do not currently support arbitrary source term addition" << std::endl;
      throw std::runtime_error( msg.str() );
    }
#   endif
  }

  fieldDeps = myFieldAggregator_;

  for( const FieldAggregators::value_type& vt: fieldAggregators_ ){
    exprDeps.requires_expression( vt.second->get_tags() );
    // Ensure that all children compute at least as many ghosts as this expression requires
    vt.second->set_min_ghosts( myGhosts_ );
  }
}

//--------------------------------------------------------------------

void
ExpressionBase::base_bind_fields( FieldManagerList& fldMgrList )
{
  this->bind_my_fields( fldMgrList );

  // modifiers don't have modifiers or source terms, so we can skip directly to binding their fields
  for( ExpressionBase* modifier: modifiers_ ){
    modifier->bind_my_fields( fldMgrList );
  }

  // sources don't have modifiers or source terms, so we can skip directly to binding their fields
  for( const SrcTermInfo& src: srcTerms_ ){
    ExpressionBase* expr = const_cast<ExpressionBase*>( src.srcExpr );
    expr->bind_my_fields( fldMgrList, true );
  }
}

void
ExpressionBase::base_bind_fields( FMLMap& fmls )
{
# ifndef NDEBUG
  if( fmls.size()>1 ) assert( fmlID_ != DEFAULT_FML_ID );
# endif
  base_bind_fields( *extract_field_manager_list( fmls, fmlID_ ) );
}

//--------------------------------------------------------------------

void
ExpressionBase::base_bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  try{
    this->bind_operators( opDB );

    for( ExpressionBase* modifier: modifiers_ ){
      modifier->bind_operators( opDB );
    }
  }
  catch( std::runtime_error& err ){
    std::ostringstream msg;
    msg << "ERROR trapped in bind_operators() for expression: " << get_tags() << std::endl
        << " details follow.\n" << err.what();
    throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------
void
ExpressionBase::activate_sensitivity( const TagSet& vars )
{
  for( const Tag& var: vars ) activate_sensitivity(var);
}
//--------------------------------------------------------------------
void ExpressionBase::override_sensitivity( const Tag& tag, const double value )
{
  sensOverride_[tag] = value;

  for( FieldAggregators::value_type& vt: fieldAggregators_ ){
    vt.second->remove_sensitivity( tag );
  }
}
//--------------------------------------------------------------------
bool ExpressionBase::overrides_sensitivity( const Tag& tag ) const
{
  return sensOverride_.find(tag) != sensOverride_.end();
}
//--------------------------------------------------------------------
void ExpressionBase::setup_self_sensitivity( const Expr::Tag& ivarTag )
{
  override_sensitivity( ivarTag, 1.0 );
  activate_sensitivity( ivarTag );
}
//--------------------------------------------------------------------
void ExpressionBase::setup_empty_sensitivity( const Expr::Tag& ivarTag )
{
  override_sensitivity( ivarTag, 0.0 );
  activate_sensitivity( ivarTag );
}
//--------------------------------------------------------------------

void
ExpressionBase::sensitivity( const Tag& )
{
  if( !myFieldAggregator_->get_sensitivity().empty() ){
    std::ostringstream msg;
    msg << "ERROR: no sensitivity calculation has been implemented for " << this->get_tags()[0] << std::endl;
    throw std::runtime_error( msg.str() );
  }
}

//--------------------------------------------------------------------

void
ExpressionBase::add_modifier( ExpressionBase* const modifier, const Tag& tag )
{
  // ensure that we don't already have this one.
  for( ExpressionBase* m: modifiers_ ){
    if( m == modifier ) return;
  }
  modifiers_.push_back( modifier );
  modifierNames_.push_back( tag );
}

//--------------------------------------------------------------------

void
ExpressionBase::attach_source_expression( const ExpressionBase* const src,
                                          const SourceExprOp op,
                                          const int myFieldIx,
                                          const int srcFieldIx )
{
  assert( !is_placeholder() || op == NULL_OP );
  srcTerms_.insert( SrcTermInfo(src,op,myFieldIx,srcFieldIx) );
}

//--------------------------------------------------------------------

const Tag&
ExpressionBase::get_tag() const
{
  if( exprNames_.size() > 1 ){
    std::ostringstream msg;
    msg << "ERROR from " << __FILE__ << " : " << __LINE__ << std::endl
        << "  get_tag() called in an expression which evaluates a vector of fields:" << std::endl;
    for( TagList::const_iterator i=exprNames_.begin(); i!=exprNames_.end(); ++i ){
      msg << "  " << *i;
    }
    msg << "\nUse get_tags() instead" << std::endl;
    throw std::runtime_error( msg.str() );
  }
  return exprNames_[0];
}

//--------------------------------------------------------------------

} // namespace Expr
