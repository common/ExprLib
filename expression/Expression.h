/**
 * \file Expression.h
 * \author James C. Sutherland
 *
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef Expression_h
#define Expression_h

#include <map>

#include <boost/signals2.hpp>
#include <boost/lexical_cast.hpp>

#include <expression/ExpressionBase.h>
#include <expression/ExpressionFactory.h>
#include <expression/FieldManager.h>
#include <expression/FieldRequest.h>

#include <spatialops/Nebo.h>
#include <spatialops/structured/SpatialFieldStore.h>

namespace Expr{

//====================================================================


  template<typename FieldT> struct FieldType : public FieldTypeBase
  {
    FieldType( ExpressionBase& expr ) : FieldTypeBase(expr) {}

    FieldManagerBase& field_manager( FieldManagerList& fml ) const{
      return fml.field_manager<FieldT>();
    }
    const FieldManagerBase& field_manager( const FieldManagerList& fml ) const{
      return fml.field_manager<FieldT>();
    }

    bool lock_fields( FieldManagerList& fml ) const{
      bool ok = true;
      FieldManagerBase& fm = field_manager(fml);
      const FieldAggregatorBase& fa = expr_.field_aggregator();
      for( const Tag& t: fa.get_tags() ){
        ok = ok & fm.lock_field( t );
        for( const Tag& st: fa.get_sensitivity()){
          ok = ok & fm.lock_field( sens_tag(t,st) );
        }
      }
      return ok;
    }

    bool unlock_fields( FieldManagerList& fml ) const{
      bool ok = true;
      FieldManagerBase& fm = field_manager(fml);
      const FieldAggregatorBase& fa = expr_.field_aggregator();
      for( const Tag& t: fa.get_tags() ){
        ok = ok & fm.unlock_field( t );
        for( const Tag& st: fa.get_sensitivity()){
          ok = ok & fm.unlock_field( sens_tag(t,st) );
        }
      }
      return ok;
    }

    void set_field_memory_manager( FieldManagerList& fml, const MemoryManager mm, const short deviceID ) const{
      FieldManagerBase& fm = field_manager(fml);
      const FieldAggregatorBase& fa = expr_.field_aggregator();
      for( const Tag& t: fa.get_tags()        ){
        fm.set_field_memory_manager( t, mm, deviceID );
        for( const Tag& st: fa.get_sensitivity() ) {
          fm.set_field_memory_manager( sens_tag(t,st), mm, deviceID );
        }
      }
    }
  };


/**
 *  @class  Expression
 *  @author James C. Sutherland
 *  @date   May, 2007
 *
 *  The Expression class (and the parent class ExpressionBase) is the
 *  fundamental computational kernel.  It provides a hierarchical way
 *  to generate complex expressions that allow efficient evaluation of
 *  functions as well as sensitivities.
 *
 *
 *  @par Evaluation and values of Expressions
 *
 *  Evaluation of an expression is separate from value-retrieval,
 *  since we may retrieve the value of the expression many times until
 *  we must recompute it.
 *
 *
 *  @par Expression dependencies
 *
 *  An Expression indicates its dependencies by calling the
 *  \code create_field_request \endcode method during construction.
 *
 *
 *  @par Construction of Expression Objects
 *
 *  Expression objects should be constructed using an
 *  ExpressionBuilder object.  This provides a standard interface to
 *  the ExpressionFactory.  The construction process is as follows:
 *
 *   \li Register the expression's ExpressionBuilder class with the
 *       ExpressionFactory.  The factory is then responsible for
 *       building the expression and managing deletion.
 *
 *   \li When an ExpressionTree is created, the root expression is
 *       interrogated for its dependencies.  The resulting
 *       dependent expressions are recursively descended to construct a
 *       dependency graph.  This graph is then compiled and each
 *       expression in the graph is constructed through the
 *       ExpressionFactory.
 *
 *
 *  @par External modification of values computed by an Expression
 *
 *  Occasionally we need to modify values computed by an Expression.
 *  An example is imposing boundary conditions, where we must modify
 *  some of the field values.  The process_after_evaluate() method
 *  facilitates this process.  T he user provides a call-back functor
 *  which is evaluated after the expression is computed.  Note that
 *  multiple such functors may be added, but there is no guarantee
 *  for ordering of these functors when the callback occurs.
 *
 *
 *  @tparam ValT The type of value that this expression computes and
 *          returns.  This may be an integral value or a class.
 *
 *  @todo Need to rework the signal approach so that connections can be
 *        destroyed.  This is particularly important for mesh changes,
 *        where BC objects will be re-created.  The old ones must be
 *        destroyed and the signal must be disconnected.
 */
template< typename ValT >
class Expression : public ExpressionBase
{
  typedef typename boost::signals2::signal< void(ValT&) > Signal;

public:

  typedef SpatialOps::SpatFldPtr<ValT> ValPtr;
  typedef std::vector<ValPtr> ValVec;
  virtual ~Expression();

  void activate_sensitivity( const Tag& var );

  /**
   * Trigger evaluation of an expression. It should only be called by the
   * scheduler - not by user code.
   */
  inline void base_evaluate();

  /**
   *  This can be called to load additional work on this expression.
   *  It is primarily intended for applying boundary conditions to a
   *  field after it has been evaluated.
   *
   *  Any subscribers loaded on here will be executed after the
   *  expression is evaluated.  The argument to the functor is the
   *  field that the expression evaluates.
   *
   *  \param subscriber the functor to evaluate
   *  \param isGPUReady true if the functor can handle GPU call-backs.
   */
  void process_after_evaluate( typename Signal::slot_function_type subscriber,
                               const bool isGPUReady );

  /**
   *  This can be called to load additional work on this expression.
   *  It is primarily intended for applying boundary conditions to a
   *  field after it has been evaluated.
   *
   *  Any subscribers loaded on here will be executed after the
   *  expression is evaluated.  The argument to the functor is the
   *  field that the expression evaluates.
   *
   *  \param fieldName In the case of an expression that evaluates
   *         multiple fields, this supplies the name of the field to
   *         process.
   *  \param subscriber the functor to evaluate
   *  \param isGPUReady true if the functor can handle GPU call-backs.
   */
  void process_after_evaluate( const std::string fieldName,
                               typename Signal::slot_function_type subscriber,
                               const bool isGPUReady );

  /**
   * Obtain the number of modifiers and "process_after_evaluate()" functors
   * associated with this expression.
   */
  int num_post_processors() const{ return srcTerms_.size() + postEvalSignal_.size() + modifiers_.size(); }

  /**
   * Obtain the names of the modifiers and "process_after_evaluate()" functors
   * associated with this expression.
   */
  std::vector<std::string> post_proc_names() const;

  std::string field_type_name() const{ return SpatialOps::type_name<ValT>(); }

  ExpressionBase* as_placeholder_expression() const;

  const FieldTypeBase& field_type() const{ return *fieldType_; }

  template<typename FT>
  boost::shared_ptr< FieldRequest<FT> >
  create_field_request( const Tag& tag );

protected:

  void set_computed_tags( const TagList& tags, const SpatialOps::GhostData& ghosts );
  void bind_my_fields( FieldManagerList& fml, const bool computedOnly=false );

  Expression();

  /**
   * @return the value that this expression computes. If an expression computes multiple values, this throws an exception.
   */
  inline ValT& value();

  /**
   * @param tag For expressions computing multiple values, this selects which of those you want the field for.
   * @return the requested field.
   */
  inline ValT& value( const Tag& tag ){ return myFieldReqs_.get_field_request(tag)->field_ref(); }
  inline SpatialOps::SpatFldPtr<ValT> value_ptr( const Tag& tag ){ return myFieldReqs_.get_field_request(tag)->field_ptr(); }
  inline const ValT& value( const Tag& tag ) const{ return myFieldReqs_.get_field_request(tag)->field_ref(); }
  inline const SpatialOps::SpatFldPtr<ValT> value_ptr( const Tag& tag ) const{ return myFieldReqs_.get_field_request(tag)->field_ptr(); }

  /**
   * @return a vector of the fields that this expression computes, ordered the same as get_tags().
   */
  inline ValVec& get_value_vec(){ return exprValues_; }
  inline const ValVec& get_value_vec() const{ return exprValues_; }

  /**
   * @param var the variable (tag) to differentiate this expression with respect to.
   * @return the sensitivity of this w.r.t. var.
   *
   * Note that this only works for expressions that compute a single value.
   * This is equivalent to <code>sensitivity_result( get_tag(), var );</code>
   */
  inline ValT& sensitivity_result( const Tag& var ){
    return sensitivity_result( get_tag(), var );
  }
  inline const ValT& sensitivity_result( const Tag& var ) const{
    return sensitivity_result( get_tag(), var );
  }
  inline SpatialOps::SpatFldPtr<ValT> sensitivity_result_ptr( const Tag& var ){
    return sensitivity_result_ptr( get_tag(), var );
  }

  /**
   * @param var the dependent variable (must be one of the variables computed by this expression)
   * @param sensVar the independent variable (variable to differentiate with respect to).
   * @return the sensitivity of var w.r.t. sensVar.
   */
  inline ValT& sensitivity_result( const Tag& var, const Tag& sensVar ){
    return myFieldReqs_.get_field_request(var)->sens_field_ref(sensVar);
  }
  inline const ValT& sensitivity_result( const Tag& var, const Tag& sensVar ) const{
    return myFieldReqs_.get_field_request(var)->sens_field_ref(sensVar);
  }
  inline SpatialOps::SpatFldPtr<ValT> sensitivity_result_ptr( const Tag& var, const Tag& sensVar ){
    return myFieldReqs_.get_field_request(var)->sens_field_ptr(sensVar);
  }

  template<typename FT>
  void
  create_field_vector_request( const TagList& tag,
                               std::vector<boost::shared_ptr< const FieldRequest<FT> > >& vec );

private:

  typedef std::map<size_t,Signal*> SignalMap;
  SignalMap postEvalSignal_;

  FieldAggregator<ValT>& myFieldReqs_;  // fully typed FieldAggregator - shadows ExpressionBase::myFieldAggregator_

  Expression( const Expression& ); // no copying
  Expression& operator=( const Expression& ); // no assignment

  std::vector<ValPtr> exprValues_; ///< The value(s) that this expression computes.

  boost::shared_ptr<FieldTypeBase> fieldType_;
};



/**
 *  @class  PlaceHolder
 *  @author James C. Sutherland
 *  @date   June, 2008
 *
 *  @brief This expression does nothing.  It simply wraps a field.
 */
template< typename FieldT >
class PlaceHolder : public Expression<FieldT>
{
public:
  inline void evaluate(){}
  inline void sensitivity(const Tag& varTag){}

  bool is_placeholder() const{ return true; }

  /**
   *  @class Builder
   *  @brief Constructs PlaceHolder objects.
   */
  struct Builder : public ExpressionBuilder
  {
    Builder( const Tag& name,      const SpatialOps::GhostData nghost=DEFAULT_NUMBER_OF_GHOSTS ) : ExpressionBuilder(name, nghost) {}
    Builder( const TagList& names, const SpatialOps::GhostData nghost=DEFAULT_NUMBER_OF_GHOSTS ) : ExpressionBuilder(names,nghost) {}
    ~Builder(){}
    ExpressionBase* build() const { return new PlaceHolder<FieldT>(); }
  };

protected:

  PlaceHolder() : Expression<FieldT>(){
    this->set_gpu_runnable(true);
  }

  virtual ~PlaceHolder(){}

};

//====================================================================

/**
 *  @class ConstantExpr
 *  @author James C. Sutherland
 *  @date  April, 2009
 *  @brief Sets a field to a constant
 */
template< typename ValT >
class ConstantExpr : public Expression<ValT>
{
public:

  struct Builder : public ExpressionBuilder
  {
    /**
     *  @brief Builds a ConstantExpr Expression.
     */
    Builder( const Tag& name,    ///< the name of the field to set
             const double a,     ///< The constant value
             const int nghost=DEFAULT_NUMBER_OF_GHOSTS  ///< the number of ghost cells to calculate in
             )
    : ExpressionBuilder(name,nghost), a_(a) {}
    ~Builder(){}
    ExpressionBase* build() const{ return new ConstantExpr(a_); }
  private:
    const double a_;
  };

  void evaluate(){
    using SpatialOps::operator<<=;
    this->value() <<= val_;
  }

  void sensitivity( const Tag& var ){
    if( var == this->get_tag() )  this->sensitivity_result(var) <<= 1.0;
    else                          this->sensitivity_result(var) <<= 0.0;
  }

protected:
  ~ConstantExpr(){}
  ConstantExpr( const double value ) : Expression<ValT>(), val_( value )
  {
    this->set_gpu_runnable(true);
  }

  const double val_;
};


//====================================================================




//####################################################################
//
//
//                          IMPLEMENTATION
//
//
//####################################################################




//====================================================================


//--------------------------------------------------------------------
template<typename ValT>
Expression<ValT>::Expression()
  : ExpressionBase( new FieldAggregator<ValT>() ),
    myFieldReqs_( dynamic_cast<FieldAggregator<ValT>&>(*myFieldAggregator_) ),
    fieldType_( new FieldType<ValT>(*this) )
{}
//--------------------------------------------------------------------
template<typename ValT>
Expression<ValT>::~Expression()
{
  for( typename SignalMap::iterator isig=postEvalSignal_.begin(); isig!=postEvalSignal_.end(); ++isig ){
    delete isig->second;
  }
  for( FieldAggregators::value_type& vt: fieldAggregators_ ){
    delete vt.second;
  }
}
//--------------------------------------------------------------------
template<typename ValT>
ValT&
Expression<ValT>::value()
{
# ifndef NDEBUG
  if( myFieldReqs_.get_num_entries() != 1 ){
    std::ostringstream msg;
    msg << std::endl << __FILE__ << " : " << __LINE__
        << "\n\nERROR! Expression::value() called on \n\t"
        << this->get_tags()
        << "\nbut this expression does not compute just one value.\n"
        << "Apparently, it computes " << exprValues_.size() << " values\n"
        << "Use Expression:value() instead\n";
    throw std::runtime_error( msg.str() );
  }
# endif
  return myFieldReqs_.get_field_request( get_tag() )->field_ref();
}
//--------------------------------------------------------------------
template<typename ValT>
void
Expression<ValT>::set_computed_tags( const TagList& tags,
                                     const SpatialOps::GhostData& ghosts )
{
  exprNames_ = tags;
  myGhosts_  = ghosts;
  FieldAggregator<ValT>& fa = dynamic_cast<FieldAggregator<ValT>&>( *myFieldAggregator_ );
  for( const Tag& tag: tags ){
    fa.create_field_request( tag, ghosts );
  }
}
//--------------------------------------------------------------------
template<typename ValT>
inline void
Expression<ValT>::bind_my_fields( FieldManagerList& fml, const bool computedOnly )
{
  typename FieldMgrSelector<ValT>::type& fm = fml.field_manager<ValT>();

  myFieldReqs_.set_fields( fm );

  exprValues_.clear();
  for( const Tag& tag: this->get_tags() ){
    typename FieldAggregator<ValT>::FieldReqPtr fieldReq = myFieldReqs_.get_field_request(tag);
    ValPtr field = fieldReq->field_ptr();
    exprValues_.push_back( field );
#   ifdef ENABLE_CUDA
    field->set_stream( cudaStream_ );
#   endif
  }

  if( computedOnly ) return;

  for( FieldAggregators::value_type& vt: fieldAggregators_ ){
    vt.second->set_fields( fml );
  }
}
//--------------------------------------------------------------------
template<typename ValT>
void
Expression<ValT>::activate_sensitivity( const Tag& var  )
{
  myFieldAggregator_->add_sensitivity( var );

  if( override_sensitivity() ) return;
  if( overrides_sensitivity(var) ) return;

  for( FieldAggregators::value_type& vt: fieldAggregators_ ){
    vt.second->add_sensitivity(var);
  }

  for( ExpressionBase* modifier: this->modifiers_ ){
    modifier->activate_sensitivity( var );
  }
}
//--------------------------------------------------------------------
template<typename ValT>
template<typename FT>
boost::shared_ptr< FieldRequest<FT> >
Expression<ValT>::create_field_request( const Tag& tag )
{
  FieldAggregator<FT>& fa = get_aggregator<FT>( fieldAggregators_ );
  return fa.create_field_request( tag, this->ghosts() );
}
//--------------------------------------------------------------------
template<typename ValT>
template<typename FT>
void
Expression<ValT>::
create_field_vector_request( const TagList& tags,
                             std::vector<boost::shared_ptr< const FieldRequest<FT> > >& vec )
{
  FieldAggregator<FT>& fa = get_aggregator<FT>( fieldAggregators_ );
  vec.clear();
  for( const Tag& tag: tags ){
    vec.push_back( fa.create_field_request(tag,this->ghosts()) );
  }
}
//--------------------------------------------------------------------
template<typename ValT>
inline void
Expression<ValT>::base_evaluate()
{
  using namespace SpatialOps;
  this->evaluate();

  // add extra terms to this expression as needed.
  if( !is_placeholder() ){  // placeholder expressions don't have source terms
    for( ExtraSrcTerms::const_iterator isrc=srcTerms_.begin(); isrc!=srcTerms_.end(); ++isrc ){
      if( isrc->op == NULL_OP ) continue;
      const Expression<ValT>* const src = dynamic_cast<const Expression<ValT>*>( isrc->srcExpr );
      const ValT& rhsVal = *(src->get_value_vec()[isrc->srcIx]);
      ValT& exprVal = *(this->exprValues_[isrc->myIx]);
      switch( isrc->op ){
        case ADD_SOURCE_EXPRESSION     :  exprVal <<= exprVal+rhsVal;  break;
        case SUBTRACT_SOURCE_EXPRESSION:  exprVal <<= exprVal-rhsVal;  break;
        case NULL_OP                   :                               break;
      }
    }
  }

  for( typename SignalMap::iterator isig=postEvalSignal_.begin(); isig!=postEvalSignal_.end(); ++isig ){
    Signal& sig = *(isig->second);
    sig( *this->exprValues_[isig->first] );
  }

  for( ExpressionBase* modifier: this->modifiers_ ){
    static_cast<Expression<ValT>*>(modifier)->evaluate();
  }

  for( const Tag& sensTag: myFieldAggregator_->get_sensitivity() ){
    const auto sensOverride = sensOverride_.find( sensTag );
    if( sensOverride != sensOverride_.end()){
      for( const Tag& varTag: get_tags() ){
        this->sensitivity_result( varTag, sensTag ) <<= sensOverride->second;
      }
    }
    else{
      this->sensitivity( sensTag );
    }
  }

  // include sensitivities from extra terms as needed.
  for( ExtraSrcTerms::const_iterator isrc=srcTerms_.begin(); isrc!=srcTerms_.end(); ++isrc ){
    if( isrc->op == NULL_OP ) continue;
    const Tag& targetTag = this->get_tags()[isrc->myIx];
    const Expression<ValT>* const src = dynamic_cast<const Expression<ValT>*>( isrc->srcExpr );
    for( const Tag& ivarTag: myFieldAggregator_->get_sensitivity() ){
      if( overrides_sensitivity(ivarTag) ) continue;
      const ValT& srcSens = src->sensitivity_result( src->get_tags()[isrc->srcIx], ivarTag );
      ValT& exprSens = this->sensitivity_result( targetTag, ivarTag );
      switch( isrc->op ){
        case ADD_SOURCE_EXPRESSION     : exprSens <<= exprSens + srcSens; break;
        case SUBTRACT_SOURCE_EXPRESSION: exprSens <<= exprSens - srcSens; break;
        case NULL_OP                   :                                  break;
      }
    }
  }
# ifdef ENABLE_CUDA
  cudaStreamSynchronize( this->get_cuda_stream() );
# endif

  // invalidate all fields to prevent unauthorized access
  for( FieldAggregators::value_type& vt: fieldAggregators_ ){
    vt.second->invalidate();
  }
  exprValues_.clear();
}
//--------------------------------------------------------------------
template<typename ValT>
void
Expression<ValT>::process_after_evaluate( typename Signal::slot_function_type subscriber,
                                          const bool isGPUReady )
{
  if( exprValues_.size() > 1 ){
    std::ostringstream msg;
    msg << "ERROR from " << __FILE__ << " : " << __LINE__ << std::endl
        << "      Cannot add post processing for expressions with multiple fields to be evaluated!" << std::endl
        << "      Expression name(s): ";
    const TagList& names = this->get_tags();
    for( TagList::const_iterator inm=names.begin(); inm!=names.end(); ++inm ){
      msg << "'" << inm->field_name() << "', ";
    }
    msg << std::endl << std::endl;
    throw std::runtime_error( msg.str() );
  }
  process_after_evaluate( this->get_tag().field_name(), subscriber, isGPUReady );
}
//--------------------------------------------------------------------
template<typename ValT>
void
Expression<ValT>::process_after_evaluate( const std::string fieldName,
                                          typename Signal::slot_function_type subscriber,
                                          const bool isGPUReady )
{
  const TagList& names = this->get_tags();
  size_t ii=0;
  for( typename TagList::const_iterator inm=names.begin(); inm!=names.end(); ++inm, ++ii ){
    if( inm->field_name() == fieldName ) break;
  }
  if( ii == names.size() ){
    std::ostringstream msg;
    msg << "ERROR from " << __FILE__ << " : " << __LINE__ << std::endl
        << "      Expression evaluating " << std::endl
        << names << std::endl
        << "      does not have a field named '" << fieldName << "'" << std::endl;
  }
  typename SignalMap::iterator ism = postEvalSignal_.find( ii );
  if( ism == postEvalSignal_.end() ){
    Signal* sig = new Signal;
    ism = postEvalSignal_.insert( std::make_pair(ii,sig) ).first;
  }
  ism->second->connect(subscriber);

  if( this->is_gpu_runnable() ){
    if( !isGPUReady ) this->set_gpu_runnable( false );
  }
}

//--------------------------------------------------------------------

template<typename ValT>
std::vector<std::string>
Expression<ValT>::post_proc_names() const
{
  std::vector<std::string> names;
  for( const Tag& mod: modifierNames_ ){
    names.push_back( mod.name() );
  }
  for( size_t i=0; i<postEvalSignal_.size(); ++i ){
    names.push_back( "ProcAfterEval_" + boost::lexical_cast<std::string>(i+1) );
  }
  return names;
}

//--------------------------------------------------------------------

template<typename ValT>
ExpressionBase*
Expression<ValT>::as_placeholder_expression() const
{
  typedef typename Expr::PlaceHolder<ValT>::Builder Builder;
  const Builder builder( this->get_tags() );
  ExpressionBase* expr = builder.build();
  expr->set_computed_tags( this->get_tags(), this->ghosts() );
  expr->activate_sensitivity( this->myFieldAggregator_->get_sensitivity() );
  return expr;
}

//--------------------------------------------------------------------

} // namespace Expr


#endif // Expression_h
