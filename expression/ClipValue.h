/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef ClipValue_Expr_h
#define ClipValue_Expr_h

#include <expression/Expression.h>

namespace Expr{

/**
 *  \class ClipValue
 *  \author James C. Sutherland
 *  \brief Simple way of enforcing bounds on a field
 *  \par Example
 *  \code{.cpp}
 *   // clip values in "fieldTag" to [0,1]
 *   ClipValue<FieldT> clipper( clippedFieldTag, 0.0, 1.0 );
 *   factory.attach_modifier_expression( clippedFieldTag, fieldTag );
 *  \endcode
 */
template< typename FieldT >
class ClipValue
 : public Expr::Expression<FieldT>
{
public:

  enum Options{
    CLIP_MAX_ONLY, ///< clipping only on maximum value
    CLIP_MIN_ONLY, ///< clipping only on minimum value
    CLIP_BOTH      ///< clip on both max and min values
  };

private:
  const double minVal_, maxVal_;
  const Options opt_;
  
  ClipValue( const double minValue,
             const double maxValue,
             const Options opt )
    : Expr::Expression<FieldT>(),
      minVal_( minValue ),
      maxVal_( maxValue ),
      opt_( opt )
  {
    this->set_gpu_runnable(true);
  }

public:

  class Builder : public Expr::ExpressionBuilder
  {
    const double minVal_, maxVal_;
    const Options opt_;
  public:
    /**
     *  @brief Build a ClipValue expression
     *  @param fieldTag the tag for the field we want to clip
     *  @param minValue the minimum allowable value in the field
     *  @param maxValue the maximum allowable value in the field
     *  @param opt Allows you to choose to disregard either min or max.
     *  @param nghost [optional] the number of ghost cells to compute on
     */
    Builder( const Expr::Tag& fieldTag,
             const double minValue,
             const double maxValue,
             const Options opt=CLIP_BOTH,
             const int nghost = DEFAULT_NUMBER_OF_GHOSTS )
      : ExpressionBuilder( fieldTag, nghost ),
        minVal_( minValue ),
        maxVal_( maxValue ),
        opt_( opt )
    {}

    Expr::ExpressionBase* build() const{
      return new ClipValue<FieldT>( minVal_, maxVal_, opt_ );
    }
  };

  ~ClipValue(){}

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    switch( opt_ ){
      case CLIP_MAX_ONLY: result <<= min( result, maxVal_ ); break;
      case CLIP_MIN_ONLY: result <<= max( result, minVal_ ); break;
      case CLIP_BOTH:
      default:
        result <<= max( min( result,
                             maxVal_ ),
                        minVal_ );
    }
  }
};

} // namespace Expr

#endif // ClipValue_Expr_h
