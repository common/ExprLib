/**
 *  \file   TimeStepper.h
 *  \author James C. Sutherland
 *
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef Expr_TimeStepper_h
#define Expr_TimeStepper_h

#include <set>
#include <fstream>

#include <spatialops/SpatialOpsConfigure.h> // defines thread stuff
#include <spatialops/Nebo.h>
#include <spatialops/structured/FieldHelper.h>

#include <expression/ExprFwd.h>
#include <expression/ExpressionTree.h>
#include <expression/FieldManagerList.h>
#include <expression/ExpressionFactory.h>
#include <expression/FieldManager.h>
#include <expression/FieldManagerList.h>
#include <expression/TimeAdvance.h>
#include <expression/Expression.h>

#ifdef ENABLE_THREADS
#  include <spatialops/ThreadPool.h>
#  include <boost/bind.hpp>
#endif

#ifdef ENABLE_CUDA
#include <cuda_runtime.h>
#endif

#include <boost/foreach.hpp>

namespace Expr{


  /**
   *  @class  TimeStepper
   *  @author James C. Sutherland
   *  @date   March, 2008
   *  @brief  Explicit time stepping compatible with expressions.
   *
   *  The TimeStepper class provides a way to perform explicit time
   *  integration using the Expression approach.  It works with
   *  heterogeneous Expression types.  In other words, it can integrate
   *  fields that are of different types simultaneously.  The
   *  TimeStepper class manages field registration and also builds the
   *  appropriate ExpressionTree to evaluate the RHS for all fields that
   *  it integrates.
   *
   *  \par How to use the TimeStepper
   *   \li Register all Expression and SpatialOperator objects that are needed.
   *   \li Create a Patch.
   *   \li Build a TimeStepper
   *   \li Add all desired equations via the
   *       <code>TimeStepper::add_equation()</code> method.
   *   \li Call <code>TimeStepper::finalize()</code> to register all fields
   *       and bind them to all of the relevant expressions.
   *   \li Integrate forward in time.
   *
   *  Example:
   *   \code
   *   TimeStepper integrator( patch );
   *   integrator.add_equation( "solnVar1", rhsExprTag1, nghost );
   *   integrator.add_equation( "solnVar2", rhsExprTag2, nghost );
   *   integrator.finalize();
   *   while( time < time_end ){
   *      integrator.step( timestep );
   *      time += timestep;
   *   }
   *   \endcode
   *
   *  \par The Simulation Time
   *   The TimeStepper class registers a variable "time" with state
   *   Expr::STATE_NONE that contains the solution time.
   *
   *  @todo Need to set the simulation time at each stage of the
   *        integrator - not only on each step.
   *
   *  @todo - Consider templating this class on the stepper type.  Then
   *  we could implement various RK flavors (low-storage).
   *
   *  @todo Need to deal with tree splitting.  Currently this is not
   *  supported in the integrator.
   */
  class TimeStepper
  {
    typedef SpatialOps::SingleValueField DoubleVal;

  public:

    /**
     *  @brief Create a TimeStepper
     *  @param factory The ExpressionFactory where the expressions are registered.
     *  @param method The time integration method (see TSMethod enum)
     *  @param name a name identifying this timestepper.
     *  @param patchID the ID for the patch that this TimeStepper lives on.
     *  @param timeTag the tag describing the time variable.  Defaults to "time"
     *  @param tsTag the tag for the timestep
     */
    inline TimeStepper( ExpressionFactory& factory,
                        const TSMethod method=FORWARD_EULER,
                        const std::string name="time-stepper",
                        const int patchID=0,
                        const Tag timeTag = Tag( "time",     STATE_NONE ),
                        const Tag tsTag   = Tag( "timestep", STATE_NONE ) );

    inline virtual ~TimeStepper();

    /**
     * This will result in timings for each node in the graph being reported to
     * a file.  The file is named using the name provided when the TimeStepper
     * is constructed, appended with '_timings.log'.
     *
     * @param setting [default true] activate/deactivate timings.
     */
    void request_timings( const bool setting=true ){ masterTree_->request_timings(setting); }

    /*
     * @brief a method to be implemented by users of TimeStepper.h to allow
     *        custom work to be done after each step (stage in RK integrators).
     */
    inline virtual void post_step(){}

    inline int patch_id() const{ return masterTree_->patch_id(); }

    /** @brief Get the ExpressionTree that the timestepper is using */
    inline const ExpressionTree* get_tree() const{ return masterTree_; }
    inline ExpressionTree* get_tree()      { return masterTree_; }

    /** @brief Get the list of variable names that are being solved on
     *  this TimeStepper
     */
    inline void get_variables( std::vector<std::string>& variables ) const{
      variables = solnVars_;
    }


    /** @brief set the time */
    inline void set_time( const double time );

    /**
     * @return the current time
     */
    inline double get_time() const{
      return timeShadow_;
    }

    /** @brief Take a single timestep */
    inline void step( const double timeStep );

    /**
     *  @brief Add an equation to the timestepper.
     *
     *  @param solnVarName The name of the variable that is to be
     *  integrated in time.  This variable will be automatically
     *  registered.
     *
     *  @param rhsTag The Tag for the Expression that calculates the
     *  right-hand-side function for this equation.
     *
     *  @param nghost the number of ghost cells to use in updating the solution for this variable
     *
     *  @param fmlID for use in situations where multiple FieldManagerLists are
     *  employed, this identifies which one should be associated with this RHS.
     */
    template<typename FieldT>
    inline void
    add_equation( const std::string solnVarName,
                  const Tag rhsTag,
                  const SpatialOps::GhostData nghost,
                  const int fmlID = DEFAULT_FML_ID )
    {
      typedef typename TimeAdvance<FieldT>::Builder Stepper;
      typedef typename PlaceHolder<FieldT>::Builder OldValue;

      solnVars_.push_back( solnVarName );

      const Tag phiN  ( solnVarName, STATE_N    );
      const Tag phiNP1( solnVarName, STATE_NP1  );
      const Tag phiTmp( solnVarName, STATE_NONE );

      lockedFields_.insert( phiN   );
      lockedFields_.insert( phiNP1 );
      lockedFields_.insert( phiTmp );

      factory_.register_expression( new OldValue( phiN  , nghost ), false, fmlID );
      factory_.register_expression( new OldValue( phiTmp, nghost ), false, fmlID );

      ExpressionBuilder* builder = new Stepper( solnVarName, rhsTag, method_, tsTag_, rkStageTag_ );
      exprIDs_.insert( factory_.register_expression( builder, false, fmlID ) );

      get_aggregator<FieldT>(fieldDeps_).create_field_request( phiTmp, nghost );

      typedef typename TimeStepper::Updater<FieldT> Updater;
      updaters_[fmlID].push_back( new Updater(solnVarName,method_) );
    }

    /**
     *  @brief Add a group of equations whose RHS is evaluated by a
     *         single expression to the timestepper.
     *
     *  @param solnVarNames a vector of names of the solution
     *         variables.  The order must be exactly the same as the
     *         corresponding RHS evaluation order in the expression
     *         that calculates the RHS for these variables.
     *
     *  @param rhsTags The tags for the expressions that calculate the RHS for
     *         all of these solution variables, ordered consistently with solnVarNames
     *
     *  @param nghost the number of ghost cells to use in updating the solution for these variables
     *
     *  @param fmlID for use in situations where multiple FieldManagerLists are
     *         employed, this identifies which one should be associated with
     *         this RHS.
     */
    template<typename FieldT>
    inline void
    add_equations( const std::vector<std::string>& solnVarNames,
                   const TagList& rhsTags,
                   const SpatialOps::GhostData nghost,
                   const int fmlID = DEFAULT_FML_ID )
    {
      assert( !isFinalized_ );

      if( rhsTags.size() != solnVarNames.size() ){
        std::ostringstream msg;
        msg << "ERROR: TimeStepper::add_equations() was given a set of solution variable names" << std::endl
            << "       that is inconsistent with the number of RHS evaluated by the RHS Expression ID provided." << std::endl
            << __FILE__ << " : " << __LINE__ << std::endl;
        throw std::runtime_error( msg.str() );
      }

      typedef typename TimeAdvance<FieldT>::Builder Stepper;
      typedef typename PlaceHolder<FieldT>::Builder OldValue;

      for( size_t i=0; i<rhsTags.size(); ++i ){

        solnVars_.push_back( solnVarNames[i] );

        const Tag phiN  ( solnVarNames[i], STATE_N    );
        const Tag phiNP1( solnVarNames[i], STATE_NP1  );
        const Tag phiTmp( solnVarNames[i], STATE_NONE );

        lockedFields_.insert( phiN   );
        lockedFields_.insert( phiNP1 );
        lockedFields_.insert( phiTmp );

        factory_.register_expression( new OldValue( phiN,   nghost ), false, fmlID );
        factory_.register_expression( new OldValue( phiTmp, nghost ), false, fmlID );

        ExpressionBuilder* builder = new Stepper( solnVarNames[i], rhsTags[i], method_, tsTag_, rkStageTag_ );
        exprIDs_.insert( factory_.register_expression( builder, false, fmlID ) );

        get_aggregator<FieldT>(fieldDeps_).create_field_request( phiTmp, nghost );

        typedef typename TimeStepper::Updater<FieldT> Updater;
        updaters_[fmlID].push_back( new Updater(solnVarNames[i],method_) );
      }
    }
    
    /**
     * @brief Adds a set of ids to the exprIDs set as root nodes of the tree. 
     * 
     * @param ids - the set of expression ids to be added.
     * 
     */
     inline void add_ids(IDSet ids);

    /**
     *  @brief After adding all equations to the TimeStepper, this
     *  method should be called.  This results in the variables being
     *  registered, the ExpressionTree being set up, etc.
     *
     *  @param fml - the FieldManagerList that should be associated
     *  with this TimeStepper.
     *
     *  @param opDB - the OperatorDatabase to use in retrieving
     *  operators for the Expression objects on this TimeStepper.
     *
     *  @param t - must play nicely with the FieldManagerList::allocate_fields()
     *   method, which in turn calls through to FieldManagers, passing this object.
     */
    template< typename T >
    inline void finalize( FieldManagerList& fml,
                          const SpatialOps::OperatorDatabase& opDB,
                          const T& t );
    /**
     *  @brief Use this interface when multiple FieldManagerList objects are
     *   being maintained on this TimeStepper.
     *
     *  @param fmls - the FMLMap that should be associated
     *  with this TimeStepper.
     *
     *  @param opDBs - the OperatorDatabase objects to use in retrieving
     *  operators for the Expression objects on this TimeStepper.
     *
     *  @param tmap - Map of FML ids to FieldInfo objects that will be used to
     *   allocate fields on each corresponding FieldManagerList.
     */
    template< typename T >
    inline void finalize( FMLMap& fmls,
                          const OpDBMap& opDBs,
                          std::map<int,const T*>& tmap );

  private:

    class UpdaterBase{
    protected:
      const Tag phiNTag_, phiNP1Tag_, phiTmpTag_;
      const TSMethod method_;
    public:
      virtual void update() = 0;
      virtual void pre_step() = 0;
      virtual void bind_vars( FieldManagerList& fml ) = 0;
      UpdaterBase( const std::string& varName, const TSMethod method )
        : phiNTag_  ( varName, STATE_N    ),
          phiNP1Tag_( varName, STATE_NP1  ),
          phiTmpTag_( varName, STATE_NONE ),
          method_( method )
      {}
      virtual ~UpdaterBase(){};
    };

    template<typename T>
    class Updater : public UpdaterBase
    {
      T* phiN_;
      T* phiTmp_;
      T* phiNP1_;
    public:
      Updater( const std::string& varName, const TSMethod method )
        : UpdaterBase( varName, method )
      {}

      inline void bind_vars( FieldManagerList& fml )
      {
        typename Expr::FieldMgrSelector<T>::type& fm = fml.field_manager<T>();
        phiN_   = &fm.field_ref( phiNTag_   );
        phiNP1_ = &fm.field_ref( phiNP1Tag_ );

        if( this->method_ != FORWARD_EULER ){
          phiTmp_ = &fm.field_ref( phiTmpTag_ );
          phiTmp_->set_device_as_active( phiN_->active_device_index() );
        }
      }

      inline void pre_step(){
        using namespace SpatialOps;
        if( this->method_ == FORWARD_EULER ) return;
        // this should only pay a penalty the first time through the graph
        phiTmp_->set_device_as_active( phiN_->active_device_index() );
        *phiTmp_ <<= *phiN_;
#       ifdef ENABLE_CUDA
        phiTmp_->wait_for_synchronization();
#       endif
      }

      inline void update(){
        using namespace SpatialOps;
        phiN_->swap( *phiNP1_ );
      }
    };

    const Tag timeTag_, tsTag_, rkStageTag_;

    typedef std::vector<ExpressionID> ExprIDVec;

    TagSet                  lockedFields_;
    std::vector<std::string>solnVars_;
    ExpressionFactory&      factory_;
    const TSMethod          method_;       ///< Identifies the method used for time integration
    FieldAggregators        fieldDeps_;    ///< A composite list of all fields required for this TimeStepper.
    IDSet                   exprIDs_;      ///< The ids for the rhs expressions corresponding to the variables solved on this TimeStepper.
    ExpressionTree* const   masterTree_;   ///< The tree that is executed to calculate all RHS expressions.
    bool                    isFinalized_;  ///< flag indicating if the integrator has been finalized
    std::vector<DoubleVal*> simtime_;      ///< The time
    double                  timeShadow_;   ///< The time - always on CPU, and tracks simtime_

    std::vector<DoubleVal*> rkStage_, timeStep_;

    typedef std::list< UpdaterBase* > UpdaterList;
    typedef std::map<int,UpdaterList> UpdaterMap;
    UpdaterMap updaters_;

    TimeStepper(); // no default constructor
    TimeStepper( const TimeStepper& );  // no copying
    TimeStepper& operator=( const TimeStepper& ); // no assignment

  }; // class TimeStepper



  //####################################################################
  //
  //
  //                        IMPLEMENTATIONS
  //
  //
  //####################################################################



  //--------------------------------------------------------------------

  TimeStepper::TimeStepper( ExpressionFactory& factory,
                            const TSMethod method,
                            const std::string name,
                            const int patchID,
                            const Tag timeTag,
                            const Tag tsTag )
  : timeTag_( timeTag ),
    tsTag_  ( tsTag   ),
    rkStageTag_( "RKStage", STATE_NONE ),
    factory_( factory ),
    method_( method ),
    masterTree_( new ExpressionTree( factory, patchID, name ) )
  {
    isFinalized_ = false;

    timeShadow_ = 0.0; // default start time

    lockedFields_.insert( timeTag_ );
    lockedFields_.insert( tsTag_   );
    FieldAggregator<DoubleVal>& fieldAgg = get_aggregator<DoubleVal>( fieldDeps_ );
    fieldAgg.create_field_request( timeTag_, SpatialOps::GhostData(0) );
    fieldAgg.create_field_request( tsTag_  , SpatialOps::GhostData(0) );

    if( method_ == SSPRK2 || method_ == SSPRK3 ){
      fieldAgg.create_field_request( rkStageTag_, SpatialOps::GhostData(0) );
      lockedFields_.insert( rkStageTag_ );
    }
  }

  //--------------------------------------------------------------------

  TimeStepper::~TimeStepper()
  {
    delete masterTree_;
    BOOST_FOREACH( UpdaterMap::value_type& updl, updaters_ ){
      BOOST_FOREACH( UpdaterBase* upd, updl.second ) delete upd;
    }
  }

  //--------------------------------------------------------------------

  void
  TimeStepper::set_time( const double time )
  {
    using namespace SpatialOps;
    assert(isFinalized_);
    BOOST_FOREACH( DoubleVal* t, simtime_ ){
      *t <<= time;
    }
    timeShadow_ = time;
  # ifdef ENABLE_CUDA
    BOOST_FOREACH( DoubleVal* t, simtime_ ){
      t->wait_for_synchronization();
    }
  # endif
  }

  //--------------------------------------------------------------------

  void
  TimeStepper::add_ids(IDSet ids)
  {
   assert(!isFinalized_); // can only add up until finalized;
   exprIDs_.insert(ids.begin(), ids.end());
  }

  //--------------------------------------------------------------------

  void
  TimeStepper::step( const double tstep )
  {
    using namespace SpatialOps;

    if( !isFinalized_ ){
      std::ostringstream msg;
      msg << "ERROR from " << __FILE__ " : " << __LINE__
          << "Timestepper must be finalized before calling 'step()'\n";
      throw std::runtime_error( msg.str() );
    }

    // set value of the timestep
    BOOST_FOREACH( DoubleVal* ts, timeStep_ ){
      *ts <<= tstep;
    }

    BOOST_FOREACH( UpdaterMap::value_type& updl, updaters_ ){
      BOOST_FOREACH( UpdaterBase* upd, updl.second ) upd->pre_step();
    }

  # ifdef ENABLE_CUDA
    BOOST_FOREACH( DoubleVal* ts, timeStep_ ){
      ts->wait_for_synchronization();
    }
  # endif

    switch ( method_ ){
      case FORWARD_EULER:
        masterTree_->execute_tree();
        BOOST_FOREACH( UpdaterMap::value_type& updl, updaters_ ){
          BOOST_FOREACH( UpdaterBase* upd, updl.second ) upd->update();
        }
        post_step();
        break;

      case SSPRK2:
      case SSPRK3:{
        const short int nstage = (method_ == SSPRK2) ? 2 : 3;

        SpatFldPtr<DoubleVal> t0 = SpatialFieldStore::get<DoubleVal>( *simtime_[0] );
        *t0 <<= *simtime_[0];
  #     ifdef ENABLE_CUDA
        t0->wait_for_synchronization();
  #     endif

        for( short int istage=1; istage<=nstage; ++istage ){

          // set the RK stage
          BOOST_FOREACH( DoubleVal* rk, rkStage_ ){
            *rk <<= istage;
          }
  #       ifdef ENABLE_CUDA
          BOOST_FOREACH( DoubleVal* rk, rkStage_ ){
            rk->wait_for_synchronization();
          }
  #       endif

          // set the simulation time
          BOOST_FOREACH( DoubleVal* t, simtime_ ){
            switch( istage ){
              case 1: *t <<= *t0;             break;
              case 2: *t <<= *t0 + tstep;     break;
              case 3: *t <<= *t0 + 0.5*tstep; break;
              default: break;
            }
          }
  #       ifdef ENABLE_CUDA
          BOOST_FOREACH( DoubleVal* t, simtime_ ){
            t->wait_for_synchronization();
          }
  #       endif
          masterTree_->execute_tree();

          BOOST_FOREACH( UpdaterMap::value_type& updl, updaters_ ){
            BOOST_FOREACH( UpdaterBase* upd, updl.second ) upd->update();
          }
          post_step();
        }
        // reset the time - updated later
        BOOST_FOREACH( DoubleVal* t, simtime_ ){
          *t <<= *t0;
        }
  #     ifdef ENABLE_CUDA
        BOOST_FOREACH( DoubleVal* t, simtime_ ){
          t->wait_for_synchronization();
        }
  #     endif
        break;
      }

      default: assert(0); break;
    } // switch

    // update simulation time
    BOOST_FOREACH( DoubleVal* t, simtime_ ){
      *t <<= *t + tstep;
    }
  # ifdef ENABLE_CUDA
    BOOST_FOREACH( DoubleVal* t, simtime_ ){
      t->wait_for_synchronization();
    }
  # endif
    timeShadow_ += tstep;
  }

  //--------------------------------------------------------------------

  template< typename T >
  void
  TimeStepper::finalize( FieldManagerList& fml,
                         const SpatialOps::OperatorDatabase& opDB,
                         const T& t )
  {
    FMLMap fmls;                  fmls [DEFAULT_FML_ID] = &fml;
    std::map<int,const T*> tmap;  tmap [DEFAULT_FML_ID] = &t;
    OpDBMap opDBs;                opDBs[DEFAULT_FML_ID] = &opDB;
    finalize<T>(fmls,opDBs,tmap);
  }

  //--------------------------------------------------------------------

  template< typename T >
  void
  TimeStepper::finalize( FMLMap& fmls,
                         const OpDBMap& opDBMap,
                         std::map<int,const T*>& tmap )
  {
    BOOST_FOREACH( FMLMap::value_type fmlpair, fmls ){
      const int fmlID = fmlpair.first;
      typedef PlaceHolder<DoubleVal>::Builder Stub;
      factory_.register_expression( new Stub(timeTag_), true, fmlID );
      factory_.register_expression( new Stub(tsTag_  ), true, fmlID );
      if( method_ == SSPRK2 || method_ == SSPRK3 ){
        factory_.register_expression( new Stub(rkStageTag_), true, fmlID );
      }
    }

    masterTree_->insert_tree( exprIDs_ );

    //--- register fields
    masterTree_->register_fields( fmls );

    BOOST_FOREACH( FieldAggregators::value_type& vt, fieldDeps_ ){
      BOOST_FOREACH( FMLMap::value_type fmlpair, fmls ){
        vt.second->register_fields( *fmlpair.second );
      }
    }

    timeStep_.clear();
    rkStage_.clear();

    //--- allocate fields
    assert( fmls.size() == tmap.size() );
    BOOST_FOREACH( FMLMap::value_type fmlval, fmls ){
      const int id = fmlval.first;
      FieldManagerList* const fml = fmlval.second;
      UpdaterList& updl = updaters_[id];

      // extract the appropriate FieldInfo to allocate fields on this fml
      typename std::map<int,const T*>::iterator itm = tmap.find(id);
      assert( itm != tmap.end() );
      fml->allocate_fields( *itm->second );

      // lock fields as necessary
      for( FieldManagerList::iterator ifm = fml->begin(); ifm != fml->end(); ++ifm ){
        BOOST_FOREACH( const Tag& t, lockedFields_ ){
          ifm->second->lock_field( t );
        }
      }

      timeStep_.push_back( &fml->field_ref<DoubleVal>(tsTag_) );

      if( method_ == SSPRK2 || method_ == SSPRK3 )
        rkStage_.push_back( &fml->field_ref<DoubleVal>(rkStageTag_) );

      // resolve the time field
      DoubleVal& t = fml->field_ref<DoubleVal>(timeTag_);
      t <<= timeShadow_;
      simtime_.push_back( &t );

      /**
       * \note bind_vars would need to be called inside the step()
       *       method if field sizes etc. were changing in time.
       */
      BOOST_FOREACH( UpdaterBase* upd, updl ){
        upd->bind_vars( *fml );
      }

      t.wait_for_synchronization();
    }

    //--- bind all fields on the tree
    try{
      masterTree_->bind_fields( fmls );
    }
    catch( std::runtime_error& err ){
      std::ofstream out("tree_bind_failed.dot");
      masterTree_->write_tree( out );
      std::ostringstream msg;
      msg << __FILE__ << ":" << __LINE__ << std::endl
          << "Error binding fields."
          << " The expression tree has been written to 'tree_bind_failed.dot'"
          << std::endl << std::endl
          << err.what();
      throw std::runtime_error( msg.str() );
    }

    //--- bind operators to all expressions in the tree
    masterTree_->bind_operators( opDBMap );

    isFinalized_ = true;
  }

} // namespace Expr

#endif // Expr_TimeStepper_h
