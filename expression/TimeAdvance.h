/*
 * The MIT License
 *
 * Copyright (c) 2014-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef TimeAdvance_Expr_h
#define TimeAdvance_Expr_h


#include <spatialops/structured/FVStaggered.h>

#include <expression/ExprFwd.h>

/**
 *  \class TimeAdvance
 *  \author Tony Saad, James C. Sutherland
 *  \brief Calculates an updated solution variable.
 *  \tparam FieldT the field type for the TimeAdvance (nominally the scalar volume field)
 */
template< typename FieldT >
class TimeAdvance
 : public Expr::Expression<FieldT>
{
  typedef typename SpatialOps::SingleValueField SingleValue;

  const Expr::TSMethod ts_;

  DECLARE_FIELDS( FieldT, phiN_, phiStage_, rhs_ )
  DECLARE_FIELDS( SingleValue, dt_, rkStage_ )

  double alpha_[3];
  double beta_[3];

  TimeAdvance( const std::string& solnVarName,
               const Expr::Tag& rhsTag,
               const Expr::Tag& timeStepTag,
               const Expr::Tag& rkStageTag,
               const Expr::TSMethod ts )
    : Expr::Expression<FieldT>(),
      ts_( ts )
  {
    this->set_gpu_runnable( true );

    switch( ts_ ){
      default:
      case Expr::FORWARD_EULER:
        alpha_[0] = 1.0; beta_[0]  = 1.0;
        alpha_[1] = 0.0; beta_[1]  = 0.0;
        alpha_[2] = 0.0; beta_[2]  = 0.0;
        break;
      case Expr::SSPRK2:
        alpha_[0] = 1.0; beta_[0]  = 1.0;
        alpha_[1] = 0.5; beta_[1]  = 0.5;
        alpha_[2] = 0.0; beta_[2]  = 0.0;
        break;
      case Expr::SSPRK3:
        alpha_[0] = 1.0;     beta_[0]  = 1.0;
        alpha_[1] = 0.75;    beta_[1]  = 0.25;
        alpha_[2] = 1.0/3.0; beta_[2]  = 2.0/3.0;
        break;
    }

    rhs_  = this->template create_field_request<FieldT>( rhsTag  );
    phiN_ = this->template create_field_request<FieldT>( Expr::Tag( solnVarName, Expr::STATE_N ) );
    dt_   = this->template create_field_request<SpatialOps::SingleValueField>( timeStepTag );

    switch( ts_ ){
      case Expr::FORWARD_EULER:
        break;
      case Expr::SSPRK2:
      case Expr::SSPRK3:
        rkStage_ = this->template create_field_request<SpatialOps::SingleValueField>( rkStageTag );
        phiStage_= this->template create_field_request<FieldT>( Expr::Tag( solnVarName, Expr::STATE_NONE ) );
        break;
    }
  }

public:
  class Builder : public Expr::ExpressionBuilder
  {
  public:

    /**
     * @param phiName the solution variable name
     * @param rhsTag the RHS for the solution variable
     * @param ts the time integration method
     * @param timeStepTag the tag for the timestep value
     * @param rkStageTag the tag indicating which RK stage we are on (only for RK integrators)
     */
    Builder( const std::string& phiName,
             const Expr::Tag& rhsTag,
             const Expr::TSMethod ts,
             const Expr::Tag timeStepTag,
             const Expr::Tag rkStageTag = Expr::Tag() )
      : ExpressionBuilder( Expr::Tag( phiName, Expr::STATE_NP1 ) ),
        phiName_( phiName     ),
        rhst_   ( rhsTag      ),
        tsTag_  ( timeStepTag ),
        rkTag_  ( rkStageTag  ),
        ts_     ( ts          )
    {}

    
    ~Builder(){}
    Expr::ExpressionBase* build() const
    {
      return new TimeAdvance<FieldT>( phiName_, rhst_, tsTag_, rkTag_, ts_ );
    }

  private:
    const std::string phiName_;
    const Expr::Tag rhst_, tsTag_, rkTag_;
    const Expr::TSMethod ts_;
  };

  ~TimeAdvance(){}

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& phi = this->value();

    const FieldT& rhs  = rhs_ ->field_ref();
    const FieldT& phiN = phiN_->field_ref();

    const SpatialOps::SingleValueField& dt = dt_->field_ref();

    const double a2 = alpha_[1];
    const double a3 = alpha_[2];

    const double b2 = beta_[1];
    const double b3 = beta_[2];

    switch( ts_ ){

      default:
      case Expr::FORWARD_EULER:
        phi <<= phiN + dt * rhs;
        break;

      case Expr::SSPRK2:
      case Expr::SSPRK3:
        // Since rkStage_ is a SpatialField, we cannot dereference it and use "if"
        // statements since that will break GPU execution. Therefore, we use cond here
        // to allow GPU execution of this expression, despite the fact that it causes
        // branching on the inner loop.  However, the same branch is followed for all
        // points in the loop, which shouldn't degrade GPU execution and branch
        // prediction on CPU should limit performance degradation.
        const SingleValueField& rkStage = rkStage_->field_ref();
        const FieldT& phiStage = phiStage_->field_ref();
        phi <<= cond( rkStage == 1.0, phiN + dt * rhs )
                    ( rkStage == 2.0, a2 * phiStage + b2 * ( phiN + dt * rhs ) )
                    ( rkStage == 3.0, a3 * phiStage + b3 * ( phiN + dt * rhs ) )
                    ( -11 ); // should never get here.
        break;
    }
  }

};



///**
// *  \class ShuffleSolutionVar
// */
//template< typename FieldT >
//class ShuffleSolutionVar : public Expr::Expression<FieldT>
//{
//  DECLARE_FIELD( FieldT, phiNew_ )
//
//  /* declare operators associated with this expression here */
//
//  ShuffleSolutionVar( const Expr::Tag& phiNewTag )
//    : Expr::Expression<FieldT>()
//  {
//    phiNew_ = this->template create_field_request<FieldT>(phiNewTag);
//  }
//
//public:
//  class Builder : public Expr::ExpressionBuilder
//  {
//  public:
//    /**
//     *  @brief Build a ShuffleSolutionVar expression
//     *  @param resultTag the tag for the value that this expression computes
//     */
//    Builder( const Expr::Tag& resultTag,
//             const Expr::Tag& phiNewTag )
//      : ExpressionBuilder( resultTag ),
//        phiNewTag_( phiNewTag )
//    {}
//
//    Expr::ExpressionBase* build() const{ return new ShuffleSolutionVar<FieldT>( phiNewTag_ ); }
//
//  private:
//    const Expr::Tag phiNewTag_;
//  };
//
//  void evaluate()
//  {
//    using namespace SpatialOps;
//    this->value() <<= phiNew_->field_ref();
//  }
//};


//--------------------------------------------------------------------

#endif // TimeAdvance_Expr_h
