/*
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

//Debug Flags : DEBUG_LOCK_ALL_FIELDS ( default undefined )
//#define DEBUG_LOCK_ALL_FIELDS

/* --- standard includes --- */
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cxxabi.h>

/* --- Boost includes --- */
#include <boost/graph/graphviz.hpp>
#include <boost/graph/transpose_graph.hpp>
#include <boost/graph/depth_first_search.hpp>

/* --- Expression includes --- */
#include <expression/ExpressionTree.h>
#include <expression/ExpressionFactory.h>

#ifdef ENABLE_THREADS
#include <spatialops/ThreadPool.h>
#endif

using std::cout;
using std::endl;

//#define PRUNE_DIAGNOSTICS

namespace Expr{

//------------------------------------------------------------------

template<typename Graph>
void print( std::ostream& os, Graph& g )
{
  os << "Vertices:";
  std::pair<VertIter,VertIter> verts = vertices(g);
  for( VertIter iv = verts.first; iv!=verts.second; ++iv ){
    const VertexProperty& vp = g[*iv];
    os << "\n\t(ix=" << vp.index << ") : ";
    const TagList& names = vp.expr->get_tags();
    for( const Tag& t: names ){
      os << t.field_name() << " ";
    }
  }
  os << endl;

  os << "Edges:";
  std::pair<EdgeIter,EdgeIter> es = edges(g);
  for( EdgeIter ie = es.first; ie!=es.second; ++ie ){
    const Vertex v1 = boost::source( *ie, g );
    const Vertex v2 = boost::target( *ie, g );
    os << "\n\t( " << g[v1].expr->get_tags()[0].field_name() << " -> " << g[v2].expr->get_tags()[0].field_name() << " )";
  }
  os << std::endl;
}

//------------------------------------------------------------------

struct ParaCalcVisitor : public boost::default_bfs_visitor
{
  double tim_;
  ParaCalcVisitor( ExpressionTree* parent ) {
    parent_ = parent;
    tim_ = 0.0;
  }
  ExpressionTree* parent_;

  void discover_vertex( Vertex v, const Graph& g )
  {
    Graph& g2 = const_cast<Graph&>(g);
    const VertexProperty& vp = g2[v];
    tim_ += vp.lastExecTime.elapsed_time();
    parent_->set_tinf( std::max( parent_->get_tinf(), tim_ ) );
  }
};

//------------------------------------------------------------------

class CleaveVisitor : public boost::default_bfs_visitor
{
  const IDSet& tagged_;
  IDSet& buildList_;
  IDSet& haveSeen_;

public:

  /**
   * @brief Used to determine vertices in child graphs resulting from cleaving
   * @param taggedIDs The set of IDs that we want to cleave.
   * @param haveSeen A set of tagged IDs that have been seen already during the
   *        search, indicating that there are multiple pathways to get to these
   *        through the graph we are searching.  Note that this set can have
   *        values pre-populated, which results in discovered nodes not being
   *        added to the buildList. Only nodes that have not been seen and are
   *        tagged will be added to the buildList.
   * @param buildList The list of tagged vertices that are found in the graph
   *        and are not already in the haveSeen set.
   */
  CleaveVisitor( const IDSet& taggedIDs,
                 IDSet& haveSeen,
                 IDSet& buildList )
    : tagged_( taggedIDs ),
      buildList_( buildList ),
      haveSeen_( haveSeen )
  {}

  void examine_vertex( const Vertex& v,
                       const Graph& g )
  {
    const VertexProperty& vp = g[v];
    const ExpressionID& id = vp.id;

    // see if this is a node we have tagged.  If it is, then we
    // add it to the build list unless we have already visited it.
    if( tagged_.find(id) != tagged_.end() ){
      if( haveSeen_.find(id) != haveSeen_.end() ){
        // already seen - remove from build list, since this will be a
        // root node of a descendant tree.
        buildList_.erase( id );
      }
      else{
        buildList_.insert( id );
      }
      haveSeen_.insert( id );
    }
  }

};

//------------------------------------------------------------------

class FieldPruneVisitor : public boost::default_bfs_visitor
{
  IDSet& retainedFields_;
  const IDSet& removalFields_;
public:
  FieldPruneVisitor( IDSet& retained,
                     const IDSet& toBePruned )
    : retainedFields_( retained ),
      removalFields_( toBePruned )
  {}

  void examine_edge( const Edge& e,
                     const Graph& g )
  {
    // if a field on the "source" side of the edge has not been tagged
    // for removal, then retain the source and destination fields.
    const ExpressionID sid = g[source(e,g)].id;
    if( removalFields_.find( sid ) == removalFields_.end() ){
      const ExpressionID tid = g[target(e,g)].id;
      retainedFields_.insert( sid );
      retainedFields_.insert( tid );
    }
  }
};

//--------------------------------------------------------------------

void cleave_tree( ExpressionTree::TreePtr parent,
                  IDSet& taggedList,
                  ExpressionTree::TreeList& treeList )
{
  if( parent->is_cleaved() ) return;
  parent->isCleaved_ = true;

  if( taggedList.empty() ){
    // terminate recursion - we are done.
    treeList.insert( treeList.begin(), parent );
    return;
  }

  IDSet buildList, haveSeen;
  Graph& graph = *parent->graph_;

  for( const ExpressionID id: taggedList ){
    // resolve this node in the graph and conduct a BFS off of it.
    CleaveVisitor cv( taggedList, haveSeen, buildList );
    boost::breadth_first_search( graph,
                                 find_vertex(graph,id),
                                 boost::color_map( boost::get(&VertexProperty::color,graph)).visitor(cv) );
  }

  // build a child graph containing the points marked for cleaving
  std::string childName;
  {
    const std::string::size_type n = parent->name().find("_child_");
    unsigned childNum = 1;
    if( n != std::string::npos ){
      childNum = boost::lexical_cast<unsigned>( parent->name().substr(n+7) ) + 1;
    }
    childName = parent->name().substr(0,n) + "_child_" + boost::lexical_cast<std::string>(childNum);
  }
  ExpressionTree::TreePtr child( new ExpressionTree( buildList,
                                                     parent->factory_,
                                                     parent->patch_id(),
                                                     childName ) );

  { // imprint persistence on child
    const std::pair<VertIter,VertIter> vertx = vertices(*(parent->graph_));
    for( VertIter iv = vertx.first; iv != vertx.second; iv++ ){
      const VertexProperty& vp = (*(parent->graph_))[*iv];
      if( child->has_expression(vp.id) ) (*child->graph_)[ find_vertex(*child->graph_,vp.id) ] = vp;
    }
  }

  // remove the cleaved nodes from the list of tagged nodes remaining to be cleaved.
  for( const ExpressionID id: buildList ){
    taggedList.erase( id );
  }

  // prune the child graph from the parent and then push the parent onto the treeList
  parent->prune( child );
  treeList.insert( treeList.begin(), parent );

  // now recurse onto the child graph to cleave it further if required
  cleave_tree( child, taggedList, treeList );
}

//===================================================================

ExpressionTree::ExpressionTree( const ExpressionID rootID,
                                ExpressionFactory& exprFactory,
                                const int patchid,
                                const std::string name )
  : name_   ( name        ),
    patchID_( patchid     ),
    factory_( exprFactory ),
    graph_ ( NULL ),
    graphT_( NULL )
{
# ifdef ENABLE_CUDA
  cudaError err;
  deviceID_        = 0;
  deviceAlreadySet_  = false;
# endif

  logNodeTimings_           = false;
  hasRequestedSensitivitiy_ = false;
  hasRegisteredFields_      = false;
  hasBoundFields_           = false;
  bUpdatePScore_            = false;
  bTimingsReady_            = false;
  isCleaved_                = false;

  rootIDs_.insert( rootID );

  compile_expression_tree();
}

//--------------------------------------------------------------------

ExpressionTree::ExpressionTree( const RootIDList& ids,
                                ExpressionFactory& factory,
                                const int patchid,
                                const std::string name )
  : name_   ( name    ),
    patchID_( patchid ),
    rootIDs_( ids.begin(), ids.end() ),
    factory_( factory ),
    graph_ ( NULL ),
    graphT_( NULL )
{
# ifdef ENABLE_CUDA
  cudaError err;
  deviceID_        = 0;
  deviceAlreadySet_  = false;
# endif

  logNodeTimings_           = false;
  hasRequestedSensitivitiy_ = false;
  hasRegisteredFields_      = false;
  hasBoundFields_           = false;
  bUpdatePScore_            = false;
  bTimingsReady_            = false;
  isCleaved_                = false;

  compile_expression_tree();
}

//--------------------------------------------------------------------

ExpressionTree::ExpressionTree( ExpressionFactory& exprFactory,
                                const int patchid,
                                const std::string name )
  : name_   ( name        ),
    patchID_( patchid     ),
    factory_( exprFactory ),
    graph_ (NULL),
    graphT_(NULL)
{
# ifdef ENABLE_CUDA
  deviceID_        = 0;
  deviceAlreadySet_  = false;
# endif

  logNodeTimings_           = false;
  hasRequestedSensitivitiy_ = false;
  bUpdatePScore_            = false;
  bTimingsReady_            = false;
  hasRegisteredFields_      = false;
  hasBoundFields_           = false;
  isCleaved_                = false;
}

//--------------------------------------------------------------------

ExpressionTree::~ExpressionTree()
{
  delete graph_;
  delete graphT_;
}

//--------------------------------------------------------------------

void
ExpressionTree::request_timings( const bool setting )
{
  logNodeTimings_ = setting;
  if( scheduler_ ){
    scheduler_->request_timings( logNodeTimings_ );
  }
}

//--------------------------------------------------------------------

void
ExpressionTree::insert_tree( const IDSet& ids, const bool recompile )
{
  for( const ExpressionID id: ids ){
    rootIDs_.insert( id );
  }
  hasRegisteredFields_ = false;
  if( recompile ) compile_expression_tree();
}

//--------------------------------------------------------------------

void
ExpressionTree::insert_tree( const ExpressionID rootID, const bool recompile )
{
  IDSet ids;
  ids.insert(rootID);
  insert_tree( ids, recompile );
}

//--------------------------------------------------------------------

void
ExpressionTree::insert_tree( ExpressionTree& tree, const bool recompile )
{
  insert_tree( tree.get_roots(), recompile );
}

//--------------------------------------------------------------------

void
ExpressionTree::execute_tree()
{
  if( !hasBoundFields_ ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "ERROR! Tree named '" << name() << "' has not yet bound fields" << std::endl;
    throw std::runtime_error( msg.str() );
  }
  if( !hasRegisteredFields_ ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << std::endl
        << "ERROR! Tree named '" << name() << "' has not yet registered fields" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  // be sure that we have a graph built.
  if( graph_ == NULL ){
    std::ostringstream msg;
    msg << __FILE__ << ":" << __LINE__ << std::endl
        << "ERROR!  Cannot execute the tree with root set: " << std::endl;
    for( const ExpressionID& id : rootIDs_ ){
      msg << "        "  << id << ", " << factory_.get_labels(id) << std::endl;
    }
    msg << "  because the graph does not exist." << std::endl
        << "  You must first run compile_expression_tree()" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  scheduler_->setup( hasRegisteredFields_ );

  if( !bUpdatePScore_ ){
    scheduler_->run();
  }
  else{
    scheduler_->run();
    bTimingsReady_ = true;
    push_schedule_timings();
    bUpdatePScore_ = false;
  }
}

//--------------------------------------------------------------------

void
ExpressionTree::register_fields( FieldManagerList& fml )
{
  // jcs deprecate use?
  FMLMap fmls; fmls[DEFAULT_FML_ID] = &fml;
  register_fields(fmls);
}

//--------------------------------------------------------------------

void
ExpressionTree::register_fields( FMLMap& fmls )
{
  for( ExprFieldMap::value_type& vt: exprFieldMap_ ){
    const ExpressionID id = vt.first;
    FieldAggregatorBase& fa = *vt.second;
    const ID2Vert::const_iterator ievmT = exprVertexMapT_.find( id );
    VertexProperty& vpT = (*graphT_)[ievmT->second];
    VertexProperty& vp  = (*graph_)[ find_vertex(*graph_,vpT.id) ];

    FieldManagerList* fml = NULL;

    // on cleaving, we may not have a vertex for the "shadow" fields.
    if( ievmT != exprVertexMapT_.end() ) {
      try{
        fml = extract_field_manager_list( fmls, vpT.fmlid );
      }
      catch( std::exception& err ){
        std::ostringstream msg;
        msg <<  __FILE__ << " : " << __LINE__
            << "\nError from register_fields() when extracting field manager list for expression:\n\t"
            << vpT.expr->get_tags()
            << "\nDetails follow\n" << err.what() << std::endl;
        throw std::runtime_error( msg.str() );
      }

      // DYNAMIC fields must be locked to allow their values to be "remembered"
      // jcs what about the possibility of other tags being on the list???
      const Expr::TagList& tags = vp.expr->get_tags();
      if( tags[0].context() == STATE_DYNAMIC ){
        vpT.set_is_persistent(true);
        vp .set_is_persistent(true);
      }

    }
    else if( fmls.size() == 1 ){
      // When cleaving, we need to register the first level cleaved fields
      // (out-edges). However, this will not function properly with multiple
      // FieldManagerList objects since we do not have a way to determine
      // which FML the fields should come off of.
      fml = fmls[0];
    }
    else{
      // The only way we get here is if we have a field that we depend on but no
      // corresponding expression in the graph.  This should only be due to cleaving.
      // In that case, a cleaved edge retains the field dependency but no the
      // actual expression, which is contained in a child graph.  To fix this,
      // we would need to include FieldManagerList IDs in with the FieldDeps
      // so we knew which FML to pull the field off of.
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "ERROR from: ExpressionTree::register_fields()" << std::endl
          << "  When using multiple FieldManagerList objects, cleaving is not supported" << std::endl;
      throw std::runtime_error(msg.str());
    }

    fa.register_fields( *fml );
  }

# ifdef DEBUG_LOCK_ALL_FIELDS
  std::cout << "Locking all fields" << std::endl;
  lock_fields(fmls);
# endif

  hasRegisteredFields_ = true;
  scheduler_->set_fmls( fmls );
  scheduler_->setup( hasRegisteredFields_ );
}

//--------------------------------------------------------------------

void
ExpressionTree::lock_fields( FMLMap& fmls )
{
  for( FMLMap::value_type& i: fmls ) lock_fields( *i.second );
}

void
ExpressionTree::lock_fields( FieldManagerList& fml )
{
  const std::pair<VertIter,VertIter> vertx = vertices(*graph_);
  for( VertIter iv = vertx.first; iv != vertx.second; iv++ ){
    VertexProperty& vp = (*graph_)[*iv];
    vp.expr->field_type().lock_fields( fml );
  }
}

//--------------------------------------------------------------------

void
ExpressionTree::unlock_fields( FMLMap& fmls ){
  for( FMLMap::value_type& i: fmls ) unlock_fields(*i.second);
}

void
ExpressionTree::unlock_fields( FieldManagerList& fml )
{
  const std::pair<VertIter,VertIter> vertx = vertices(*graph_);
  for( VertIter iv = vertx.first; iv != vertx.second; iv++ ){
    VertexProperty& vp = (*graph_)[*iv];
    vp.expr->field_type().unlock_fields( fml );
  } // loop over vertices
}

//--------------------------------------------------------------------
void
ExpressionTree::bind_fields( FieldManagerList& fml )
{
  // be sure that we have a graph built.
  if( graph_ == NULL ){
    std::ostringstream msg;
    msg << __FILE__ << ":" << __LINE__ << std::endl
        << "ERROR!  Cannot bind fields on the tree with root set: " << std::endl;
    for( const ExpressionID id : rootIDs_ ){
      msg << "        "  << id << ", " << factory_.get_labels(id) << std::endl;
    }
    msg << "  because the graph does not exist." << std::endl
        << "  You must first run compile_expression_tree()" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  FMLMap fmls; fmls[0]=&fml;
  scheduler_->set_fmls(fmls);
  hasBoundFields_ = true;
}
void
ExpressionTree::bind_fields( FMLMap& fmls )
{
  // be sure that we have a graph built.
  if( graph_ == NULL ){
    std::ostringstream msg;
    msg << __FILE__ << ":" << __LINE__ << std::endl
        << "ERROR!  Cannot bind fields on the tree with root set: " << std::endl;
    for( const ExpressionID id: rootIDs_ ){
      msg << "        "  << id << ", " << factory_.get_labels(id) << std::endl;
    }
    msg << "  because the graph does not exist." << std::endl
        << "  You must first run compile_expression_tree()" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  scheduler_->set_fmls(fmls);
  hasBoundFields_ = true;
}

//--------------------------------------------------------------------

void
ExpressionTree::bind_operators( const SpatialOps::OperatorDatabase& opDB )
{
  OpDBMap opDBs; opDBs[0] = &opDB;
  bind_operators( opDBs );
}

//--------------------------------------------------------------------

void
ExpressionTree::bind_operators( const OpDBMap& opDBs )
{
  // be sure that we have a graph built.
  if( graph_ == NULL ){
    std::ostringstream msg;
    msg << __FILE__ << ":" << __LINE__ << std::endl
        << "ERROR!  Cannot bind operators on tree with root set: " << std::endl;
    for( const ExpressionID id: rootIDs_ ){
      msg << "        "  << id << ", " << factory_.get_labels(id) << std::endl;
    }
    msg << "  because the graph does not exist." << std::endl
        << "  You must first run compile_expression_tree()" << std::endl;
    throw std::runtime_error( msg.str() );
  }

  const std::pair<VertIter,VertIter> verts = vertices(*graph_);
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    ExpressionBase* const expr = (*graph_)[*iv].expr;
    const SpatialOps::OperatorDatabase* opDB = opDBs.begin()->second;
    if( opDBs.size() > 1 ){
      const int fmlID = expr->field_manager_list_id();
      const OpDBMap::const_iterator iop = opDBs.find(fmlID);
      assert( iop != opDBs.end() );
      opDB = iop->second;
    }
    expr->base_bind_operators( *opDB );
  }
}

//--------------------------------------------------------------------

void
ExpressionTree::compile_expression_tree()
{
  tOne_ = 0; tINF_ = 0;
  exprVertexMapT_.clear();
  delete graph_;
  delete graphT_;

  graph_  = new Graph();
  graphT_ = new Graph();

  {
    ID2Index id2index;
    int vertIx=0;
    int depth = 0;
    for( const ExpressionID iid: rootIDs_ ){
      try{
        bootstrap_dependents( iid, vertIx, id2index, depth );
      }
      catch( std::runtime_error& err ){
        std::ostringstream msg;
        msg << __FILE__ << ":" << __LINE__ << std::endl
            << "Fatal error constructing expression tree while examining root node\n\t"
            << factory_.get_labels(iid)
            << std::endl << std::endl
            << err.what();
        throw std::runtime_error( msg.str() );
      }
    }
  }

  exclude_cycles();

  set_computed_fields();

  for( PollerPtr p: factory_.get_pollers() ){
    if( has_expression( p->target_tag() ) ){
      pollers_.insert( p );
      p->deactivate_all();  // jcs should be deactivated by default and activated when a node completes.
      // push onto vertex properties
      Graph& g = *graph_;
      const Vertex v = find_vertex( g, get_id(p->target_tag()), true );
      VertexProperty& vp = g[v];
      vp.poller = p;
    }
  }
  for( NonBlockingPollerPtr p: factory_.get_nonblocking_pollers() ){
    if( has_expression( p->target_tag() ) ){
      nonBlockPollers_.insert( p );
      p->deactivate_all();  // jcs should be deactivated by default and activated when a node completes.
      // push onto vertex properties
      Graph& g = *graph_;
      const Vertex v = find_vertex( g, get_id(p->target_tag()), true );
      VertexProperty& vp = g[v];
      vp.nonBlockPoller = p;
    }
  }

  boost::transpose_graph( *graph_, *graphT_,
                          boost::vertex_index_map( boost::get(&VertexProperty::index, *graph_) ) );

# ifdef ENABLE_CUDA
  scheduler_.reset( new HybridScheduler( *graph_, *graphT_, logNodeTimings_, name() ) );
# else
  scheduler_.reset( new PriorityScheduler( *graph_, *graphT_, logNodeTimings_, name() ) );
# endif

  for( PollerPtr p: factory_.get_pollers() ){
    scheduler_->set_poller(p);
  }
  for( NonBlockingPollerPtr p: factory_.get_nonblocking_pollers() ){
    scheduler_->set_nonblocking_poller(p);
  }

  // Setup expression => vertex property map
  {
    std::pair<VertIter,VertIter> verts = boost::vertices(*graphT_);
    for( VertIter iv = verts.first; iv != verts.second; ++iv ){
      exprVertexMapT_.insert( std::make_pair( (*graphT_)[*iv].id, *iv ) );
    }
  }

  // ensure that all root nodes are actually roots now that we have the graph built.
  {
    for( RootIDList::const_iterator iid=rootIDs_.begin(); iid!=rootIDs_.end(); ){
      if( boost::out_degree( exprVertexMapT_[*iid], *graphT_ ) > 0 ){
        if( factory_.is_logging_active() ){
          std::cout << "WARNING: Node labeled '" << factory_.get_labels(*iid)[0] << "'" << std::endl
                    << "  was set as a root node but has in-edges." << std::endl
                    << "  Removing from root id list." << std::endl << std::endl;
        }
        iid = rootIDs_.erase(iid);
      }
      else{
        ++iid;
      }
    }
  }
}

//--------------------------------------------------------------------

void
ExpressionTree::bootstrap_dependents( const ExpressionID& id,
                                      int& vertIx,
                                      ID2Index& id2index,
                                      int& depth )
{
  const int MAX_DEPTH = 30;
  if( depth>MAX_DEPTH ){
    std::ostringstream msg;
    msg << __FILE__ << ":" << __LINE__ << std::endl
        << "ERROR! Maximum recursion depth (" << MAX_DEPTH << ") exceeded in the tree." << std::endl
        << "       This may indicate a circular dependency in the graph" << std::endl
        << "       The current graph is in 'graph_problems.dot'" << std::endl
        << std::endl
        << "       ROOT ID(s) follow:" << std::endl;
    for( const ExpressionID id: rootIDs_ ){
      msg << "       " << id << ",   "
          << factory_.get_labels(id) << std::endl;
    }
    std::ofstream fout( "graph_problems.dot" );
    write_tree( fout );
    throw std::runtime_error( msg.str() );
  }

  if( id == ExpressionID::null_id() ){
    std::ostringstream msg;
    msg << __LINE__ << " : " << __FILE__ << endl
        << "Invalid ExpressionID (null ID) detected in the tree." << endl;
    throw std::runtime_error( msg.str() );
  }

  //
  // 1. obtain the expression corresponding to this ID
  //
  ExpressionBase& expr = factory_.retrieve_expression(id,patchID_,false);

  //
  // 2. Determine its dependencies
  //
  ExprDeps exprDeps;
  FieldAggregatorBase* fieldDeps;
  try{
    expr.base_advertise_dependents( exprDeps, fieldDeps );
    exprFieldMap_[id] = fieldDeps;
  }
  catch( std::runtime_error& e ){
    std::ostringstream msg;
    msg << "ERROR analyzing dependents of expression " << expr.get_tags()[0] << std::endl
        << e.what() << std::endl;
    throw std::runtime_error( msg.str() );
  }

  //
  // 3. Insert this information into the graph
  //
  // jcs should this be done in the main compile_ function?
  bool parentAlreadyPresent;
  Vertex parentVertex;
  add_vertex_to_graph( parentVertex, parentAlreadyPresent, vertIx, id2index, id );

  if( exprDeps.begin() == exprDeps.end() ){
    (*graph_)[parentVertex].set_is_persistent(true);
  }

  // iterate through the children
  for( ExprDeps::const_iterator iexd=exprDeps.begin(); iexd!=exprDeps.end(); ++iexd ){

    Vertex childVertex;
    try{
      const ExpressionID childID = factory_.get_id(*iexd);
      bool childAlreadyPresent;
      add_vertex_to_graph( childVertex, childAlreadyPresent, vertIx, id2index, childID );
      { // add this edge?
        bool edgeExists = false;
        typedef boost::graph_traits<Graph>::adjacency_iterator AdjIter;
        typedef std::pair<AdjIter,AdjIter> AdjVerts;
        const AdjVerts verts = boost::adjacent_vertices( parentVertex, *graph_ );
        for( AdjIter iv=verts.first; iv!=verts.second; ++iv ){
          if( *iv == childVertex ){
            edgeExists = true;
            break;
          }
        }
        if( !edgeExists ){
          boost::add_edge( parentVertex, childVertex, *graph_ );
          // "interior" nodes are set as not persistent unless over-ridden elsewhere.
          (*graph_)[childVertex].set_is_persistent(false);
        }
      }
      if( !childAlreadyPresent ){
        //
        // 4. Recurse on this child
        //
        bootstrap_dependents( childID, vertIx, id2index, ++depth );
      }
    }
    catch( FactoryException& err ){
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__
          << "\nERROR: while examining dependents of " << expr.get_tags() << std::endl
          << "       Could not find child " << *iexd << std::endl
          << "NOTE: some dependencies can be injected through modifier expressions.\n";
      throw std::runtime_error( err.what() );
    }
    catch( std::runtime_error& e ){
      std::ostringstream msg;
      msg << "ERROR: while examining dependents of '" << expr.get_tags() << "'\n"
          << e.what() << std::endl;
      throw std::runtime_error( msg.str() );
    }
  }
  --depth;
}

//--------------------------------------------------------------------

void
ExpressionTree::add_vertex_to_graph( Vertex& v,
                                     bool& alreadyPresent,
                                     int& vertIx,
                                     ID2Index& id2index,
                                     const ExpressionID& id )
{
  std::pair<ID2Index::const_iterator,bool> result = id2index.insert( std::make_pair(id,vertIx) );
  if( result.second ){
    ExpressionBase& expr = factory_.retrieve_expression(id,patchID_);
    const VertexProperty vpsrc( vertIx++, id, &expr );
    v = boost::add_vertex( vpsrc, *graph_ );
    alreadyPresent = false;
  }
  else{
    alreadyPresent = true;
    v = find_vertex( *graph_, id, true );
  }
}

//--------------------------------------------------------------------

ExpressionTree::TreeList
ExpressionTree::split_tree()
{
  /******* Algorithm *******

    1. Collect a list of tagged expressions for nodes that are to
       become new "root" nodes (out-edges from cleaved edge)

    2. Determine the subset of tagged IDs that will become the root
       nodes of the child graph

        - construct a child graph from the subset of tagged IDs

        - remove the id subset from the tagged list

        - any nodes that are in the child graph and have edges that were cleaved
          are marked as persistent, since they will be required for use later on.

    3. prune the child graph from the parent graph.

    4. recurse to step 2 with the child becoming the parent

   ******* Algorithm *******/

  // look for nodes tagged to be cleaved
  IDSet taggedIDs;
# ifdef PRUNE_DIAGNOSTICS
  cout << endl << "The following nodes have been marked for cleaving: " << endl;
# endif // PRUNE_DIAGNOSTICS
  const std::pair<VertIter,VertIter> verts = vertices(*graph_);
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    VertexProperty& vp = (*graph_)[*iv];
    if( vp.expr->cleave_from_parents() ){
      vp.set_is_persistent(true);
      // Make sure node is not a root
      if( std::find( rootIDs_.begin(), rootIDs_.end(), vp.id ) == rootIDs_.end() ){
        // this id forms a root in the child graph
        const std::pair<IDSet::iterator,bool> it = taggedIDs.insert( vp.id );
#       ifdef PRUNE_DIAGNOSTICS
        if( it.second )
          cout << "   " << factory_.get_labels(vp.id) << endl;
#       endif // PRUNE_DIAGNOSTICS
      }
    }

    if( vp.expr->cleave_from_children() ){
      // children may form roots of the new graph.  In some cases, however,
      // a child may be an interior node in a resulting subgraph.
      typedef boost::graph_traits<Graph>::out_edge_iterator OutEdgeIter;
      std::pair<OutEdgeIter,OutEdgeIter> es = boost::out_edges(*iv,*graph_);
      for( OutEdgeIter ie = es.first; ie!=es.second; ++ie ){
        const Vertex& outv = boost::target( *ie, *graph_ );
        VertexProperty& outvp = (*graph_)[outv];
        outvp.set_is_persistent(true);
        const std::pair<IDSet::iterator,bool> it = taggedIDs.insert( outvp.id );
#       ifdef PRUNE_DIAGNOSTICS
        if( it.second )
          cout << "   " << factory_.get_labels(outvp.id) << endl;
#       endif
      }
    }
  }
# ifdef PRUNE_DIAGNOSTICS
  cout << endl;
# endif // PRUNE_DIAGNOSTICS

  // Build the list of trees
  TreeList treeList;
  TreePtr parent( new ExpressionTree( rootIDs_, factory_, patchID_, name_ ) );

  // Push down persistence properties
  const std::pair<VertIter,VertIter> vertx = vertices(*(parent->graph_));
  for( VertIter iv = vertx.first; iv != vertx.second; iv++ ){
    VertexProperty& vp = (*(parent->graph_))[*iv];

    if( has_expression( vp.id ) ){
      const TagList& tgl = factory_.get_labels( vp.id );
      for( TagList::const_iterator it=tgl.begin(); it!=tgl.end(); ++it ){
        vp.set_is_persistent( is_persistent( *it ) );
      }
    }
  }
  cleave_tree( parent, taggedIDs, treeList );

  return treeList;
}

//--------------------------------------------------------------------

void
ExpressionTree::write_tree( std::ostream& os,
                            const bool execTree,
                            const bool details ) const
{
  assert( graph_!=NULL );
  Graph& g = execTree ? *graphT_ : *graph_;

  os << "digraph {\n";
  if( details ) os << "compound=true;\n";
  os << "node[style=filled]  /* remove this line to have empty (rather than filled) nodes */\n\n";

  // write out vertex (node) information
  os << "\n/*\n * Write out node labels\n */\n";
  const std::pair<VertIter,VertIter> iters = boost::vertices(g);
  for( VertIter iv=iters.first; iv!=iters.second; ++iv  ){
    const VertexProperty& vp = g[*iv];
    os << vp.id << "[";
    if( vp.expr->is_gpu_runnable() ) os << " /* GPU-enabled */ color=darkkhaki,";
    if( vp.expr->is_placeholder()   ) os << " /* placeholder */ shape=diamond,color=powderblue,";
    os << "label=\"";
    const TagList& names = vp.expr->get_tags();
    for( const Expr::Tag& tag: names ){
      if( tag != names[0] )  os << "\n" << tag;
      else                   os << tag << " ";
    }
    if( details ){
      std::string exprIDName = SpatialOps::type_name( *vp.expr );
      const size_t p0 = exprIDName.find_first_of("<");
      const size_t p1 = exprIDName.find_last_of(">");
      if( p0 != std::string::npos && p1 != std::string::npos ){
        exprIDName.erase(p0, p1 - p0 + 1);
      }
      os << "\n" << exprIDName << std::endl;
    }
    const int npp = vp.expr->num_post_processors();
    if( npp > 0 ){
      os << "\n(" << npp << " post-procs)";
    }
    os <<"\"]" << std::endl;
    if( details && vp.expr->num_post_processors() > 0 ){
      os << "\nsubgraph cluster" << vp.id << "{\n"
         << "\t/* this contains modifiers & functors evaluated in conjunction with the expression */\n";
      const std::vector<std::string> names = vp.expr->post_proc_names();
      int imod=1;
      for( const std::string& name: names ){
        os << "\t" << vp.id << "." << imod << "[";
        if( vp.expr->is_gpu_runnable() ) os << " /* GPU-enabled */ color=darkkhaki,";
        os << "label=\"" << name << "\"];\n";
        os << "\t" << vp.id << " -> " << vp.id << "." << imod << "\n";
        ++imod;
      }
      os << "}\n\n";
    }

  }

  // write out edge information
  os << "\n/*\n * Write out edge information\n */\n";
  const std::pair<EdgeIter,EdgeIter> edgeIters = boost::edges(g);
  for( EdgeIter ie=edgeIters.first; ie!=edgeIters.second; ++ie ){
    const VertexProperty& svp = g[source(*ie,g)];
    const VertexProperty& dvp = g[target(*ie,g)];
    if( details && svp.expr->num_post_processors() > 0 ){
      os << svp.id << " -> " << dvp.id << " [ltail=cluster" << svp.id << "];\n";
    }
    else{
      os << svp.id << " -> " << dvp.id << std::endl;
    }
  }

  // look for pollers and add those...
  if( pollers_.size() > 0 ) os << "\n/*\n * Poller information augmenting graph\n */\n";
  for( const PollerPtr poller: pollers_ ){
    const VertexProperty& vp = *poller->get_vertex_property();
    os << vp.id << ".1[shape=box,fillcolor=beige,label=\"POLLER\"]\n";
    os << vp.id << " -> " << vp.id << ".1 [style=dotted]\n\n";
    // jcs need to get an out edge from the poller onto the field it polls.
    // but we don't have a way to accomplish this currently...
  }

  os << "\nsubgraph cluster_01 {"
     << "\n label = \"Legend\";\n"
     << "\n a[ /* GPU-enabled */ color=darkkhaki,label=\"GPU Enabled\", weight=0]"
     << "\n b[ label=\"CPU Enabled\", weight=1]"
     << "\n c[ shape=diamond,color=powderblue,label=\"Placeholder\", weight =2]"
     << "\n edge[style=invis];"
     << "\n  a->b->c"
     << "\n}"
    << std::endl;

  os << "\n}\n" << std::endl;
}

//--------------------------------------------------------------------

void
ExpressionTree::dump_vertex_properties( std::ostream& os, const bool execGraph ) const
{
  Graph& g = (execGraph ? *graphT_ : *graph_);
  const std::pair<VertIter,VertIter> iters = boost::vertices(g);
  os << "\n---------------------------------------\n";
  if( execGraph ) os << "Execution Graph Vertex Properties:\n";
  else            os << "Dependency Graph Vertex Properties:\n";
  for( VertIter iv=iters.first; iv!=iters.second; ++iv  )
    os << g[*iv];
  os << "---------------------------------------\n\n";
}

//--------------------------------------------------------------------

#ifdef ENABLE_CUDA

//--------------------------------------------------------------------

void
ExpressionTree::set_device_index( const unsigned int deviceIndex, FieldManagerList& fml )
{
  // If a device ID is set from outside of ExprLib like Wasatch, the information should
  // be passed downstream

  SpatialOps::DeviceTypeTools::check_valid_index( deviceIndex, __FILE__, __LINE__ );

  if( !deviceAlreadySet_ ){
    //set the index to the tree
    deviceID_ = deviceIndex;

    // set the device index to the Scheduler
    scheduler_->set_device_index( deviceIndex );
    deviceAlreadySet_ = true;
  }
  else if( deviceID_ != deviceIndex && deviceAlreadySet_ ){
    std::cout << "Warning : device context is changed to ID(" << deviceIndex << ") from the previous ID(" << deviceID_ << ") \n";

    // Release the resources from the old device index (deviceID_)
    fml.deallocate_fields();

    // Allocate or reset the resources for the new device index (deviceIndex)
    deviceID_ = deviceIndex;
    scheduler_->set_device_index( deviceID_ );
    scheduler_->invalidate();
    register_fields( fml );
  }
}

//--------------------------------------------------------------------

int
ExpressionTree::get_device_index() const
{
  return deviceID_;
}

#endif // ENABLE_CUDA

//--------------------------------------------------------------------

void
ExpressionTree::set_computed_fields()
{
  struct ComputedFieldsVisitor : public boost::default_bfs_visitor
  {
    Tag2IDMap& tags_;
    ComputedFieldsVisitor( Tag2IDMap& tagmap ) : tags_(tagmap) {}
    void discover_vertex( Vertex v, const Graph& g )
    {
      Graph& g2 = const_cast<Graph&>(g);
      const VertexProperty& vp = g2[v];
      const TagList& tags = vp.expr->field_aggregator().get_tags();
      for( const Expr::Tag& tag: tags ){
        tags_.insert( std::make_pair(tag,vp.id) );
        const TagSet& sensTags = vp.expr->field_aggregator().get_sensitivity();
        for( const Expr::Tag& stag: sensTags ){
          tags_.insert( std::make_pair( sens_tag(tag,stag), vp.id ) );
        }
      }
    }
  };

  tag2ID_.clear();

  // search through the graph and record the fields that each expression computes.
  for( const ExpressionID id: rootIDs_ ){
    boost::breadth_first_search( *graph_,
                                 find_vertex(*graph_,id),
                                 boost::color_map(boost::get(&VertexProperty::color,*graph_))
                                  .visitor( ComputedFieldsVisitor(tag2ID_) ) );
  }
}

//--------------------------------------------------------------------

bool
ExpressionTree::operator==( const ExpressionTree& other ) const
{
  return this->rootIDs_ == other.rootIDs_;
}

//--------------------------------------------------------------------

ExpressionID
ExpressionTree::get_id( const Tag& tag ) const
{
  const auto iter = tag2ID_.find(tag);
  assert( iter != tag2ID_.end() );
  return iter->second;
}

//--------------------------------------------------------------------

bool
ExpressionTree::has_expression( const ExpressionID& id ) const
{
  return ( find_vertex(*graph_,id,false) != Vertex() );
}

//--------------------------------------------------------------------

bool
ExpressionTree::has_field( const Tag& tag ) const
{
  return tag2ID_.find(tag) != tag2ID_.end();
}

//--------------------------------------------------------------------

bool ExpressionTree::is_persistent( const Tag& tag ) const
{
  const auto iter = tag2ID_.find(tag);
  if( iter == tag2ID_.end() ) return false;
  const VertexProperty& vpT = (*graphT_)[exprVertexMapT_.find(iter->second)->second];
  return vpT.get_is_persistent();
}

//--------------------------------------------------------------------

void ExpressionTree::set_expr_is_persistent( const Tag& tag, FieldManagerList& fml )
{
  const auto iter = tag2ID_.find(tag);
  if( iter == tag2ID_.end() ) return;

  const ExpressionID id = iter->second;

  ID2Vert::iterator ievmt = exprVertexMapT_.find( id );

  if( ievmt == exprVertexMapT_.end() ) return;

  VertexProperty& vpT = (*graphT_)[ievmt->second];
  VertexProperty& vp  = (*graph_ )[ find_vertex(*graph_,id) ];

  vpT.set_is_persistent( true );
  vp .set_is_persistent( true );

  if( hasRegisteredFields_ ){
    FieldManagerBase& fm = vpT.expr->field_type().field_manager(fml);
    fm.lock_field( tag );
    const FieldAggregatorBase& fa = vpT.expr->field_aggregator();
    const TagSet& sensVars = fa.get_sensitivity();
    TagSet tags;
    tags.insert(tag);
    for( const Tag& t: sensVars ) tags.insert( sens_tag(tag,t) );
    for( const Tag& t: tags ){
      if( vpT.execTarget == CPU_INDEX ){
        fm.set_field_memory_manager( t, MEM_EXTERNAL, vpT.execTarget );
      }
#     ifdef ENABLE_CUDA
      else if ( IS_GPU_INDEX(vpT.execTarget) ){
        fm.set_field_memory_manager( t, MEM_STATIC_GPU, vpT.execTarget );
      }
#     endif
      else{
        std::ostringstream msg;
        msg << "Invalid Target execution Target : " << vpT.execTarget << " at "
            << __FILE__ << " : " << __LINE__ << std::endl;
        throw ( std::runtime_error( msg.str()));
      }
    }
  }
}

//--------------------------------------------------------------------

bool
ExpressionTree::computes_field( const Tag& tag ) const
{
  if( !has_field(tag) ) return false;

  const auto ivert = exprVertexMapT_.find( get_id(tag) );
  if( ivert == exprVertexMapT_.end() ) return false;

  const Vertex vert = ivert->second;
  const ExpressionBase* expr = (*graph_)[vert].expr;

  // is it a sensitivity field?
  const auto& exprTags = expr->field_aggregator().get_tags();
  const auto& sensTags = expr->field_aggregator().get_sensitivity();
  for( const Tag& et: exprTags){
    for( const Tag& st: sensTags){
      if( tag == sens_tag(et,st) ) return true;
    }
  }

  // placeholders don't compute fields (sensitivity is special)
  if( expr->is_placeholder() ) return false;

  return true;
}

//--------------------------------------------------------------------

bool
ExpressionTree::has_expression( const Tag& tag ) const
{
  return tag2ID_.find(tag) != tag2ID_.end();
}

//------------------------------------------------------------------

TagList
ExpressionTree::get_tags( const ExpressionID id ) const
{
  const auto iter = exprFieldMap_.find(id);
  if( iter == exprFieldMap_.end() ) return TagList();
  return iter->second->get_tags();
}

//------------------------------------------------------------------

struct SensitivityInfo{
  std::map<Tag,TagSet> sensFields, zeroSensFields;

  void print( std::ostream& os ) const{
    if( !sensFields.empty() )
      for( const auto& s: sensFields )
        os << "\tSens fields w.r.t. " << s.first << ":\n\t\tDvars: " << s.second << std::endl;
    if( !zeroSensFields.empty() )
      for( const auto& s: zeroSensFields )
        os << "\tZero  sens fields w.r.t. " << s.first << ":\n\t\tDvars: " << s.second << std::endl;
  }
  void insert( const SensitivityInfo& other )
  {
    for( const auto& s: other.sensFields     )     sensFields[s.first].insert( s.second.begin(), s.second.end() );
    for( const auto& z: other.zeroSensFields ) zeroSensFields[z.first].insert( z.second.begin(), z.second.end() );
  }

  /**
   * @brief instead of mapping [ivar -> sens], map [sens -> ivar]
   */
  SensitivityInfo invert() const
  {
    SensitivityInfo inv;
    for( const auto& s: sensFields ){
      for( const Tag& t: s.second ){
        inv.sensFields[t].insert( s.first );
      }
    }
    for( const auto& s: zeroSensFields ){
      for( const Tag& t: s.second ){
        inv.zeroSensFields[t].insert( s.first );
      }
    }
    return inv;
  }
};

SensitivityInfo
search_sens( Graph& graph,           // dependency graph
             const ExpressionID id,  // starting point for the search
             const TagList& stopVars // termination points for the search
           )
{
  SensitivityInfo info;

  const Vertex v = find_vertex(graph,id,true);
  const ExpressionBase* expr = graph[ v ].expr;
  const Expr::TagList& tags = expr->get_tags();

  if( expr->override_sensitivity() ){
    for( const Tag& st: stopVars ){
      for( const Tag& t: tags ) info.sensFields[st].insert(t);
    }
    return info;
  }

  // examine this vertex to see if it contains a stop criteria tag. If so, don't continue the search.
  bool foundStopVar = false;
  for( const Tag& t: tags ){
    for( const Tag& st: stopVars ){
      if( t == st ){
        info.sensFields[t].insert( t );
        foundStopVar = true;
      }
    }
  }
  if( foundStopVar ) return info;

  TagSet notFound; // variables not found on the child branch.

  // search outward
  std::pair<OutEdgeIter,OutEdgeIter> edges = boost::out_edges(v,graph);
  for( OutEdgeIter iedge = edges.first; iedge != edges.second; ++iedge ){
    const VertexProperty& target = graph[boost::target( *iedge, graph )];
    const SensitivityInfo found = search_sens( graph, target.id, stopVars );

    // look for empty branches and save them off.
    for( const Tag& st: stopVars ){
      if( found.sensFields.find( st ) == found.sensFields.end() &&
          info .sensFields.find( st ) == info .sensFields.end() )
      {
        notFound.insert( st );
      }
      else notFound.erase( st );
    }

    info.insert( found );

    //  propagate sensitivity onto this node
    for( auto& st: found.sensFields ){
      info.sensFields[st.first].insert( tags.begin(), tags.end());
    }
  }

  if( info.sensFields.empty() ){
    notFound.insert( stopVars.begin(), stopVars.end() );
  }

  // look for empty branches
  for( const Tag& nf: notFound ){
//    std::cout << "On " << tags[0] << ", found empty sens w.r.t. " << nf << std::endl;
    if( std::find( stopVars.begin(), stopVars.end(), nf ) != stopVars.end() )
      info.zeroSensFields[nf] = TagSet( tags.begin(), tags.end() );  // replace
  }

  return info;
}

void
ExpressionTree::compute_sensitivities( const TagList& depVarTags,
                                       const TagList& indepVarTags ){
  if( hasRequestedSensitivitiy_ ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__ << "\nExpressionTree::compute_sensitivities() must only be called ONCE!\n";
    throw std::runtime_error( msg.str());
  }

  hasRequestedSensitivitiy_ = true;

  if( is_cleaved()){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__
        << "\nExpressionTree::compute_sensitivities() must be called before cleaving the tree\n";
    throw std::runtime_error( msg.str());
  }
  if( hasRegisteredFields_ ){
    std::ostringstream msg;
    msg << __FILE__ << " : " << __LINE__
        << "\nExpressionTree::compute_sensitivities() must be called before register_fields()\n";
    throw std::runtime_error( msg.str());
  }

  SensitivityInfo sens;
  for( const Tag& dvar: depVarTags ){
    sens.insert( search_sens( *graph_, get_id( dvar ), indepVarTags ) );
  }

  // independent variables have zero cross-sensitivities.
  for( const Tag& ivar1: indepVarTags ){
    for( const Tag& ivar2: indepVarTags ){
      if( ivar1 == ivar2 ) continue;
      sens.zeroSensFields[ivar1].insert( ivar2 );
    }
  }

  const SensitivityInfo invSens = sens.invert();

  // set up zero sensitivities:
  for( const auto& zero: invSens.zeroSensFields ){
    const Tag& dvar = zero.first;
    const TagSet& ivars = zero.second;
    ExpressionBase& dvarExpr = factory_.retrieve_expression( dvar, patch_id(), true );

    for( const Tag& ivar: ivars ){
      dvarExpr.setup_empty_sensitivity( ivar );
    }
  }

  for( const auto& sens: invSens.sensFields ){
    const Tag& dvarTag = sens.first;
    ExpressionBase& expr = factory_.retrieve_expression( dvarTag, patchID_, true );
    for( const Tag& ivar: sens.second ){
      if( dvarTag == ivar ){
        expr.setup_self_sensitivity( ivar );
      }
      else{
        if( expr.is_placeholder()){
          expr.setup_empty_sensitivity( ivar );
        }
        else{
          expr.activate_sensitivity( ivar );
        }
      }
    }
  }

  set_computed_fields();
}

//------------------------------------------------------------------

void
ExpressionTree::prune( TreePtr child )
{
# ifdef PRUNE_DIAGNOSTICS
  cout << " Pruning tree named '" << name() << "' with roots: " << endl;
  for( const ExpressionID id: rootIDs_ ){
    cout << "    " << factory_.get_labels(id) << endl;
  }
  cout << endl;
# endif // PRUNE_DIAGNOSTICS
  IDSet exprRemoval;
  {
    const Graph& childGraph = *(child->graph_);
    const std::pair<VertIter,VertIter> verts = boost::vertices( childGraph );
    for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
      const ExpressionID& eid = childGraph[*iv].id;
      exprRemoval.insert( eid );
    }
  }

  IDSet retainedFields;
  for( const ExpressionID id: rootIDs_ ){
    FieldPruneVisitor fp( retainedFields, exprRemoval );
    boost::breadth_first_search( *graph_, find_vertex(*graph_,id),
                                 boost::color_map( boost::get(&VertexProperty::color,*graph_)).visitor(fp) );
  }
  //
  // remove vertices (expressions) from the parent graph
  // propagate forced persistence characteristics from parent to child graph
  //
  const IDSet& childRoots = child->get_roots();

  for( const ExpressionID id: exprRemoval ){

    // See if this vertex is a root node in the child graph. We don't remove
    // those fields since they will be connected to the parent graph too.
    const bool isChildRoot     =     childRoots.count( id ) > 0;
    const bool isRetainedField = retainedFields.count( id ) > 0;
    const bool keepField = isChildRoot || isRetainedField;

    // find the vertex corresponding to this ID, then prune it
    const Vertex& vert = find_vertex( *graph_, id );
    if( keepField ){
      // replace the existing expression by a placeholder
      ExpressionBase* expr = (*graph_)[vert].expr;

      // jcs note that this will leak memory.
      (*graph_)[vert].expr = expr->as_placeholder_expression();

      // if we have any edges connecting to this vertex in the child graph, mark them as persistent
      if( child->has_expression(id) ){
#       ifdef PRUNE_DIAGNOSTICS
        cout << "  setting persistence on field: " << factory_.get_labels(id) << endl;
#       endif
        (*child->graph_ )[find_vertex(*child->graph_,id)].set_is_persistent(true);
        (*       graph_ )[find_vertex(       *graph_,id)].set_is_persistent(true);
        (*child->graphT_)[child->exprVertexMapT_[id]    ].set_is_persistent(true);
      }
    }
    else{
#     ifdef PRUNE_DIAGNOSTICS
      cout << "  removing expression: " << factory_.get_labels(id) << endl;
      cout << "  removing field: " << factory_.get_labels(id) << endl;
#     endif
      // remove the expression from the graph.
      boost::clear_vertex ( vert, *graph_ );
      boost::remove_vertex( vert, *graph_ );
      exprFieldMap_.erase( id );
    }

  }
# ifdef PRUNE_DIAGNOSTICS
  const std::pair<VertIter,VertIter> verts = boost::vertices( *graph_ );
  cout << endl << "  retained expressions: " << endl;
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    cout << "    " << (*graph_)[*iv].expr->get_tags() << endl;
  }
  cout << endl;
# endif

# ifdef PRUNE_DIAGNOSTICS
  cout << endl << "  retained fields: " << endl;
  for( ExprFieldMap::const_iterator ifld = exprFieldMap_.begin(); ifld!=exprFieldMap_.end(); ++ifld )
    cout << "    " <<  *(ifld->second) << endl;
  cout << endl << " done" << endl;
# endif
  //
  // reset index map on VertexProperty to ensure that it ranges from 0 to #verts
  //
  {
    int ix=0;
    const std::pair<VertIter,VertIter> verts = boost::vertices(*graph_);
    for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
      (*graph_)[*iv].index = ix++;
    }
  }

  delete graphT_;
  graphT_ = new Graph();
  boost::transpose_graph( *graph_, *graphT_, boost::vertex_index_map( boost::get(&VertexProperty::index, *graph_) ) );

# ifdef ENABLE_CUDA
  scheduler_.reset( new HybridScheduler(*graph_,*graphT_,logNodeTimings_,name()) );
# else
  scheduler_.reset( new PriorityScheduler(*graph_,*graphT_,logNodeTimings_,name()) );
# endif

  // Rebuild vertex mappings
  exprVertexMapT_.clear();
  std::pair<VertIter, VertIter> vertx = boost::vertices(*graphT_);
  for( VertIter vit = vertx.first; vit != vertx.second; ++vit ){
    exprVertexMapT_.insert( std::make_pair( (*graphT_)[*vit].id, *vit) );
  }
}

//------------------------------------------------------------------

//This will transpose the graph, and calculate optimal start and finish times for each node.
bool
ExpressionTree::push_schedule_timings()
{
  if( !bTimingsReady_ ) { pScore_ = -1; return false; }

  tINF_ = 0.0;

  const std::pair<VertIter,VertIter> verts = boost::vertices(*graphT_);
  // find the end vertices in the original graph.
  tOne_ = 0;
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    VertexProperty& vp = (*graphT_)[*iv];
    tOne_ += vp.lastExecTime.elapsed_time();

    if( vp.nparents == 0 ){
      boost::breadth_first_search( *graphT_, *iv,
                                   boost::color_map(boost::get(&VertexProperty::color,*graphT_))
                                   .visitor(ParaCalcVisitor(this)) );
    }
  }

  sINF_ = tOne_ / tINF_;

  pScore_ = ( ( 1.0 - ( 1.0 / sINF_ ) ) / ( 1.0 - ( 1.0 / (double)boost::num_vertices(*graphT_) ) ) );

  return true;
}

//------------------------------------------------------------------

ExpressionBase*
ExpressionTree::get_expression( const Tag& tag ) const
{
  const auto iter = tag2ID_.find(tag);
  if( iter == tag2ID_.end() ) return nullptr;

  const Vertex v = exprVertexMapT_.find( iter->second )->second;
  const VertexProperty vp = (*graph_)[ v ];
  return vp.expr;
}

//------------------------------------------------------------------

void
ExpressionTree::exclude_cycles() const
{
  class CycleVisitor : public boost::dfs_visitor<>
  {
    bool& hasCycle_;
  public:
    CycleVisitor( bool& hasCycle ) : hasCycle_( hasCycle ){}
    void back_edge( const Edge& e, const Graph& g ){ hasCycle_ = true; }
  };

  bool hasCycle = false;
  CycleVisitor cv(hasCycle);
  boost::depth_first_search(*graph_,
                            boost::color_map( boost::get(&VertexProperty::color,*graph_)).visitor(cv) );

  if( hasCycle ){
    std::ofstream fout( "cycle.dot" );
    write_tree(fout,false,true);
    std::ostringstream msg;
    msg << "\nERROR: cycle detected in graph construction.  See 'cycle.dot' for details\n";
    throw std::runtime_error( msg.str() );
  }
}

//------------------------------------------------------------------
#ifdef ENABLE_CUDA
bool
ExpressionTree::is_homogeneous_gpu() const
{
  const Graph& graph = *graphT_;
  const std::pair<VertIter,VertIter> verts = boost::vertices(graph);

  // Introspecting GPU runnable property for expressions
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    const VertexProperty& vp = graph[*iv];
    if( !vp.expr->is_gpu_runnable() ) return false;
  }
  return true;
}

//------------------------------------------------------------------

void
ExpressionTree::turn_off_gpu_runnable()
{
  const Graph& execGraph = this->get_graph( true );
  const std::pair<VertIter,VertIter> verts = boost::vertices(execGraph);

  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    const VertexProperty& vp = execGraph[*iv];
    ExpressionBase& expr = *(vp.expr);

    // store the GPU runnable property of all expressions before flipping,
    // so that this property can be restored.
    exprTargetMap_.insert( std::pair< ExpressionID, bool >(vp.id, expr.is_gpu_runnable()) );

    // Set the expression to CPU runnable
    expr.set_gpu_runnable( false );
  }
}

//------------------------------------------------------------------

void
ExpressionTree::restore_gpu_runnable()
{
  const Graph& graph = *graphT_;
  const std::pair<VertIter,VertIter> verts = boost::vertices(graph);
  for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
    const ExpressionID& exprID = graph[*iv].id;
    const VertexProperty& vp = graph[*iv];

    const bool gpuBeforeFlip = exprTargetMap_.find(exprID)->second; // property before the flip
    const bool gpuAfterFlip  = vp.expr->is_gpu_runnable();         // property after the flip

    if( exprTargetMap_.find(exprID) != exprTargetMap_.end() ) {
      if( !gpuAfterFlip && gpuBeforeFlip ) vp.expr->set_gpu_runnable(true);
    }
    else{
      std::ostringstream msg;
      msg << __FILE__ << " : " << __LINE__ << std::endl
          << "ERROR ! restore_gpu_runnable() for expression : " << vp.expr->get_tags()[0] << std::endl
          << ". turn_off_gpu_runnable() should be called before using this method." << std::endl;
      throw std::runtime_error(msg.str());
    }
  }

  // Change of node hardware targets and field locations has to be informed to the Hybrid scheduler
  scheduler_->invalidate();
  scheduler_->setup( true );
}
# endif // ENABLE_CUDA


} // namespace Expr
