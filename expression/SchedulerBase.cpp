/*
 * SchedulerBase.cpp
 *
 *  Created on: Nov 27, 2012
 *      Author: "James C. Sutherland"
 *
 * Copyright (c) 2012-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */


#include "SchedulerBase.h"
#include "ExpressionBase.h"

namespace Expr{

  Scheduler::Scheduler( Graph& execGraph,
                        Graph& depGraph,
                        const bool logNodeTimes,
                        const std::string name )
  : execGraph_(execGraph),
    depGraph_ (depGraph ),
    logFileName_( name+"_timings.log" ),
    timer_( NULL )
#     ifdef ENABLE_THREADS
    , pool_ ( SpatialOps::ThreadPool::self() )
    , poolx_( SpatialOps::ThreadPoolFIFO::self() )
    , schedBarrier_(0)
#     endif
  {
    invalid_       = true;
    doNodeTimings_ = logNodeTimes;

    request_timings( logNodeTimes );
//#     ifdef ENABLE_THREADS
//      SpatialOps::set_hard_thread_count( NTHREADS );
//      SpatialOps::set_soft_thread_count( NTHREADS );
//      Expr::set_soft_thread_count( NTHREADS );
//      Expr::set_hard_thread_count( 2 );
//#     endif
  }

  Scheduler::~Scheduler()
  {
    if( timer_ ) delete timer_;
  }

  void Scheduler::request_timings( const bool setting )
  {
    doNodeTimings_ = setting;

    if( doNodeTimings_ && !timer_ ){
      timer_ = new SpatialOps::TimeLogger( logFileName_, SpatialOps::TimeLogger::JSON );
    }
    else if( !doNodeTimings_ && timer_ ){
      delete timer_;
      timer_ = NULL;
    }
  }

  void Scheduler::run_pollers( VertexProperty& target,
                               FieldManagerList* const fml )
  {
    if( pollers_.size() == 0 && nonBlockingPollers_.size() == 0 ) return;

#   ifdef ENABLE_THREADS
    // note that this mutex type is the same as the one in block_pollers().
    // This ensures that neither method can be executed by multiple threads
    // concurrently, which is important.
    //
    // jcs Potential problem: since we are protecting this with a mutex but this
    //     results in a callback, could we end up blocking numerous threads here?
    ExecMutex<100> lock; // only one thread can execute pollers at a time.
#   endif

    if( target.poller.get() ){
      // jcs shouldn't this be the same as target.poller ???
      PollerPtr p = *pollers_.find( target.poller );
      p->activate_all();
    }
    if( target.nonBlockPoller.get() ){
      NonBlockingPollerPtr p = *nonBlockingPollers_.find( target.nonBlockPoller );
      p->activate_all();
    }

    // Run the blocking pollers.  If ready, then these force a call-back on the
    // vertex they are associated with.
    BOOST_FOREACH( PollerPtr poller, pollers_ ){
      if( poller->is_active() ){
        if( poller->run( fml ) ){
          // fire a call-back on the vertex associated with this poller.
          VertexProperty* vp = poller->get_vertex_property();
          (*vp->execSignalCallback)(vp->self);
        }
      }
    }

    // Run the non-blocking pollers. There are no call-backs associated with this.
    if( target.nonBlockPoller.get() ){
      target.nonBlockPoller->activate_all();
    }
    BOOST_FOREACH( NonBlockingPollerPtr poller, nonBlockingPollers_ ){
      poller->run();
    }
  }

  void Scheduler::block_pollers()
  {
    if( pollers_.size() == 0 && nonBlockingPollers_.size() == 0 ) return;

    // ensure that all pollers have completed.  If not, force them to finish.
#   ifdef ENABLE_THREADS
    ExecMutex<100> lock; // only one thread can execute pollers at a time.
#   endif
    bool anyActive = false;
    do{
      anyActive = false;
      BOOST_FOREACH( PollerPtr poller, pollers_ ){
        if( poller->is_active() ){
          anyActive = true;
          VertexProperty* const vp = poller->get_vertex_property();
          FieldManagerList* const fml = extract_field_manager_list( this->fmls_, vp->fmlid );
          const bool done = poller->run(fml);
          if( done ){
            // fire a call-back on the vertex associated with this poller and then
            // remove the poller from the list of pollers since it is done.
            (*vp->execSignalCallback)(vp->self);
          }
        }
      }
      BOOST_FOREACH( NonBlockingPollerPtr poller, nonBlockingPollers_){
        if( poller->is_active() ){
          anyActive = true;
          poller->run();
        }
      }
    } while( anyActive );
  }

  void Scheduler::set_poller( PollerPtr p )
  {
    pollers_.insert( p );
  }

  void Scheduler::set_nonblocking_poller( NonBlockingPollerPtr p ){
    nonBlockingPollers_.insert( p );
  }

  void Scheduler::release_fields_as_allowed( VertexProperty& vpJustFinished )
  {
#   ifndef DEBUG_NO_FIELD_RELEASE
    std::vector<VertexProperty*>::iterator vpit  = vpJustFinished.ancestorList.begin();
    std::vector<VertexProperty*>::iterator vpend = vpJustFinished.ancestorList.end();
    for( ; vpit!=vpend; ++vpit ){
      VertexProperty* vp = *vpit;
      if( vp->consumer_finished() ){
        const TagList& tags = vp->expr->get_tags();
        vp->expr->field_aggregator().release( *extract_field_manager_list( fmls_, vp->fmlid ) );
      }
    }
#   endif
  }


} // namespace Expr
