/*
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <expression/Tag.h>

#include <sstream>
#include <boost/functional/hash.hpp>
#include <boost/foreach.hpp>

namespace Expr{

  FieldID get_id( const std::string& name ){
    static boost::hash<std::string> hash;
    return hash(name);
  }

  inline FieldID get_id( const std::string& name, const Context c ){
    return get_id( name+context2str(c) );
  }

  inline FieldID get_id( const Tag& tag ){
    return get_id( tag.name(), tag.context() );
  }


  Tag::Tag( const std::string fieldName,
            const Context tag )
    : fieldName_( fieldName ),
      context_  ( tag ),
      id_( get_id(fieldName_,context_) )
  {}

  Tag::Tag( const Tag& s )
    : fieldName_( s.fieldName_ ),
      context_  ( s.context_   ),
      id_( get_id(fieldName_,context_) )
  {}


  Tag::Tag( const Tag& el,
            const std::string s )
    : fieldName_( el.fieldName_ + s ),
      context_  ( el.context_ ),
      id_( get_id(fieldName_,context_) )
  {}

  Tag::Tag()
    : fieldName_(""),
      context_( INVALID_CONTEXT ),
      id_( get_id(fieldName_,context_) )
  {}

  Tag&
  Tag::operator=( const Tag& s )
  {
    fieldName_ = s.fieldName_;
    context_   = s.context_;
    id_        = s.id_;
    return *this;
  }

  void
  Tag::reset_id()
  {
    id_ = get_id( fieldName_, context_ );
  }

  void
  Tag::reset_name( const std::string& name )
  {
    fieldName_ = name;
    reset_id();
  }

  void
  Tag::reset_context( const Context& c )
  {
    context_ = c;
    reset_id();
  }

  void
  Tag::reset( const std::string& name, const Context& c )
  {
    fieldName_ = name;
    context_ = c;
    reset_id();
  }


  std::ostream&
  operator<<( std::ostream& os, const Tag& s )
  {
    os << "( " << s.field_name() << ", " << s.context() << " )";
    return os;
  }

  std::ostream&
  operator<<( std::ostream& os, const TagList& tl )
  {
    if( tl.size() == 0 ) return os;
    TagList::const_iterator it=tl.begin();
    os << *it; ++it;
    for( ; it!=tl.end(); ++it ){
      os << ", " << *it;
    }
    return os;
  }

  std::ostream&
  operator<<( std::ostream& os, const TagSet& tl )
  {
    if( tl.size() == 0 ) return os;
    TagSet::const_iterator it=tl.begin();
    os << *it; ++it;
    for( ; it!=tl.end(); ++it ){
      os << ", " << *it;
    }
    return os;
  }

  bool entry_present( const Tag& tag, const TagSet& dvars ){
    return dvars.find( tag ) != dvars.end();
  }

  bool entry_present( const Tag& tag, const TagList& dvars ){
    BOOST_FOREACH( const Tag& dTag, dvars ){
      if( tag == dTag ) return true;
    }
    return false;
  }

  TagList tag_list( const std::vector<std::string>& names, const Context context,
                    const std::string prefix,
                    const std::string suffix )
  {
    TagList tags;
    BOOST_FOREACH( const std::string& nam, names ){
      tags.push_back( Tag(prefix+nam+suffix,context) );
    }
    return tags;
  }

} // namespace Expr
