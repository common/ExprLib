/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/**
 *  \file   GraphType.cpp
 *  \date   Sep 3, 2015
 *  \author james
 */

#include <expression/GraphType.h>
#include <expression/ExpressionFactory.h>

#include <boost/foreach.hpp>

namespace Expr{

  void copy_vertex_properties( const Graph& src, Graph& dest )
  {
    const std::pair<VertIter,VertIter> verts = boost::vertices(src);
    for( VertIter ivs=verts.first; ivs!=verts.second; ++ivs ){
      const VertexProperty& vps = src[*ivs];
      const Vertex vdest = find_vertex( dest, vps.id );
      dest[vdest] = vps;
    }
  }

  void dump_vertex_properties( std::ostream& os, Graph& g )
  {
    os << "\n=================================\n";
    const std::pair<VertIter,VertIter> verts = boost::vertices(g);
    for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
      const VertexProperty& vp = g[*iv];
      os << vp << std::endl;
    }
    os << "=================================\n\n";
  }

  Vertex find_vertex( const Graph& g, const ExpressionID id, const bool requireExists )
  {
    const std::pair<VertIter,VertIter> verts = boost::vertices(g);
    for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
      if( g[*iv].id == id ) return *iv;
    }
    if( !requireExists ) return Vertex();

    std::ostringstream msg;
    msg << std::endl << __FILE__ << " : " << __LINE__ << std::endl
        << "in find_vertex()" << std::endl
        << "no expression with id " << id << " was found" << std::endl
        << "Expressions on this graph follow: ";
    for( VertIter iv=verts.first; iv!=verts.second; ++iv ){
      msg << g[*iv].expr->get_tags()[0] << std::endl;
    }
    throw std::runtime_error( msg.str() );
  }

}


