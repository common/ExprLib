/*
 * The MIT License
 *
 * Copyright (c) 2015-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#ifndef MaskedExpression_Expr_h
#define MaskedExpression_Expr_h

#include <expression/Expression.h>
#include <boost/shared_ptr.hpp>

namespace Expr{

/**
 *  \class MaskedExpression
 *
 *  \brief A base class for expressions that will be applied on masks.
 */
template< typename FieldT >
class MaskedExpression : public Expression<FieldT>
{
public:
  typedef SpatialOps::SpatialMask<FieldT> MaskType;
  typedef boost::shared_ptr<MaskType>     MaskPtr;

  /**
   * \brief obtain the mask to apply this expression on.
   */
  const MaskType& get_mask() const{ return *mask_; }

  /**
   * \brief reset the mask on this expression
   */
  void reset_mask( MaskPtr mask ){ mask_ = mask; }

  virtual ~MaskedExpression(){}

protected:
  /**
   *  @brief Build a MaskedExpression expression
   *  @param mask the SpatialMask that this expression should be applied on
   *
   *  Note that the mask here is a shared pointer to avoid deep copies of the masked points.
   */
  MaskedExpression( MaskPtr mask ) : Expression<FieldT>(), mask_(mask) {}

private:
  const MaskPtr mask_;

};

//--------------------------------------------------------------------

/**
 * \class ConstantMaskedExpression
 * \author James C. Sutherland
 * \date November, 2014
 * \brief Provides a way to assign a constant value over a mask using an Expression.
 */
template<typename FieldT>
class ConstantMaskedExpression : public MaskedExpression<FieldT>
{
  const double value_;

  ConstantMaskedExpression( typename MaskedExpression<FieldT>::MaskPtr mask,
                            const double value )
  : MaskedExpression<FieldT>(mask),
    value_(value)
  {
    this->set_gpu_runnable(true);
  }

public:
  class Builder : public ExpressionBuilder
  {
  public:
    typedef typename MaskedExpression<FieldT>::MaskPtr  MaskPtr;
    typedef typename MaskedExpression<FieldT>::MaskType MaskType;

    /**
     *  @brief Build a ConstantBCExpression expression
     *  @param resultTag the tag for the value that this expression computes
     *  @param mask the point(s) that this expression should apply to
     *  @param value the value to set
     *  @param nghost the number of ghost cells
     */
    Builder( const Tag& resultTag,
             typename MaskedExpression<FieldT>::MaskPtr mask,
             const double value,
             const unsigned nghost = DEFAULT_NUMBER_OF_GHOSTS )
    : ExpressionBuilder( resultTag, nghost ),
      mask_(mask), value_(value)
    {}

    Expr::ExpressionBase* build() const{ return new ConstantMaskedExpression<FieldT>(mask_,value_); }

  private:
    typename MaskedExpression<FieldT>::MaskPtr mask_;
    const double value_;
  };

  ~ConstantMaskedExpression(){}

  void evaluate()
  {
    using namespace SpatialOps;
    FieldT& result = this->value();
    masked_assign( this->get_mask(), result, value_ );
  }

};
} // namespace Expr

#endif // MaskedExpression_Expr_h
