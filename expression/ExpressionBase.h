/**
 * \file ExpressionBase.h
 * \author James C. Sutherland
 *
 * Copyright (c) 2011-2017 The University of Utah
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
#ifndef ExpressionBase_h
#define ExpressionBase_h

#include <set>
#include <cassert>

#include <expression/ExprLib_Configure.h>
#include <expression/ExpressionID.h>
#include <expression/Tag.h>
#include <expression/SourceExprOp.h>
#include <expression/FieldManagerList.h>
#include <expression/ExprDeps.h>
#include <expression/FieldRequest.h>

#ifdef ENABLE_CUDA
#include <cuda_runtime.h>
#endif

namespace SpatialOps{ class OperatorDatabase; }

namespace Expr{

  class ExpressionBase;

  /**
   * @struct FieldTypeBase
   * @author James C. Sutherland
   *
   * @brief Provides a way of getting typed information about an expression.
   * This is primarily used by the graph so that, given an ExpressionBase
   * object, typed operations (e.g. interaction with FieldManager objects)
   * can occur.
   */
  class FieldTypeBase
  {
  protected:
    ExpressionBase& expr_;
    FieldTypeBase( ExpressionBase& expr ) : expr_( expr ) {}
  public:
    virtual ~FieldTypeBase(){}

    /**
     * @param fml the FieldManagerList
     * @return the FieldManagerBase object associated with the field
     */
    virtual FieldManagerBase& field_manager( FieldManagerList& fml ) const = 0;
    /**
     * @param fml the FieldManagerList
     * @return the FieldManagerBase object associated with the field
     */
    virtual const FieldManagerBase& field_manager( const FieldManagerList& fml ) const = 0;

    virtual bool lock_fields( FieldManagerList& fml ) const = 0;
    virtual bool unlock_fields( FieldManagerList& fml ) const = 0;
    virtual void set_field_memory_manager( FieldManagerList& fml, const MemoryManager mm, const short deviceID ) const = 0;
  };


  /**
   *  @class  ExpressionBuilder
   *  @author James C. Sutherland
   *  @date   June, 2007
   *
   *  @brief Base class for use in building expressions.  Interfaces to
   *  the ExpressionFactory.
   */
  class ExpressionBuilder
  {
  private:
    SpatialOps::GhostData ghosts_;
    const TagList computedFields_;
    int fmlID_;

  public:

    /**
     * @brief Construct an ExpressionBuilder for an Expression that computes multiple fields
     */
    ExpressionBuilder( const TagList& computedFields,
                       const SpatialOps::GhostData ghosts = DEFAULT_NUMBER_OF_GHOSTS )
      : ghosts_( ghosts ),
        computedFields_( computedFields )
    {
      fmlID_=-1;
    }

    /**
     * @brief Construct an ExpressionBuilder for an Expression that computes one field
     */
    ExpressionBuilder( const Tag& tag,
                       const SpatialOps::GhostData ghosts = DEFAULT_NUMBER_OF_GHOSTS )
      : ghosts_( ghosts ),
        computedFields_( tag_list(tag) )
    {
      fmlID_=-1;
    }

    virtual ~ExpressionBuilder(){};

    /**
     *  This method is called to construct the Expression. It should
     *  build the Expression using the \c new command so that it is
     *  allocated on the heap.  Ownership of the object is transferred
     *  to the ExpressionFactory.
     */
    virtual ExpressionBase* build() const = 0;

    /**
     * @brief Obtain the TagList for the fields computed by the expression.
     */
    const TagList& get_tags() const{ return computedFields_; }

    const SpatialOps::GhostData& get_ghosts() const{ return ghosts_; }

    void set_ghosts( const SpatialOps::GhostData g ){ ghosts_ = g; }
  };


  //====================================================================


  /**
   *  @class  ExpressionBase
   *  @date   June, 2007
   *  @author James C. Sutherland
   *
   *  @brief Abstract base class for all Expression classes.
   *
   *  This class is not templated so that we can have containers of
   *  ExpressionBase objects.  Expression classes should not generally
   *  derive directly from this class.  Rather, they should derive from
   *  the Expression class, which provides additional type information.
   *
   *  See documentation of the Expression class for more information.
   */
  class ExpressionBase
  {
  public:

    /** \brief set the expression as GPU runnable */
    void set_gpu_runnable( const bool b );

    /** \brief returns Expression's GPU eligibility flag */
    inline bool is_gpu_runnable() const{ return exprGpuRunnable_; }

    /**
     *  @brief Construct an ExpressionBase object.
     */
    ExpressionBase( FieldAggregatorBase* );

    virtual ~ExpressionBase();

    /**
     *  @brief Set the Tag(s) that this expression computes.
     *         Only called by the ExpressionFactory class.
     */
    virtual void set_computed_tags( const TagList& tags,
                                    const SpatialOps::GhostData& ghosts ) = 0;

    /** @brief obtain the Tag (name) for this expression */
    const Tag& get_tag() const;

    /** @brief obtain the Tags (names) computed by this expression */
    const TagList& get_tags() const{ return exprNames_; }

    /** @brief the ghost information for the field(s) computed by this expression */
    const SpatialOps::GhostData& ghosts() const{ return myGhosts_; }

    /**
     *  \brief returns true if this expression is a placeholder
     *  (i.e. does not compute anything, just serves to wrap a field).
     */
    virtual bool is_placeholder() const{ return false; }

    /**
     *  @brief Add an expression to this one that should be subtracted from it.
     *
     *  This is particularly useful to add MMS source terms to RHS
     *  expressions.  In general, it should not be used.
     *
     *  \param src The expression to attach to this one
     *
     *  \param op The operation to be performed to augment this expression
     *     by the "src" expression.
     *
     *  \param myFieldIx For expressions computing multiple fields, this
     *    must be specified to indicate which field should be augmented by
     *    the source term expression.
     *
     *  \param srcFieldIx For src expressions computing multiple fields, this
     *    must be specified to indicate which field should be used to augment
     *    this expression value.
     */
    void attach_source_expression( const ExpressionBase* const src,
                                   const SourceExprOp op,
                                   const int myFieldIx = 0,
                                   const int srcFieldIx = 0 );

    /** \brief query if this expression is to be cleaved from its parents */
    inline bool cleave_from_parents() const{ return cleaveFromParents_; }

    /** \brief query if this expression is to be cleaved from its children */
    inline bool cleave_from_children() const{ return cleaveFromChildren_; }

    /**
     *  \brief Call this method on the expression to indicate that
     *  this expression should be cleaved from its parents when
     *  constructing the tree.
     */
    inline void cleave_from_parents( const bool r ){ cleaveFromParents_=r; }

    /**
     *  \brief Call this method on the expression to indicate that
     *  this expression should be cleaved from its children when
     *  constructing the tree.
     */
    inline void cleave_from_children( const bool r ){ cleaveFromChildren_=r; }

    /**
     * \brief in situations where more than one FieldManagerList is present
     *  (e.g. multiple patches in a single graph), this provides a mechanism
     *  to associate this expression with the appropriate FieldManagerList.
     * \param fmlid the identifier for the FieldManagerList
     */
    inline void set_field_manager_list_id( const int fmlid ){ fmlID_=fmlid; }

    inline int field_manager_list_id() const{ return fmlID_; }

    /**
     *  \brief Expose dependencies and store them in the supplied
     *   objects. Only intended for internal use by the ExpressionTree.
     *
     *   \param exprDeps expressions that are required by this expression.
     *   \param fieldDeps fields computed by this expression
     */
    void base_advertise_dependents( ExprDeps& exprDeps,
                                    FieldAggregatorBase*& fieldDeps );

    /**
     *  \brief Bind fields used by this Expression. Only intended for
     *   internal use by the ExpressionTree.
     */
    void base_bind_fields( FieldManagerList& fldMgrList );
    void base_bind_fields( FMLMap& fmls );

    /**
     *  \brief Bind operators used by this Expression. Only intended
     *   for internal use by the ExpressionTree.
     */
    void base_bind_operators( const SpatialOps::OperatorDatabase& opDB );

    // virtual and implemented in Expression class since we need type information.
    virtual void base_evaluate() = 0;

    /**
     * \brief Add a modifier expression to this one.  This method should only be
     *  accessed by the ExpressionFactory in general.
     *
     *  Modifier expressions provide a mechanism to set boundary conditions on
     *  fields and introduce arbitrary additional dependencies to do so.  This
     *  is needed to impose non-trivial boundary conditions and ensures proper
     *  ordering of the resulting expression graph.  The modifier expression
     *  is not a "first class" expression in that it is not directly represented
     *  on the graph. Rather, it influences the graph by piggy-backing on an
     *  expression, where its dependencies are incorporated.  Modifiers are
     *  executed immediately after an expression completes.
     *
     *  Modifiers will be executed in the order in which they are added.
     *
     * \param modifier the expression to modify this one.
     * \param modTag the original tag used to identify this modifier. Because the
     *   modifier "evaluates" the same field as this expression that it is
     *   attached to, the modTag allows us to remember the name originally
     *   associated with the expression.
     */
    void add_modifier( ExpressionBase* const modifier, const Tag& modTag );

    /**
     * \brief Obtain the type name for the field type that this expression computes.
     *
     * This is primarily used internally for type consistency checking when only
     * base class objects are available.
     *
     * @return the typeid name for the field type associated with this expression
     */
    virtual std::string field_type_name() const = 0;

    /**
     * Returns the number of "post-processing" functors or expressions hanging
     * on this expression. These are attached via calls to \c process_after_evaluate()
     * or \c ExpressionFactory::attach_modifier().
     */
    virtual int num_post_processors() const = 0;

    /**
     * @return the names of all of the functors and modifiers on this expression.
     * See also num_post_processors().
     */
    virtual std::vector<std::string> post_proc_names() const =0;

    /**
     * Allows conversion of this expression into a PlaceHolder.  This should only be used for cleaving.
     */
    virtual ExpressionBase* as_placeholder_expression() const =0;

    /**
     * \brief  Override the sensitivity w.r.t. the given variable to be unity.  Should only be called from ExpresisonTree::compute_sensitivities
     */
    void setup_self_sensitivity( const Expr::Tag& sensTag );

    /**
     * \brief Override the sensitivity w.r.t. the given variable to be zero.  Should only be called from ExpresisonTree::compute_sensitivities
     */
    void setup_empty_sensitivity( const Expr::Tag& sensTag );

    /**
     * Allows an expression to override the sensitivity calculation. Typically,
     * sensitivity is determined via chain rule with the expression's dependencies.
     * In the case where an expression does not use chain rule, but something
     * else to determine sensitivities, this method should be implemented and
     * return `true`. In that case, no sensitivity information is available on
     * fields that this expression depends on; only values. Buyer beware...
     */
    virtual bool override_sensitivity() const{ return false; }

    void override_sensitivity( const Tag& tag, const double value );
    bool overrides_sensitivity( const Tag& tag ) const;

    /**
     * Create a FieldTypeBase object from this expression.
     * @return
     */
    virtual const FieldTypeBase& field_type() const = 0;

#   ifdef ENABLE_CUDA

    /**
     * \brief creates a cuda stream for this expression on a specified device.
     *
     * @param deviceIndex which provides device context for creating streams
     *
     * Note : If the stream already exists and there is a change in device context,
     *        the previous existing stream is destroyed and new stream is created for the device context.
     */
    void create_cuda_stream( int deviceIndex );

    /**
     * \brief returns the cudaStream set on the expression
     */
    cudaStream_t get_cuda_stream();

#   endif

    FieldAggregatorBase& field_aggregator(){ return *myFieldAggregator_; }

    const FieldAggregatorBase& field_aggregator() const{ return *myFieldAggregator_; }

    /**
     * Inform this expression that it should calculate sensitivities for the given TagSet
     * @param vars The set of variables that this expression should calculate sensitivities to.
     *
     * Note that this should only be called by the ExpressionTree since it can
     * view the graph to determine if a given node actually has sensitivities.
     * This facilitates short-circuiting sensitivity calculations.
     */
    void activate_sensitivity( const TagSet& vars );

    /**
     * Inform this expression that it should calculate sensitivities for the given Tag
     * @param var The variable that this expression should calculate sensitivity to.
     *
     * Note that this should only be called by the ExpressionTree since it can
     * view the graph to determine if a given node actually has sensitivities.
     * This facilitates short-circuiting sensitivity calculations.
     */
    virtual void activate_sensitivity( const Tag& var ) = 0;

  protected:

    /**
     *  @brief Evaluate this expression and cache the result.
     *
     *  The <code>evaluate()</code> method should perform the following:
     *
     *   \li Obtain the values of expressions that it depends on via the
     *   <code>Expression::value()</code> method.
     *
     *   \li Calculate the value of this expression and store it in the
     *   variable returned by <code>Expression::value()</code> for
     *   single variable expressions and
     *   <code>Expression::get_value_vec()</code> for multi-value
     *   expressions.
     */
    virtual void evaluate() = 0;

    /**
     * @brief Evaluate the sensitivity of this expression with respect to the
     *        specified solution variable.
     *
     * Note that if this is not implemented by a derived class, an exception will be thrown.
     *
     * For solutions on grids (the typical use case for expressions), this is
     * designed to calculate point-wise (local) sensitivities.
     * For example, if we had an expression for a diffusive flux:
     * \f[
     *  \phi = \sin(a x) + b x
     * \f]
     * then the sensitivity w.r.t. \f$\gamma\f$ would be:
     * \f[
     * \frac{ \partial \phi }{ \partial \gamma } =
     *  \left( a \cos(a x) + b \right) \frac{ \partial x }{ \partial gamma }
     * \f]
     *
     * When calculating sensitivities, both the original dependent variables and
     * their sensitivities are available, as needed for the chain rule.
     * For the above example, the sensitivity method might look like:
     * \code{.cpp}
     * void sensitivity( const Tag& var )
     * {
     *   FieldT& sens = this->sensitivity_result( var );
     *
     *   if( var == phiTag ){
     *     sens <<= 1;
     *     return;
     *   }
     *
     *   // chain rule:
     *   const FieldT& x     = x_->field_ref();
     *   const FieldT& xSens = x_->sens_field_ref( var );
     *
     *   sens <<= (a_ * cos(a_*x) + b_ ) * xSens;
     * }
     * \endcode
     */
    virtual void sensitivity( const Tag& var );

    /**
     *  @brief Obtain pointers to spatial operators as necessary.
     *
     *  Derived classes should implement this method to obtain pointers
     *  to spatial operators as needed.  In the case of mesh adaptivity,
     *  this method may be called during execution so that operators can
     *  be re-bound.
     *
     *  This method has a default implementation which does nothing. It
     *  is not pure virtual since many expressions do not require any
     *  operators and we don't want to add unneeded complexity in that
     *  case.
     */
    virtual void bind_operators( const SpatialOps::OperatorDatabase& opDB ){}

    struct SrcTermInfo{
      SrcTermInfo( const ExpressionBase* const expr, const SourceExprOp _op, const int thisIx, const int srcTermIx )
        : srcExpr( expr ), op(_op), myIx( thisIx ), srcIx( srcTermIx )
      {}
      const ExpressionBase* const srcExpr;
      const SourceExprOp op;
      const int myIx, srcIx;
    };
    struct SrcTermInfoCompare{
      bool operator()( const SrcTermInfo& s1, const SrcTermInfo& s2 ){
        bool isLess = s1.srcExpr->get_tags()[0].id() < s2.srcExpr->get_tags()[0].id();
        if( s1.srcExpr->get_tags()[0].id() == s2.srcExpr->get_tags()[0].id() ){
          isLess = s1.myIx < s2.myIx;
          if( s1.myIx == s2.myIx ){
            isLess = s1.srcIx < s2.srcIx;
          }
        }
        return isLess;
      }
    };
    typedef std::set<SrcTermInfo,SrcTermInfoCompare> ExtraSrcTerms;
    ExtraSrcTerms srcTerms_;

    /**
     * \brief  Bind the field(s) that this expression computes.
     *
     *  This method is called from base_bind_fields(), and is hidden
     *  from user implemented expressions.  It automatically hooks up
     *  the field(s) that a user-defined expression computes.
     *
     *  \param fml the FieldManagerList to use when binding fields
     *  \param computedOnly if true, only bind the fields computed by this expression, not any of its dependent fields.
     */
    virtual void bind_my_fields( FieldManagerList& fml, const bool computedOnly=false ) = 0;

    std::vector<ExpressionBase*> modifiers_;
    TagList modifierNames_;

#   ifdef ENABLE_CUDA
    cudaStream_t cudaStream_;      ///< cuda stream set on this expression
    bool cudaStreamAlreadyExists_; ///< Is true, when a stream exists for the expression
    int deviceID_;                 ///< GPU device ID set for expressions
#   endif

    FieldAggregatorBase* const myFieldAggregator_;  ///> holds fields that this expression computes
    FieldAggregators fieldAggregators_;             ///> holds fields that this expression requires (reads)

    TagList exprNames_;              ///< the Tags for the fields I compute
    SpatialOps::GhostData myGhosts_; ///< the number of ghost cells for the fields I compute

    std::map<Tag,double> sensOverride_;  ///< Used to reset sensitivity values to truncate graph traversal in cases of zero sensitivities.

  private:

    ExpressionBase( const ExpressionBase& );          ///< no copying
    ExpressionBase& operator=(const ExpressionBase&); ///< no assignment

    bool cleaveFromParents_, cleaveFromChildren_;
    bool exprGpuRunnable_;                            ///< Is true, this expression can run on GPU

    int fmlID_;
  };

} // namespace Expr

#endif // ExpressionBase_h
